// ==UserScript==
// @name         H-E gallery enchanter
// @namespace    https://tmokenc.com/
// @version      0.1.11
// @description  try to take over the world!
// @author       tmokenc
// @match        https://e-hentai.org/g/*
// @match        https://exhentai.org/g/*
// @grant        none
// ==/UserScript==

const config = {
  mpvSelectBox: false,
  favoriteBackgroundOpacity: 0.4,
  historyDbKey: "tmkHistory",
  cacheDbKey: "tmkCache"
};

class Gallery {
  constructor(url = window.location) {
    const reg = /(\d+)\/([0-9a-f]+)/i;
    const data = url.toString().match(reg);

    if (data.length !== 3) {
      throw new Error("Not a gallery url");
    }

    this.gid = Number(data[1]);
    this.token = data[2];
  }

  updateFav(favcat, favnote) {
    const opt = {
        method: "POST",
        body: `favcat=${favcat}&favnote=&update=1`,
        headers: [
            ['Content-Type', 'application/x-www-form-urlencoded']
        ]
    }
    let url = `https://e-hentai.org/gallerypopups.php?gid=${this.gid}"&t=${this.token}&act=addfav`;

    if (favnote) {
      opt.body += "&favnote=" + favnote.replace("\n", " ");
    }

    _fetch(url, opt)
  }

  async fetchFavHtml() {
    const url = `https://e-hentai.org/gallerypopups.php?gid=${this.gid}&t=${this.token}&act=addfav`;
    const text = await _fetch(url);
    const dom = new DOMParser()
    return dom.parseFromString(text, 'text/html')
  }

  async fetchNote() {
    const doc = await this.fetchFavHtml();
    return doc.querySelector("textarea").value
  }

  async updateNote(note) {
    const doc = await this.fetchFavHtml();

    if (doc.querySelector("div.nosel").childElementCount === 10) {
      console.error("the gallery is not in favorite list");
      return
    }

    const favcat = doc.querySelector("div.nosel > div input[checked]").value;
    await this.updateFav(favcat, note);
    console.debug("updated note to:", note);
  }
}

const gallery = new Gallery();

(function() {
    'use strict';

    if (gallery) {
      console.log(gallery);
      updateHistory();
    }

    Promise.all([
        favoriteEnchanter(),
        mpvEnchanter()
    ])
    .catch(console.log)

  initHistoryUi();
})();

async function initHistoryUi() {
  const footer = document.querySelector(".dp");
  const ui = document.createElement("div");

  footer.parentNode.insertBefore(ui, footer);
  const historyList = getHistory()
    .filter(e => !gallery || (e.gid !== gallery.gid && e.token !== gallery.token));

  if (!historyList.length) {
    return
  }

  const historyData = await fetchGalleryData(historyList);
  const history = historyData
    .map(e => `<div class="gl1t">
      <a href="/g/${e.gid}/${e.token}">
        <div class="gl4t glname glink">${e.title}</div>
      </a>

      <div class="gl3t" style="height: 340px;width:250px">
        <a href="/g/${e.gid}/${e.token}">
          <img style="width=100%" alt="${e.title}" src="${e.thumb}">
        </a>
      </div>

      <div class="gl6t">
        <div class="cs ct7">${e.category}</div>
        <div id="posted_${e.posted}">${e.posted}</div>
      </div>

      <div class="gl5t">
        <div></div>
        <div>
         <div class="ir" style="background-position:0px -21px;opacity:1"></div>
          <div>${e.filecount} pages</div>
        </div>
      </div>
    </div>`)
    .join("");

  ui.innerHTML = `
    <div>
    <h2> History </h2>
    </div>

    <div id="historylist" class="itg gld" style="display: flex; justify-content: center">
    ${history}
    </div>
    <div class="c"></div>
  `;
}

function getHistory() {
  const data = JSON.parse(localStorage.getItem(config.historyDbKey) || "{}");
  const result = data.visited || [];
  return result.map(e => new Gallery(e))
}

function updateHistory() {
  const current = `${gallery.gid}/${gallery.token}`;
  const data = JSON.parse(localStorage.getItem(config.historyDbKey) || "{}");

  data.lastUpdate = Date.now();

  if (data.visited && data.visited.length > 0) {
    data.visited = data.visited.filter(e => e !== current);
    data.visited.unshift(current);

    while (data.visited.length > 10) {
      data.visited.pop();
    }
  } else {
    data.visited = [current];
  }

  localStorage.setItem(config.historyDbKey, JSON.stringify(data));
}

async function favoriteEnchanter(opacity = config.favoriteBackgroundOpacity) {
    const bgColors = [0x858585, 0xff7575, 0xffcba4, 0xfffaa4, 0xa4ffb9, 0xe0ffb3, 0xb3ffff, 0xaaa4ff, 0xdab3ff, 0xffb3e8]

    const doc = await gallery.fetchFavHtml();

    const arr = doc.querySelectorAll('#galpop .nosel > div')
    const selectBox = document.createElement('select')

    Object.assign(selectBox.style, {
      width: "100%",
      textAlign: "center",
      marginTop: "1em",
      fontWeight: "bold"
    });

    selectBox.innerHTML += '<option value="favdel">-----------------------------</option>'
    for (let i = 0; i < 10; i++) {
        let opt = document.createElement('option')
        opt.value = i.toString()
        opt.innerHTML += arr[i].textContent.match(/\w+/g).join(' ')

        if (arr.length > 10 && arr[i].children[0].firstChild.checked) {
            opt.selected = true
            selectBox.style.borderColor = '#' + bgColors[i].toString(16)
            selectBox.style.backgroundColor = 'rgba(' + numberToRgb(bgColors[i]).join(', ') + ', ' + opacity + ')'
        }
        selectBox.appendChild(opt)
    }

    selectBox.addEventListener('change', async function() {
      if (this.value === 'favdel') {
        this.style.borderColor = '';
        this.style.backgroundColor = '';
      } else {
        this.style.borderColor = '#' + bgColors[Number(this.value)].toString(16);
        this.style.backgroundColor = `rgba(${numberToRgb(bgColors[Number(this.value)]).join(', ')}, ${opacity})`;
      }

      gallery.updateFav(this.value);
    })

    document.querySelector('#gdf').outerHTML = ''
    document.querySelector('#gd3').appendChild(selectBox)
}

async function mpvEnchanter(mpvSelectBox = config.mpvSelectBox) {
    const path = location.pathname.replace('g', 'mpv')
    Array.from(document.querySelectorAll('.gdtl')).forEach(function(v) {
        const page = Number(v.textContent)
        v.innerHTML += '<span> | </span><a href=' + path + '#page' + page + '>MPV</a>'
    })

    if (mpvSelectBox) {
        const right = document.querySelectorAll(".g2")
        const mpv = right[right.length-1]

        if (mpv.textContent !== " Multi-Page Viewer") {
            return
        }

        const link = mpv.querySelector('a')
        const page = document.querySelectorAll(".gdt2")[5].textContent.match(/\d+/)[0];
        const num = Number(page);

        const selectBox = document.createElement('select')
        selectBox.innerHTML = "<option value='1'>Choose page</option>"

        for (let i = 1; i <= num; i++) {
            let opt = document.createElement('option')
            opt.value = i.toString()
            opt.textContent = i.toString()

            selectBox.appendChild(opt)
        }

        mpv.appendChild(selectBox)

        selectBox.addEventListener('change', function() {
            link.href = path + '#page' + selectBox.value
        })
    }
}

function numberToRgb(num) {
    const r = num >> 16;
    const g = (num >> 8) & ((1 << 8) - 1);
    const b = num & ((1 << 8) - 1);

    return [r, g, b]
}

function _fetch(url, {method = "GET", body = "", headers = []} = {}) {
    return new Promise((res, rej) => {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status !== 200) {
                    rej(this)
                } else {
                    res(this.responseText)
                }
            }
        };
        xhttp.open(method, url, true);

        for (const header of headers) {
            xhttp.setRequestHeader(...header)
        }

        xhttp.send(body);
    })
}

async function fetchGalleryData(galleries) {
  const endpoint = "https://api.e-hentai.org/api.php";
  const body = {
    method: "gdata",
    gidlist: [],
    namespace: 1
  };

  if (Array.isArray(galleries)) {
    body.gidlist = galleries.map(e => [e.gid, e.token]);
  } else {
    body.gidlist = [galleries.gid, galleries.token];
  }

  const result = await _fetch(endpoint, {
    method: "POST",
    body: JSON.stringify(body)
  });

  return JSON.parse(result).gmetadata
}
