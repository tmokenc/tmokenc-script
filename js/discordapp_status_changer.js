// ==UserScript==
// @name         Discord status changer
// @namespace    http://tmokenc.com/
// @version      0.1
// @description  try to take over the world!
// @author       tmokenc
// @match        https://discordapp.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var statuses = [
        'extern crate brain;',
        '#include <brain.h>',
        'import { brain } from \'brain.js\'',
        'import brain',
        'package brain;'
    ]
    
    var refeshTime = 15000
    var token = ''

    setInterval(function(){
        console.log('changing status')
        changeStatus(statuses[statuses.length * Math.random() << 0])
    }, refeshTime)

})();

function changeStatus(token, text) {
    console.log('New status:', text)
    var status = {
        "custom_status": {
            "text": text
        }
    }

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            console.log("Changed successfully")
        } else {
            console.log(this)
        }
    };

    xhttp.open("PATCH", "https://discordapp.com/api/v6/users/@me/settings", true);
	xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.setRequestHeader('Authorization', token)
    xhttp.send(JSON.stringify(status));
}