// ==UserScript==
// @name         tmokenc hentaiverse enchanter
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  RGB-ist monsters on HentaiVerse
// @author       tmokenc
// @match        https://hentaiverse.org/?s=Battle
// @grant        none
// ==/UserScript==

const icons = [
  "https://i.imgur.com/03V2Zs7.png",
  "https://i.imgur.com/Vf0KN2t.png",
  "https://i.imgur.com/TJYFThd.png",
  "https://i.imgur.com/MICVkJs.png",
  "https://i.imgur.com/cDxBW6X.png",
  "https://i.imgur.com/wnbKTMG.png",
  "https://i.imgur.com/gn0gvCr.png",
  "https://i.imgur.com/AT36Tjw.png",
  "https://i.imgur.com/pME0hDx.png",
  "https://i.imgur.com/JeBYMUV.png",
  "https://i.imgur.com/xfkVc7V.png",
  "https://i.imgur.com/EuYULHJ.png",
  "https://i.imgur.com/OjDCJiZ.png",
  "https://i.imgur.com/QmWjpIH.png",
  "https://i.imgur.com/ubuWnsk.png",
  "https://i.imgur.com/sjBFBvz.png",
  "https://i.imgur.com/xZxiFQQ.png",
  "https://i.imgur.com/ECLvyzb.png",
  "https://i.imgur.com/AywQOdT.png",
  "https://i.imgur.com/gxE8Knl.png",
  "https://i.imgur.com/GoIL2MY.png",
  "https://i.imgur.com/pBzYSzf.png",
  "https://i.imgur.com/CCzKsSo.png",
  "https://i.imgur.com/ycGoPw9.png",
  "https://i.imgur.com/MbSZYDD.png",
  "https://i.imgur.com/2lq7SZn.png",
  "https://i.imgur.com/dKG7U8x.png",
  "https://i.imgur.com/Bmz6OLE.png",
  "https://i.imgur.com/jzsBAER.png",
  "https://i.imgur.com/OfLGrJV.png"
];

const boss_icons = [
  "https://i.imgur.com/lDlN7rO.gif"
];

(function() {
    'use strict';

    setup();
    modify_icons();
})();

function setup() {
    const css = `
      #pane_monster .btm2 img {
          max-width: 80%;
      }
    `;

    modify_api();
    push_css(css);
}

function push_css(css) {
    const e = document.createElement("style")
    e.type = "text/css"
    e.innerText = css
    document.head.appendChild(e)
}

function modify_api() {
  window.api_response = function(b) {
    if (b.readyState == 4) {
      if (b.status == 200) {
        var a = JSON.parse(b.responseText);
        console.log(a);
        if (a.pane_monster) {
          let e = document.createElement("p");
          e.innerHTML = a.pane_monster;

          modify_icons(e.querySelectorAll("p > *"))
          a.pane_monster = e.innerHTML;
        }
        if (a.login != undefined) {
          top.location.href = login_url
        } else {
          return a
        }
      } else {
        alert('Server communication failed: ' + b.status + ' (' + b.responseText + ')');
        document.location = document.location + ''
      }
    }
    return false
  }
}

function modify_icons(monsters = window.document.querySelectorAll("#pane_monster > *")) {
  let count = 1;
  for (const monster of monsters) {
    const is_boss = monster.querySelector(".btm2").attributes.style !== undefined;
    const data = is_boss ? boss_icons : icons;

    const name = monster.querySelector(".btm3").innerText;
    const num = name.split("").map(e => e.charCodeAt(0)).reduce((v, x) => v + x, 0);
    const color = num**2 % 0xffffff;

    if (data.length > 0) {
      const index = num % data.length;
      monster.querySelector("img").src = data[index];
    }

    monster.querySelector(".fc4 > div").textContent = count++;
    monster.style.borderColor = "#" + color.toString(16);
  }
}