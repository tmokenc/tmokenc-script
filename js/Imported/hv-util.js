// ==UserScript==
// @name           HV Utils
// @namespace      HVUT
// @version        2.13.4
// @date           2022-06-22
// @author         sssss2
// @match          *://*.hentaiverse.org/*
// @match          *://e-hentai.org/*
// @connect        hentaiverse.org
// @connect        e-hentai.org
// @exclude        https://hentaiverse.org/?s=Battle
// @exclude        https://hentaiverse.org/equip/*
// @exclude        http://alt.hentaiverse.org/?s=Battle
// @exclude        http://alt.hentaiverse.org/equip/*
// @exclude        https://hentaiverse.org/isekai/?s=Battle
// @exclude        https://hentaiverse.org/isekai/equip/*
// @exclude        http://alt.hentaiverse.org/isekai/?s=Battle
// @exclude        http://alt.hentaiverse.org/isekai/equip/*
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_addStyle
// @grant          GM_xmlhttpRequest
// @grant          GM_setClipboard
// @grant          unsafeWindow
// @run-at         document-end
// ==/UserScript==

var settings = {

// [GLOBAL]
randomEncounter : true, // Random Encounter Notification
reIsekai : true, // use it in Isekai
reGallery : true, // use it on the gallery
reGalleryAlt : false, // open RE links to alt.hentaiverse.org on the gallery
reBeep : {volume:0.2, frequency:500, time:0.5, delay:1}, // beep when a RE is ready; set volume to 0 to disable
//reBeepTest : setTimeout(()=>play_beep(settings.reBeep),100), // you can test the beep by removing slashes in front of this line
reBattleCSS : "top:10px;left:600px;position:absolute;cursor:pointer;font-size:10pt;font-weight:bold", // modify top and left to locate the timer

reloadBattleURL : 1, // when entering the battle; 0:do nothing, 1:change the url only (recommended), 2:reload the page
ajaxRound : true, // support Monsterbation

topMenuAlign : "left", // "left", "center", "right" (set to "flex-end" in Google Chrome), or "space-between", "space-around", "space-evenly"
topMenuIntegrate : false, // integrate menus into one button
topMenuLinks : ["Character", "Equipment", "Item Inventory", "Equip Inventory", "Equipment Shop", "Item Shop", "The Market", "Monster Lab", "The Shrine", "MoogleMail", "The Arena", "The Tower", "Ring of Blood", "GrindFest", "Item World"],
confirmStaminaRestorative : true, // confirm whether to use a stamina restorative item
disableStaminaRestorative : 85, // disable a stamina restorative button when your stamina is higher than this
warnLowStamina : 2, // warn when your stamina is lower than this

showCredits : false, // show your credits balance on all pages
showEquipSlots : 1, // show Equip Inventory Slots; 0:disable, 1:battle pages only, 2:always
showLottery : true, // show weapon and armor that are currently in the lottery
	showLotteryIsekai : false,

equipSort : true, // sort equipment list, order by category
equipColor : true, // set background colors for equipment; Default colors: peerless red, legendary orange, magnificent blue, exquisite green

equipmentKeyFunctions : true, // support some keyboard actions when mouse over the equipment
                              // "V": open equipment links in a new tab, instead of in a popup
                              // "L": show link code [url=...]...[/url]
equipmentMouseFunctions : true, // support some mouse actions when mouse over the equipment
                                // doubleclick : open equipment links

// [CHARACTER]
character : true,
	characterExpCalculator : true, // Level/Exp/Proficiency Calculator
	characterExpandStatsByDefault : true,
equipment : true,
	equipmentStatsAnalyzer : true, // calculate magic score, proficiency, resist chance of monsters and expected damage
abilities : true, // show current and maximum level of every ability, and set ability to the required level in one click
training : true, // calculate costs
	trainingTimer : true, // plan training to a preset level and start training automatically
itemInventory : true, // WTS forum code generator
	itemInventoryExtraCrystals : false, // show Extra Crystals Button
equipInventory : true, // WTS forum code generator
	equipInventoryIntegration : true, // integrate all equip types on the default inventory page
	equipCode : "[{$_eid}] [url={$url}]{$bbcode}[/url] (Lv.{$level}){$price: @ $price}{$note: ($note)}",
settings : true,

// [BAZAAR]
equipmentShop : true, // sell and salvage equipment at once
	equipmentShopIntegration : true, // integrate all equip types on the default shop page
	equipmentShopShowLevel : true, // show equipment's level
	equipmentShopShowPAB : true, // show equipment's pab
	equipmentShopConfirm : 1, // 0:no, 1:confirm 'unwise' actions (e.g. selling mag cotton or salvaging shade), 2:confirm all actions
	equipmentShopAutoLock : true, // automatically lock protected equipment
	equipmentShopProtect : [ // prevent YOUR valuable equipment from being selected by "Select All" button
		"Peerless",
		"Legendary",
		"Magnificent && Rapier && Slaughter",
		"Magnificent && (Force Shield || Buckler && Barrier)",
		"Magnificent && Fiery && Redwood && (Destruction || Elementalist || Surtr)",
		"Magnificent && Arctic && Redwood && (Destruction || Elementalist || Niflheim)",
		"Magnificent && Shocking && (Willow || Redwood) && (Destruction || Elementalist || Mjolnir)",
		"Magnificent && Tempestuous && (Willow || Redwood) && (Destruction || Elementalist || Freyr)",
		"Magnificent && Hallowed && (Oak || Katalox) && (Destruction || Heaven-sent || Heimdall)",
		"Magnificent && Demonic && (Willow || Katalox) && (Destruction || Demon-fiend || Fenrir)",
		"Magnificent && (Radiant || Charged) && Phase",
		"Magnificent && Charged && (Elementalist || Heaven-sent || Demon-fiend)",
		"Magnificent && (Savage || Agile) && Shadowdancer",
		"Magnificent && Power && Slaughter",
	],
	equipmentShopCheckIW : true, // check IWed equipment in BAZZAR
	equipmentShopBazaar : [ // check valuable equipment in BAZZAR, then hide all other trash
		"Peerless",
		"Legendary",
		"Magnificent && Rapier && Slaughter",
		"Magnificent && (Force Shield || Buckler && Barrier)",
		"Magnificent && Fiery && Redwood && (Destruction || Elementalist || Surtr)",
		"Magnificent && Arctic && Redwood && (Destruction || Elementalist || Niflheim)",
		"Magnificent && Shocking && (Willow || Redwood) && (Destruction || Elementalist || Mjolnir)",
		"Magnificent && Tempestuous && (Willow || Redwood) && (Destruction || Elementalist || Freyr)",
		"Magnificent && Hallowed && (Oak || Katalox) && (Destruction || Heaven-sent || Heimdall)",
		"Magnificent && Demonic && (Willow || Katalox) && (Destruction || Demon-fiend || Fenrir)",
		"Magnificent && (Radiant || Charged) && Phase",
		"Magnificent && Charged && (Elementalist || Heaven-sent || Demon-fiend)",
		"Magnificent && (Savage || Agile) && Shadowdancer",
		"Magnificent && Power && Slaughter",
		"(Magnificent||Exquisite) && (Rapier || Shortsword || Club) && Slaughter && (Eth||Fie||Arc||Shoc||Tem||Hal||Dem)",
		"(Magnificent||Exquisite) && Katana && Slaughter && Ethereal",
		"(Magnificent||Exquisite) && Wakizashi && (Balance||Nimble) && (Eth||Fie||Arc||Shoc||Tem||Hal||Dem)",
		"(Magnificent||Exquisite) && (Force Shield || Buckler&&Barrier || Kite Shield)",
		"(Magnificent||Exquisite) && Shade && (Shadowdancer || Fleet)",
		"(Magnificent||Exquisite) && Power && Slaughter",
		"Magnificent && Power",
	],

itemShop : true,
	itemShopHideInventory : ["Energy Drink", "Caffeinated Candy", "Infusion of", "Scroll of", "Flower Vase", "Bubble-Gum", "Precursor Artifact", "ManBearPig Tail", "Holy Hand Grenade of Antioch", "Mithra's Flower", "Dalek Voicebox", "Lock of Blue Hair", "Bunny-Girl Costume", "Hinamatsuri Doll", "Broken Glasses", "Black T-Shirt", "Sapling", "Unicorn Horn", "Noodly Appendage", "Soul Fragment", "Crystal of", "Monster Chow", "Monster Edibles", "Monster Cuisine", "Happy Pills", "-Grade", "Crystallized Phazon", "Shade Fragment", "Repurposed Actuator", "Defense Matrix Modulator", "Binding of", "Shard", "Figurine"],
	// hide items, Inventory side
	itemShopHideStore : ["Crystal of", "Monster Chow", "Monster Edibles", "Monster Cuisine", "Happy Pills", "-Grade", "Shard"],
	// hide items, Store side

itemBackorder : true, // show all current Item Backorder bids
monsterLab : true, // record gifts, and feed crystals or chaos tokens to monsters
	monsterLabDefaultSort : "#", // #, name, class, pl, wins, kills, +, gifts, morale, hunger
	monsterLabMorale : 12000, // morale threshold (max 24000)
	monsterLabHunger : 18000, // hunger threshold (max 24000)
	monsterLabCloseDefaultPopup : true,
shrine : true, // record rewards
	shrineHideItems : ["Figurine", "Coupon", "Peerless Voucher"],
	shrineTrackEquip : ["Peerless", "Legendary"], // track high quality equipment only
moogleMail : true, // Advanced MoogleMail-Sender and MoogleMail-Box
	moogleMailCouponClipper : false, // if the subject of MoogleMail contains "Coupon Clipper" or "Item Shop", take credits, buy requested items, then send them back.
	moogleMailDarkDescent : false, // if the subject of MoogleMail contains "Dark Descent" or "reforge", take equipment, reforge, then send it back.
	moogleMailTemplate : {
		//"WTS": {"to":"", "subject":"", "body":""},
		//"WTB": {"to":"", "subject":"", "body":""},
		//"Free": {"to":"", "subject":"Free Potions", "body":"500 x Health Draught\n500 x Mana Draught\n200 x Spirit Draught\n100 x Health Potion\n100 x Mana Potion\n50 x Spirit Potion", attach:true},
	},

lottery : true,
	lotteryCheck : [
		"Rapier && Slaughter",
		"Ethereal && (Rapier || Wakizashi) && (Balance || Nimble)",
		"Wakizashi && Battlecaster",
		"Ethereal && (Axe || Club || Shortsword || Estoc || Katana || Longsword || Mace) && Slaughter",
		"Force Shield || Buckler && (Barrier || Battlecaster)",
		"Fiery && (Willow || Redwood) && (Destruction || Elementalist || Surtr)",
		"Arctic && (Willow || Redwood) && (Destruction || Elementalist || Niflheim)",
		"Shocking && (Willow || Redwood) && (Destruction || Elementalist || Mjolnir)",
		"Tempestuous && (Willow || Redwood) && (Destruction || Elementalist || Freyr)",
		"Hallowed && (Oak || Katalox) && (Destruction || Heaven-sent || Heimdall)",
		"Demonic && (Willow || Katalox) && (Destruction || Demon-fiend || Fenrir)",
		"(Radiant || Charged) && Phase",
		"Charged && (Elementalist || Heaven-sent || Demon-fiend)",
		"(Savage || Agile) && Shadowdancer",
		"Power && Slaughter",
		"Power && Savage && Balance",
	],

// [BATTLE]
equipEnchant : true, // Equipment Enchant and Repair Pane
	equipEnchantPosition : "left", // position of the pane. left or right
	equipEnchantWeapon : 4, // number of enchantments for your weapon
	equipEnchantArmor : 3, // number of enchantments for armors
	equipEnchantRepairThreshold : 55, // if the value is between 0 and 1, it means the condition % of the equipment (e.g., 0.6 => 60%). else if the value is larger than 1, it means a margin to 50% condition (e.g., 55 => 205/300). in this case, the recommended value for grindfest is 55.
	equipEnchantInventory : {"Health Draught":500, "Mana Draught":500, "Spirit Draught":500, "Health Potion":300, "Mana Potion":300, "Spirit Potion":300, "Health Elixir":100, "Mana Elixir":100, "Spirit Elixir":100},
	equipEnchantCheckArmors : false,

arena : true,
ringofblood : true,
grindfest : true,
itemWorld : true, // calculate pxp gain and potency level

// [FORGE]
upgrade : true, // calculate materials and costs
salvage : true, // warn before salvaging an equipment


// [Item Code Generator]
itemCodeTemplate :
`
// set itemCode = {$zero:[color=transparent]$zero[/color]}{$count} x {$name} @ {$price}
// set unpricedCode = {$zero:[color=transparent]$zero[/color]}{$count} x {$name}
// set unpricedCode = 0; DELETE THIS LINE TO ADD UNPRICED ITEMS TO THE CODE
// set stockoutCode = [color=#999][s]{$zero}{$count} x {$name}{$price: @ $price}[/s][/color]
// set stockDigits = 0
// set keepStock = 0

[size=3][color=#00B000][Consumables][/color][/size]

// set stockDigits = block
{Health Draught}
{Mana Draught}
{Spirit Draught}

// set stockDigits = block
{Health Potion}
{Mana Potion}
{Spirit Potion}

// set stockDigits = block
{Health Elixir}
{Mana Elixir}
{Spirit Elixir}
{Last Elixir}

// set stockDigits = block
{Flower Vase}
{Bubble-Gum}

// set stockDigits = 0
{Energy Drink}

// set stockDigits = block
{Infusion of Flames}
{Infusion of Frost}
{Infusion of Lightning}
{Infusion of Storms}
{Infusion of Divinity}
{Infusion of Darkness}

// set stockDigits = block
{Scroll of Swiftness}
{Scroll of Protection}
{Scroll of the Avatar}
{Scroll of Absorption}
{Scroll of Shadows}
{Scroll of Life}
{Scroll of the Gods}
// set stockDigits = 0


[size=3][color=#BA05B4][Crystals][/color][/size]

{Crystal Pack}

// set keepStock = min( Crystal of Vigor, Crystal of Finesse, Crystal of Swiftness, Crystal of Fortitude, Crystal of Cunning, Crystal of Knowledge )
{Crystal of Vigor}
{Crystal of Finesse}
{Crystal of Swiftness}
{Crystal of Fortitude}
{Crystal of Cunning}
{Crystal of Knowledge}

// set keepStock = min( Crystal of Flames, Crystal of Frost, Crystal of Lightning, Crystal of Tempest, Crystal of Devotion, Crystal of Corruption )
{Crystal of Flames}
{Crystal of Frost}
{Crystal of Lightning}
{Crystal of Tempest}
{Crystal of Devotion}
{Crystal of Corruption}
// set keepStock = 0


[size=3][color=#489EFF][Monster Foods][/color][/size]

// set stockDigits = block
{Monster Chow}
{Monster Edibles}
{Monster Cuisine}
// set stockDigits = 0
{Happy Pills}


[size=3][color=#f00][Shards][/color][/size]

// set stockDigits = block
{Voidseeker Shard}
{Aether Shard}
{Featherweight Shard}
{Amnesia Shard}


[size=3][color=#f00][Materials][/color][/size]

// set stockDigits = block
{Scrap Cloth}
{Scrap Leather}
{Scrap Metal}
{Scrap Wood}

// set stockDigits = block
{Low-Grade Cloth}
{Mid-Grade Cloth}
{High-Grade Cloth}

{Low-Grade Leather}
{Mid-Grade Leather}
{High-Grade Leather}

{Low-Grade Metals}
{Mid-Grade Metals}
{High-Grade Metals}

{Low-Grade Wood}
{Mid-Grade Wood}
{High-Grade Wood}

// set stockDigits = block
{Crystallized Phazon}
{Shade Fragment}
{Repurposed Actuator}
{Defense Matrix Modulator}


[size=3][color=#f00][Bindings][/color][/size]

// set stockDigits = block
{Binding of Slaughter}
{Binding of Balance}
{Binding of Isaac}
{Binding of Destruction}
{Binding of Focus}
{Binding of Friendship}

{Binding of Protection}
{Binding of Warding}
{Binding of the Fleet}
{Binding of the Barrier}
{Binding of the Nimble}
{Binding of Negation}

{Binding of the Ox}
{Binding of the Raccoon}
{Binding of the Cheetah}
{Binding of the Turtle}
{Binding of the Fox}
{Binding of the Owl}

{Binding of Surtr}
{Binding of Niflheim}
{Binding of Mjolnir}
{Binding of Freyr}
{Binding of Heimdall}
{Binding of Fenrir}

{Binding of the Elementalist}
{Binding of the Heaven-sent}
{Binding of the Demon-fiend}
{Binding of the Curse-weaver}
{Binding of the Earth-walker}

{Binding of Dampening}
{Binding of Stoneskin}
{Binding of Deflection}

{Binding of the Fire-eater}
{Binding of the Frost-born}
{Binding of the Thunder-child}
{Binding of the Wind-waker}
{Binding of the Thrice-blessed}
{Binding of the Spirit-ward}


[size=3][color=#0000FF][Figurines][/color][/size]

// set stockDigits = block
// set keepStock = 1
{Twilight Sparkle Figurine}
{Rainbow Dash Figurine}
{Applejack Figurine}
{Fluttershy Figurine}
{Pinkie Pie Figurine}
{Rarity Figurine}
{Trixie Figurine}
{Princess Celestia Figurine}
{Princess Luna Figurine}
{Apple Bloom Figurine}
{Scootaloo Figurine}
{Sweetie Belle Figurine}
{Big Macintosh Figurine}
{Spitfire Figurine}
{Derpy Hooves Figurine}
{Lyra Heartstrings Figurine}
{Octavia Figurine}
{Zecora Figurine}
{Cheerilee Figurine}
{Vinyl Scratch Figurine}
{Daring Do Figurine}
{Doctor Whooves Figurine}
{Berry Punch Figurine}
{Bon-Bon Figurine}
{Fluffle Puff Figurine}
{Angel Bunny Figurine}
{Gummy Figurine}

// set stockDigits = 0
// set keepStock = 0
`,


// [ITEM PRICE], are used in Equipment Shop, Item Shop, Moogle Mail, Monster Lab and Upgrade
itemPrice : {

	"WTS" : { // WTS price
		"Health Draught": 23,
		"Mana Draught": 45,
		"Spirit Draught": 45,

		"Health Potion": 45,
		"Mana Potion": 90,
		"Spirit Potion": 90,

		"Health Elixir": 450,
		"Mana Elixir": 900,
		"Spirit Elixir": 900,
	},

	"WTB" : { // MoogleMail CoD
		"Health Potion": 45,
		"Mana Potion": 90,
		"Spirit Potion": 90,

		"Scrap Cloth": 90,
		"Scrap Leather": 90,
		"Scrap Metal": 90,
		"Scrap Wood": 90,
		"Energy Cell": 180,

		"ManBearPig Tail": 1300,
		"Holy Hand Grenade of Antioch": 1300,
		"Mithra's Flower": 1300,
		"Dalek Voicebox": 1300,
		"Lock of Blue Hair": 1300,
		"Bunny-Girl Costume": 2600,
		"Hinamatsuri Doll": 2600,
		"Broken Glasses": 2600,
		"Black T-Shirt": 5200,
		"Sapling": 5200,
		"Unicorn Horn": 5000,
		"Noodly Appendage": 5000,
	},

	"Materials" : { // Monster Lab, Upgrade and Salvage
		"Wispy Catalyst": 90,
		"Diluted Catalyst": 450,
		"Regular Catalyst": 900,
		"Robust Catalyst": 2250,
		"Vibrant Catalyst": 4500,
		"Coruscating Catalyst": 9000,

		"Legendary Weapon Core": 5000,
		"Legendary Staff Core": 25000,
		"Legendary Armor Core": 5000,
		"Peerless Weapon Core": 100000,
		"Peerless Staff Core": 500000,
		"Peerless Armor Core": 100000,

		"Scrap Cloth": 90,
		"Scrap Leather": 90,
		"Scrap Metal": 90,
		"Scrap Wood": 90,
		"Energy Cell": 180,

		"Low-Grade Cloth": 2,
		"Mid-Grade Cloth": 400,
		"High-Grade Cloth": 12500,

		"Low-Grade Leather": 2,
		"Mid-Grade Leather": 10,
		"High-Grade Leather": 50,

		"Low-Grade Metals": 10,
		"Mid-Grade Metals": 50,
		"High-Grade Metals": 100,

		"Low-Grade Wood": 2,
		"Mid-Grade Wood": 150,
		"High-Grade Wood": 1500,

		"Crystallized Phazon": 350000,
		"Shade Fragment": 1000,
		"Repurposed Actuator": 20000,
		"Defense Matrix Modulator": 1000,

		"Binding of Slaughter": 50000,
		"Binding of Balance": 100,
		"Binding of Isaac": 500,
		"Binding of Destruction": 60000,
		"Binding of Focus": 10,
		"Binding of Friendship": 100,

		"Binding of Protection": 55000,
		"Binding of Warding": 1000,
		"Binding of the Fleet": 10000,
		"Binding of the Barrier": 500,
		"Binding of the Nimble": 300,
		"Binding of Negation": 100,

		"Binding of the Ox": 2500,
		"Binding of the Raccoon": 2500,
		"Binding of the Cheetah": 35000,
		"Binding of the Turtle": 500,
		"Binding of the Fox": 35000,
		"Binding of the Owl": 35000,

		"Binding of Surtr": 10,
		"Binding of Niflheim": 10,
		"Binding of Mjolnir": 10,
		"Binding of Freyr": 10,
		"Binding of Heimdall": 10,
		"Binding of Fenrir": 10,

		"Binding of the Elementalist": 100,
		"Binding of the Heaven-sent": 10,
		"Binding of the Demon-fiend": 10,
		"Binding of the Curse-weaver": 10,
		"Binding of the Earth-walker": 10,

		"Binding of Dampening": 300,
		"Binding of Stoneskin": 10,
		"Binding of Deflection": 10,
		"Binding of the Fire-eater": 10,
		"Binding of the Frost-born": 10,
		"Binding of the Thunder-child": 10,
		"Binding of the Wind-waker": 10,
		"Binding of the Thrice-blessed": 10,
		"Binding of the Spirit-ward": 10,
	},

	"Trophies" : {
		"ManBearPig Tail": 1300,
		"Holy Hand Grenade of Antioch": 1300,
		"Mithra's Flower": 1300,
		"Dalek Voicebox": 1300,
		"Lock of Blue Hair": 1300,
		"Bunny-Girl Costume": 2600,
		"Hinamatsuri Doll": 2600,
		"Broken Glasses": 2600,
		"Black T-Shirt": 5200,
		"Sapling": 5200,
		"Unicorn Horn": 5000,
		"Noodly Appendage": 5000,
	},

	"Crystals" : { // 12 types of Crystals
		"Crystal of Vigor": 0,
		"Crystal of Finesse": 0,
		"Crystal of Swiftness": 0,
		"Crystal of Fortitude": 0,
		"Crystal of Cunning": 0,
		"Crystal of Knowledge": 0,
		"Crystal of Flames": 0,
		"Crystal of Frost": 0,
		"Crystal of Lightning": 0,
		"Crystal of Tempest": 0,
		"Crystal of Devotion": 0,
		"Crystal of Corruption": 0,
	},

	// add any list if needed

}

};

// END OF SETTINGS


function $id(id,d){return (d||document).getElementById(id);}
function $qs(q,d){return (d||document).querySelector(q);}
function $qsa(q,d){return Array.from((d||document).querySelectorAll(q));}
function $doc(h){var t=document.createElement("template");t.innerHTML=h;return t.content;}
function $element(t,p,a,f){let e;if(t){e=document.createElement(t);}else if(t===""){e=document.createTextNode(a);a=null;}else{return document.createDocumentFragment();}if(a!==null&&a!==undefined){if(typeof a==="number"||typeof a==="string"){e.textContent=a;}else if(Array.isArray(a)){a.forEach(a=>{let an=({" ":"textContent","#":"id",".":"className","!":"style","/":"innerHTML"})[a[0]];if(an){e[an]=a.slice(1);}});}else if(typeof a==="object"){Object.entries(a).forEach(([an,av])=>{if(typeof av==="object"){let a;if(an in e){a=e[an];}else{a=e[an]={};}Object.entries(av).forEach(([an,av])=>{a[an]=av;});}else{if(an==="style"){e.style.cssText=av;}else if(an in e){e[an]=av;}else{e.setAttribute(an,av);}}});}}if(f){if(typeof f==="function"){e.addEventListener("click",f,false);}else if(typeof f==="object"){Object.entries(f).forEach(([ft,fl])=>{e.addEventListener(ft,fl,false);});}}if(p){if(p.nodeType===1||p.nodeType===11){p.appendChild(e);}else if(Array.isArray(p)){if(["beforebegin","afterbegin","beforeend","afterend"].includes(p[1])){p[0].insertAdjacentElement(p[1],e);}else if(!isNaN(p[1])){p[0].insertBefore(e,p[0].childNodes[p[1]]);}else{p[0].insertBefore(e,p[1]);}}}return e;}
function time_format(t,o){t=Math.floor(t/1000);var h=Math.floor(t/3600).toString().padStart(2,"0"),m=Math.floor(t%3600/60).toString().padStart(2,"0"),s=(t%60).toString().padStart(2,"0");return !o?h+":"+m+":"+s:o===1?h+":"+m:o===2?m+":"+s:"";}
function object_sort(o,x=[]){var index={},_x=x.length+1;x.forEach((e,i)=>{index[e]=i+1;});Object.keys(o).sort((a,b)=>{var _a=index[a]||_x,_b=index[b]||_x;return _a-_b||(a<b?-1:1);}).forEach(e=>{var v=o[e];delete o[e];o[e]=v;});}
function scrollIntoView(e,p=e.parentNode){p.scrollTop+=e.getBoundingClientRect().top-p.getBoundingClientRect().top;}
function confirm_event(n,e,m,c,f){if(!n){return;}var a=n.getAttribute("on"+e);n.removeAttribute("on"+e);n.addEventListener(e,function(e){if(!c||c()){if(confirm(m)){if(f){f();}}else{e.preventDefault();e.stopImmediatePropagation();}}},true);n.setAttribute("on"+e,a);}
function toggle_button(e,s,h,t,c,d){function f(){if(t.classList.contains(c)){t.classList.remove(c);e.value=h;}else{t.classList.add(c);e.value=s;}}e.value=h;e.addEventListener("click",f);if(d){f();}}
function play_beep(s={volume:0.2,frequency:500,time:0.5,delay:1}){if(!s.volume){return;}var c=new window.AudioContext(),o=c.createOscillator(),g=c.createGain();o.type="sine";o.frequency.value=s.frequency;g.gain.value=s.volume;o.connect(g);g.connect(c.destination);o.start(s.delay);o.stop(s.delay+s.time);}
function popup(t,s){var r=function(e){e.stopImmediatePropagation();e.preventDefault();if(e.which===1||e.which===13||e.which===27||e.which===32){w.remove();document.removeEventListener("keydown",r);}},w=$element("div",document.body,["!position:fixed;top:0;left:0;width:1236px;height:702px;padding:3px 100% 100% 3px;background-color:#0009;z-index:1001;display:flex;justify-content:center;align-items:center"]),d=$element("div",w,["/"+t,"!min-width:400px;min-height:100px;max-width:100%;max-height:100%;padding:10px;background-color:#fff;border:1px solid;cursor:pointer;display:flex;flex-direction:column;justify-content:center;font-size:10pt;color:#333;"+(s||"")],r);document.addEventListener("keydown",r);return d;}
function popup_text(m,s,b=[]){var v,l;if(typeof m==="string"){v=m;l=m.split("\n").length;}else{v=m.join("\n");l=m.length;}var w=$element("div",document.body,["!position:fixed;top:0;left:0;width:1236px;height:702px;padding:3px 100% 100% 3px;background-color:#0009;z-index:1001;display:flex;justify-content:center;align-items:center"]),d=$element("div",w,["!border:1px solid;padding:5px;background-color:#fff"]),t=$element("textarea",d,{value:v,spellcheck:false,style:"display:block;margin:0 0 5px;font-size:9pt;line-height:1.5em;height:"+(l*1.5+1)+"em;"+(s||"")});b.forEach(o=>{$element("input",d,{type:"button",value:o.value},()=>o.click(w,t));});$element("input",d,{type:"button",value:"Close"},function(){w.remove();});return w;}
function get_message(d,s){if(typeof d==="string"){if(/(<div id="messagebox_inner">.*?<\/div>)/.test(d)){var t=document.createElement("template");t.innerHTML=RegExp.$1;d=t.content;}else{return null;}}var m=$qsa("#messagebox_inner>p",d).map(p=>p.textContent);if(s){return m;}else{return m.join("\n");}}

function getValue(k,d,p=_ns+"_"){var v=localStorage.getItem(p+k);return v===null?d:JSON.parse(v);}
function setValue(k,v,p=_ns+"_"){localStorage.setItem(p+k,JSON.stringify(v));}
function deleteValue(k,p=_ns+"_"){localStorage.removeItem(p+k);}

var _window = typeof unsafeWindow==="undefined"?window:unsafeWindow;
var _query = {};
if(location.search){location.search.slice(1).split("&").forEach(function(q){q=q.split("=",2);_query[q[0]]=decodeURIComponent(q[1].replace(/\+/g," "));});}

var _ns = "hvut";
var _isekai = location.pathname.includes("/isekai/");
if(_isekai) {
	_ns = "hvuti";
  _isekai = /(\d+ Season \d+)/.exec($id('world_text')?.textContent) ? RegExp.$1 : '1';
	if(!settings.reIsekai) {
		settings.randomEncounter = false;
	}
	settings.training = false;
	settings.trainingTimer = false;
	settings.itemBackorder = false;
	settings.monsterLab = false;
	settings.moogleMailCouponClipper = false;
	settings.moogleMailDarkDescent = false;
	settings.lottery = false;
	if(!settings.showLotteryIsekai) {
		settings.showLottery = false;
	}
}


/***** [MODULE] ajax *****/
var $ajax = {

interval : 300, // DO NOT DECREASE THIS NUMBER, OR IT MAY TRIGGER THE SERVER'S LIMITER AND YOU WILL GET BANNED
max : 4,
tid : null,
conn : 0,
index : 0,
queue : [],

add : function(method,url,data,onload,onerror,context={},headers={}) {
	if(method === "GET") {
	} else if(method === "POST") {
		if(!headers["Content-Type"]) {
			headers["Content-Type"] = "application/x-www-form-urlencoded";
		}
		if(data && typeof data === "object") {
			data = Object.entries(data).map(([k,v])=>encodeURIComponent(k)+"="+encodeURIComponent(v)).join("&");
		}
	} else if(method === "JSON") {
		method = "POST";
		if(!headers["Content-Type"]) {
			headers["Content-Type"] = "application/json";
		}
		if(data && typeof data === "object") {
			data = JSON.stringify(data);
		}
	}
	context.onload = onload;
	context.onerror = onerror;
	$ajax.queue.push({method:method,url:url,data:data,headers:headers,onload:$ajax.onload,onerror:$ajax.onerror,context:context});
	$ajax.next();
},
next : function() {
	if(!$ajax.queue[$ajax.index] || $ajax.error) {
		return;
	}
	if($ajax.tid) {
		if(!$ajax.conn) {
			clearTimeout($ajax.tid);
			$ajax.timer();
			$ajax.send();
		}
	} else {
		if($ajax.conn < $ajax.max) {
			$ajax.timer();
			$ajax.send();
		}
	}
},
timer : function() {
	$ajax.tid = setTimeout(function(){
		$ajax.tid = null;
		$ajax.next();
	},$ajax.interval);
},
send : function() {
	GM_xmlhttpRequest($ajax.queue[$ajax.index]);
	$ajax.index++;
	$ajax.conn++;
},
onload : function(r) {
	$ajax.conn--;
	var text = r.responseText.trim();
	if(text === "state lock limiter in effect") {
		if($ajax.error !== text) {
			popup("<span style='color:#f00;font-weight:bold'>"+text+"</span><br><span>Your connection speed is so fast that <br>you have reached the maximum connection limit.</span><br><span>Try again later.</span>");
		}
		$ajax.error = text;
		if(r.context.onerror) {
			r.context.onerror(r);
		}
	} else {
		if(r.context.onload) {
			r.context.onload(r);
		}
		$ajax.next();
	}
},
onerror : function(r) {
	$ajax.conn--;
	if(r.context.onerror) {
		r.context.onerror(r);
	}
	$ajax.next();
}

};
/***** [MODULE] ajax *****/


/***** [MODULE] Random Encounter *****/
var $re = {

init : function() {
	if($re.inited) {
		return;
	}
	$re.inited = true;
	$re.type = !location.hostname.includes("hentaiverse.org")||_isekai?0 : $id("navbar")?1 : $id("battle_top")?2 : false;
	$re.get();
},
get : function() {
	$re.json = getValue("re",{date:0,key:"",count:0,clear:true},"hvut_");
	var gm_json = JSON.parse(GM_getValue("re",null)) || {date:-1};
	if($re.json.date === gm_json.date) {
		if($re.json.clear !== gm_json.clear) {
			$re.json.clear = true;
			$re.set();
		}
	} else {
		if($re.json.date < gm_json.date) {
			$re.json = gm_json;
		}
		$re.set();
	}
},
set : function() {
	setValue("re",$re.json,"hvut_");
	GM_setValue("re",JSON.stringify($re.json));
},
reset : function() {
	$re.json.date = Date.now();
	$re.json.count = 0;
	$re.json.clear = true;
	$re.set();
	$re.start();
},
check : function() {
	$re.init();
	if(/\?s=Battle&ss=ba&encounter=([A-Za-z0-9=]+)(?:&date=(\d+))?/.test(location.search)) {
		var key = RegExp.$1,
			date = parseInt(RegExp.$2),
			now = Date.now();
		if($re.json.key === key) {
			if(!$re.json.clear) {
				$re.json.clear = true;
				$re.set();
			}
		} else if(date) {
			if($re.json.date < date) {
				$re.json.date = date;
				$re.json.key = key;
				$re.json.count++;
				$re.json.clear = true;
				$re.set();
			}
		} else if($re.json.date + 1800000 < now) {
			$re.json.date = now;
			$re.json.key = key;
			$re.json.count++;
			$re.json.clear = true;
			$re.set();
		}
	}
},
clock : function(button) {
	$re.init();
	$re.button = button;
	$re.button.addEventListener("click",function(e){$re.run(e.ctrlKey||e.shiftKey);});
	var date = new Date($re.json.date),
		now = new Date();
	if(date.getUTCDate()!==now.getUTCDate() || date.getUTCMonth()!==now.getUTCMonth() || date.getUTCFullYear()!==now.getUTCFullYear()) {
		$re.reset();
		$re.load();
	}
	$re.check();
	$re.start();
},
refresh : function() {
	var remain = $re.json.date + 1800000 - Date.now();
	if(remain > 0) {
		$re.button.textContent = time_format(remain,2) + " ["+$re.json.count+"]";
		$re.beep = true;
	} else {
		$re.button.textContent = (!$re.json.clear?"Expired":"Ready") + " ["+$re.json.count+"]";
		if($re.beep) {
			$re.beep = false;
			play_beep(settings.reBeep);
		}
		$re.stop();
	}
},
run : function(engage) {
	if($re.type === 2) {
		$re.load();
	} else if($re.type === 1) {
		if(!$re.json.clear || engage) {
			$re.engage();
		} else {
			$re.load(true);
		}
	} else if($re.type === 0) {
		$re.stop();
		$re.button.textContent = "Checking...";
		$ajax.add("GET","https://hentaiverse.org/",null,function(r){
			var html = r.responseText;
			if(html.includes('<div id="navbar">')) {
				if(!$re.json.clear || engage) {
					$re.engage();
				} else {
					$re.load(true);
				}
			} else {
				$re.load();
			}
		});
	}
},
load : function(engage) {
	$re.stop();
	$re.get();
	$re.button.textContent = "Loading...";
	$ajax.add("GET","https://e-hentai.org/news.php",null,function(r){
		var doc = $doc(r.responseText),
			eventpane = $id("eventpane",doc);
		if(eventpane && /\?s=Battle&amp;ss=ba&amp;encounter=([A-Za-z0-9=]+)/.test(eventpane.innerHTML)) {
			$re.json.date = Date.now();
			$re.json.key = RegExp.$1;
			$re.json.count++;
			$re.json.clear = false;
			$re.set();
			if(engage) {
				$re.engage();
				return;
			}
		} else if(eventpane && /It is the dawn of a new day/.test(eventpane.innerHTML)) {
			popup(eventpane.innerHTML);
			$re.reset();
		} else {
			popup("Failed to get a new Random Encounter key");
		}
		$re.start();
	},function(){
		popup("Failed to read the news page");
		$re.start();
	});
},
engage : function() {
	if(!$re.json.key) {
		return;
	}
	var href = "?s=Battle&ss=ba&encounter="+$re.json.key+"&date="+$re.json.date;
	if($re.type === 2) {
		return;
	} else if($re.type === 1) {
		location.href = href;
	} else if($re.type === 0) {
		window.open((settings.reGalleryAlt?"http://alt.hentaiverse.org/":"https://hentaiverse.org/")+href,"_hentaiverse");
		$re.json.clear = true;
		$re.start();
	}
},
start : function() {
	$re.stop();
	if(!$re.json.clear) {
		$re.button.style.color = "#e00";
	} else {
		$re.button.style.color = "";
	}
	$re.tid = setInterval($re.refresh,1000);
	$re.refresh();
},
stop : function() {
	if($re.tid) {
		clearInterval($re.tid);
		$re.tid = 0;
	}
}

};
/***** [MODULE] Random Encounter *****/


/***** [START] *****/
if($id("navbar")) {

// Init
var _ch = {},
	_eq = {},
	_ab = {},
	_tr = {},
	_it = {},
	_in = {},
	_se = {},

	_es = {},
	_is = {},
	_ib = {},
	_ml = {},
	_ss = {},
	_mm = {},
	_lt = {},
	_la = {},

	_ar = {},
	_rb = {},
	_gr = {},
	_iw = {},

	_re = {},
	_up = {},
	_en = {},
	_sa = {},
	_fo = {},
	_fu = {},

	_top = {},
	_bottom = {};


/***** [MODULE] Equip Parser *****/
var $equip = {};

$equip.names = (() => {
    let equipnames = getValue('equipnames', {});
    if (_isekai && (!equipnames.isekai || equipnames.isekai !== _isekai)) {
      equipnames = { isekai: _isekai };
      setValue('equipnames', equipnames);
    }
    return equipnames;
  })();

$equip.dynjs_equip = _window.dynjs_equip || {}; // equipment inventory (player, all, cache)
$equip.dynjs_eqstore = _window.dynjs_eqstore || {}; // equipment storage (player, category) OR store inventory (shop, category)
$equip.dynjs_loaded = {};
$equip.eqvalue = _window.eqvalue || {};
$equip.alias = {"1handed":"One-handed Weapon","2handed":"Two-handed Weapon","staff":"Staff","shield":"Shield","acloth":"Cloth Armor","alight":"Light Armor","aheavy":"Heavy Armor"};

$equip.sort = {
	category: {"One-handed Weapon":1,"Two-handed Weapon":2,"Staff":3,"Shield":4,"Cloth Armor":5,"Light Armor":6,"Heavy Armor":7,"Obsolete":99},
	type: {
		"Rapier":1,"Club":2,"Shortsword":3,"Axe":4,"Wakizashi":5,"Dagger":6,"Sword Chucks":7,
		"Estoc":1,"Mace":2,"Longsword":3,"Katana":4,"Scythe":5,
		"Oak Staff":1,"Willow Staff":2,"Katalox Staff":3,"Redwood Staff":4,"Ebony Staff":5,
		"Force Shield":1,"Buckler":2,"Kite Shield":3,"Tower Shield":4,
		"Phase":1,"Cotton":2,"Gossamer":3,"Silk":4,
		"Shade":1,"Leather":2,"Kevlar":3,"Dragon Hide":4,
		"Power":1,"Plate":2,"Shield":3,"Chainmail":4,
	},
	quality: {"Peerless":1,"Legendary":2,"Magnificent":3,"Exquisite":4,"Superior":5,"Fine":6,"Average":7,"Fair":8,"Crude":9,"Flimsy":10},
	prefix: {
		"Ethereal":1,"Fiery":2,"Arctic":3,"Shocking":4,"Tempestuous":5,"Hallowed":6,"Demonic":7,
		"Radiant":1,"Charged":2,"Mystic":3,"Frugal":4,
		"Savage":1,"Agile":2,"Reinforced":3,"Shielding":4,"Mithril":5,
		"Ruby":11,"Cobalt":12,"Amber":13,"Jade":14,"Zircon":15,"Onyx":16
	},
	slot: {
		"Cap":1,"Robe":2,"Gloves":3,"Pants":4,"Shoes":5,
		"Helmet":1,"Breastplate":2,"Cuirass":2,"Armor":2,"Gauntlets":3,"Greaves":4,"Leggings":4,"Sabatons":5,"Boots":5
	},
	suffix: {
		"Slaughter":1,"Balance":2,"Swiftness":3,"the Barrier":4,"the Nimble":5,"the Battlecaster":6,"the Vampire":7,"the Illithid":8,"the Banshee":9,
		"Destruction":1,"Surtr":2,"Niflheim":3,"Mjolnir":4,"Freyr":5,"Heimdall":6,"Fenrir":7,"the Elementalist":8,"the Heaven-sent":9,"the Demon-fiend":10,"the Earth-walker":11,"the Curse-weaver":12,"Focus":13,
		"the Shadowdancer":1,"the Fleet":2,"the Arcanist":3,"Negation":4,
		"Protection":21,"Warding":22,"Dampening":23,"Stoneskin":24,"Deflection":25
	}
};

$equip.reg = {
	quality : "Flimsy|Crude|Fair|Average|Fine|Superior|Exquisite|Magnificent|Legendary|Peerless",
	prefix : "Ethereal|Fiery|Arctic|Shocking|Tempestuous|Hallowed|Demonic|Ruby|Cobalt|Amber|Jade|Zircon|Onyx|Charged|Frugal|Radiant|Mystic|Agile|Reinforced|Savage|Shielding|Mithril",
	slot : "Cap|Robe|Gloves|Pants|Shoes|Helmet|Breastplate|Gauntlets|Leggings|Boots|Cuirass|Armor|Greaves|Sabatons|Coif|Hauberk|Mitons|Chausses|Boots",
	OneHanded : "Axe|Club|Rapier|Shortsword|Wakizashi|Dagger|Sword Chucks",
	TwoHanded : "Estoc|Longsword|Mace|Katana|Scythe",
	Staff : "Oak Staff|Willow Staff|Katalox Staff|Redwood Staff|Ebony Staff",
	Shield : "Buckler|Kite Shield|Force Shield|Tower Shield",
	Cloth : "Cotton|Phase|Gossamer|Silk",
	Light : "Leather|Shade|Kevlar|Dragon Hide",
	Heavy : "Plate|Power|Shield|Chainmail",
};

$equip.reg.name = new RegExp("^("+$equip.reg.quality+")(?: (?:("+$equip.reg.prefix+")|(.+?)))? (?:("+$equip.reg.OneHanded+")|("+$equip.reg.TwoHanded+")|("+$equip.reg.Staff+")|("+$equip.reg.Shield+")|(?:(?:("+$equip.reg.Cloth+")|("+$equip.reg.Light+")|("+$equip.reg.Heavy+")) ("+$equip.reg.slot+")))(?: of (.+))?$","i");
$equip.reg.html = /<div>(.+?) &nbsp; &nbsp; (?:Level (\d+|Unassigned) )?&nbsp; &nbsp; <span>(Tradeable|Untradeable|Soulbound)<\/span><\/div><div>Condition: (\d+) \/ (\d+) \(\d+%\) &nbsp; &nbsp; Potency Tier: (\d+) \((?:(\d+) \/ (\d+)|MAX)\)/;
$equip.reg.pab = /Strength|Dexterity|Agility|Endurance|Intelligence|Wisdom/g;

$equip.stats = {
	"Attack Damage" : {scale:50/3,fluc:0.0854,forge:"Physical Damage",binding:"Binding of Slaughter",potency:"Butcher",plus:0.02},
	"Attack Speed" : {scale:Infinity,fluc:0.0481,potency:"Swift Strike",plus:1.924,multi:true},
	"Attack Accuracy" : {scale:5000,fluc:0.06069,forge:"Physical Hit Chance",binding:"Binding of Balance"},
	"Attack Crit Chance" : {scale:2000,fluc:0.0105,forge:"Physical Crit Chance",binding:"Binding of Isaac",multi:true},
	"Attack Crit Damage" : {scale:Infinity,fluc:0.01,potency:"Fatality",plus:2},
	"Counter-Parry" : {scale:Infinity,fluc:0,potency:"Overpower",plus:4},
	"Magic Damage" : {scale:250/11,fluc:0.082969,forge:"Magical Damage",binding:"Binding of Destruction",potency:"Archmage",plus:0.02},
	"Casting Speed" : {scale:Infinity,fluc:0.0489,potency:"Spellweaver",plus:1.467},
	"Magic Accuracy" : {scale:5000,fluc:0.0491,forge:"Magical Hit Chance",binding:"Binding of Focus"},
	"Magic Crit Chance" : {scale:2000,fluc:0.0114,forge:"Magical Crit Chance",binding:"Binding of Friendship",multi:true},
	"Spell Crit Damage" : {scale:Infinity,fluc:0.01,potency:"Annihilator",plus:2},
	"Mana Conservation" : {scale:Infinity,fluc:0.1,potency:"Economizer",plus:5,multi:true},
	"Counter-Resist" : {scale:Infinity,fluc:0.1,potency:"Penetrator",plus:4},
	"Physical Mitigation" : {scale:2000,fluc:0.021,forge:"Physical Defense",binding:"Binding of Protection",multi:true},
	"Magical Mitigation" : {scale:2000,fluc:0.0201,forge:"Magical Defense",binding:"Binding of Warding",multi:true},
	"Evade Chance" : {scale:2000,fluc:0.025,forge:"Evade Chance",binding:"Binding of the Fleet",multi:true},
	"Block Chance" : {scale:2000,fluc:0.0998,forge:"Block Chance",binding:"Binding of the Barrier",multi:true},
	"Parry Chance" : {scale:2000,fluc:0.0894,forge:"Parry Chance",binding:"Binding of the Nimble",multi:true},
	"Resist Chance" : {scale:2000,fluc:0.0804,forge:"Resist Chance",binding:"Binding of Negation",multi:true},
	"HP Bonus" : {scale:Infinity,fluc:0},
	"MP Bonus" : {scale:Infinity,fluc:0},
	"Burden" : {scale:Infinity,fluc:0},
	"Interference" : {scale:Infinity,fluc:0},
	"Fire EDB" : {scale:200,fluc:0.0804,forge:"Fire Spell Damage",binding:"Binding of Surtr"},
	"Cold EDB" : {scale:200,fluc:0.0804,forge:"Cold Spell Damage",binding:"Binding of Niflheim"},
	"Elec EDB" : {scale:200,fluc:0.0804,forge:"Elec Spell Damage",binding:"Binding of Mjolnir"},
	"Wind EDB" : {scale:200,fluc:0.0804,forge:"Wind Spell Damage",binding:"Binding of Freyr"},
	"Holy EDB" : {scale:200,fluc:0.0804,forge:"Holy Spell Damage",binding:"Binding of Heimdall"},
	"Dark EDB" : {scale:200,fluc:0.0804,forge:"Dark Spell Damage",binding:"Binding of Fenrir"},
	"Elemental" : {scale:250/7,fluc:0.0306,forge:"Elemental Proficiency",binding:"Binding of the Elementalist"},
	"Divine" : {scale:250/7,fluc:0.0306,forge:"Divine Proficiency",binding:"Binding of the Heaven-sent"},
	"Forbidden" : {scale:250/7,fluc:0.0306,forge:"Forbidden Proficiency",binding:"Binding of the Demon-fiend"},
	"Supportive" : {scale:250/7,fluc:0.0306,forge:"Supportive Proficiency",binding:"Binding of the Earth-walker"},
	"Deprecating" : {scale:250/7,fluc:0.0306,forge:"Deprecating Proficiency",binding:"Binding of the Curse-weaver"},
	"Crushing" : {scale:Infinity,fluc:0.0155,forge:"Crushing Mitigation",binding:"Binding of Dampening",multi:true},
	"Slashing" : {scale:Infinity,fluc:0.0153,forge:"Slashing Mitigation",binding:"Binding of Stoneskin",multi:true},
	"Piercing" : {scale:Infinity,fluc:0.015,forge:"Piercing Mitigation",binding:"Binding of Deflection",multi:true},
	"Fire MIT" : {scale:Infinity,fluc:0.1,forge:"Fire Mitigation",binding:"Binding of the Fire-eater",multi:true},
	"Cold MIT" : {scale:Infinity,fluc:0.1,forge:"Cold Mitigation",binding:"Binding of the Frost-born",multi:true},
	"Elec MIT" : {scale:Infinity,fluc:0.1,forge:"Elec Mitigation",binding:"Binding of the Thunder-child",multi:true},
	"Wind MIT" : {scale:Infinity,fluc:0.1,forge:"Wind Mitigation",binding:"Binding of the Wind-waker",multi:true},
	"Holy MIT" : {scale:Infinity,fluc:0.1,forge:"Holy Mitigation",binding:"Binding of the Thrice-blessed",multi:true},
	"Dark MIT" : {scale:Infinity,fluc:0.1,forge:"Dark Mitigation",binding:"Binding of the Spirit-ward",multi:true},
	"Strength" : {scale:250/7,fluc:0.03,forge:"Strength Bonus",binding:"Binding of the Ox"},
	"Dexterity" : {scale:250/7,fluc:0.03,forge:"Dexterity Bonus",binding:"Binding of the Raccoon"},
	"Agility" : {scale:250/7,fluc:0.03,forge:"Agility Bonus",binding:"Binding of the Cheetah"},
	"Endurance" : {scale:250/7,fluc:0.03,forge:"Endurance Bonus",binding:"Binding of the Turtle"},
	"Intelligence" : {scale:250/7,fluc:0.03,forge:"Intelligence Bonus",binding:"Binding of the Fox"},
	"Wisdom" : {scale:250/7,fluc:0.03,forge:"Wisdom Bonus",binding:"Binding of the Owl"}
};

$equip.prefix = {
	"Ethereal" : [],
	"Fiery" : ["Fire EDB"],
	"Arctic" : ["Cold EDB"],
	"Shocking" : ["Elec EDB"],
	"Tempestuous" : ["Wind EDB"],
	"Hallowed" : ["Holy EDB"],
	"Demonic" : ["Dark EDB"],
	"Ruby" : ["Fire MIT"],
	"Cobalt" : ["Cold MIT"],
	"Amber" : ["Elec MIT"],
	"Jade" : ["Wind MIT"],
	"Zircon" : ["Holy MIT"],
	"Onyx" : ["Dark MIT"],
	"Charged" : ["Casting Speed"],
	"Frugal" : ["Mana Conservation"],
	"Radiant" : ["Magic Damage"],
	"Mystic" : ["Spell Crit Damage"],
	"Agile" : ["Attack Speed"],
	"Reinforced" : ["Crushing","Slashing","Piercing"],
	"Savage" : ["Attack Crit Damage"],
	"Shielding" : ["Block Chance"],
	"Mithril" : ["Burden"]
};

$equip.suffix = {
	"Slaughter" : ["Attack Damage"],
	"Balance" : ["Attack Accuracy","Attack Crit Chance"],
	"Swiftness" : ["Attack Speed"],
	"the Barrier" : ["Block Chance"],
	"the Nimble" : ["Parry Chance"],
	"the Battlecaster" : ["Magic Accuracy","Mana Conservation"],
	"the Vampire" : [],
	"the Illithid" : [],
	"the Banshee" : [],
	"Destruction" : ["Magic Damage"],
	"Surtr" : ["Fire EDB"],
	"Niflheim" : ["Cold EDB"],
	"Mjolnir" : ["Elec EDB"],
	"Freyr" : ["Wind EDB"],
	"Heimdall" : ["Holy EDB"],
	"Fenrir" : ["Dark EDB"],
	"the Elementalist" : ["Elemental"],
	"the Heaven-sent" : ["Divine"],
	"the Demon-fiend" : ["Forbidden"],
	"the Earth-walker" : ["Supportive"],
	"the Curse-weaver" : ["Deprecating"],
	"Focus" : ["Magic Accuracy","Magic Crit Chance","Mana Conservation"],
	"the Shadowdancer" : ["Attack Crit Chance","Evade Chance"],
	"the Fleet" : ["Evade Chance"],
	"the Arcanist" : ["Magic Accuracy","Interference","Intelligence","Wisdom"],
	"Negation" : ["Resist Chance"],
	"Protection" : ["Physical Mitigation"],
	"Warding" : ["Magical Mitigation"],
	"Dampening" : ["Crushing"],
	"Stoneskin" : ["Slashing"],
	"Deflection" : ["Piercing"]
};

$equip.pmax = {
	"Axe" : {"Attack Damage":[59.87,75.92],"Attack Accuracy":[12.81],"Attack Crit Chance":[5.37],"Burden":[14],"Interference":[3.5],"Fire EDB":[0,11.34],"Cold EDB":[0,11.34],"Elec EDB":[0,11.34],"Wind EDB":[0,11.34],"Holy EDB":[0,11.34],"Dark EDB":[0,11.34],"Strength":[6.33],"Dexterity":[4.08],"Agility":[3.33]},
	"Club" : {"Attack Damage":[53.04,67.72],"Attack Accuracy":[12.81,31.02],"Attack Crit Chance":[5.37,10.41],"Magic Accuracy":[0,7.91],"Mana Conservation":[0,16.11],"Parry Chance":[0,9.04],"Burden":[9.8],"Interference":[3.5],"Fire EDB":[0,11.34],"Cold EDB":[0,11.34],"Elec EDB":[0,11.34],"Wind EDB":[0,11.34],"Holy EDB":[0,11.34],"Dark EDB":[0,11.34],"Strength":[6.33],"Dexterity":[4.08],"Agility":[4.08]},
	"Rapier" : {"Attack Damage":[39.38,51.33],"Attack Accuracy":[21.92,44.68],"Attack Crit Chance":[5.37,10.41],"Magic Accuracy":[0,7.91],"Mana Conservation":[0,16.11],"Parry Chance":[18.89,26.94],"Burden":[6.3],"Interference":[3.5],"Fire EDB":[0,11.34],"Cold EDB":[0,11.34],"Elec EDB":[0,11.34],"Wind EDB":[0,11.34],"Holy EDB":[0,11.34],"Dark EDB":[0,11.34],"Strength":[4.08],"Dexterity":[6.33],"Agility":[4.08]},
	"Shortsword" : {"Attack Damage":[47.92,61.58],"Attack Speed":[0,6.54],"Attack Accuracy":[27.99,53.79],"Attack Crit Chance":[5.37,10.41],"Magic Accuracy":[0,7.91],"Mana Conservation":[0,16.11],"Parry Chance":[18.89],"Burden":[5.25],"Interference":[3.5],"Fire EDB":[0,11.34],"Cold EDB":[0,11.34],"Elec EDB":[0,11.34],"Wind EDB":[0,11.34],"Holy EDB":[0,11.34],"Dark EDB":[0,11.34],"Strength":[6.33],"Dexterity":[6.33],"Agility":[6.33]},
	"Wakizashi" : {"Attack Damage":[35.11,46.21],"Attack Speed":[12.56,18.57],"Attack Accuracy":[24.95,49.24],"Attack Crit Chance":[5.37,10.41],"Magic Accuracy":[0,7.91],"Mana Conservation":[0,16.12],"Parry Chance":[22.47,30.53],"Burden":[2.8],"Interference":[3.5],"Fire EDB":[0,11.34],"Cold EDB":[0,11.34],"Elec EDB":[0,11.34],"Wind EDB":[0,11.34],"Holy EDB":[0,11.34],"Dark EDB":[0,11.34],"Strength":[3.33],"Dexterity":[7.83],"Agility":[7.83]},

	"Estoc" : {"Attack Damage":[64.99,82.07],"Attack Accuracy":[9.77,26.47],"Attack Crit Chance":[7.99,13.56],"Magic Accuracy":[0,12.82],"Mana Conservation":[0,26.11],"Burden":[14],"Interference":[7],"Fire EDB":[0,11.34],"Cold EDB":[0,11.34],"Elec EDB":[0,11.34],"Wind EDB":[0,11.34],"Holy EDB":[0,11.34],"Dark EDB":[0,11.34],"Strength":[11.58],"Dexterity":[6.03],"Agility":[3.93]},
	"Longsword" : {"Attack Damage":[78.66,98.47],"Attack Accuracy":[12.81,31.02],"Attack Crit Chance":[8.52,14.19],"Magic Accuracy":[0,12.81],"Mana Conservation":[0,26.11],"Burden":[21],"Interference":[10.5],"Fire EDB":[0,11.34],"Cold EDB":[0,11.34],"Elec EDB":[0,11.34],"Wind EDB":[0,11.34],"Holy EDB":[0,11.34],"Dark EDB":[0,11.34],"Strength":[12.33],"Dexterity":[9.33],"Agility":[4.83]},
	"Mace" : {"Attack Damage":[64.99,82.07],"Attack Accuracy":[12.2,30.11],"Attack Crit Chance":[7.99,13.56],"Magic Accuracy":[0,12.82],"Mana Conservation":[0,26.11],"Burden":[14],"Interference":[7],"Fire EDB":[0,11.34],"Cold EDB":[0,11.34],"Elec EDB":[0,11.34],"Wind EDB":[0,11.34],"Holy EDB":[0,11.34],"Dark EDB":[0,11.34],"Strength":[11.73],"Dexterity":[7.83],"Agility":[4.08]},
	"Katana" : {"Attack Damage":[64.99,82.07],"Attack Accuracy":[27.98,53.78],"Attack Crit Chance":[8.52,14.19],"Burden":[14],"Interference":[7],"Fire EDB":[0,11.34],"Cold EDB":[0,11.34],"Elec EDB":[0,11.34],"Wind EDB":[0,11.34],"Holy EDB":[0,11.34],"Dark EDB":[0,11.34],"Strength":[12.33],"Dexterity":[9.33],"Agility":[4.83]},

	"Katalox Staff" : {"Attack Damage":[34.22],"Magic Damage":[32.39,52.2],"Magic Accuracy":[19.18,38.34],"Magic Crit Chance":[7.3,12.39],"Mana Conservation":[0,33.08],"Burden":[7],"Fire EDB":[0,11.31],"Cold EDB":[0,11.31],"Elec EDB":[0,11.31],"Wind EDB":[0,11.31],"Holy EDB":[11.31,21.76,27.39,37.84],"Dark EDB":[11.31,21.76,27.39,37.84],"Divine":[8.28,16.24],"Forbidden":[8.28,16.24],"Deprecating":[6.14],"Intelligence":[7.22],"Wisdom":[4.82]},
	"Oak Staff" : {"Attack Damage":[34.23],"Magic Damage":[31.98],"Magic Accuracy":[18.94,38],"Magic Crit Chance":[5.82,10.61],"Mana Conservation":[0,33.09],"Counter-Resist":[13.58],"Burden":[4.9],"Fire EDB":[8.1,18.56],"Cold EDB":[8.1,18.56],"Elec EDB":[0,11.32],"Wind EDB":[0,11.32],"Holy EDB":[16.14,26.6,32.22,42.68],"Dark EDB":[0,11.32],"Elemental":[6.45],"Divine":[6.45],"Supportive":[11.81,19.76],"Intelligence":[4.82],"Wisdom":[7.22]},
	"Redwood Staff" : {"Attack Damage":[34.23],"Magic Damage":[31.98,51.71],"Magic Accuracy":[18.94,38],"Magic Crit Chance":[5.82,10.61],"Mana Conservation":[0,33.09],"Burden":[4.9],"Fire EDB":[11.32,21.77,27.4,37.85],"Cold EDB":[11.32,21.77,27.4,37.85],"Elec EDB":[11.32,21.77,27.4,37.85],"Wind EDB":[11.32,21.77,27.4,37.85],"Holy EDB":[0,11.32],"Dark EDB":[0,11.32],"Elemental":[8.29,16.24],"Supportive":[4.31],"Deprecating":[4.31],"Intelligence":[6.32],"Wisdom":[6.32]},
	"Willow Staff" : {"Attack Damage":[34.23],"Magic Damage":[31.98,51.71],"Magic Accuracy":[18.94,38],"Magic Crit Chance":[5.82,10.61],"Mana Conservation":[0,33.09],"Counter-Resist":[13.58],"Burden":[4.9],"Fire EDB":[0,11.32],"Cold EDB":[0,11.32],"Elec EDB":[8.1,18.56],"Wind EDB":[8.1,18.56],"Holy EDB":[0,11.32],"Dark EDB":[16.14,26.6],"Elemental":[6.14],"Forbidden":[6.14],"Deprecating":[11.81,19.76],"Intelligence":[4.82],"Wisdom":[7.22]},

	"Buckler" : {"Attack Speed":[0,3.65],"Magic Accuracy":[0,12.82],"Mana Conservation":[0,26.1],"Physical Mitigation":[2.33,4.22],"Magical Mitigation":[2.23,6.65],"Block Chance":[31.03,37.52],"Parry Chance":[0,9.04],"Burden":[2.8,2.1],"Interference":[1.4],"Crushing":[0,3.27],"Slashing":[0,3.23],"Piercing":[0,3.16],"Strength":[6.33],"Dexterity":[6.33],"Endurance":[6.33],"Agility":[6.33]},
	"Force Shield" : {"Physical Mitigation":[3.38,5.48],"Magical Mitigation":[3.24,7.86],"Block Chance":[38.52],"Burden":[2.8],"Interference":[28],"Crushing":[0,7.14],"Slashing":[0,7.05],"Piercing":[0,6.91],"Fire MIT":[0,26.1],"Cold MIT":[0,26.1],"Elec MIT":[0,26.1],"Wind MIT":[0,26.1],"Holy MIT":[0,26.1],"Dark MIT":[0,26.1],"Strength":[6.33],"Dexterity":[6.33],"Endurance":[6.33],"Agility":[6.33]},
	"Kite Shield" : {"Attack Speed":[0,3.65],"Physical Mitigation":[3.38,5.48],"Magical Mitigation":[3.24,7.86],"Block Chance":[36.02],"Burden":[10.5,7.91],"Interference":[10.5],"Crushing":[0,3.27,7.14,10.24],"Slashing":[0,3.23,7.05,10.11],"Piercing":[0,3.16,6.91,9.91],"Strength":[6.33],"Dexterity":[6.33],"Endurance":[6.33],"Agility":[6.33]},

	"Phase Cap" : {"Attack Accuracy":[4.62],"Magic Damage":[0,4.23],"Casting Speed":[0,3.47],"Magic Accuracy":[5.45],"Spell Crit Damage":[0,3.91],"Mana Conservation":[0,3.61],"Physical Mitigation":[3.38],"Magical Mitigation":[4.24],"Evade Chance":[5.28],"Resist Chance":[6.52],"Fire EDB":[0,16.97],"Cold EDB":[0,16.97],"Elec EDB":[0,16.97],"Wind EDB":[0,16.97],"Holy EDB":[0,16.97],"Dark EDB":[0,16.97],"Crushing":[2.5],"Fire MIT":[0,26.11],"Cold MIT":[0,26.11],"Elec MIT":[0,26.11],"Wind MIT":[0,26.11],"Holy MIT":[0,26.11],"Dark MIT":[0,26.11],"Agility":[6.03],"Intelligence":[7.08],"Wisdom":[7.08]},
	"Phase Robe" : {"Attack Accuracy":[5.41],"Magic Damage":[0,4.9],"Casting Speed":[0,4.06],"Magic Accuracy":[6.43],"Spell Crit Damage":[0,4.67],"Mana Conservation":[0,4.11],"Physical Mitigation":[4.01],"Magical Mitigation":[5.05],"Evade Chance":[6.28],"Resist Chance":[7.64],"Fire EDB":[0,20.18],"Cold EDB":[0,20.18],"Elec EDB":[0,20.18],"Wind EDB":[0,20.18],"Holy EDB":[0,20.18],"Dark EDB":[0,20.18],"Crushing":[2.96],"Fire MIT":[0,31.11],"Cold MIT":[0,31.11],"Elec MIT":[0,31.11],"Wind MIT":[0,31.11],"Holy MIT":[0,31.11],"Dark MIT":[0,31.11],"Agility":[7.17],"Intelligence":[8.43],"Wisdom":[8.43]},
	"Phase Gloves" : {"Attack Accuracy":[4.25],"Magic Damage":[0,3.9],"Casting Speed":[0,3.18],"Magic Accuracy":[4.96],"Spell Crit Damage":[0,3.53],"Mana Conservation":[0,3.41],"Physical Mitigation":[3.07],"Magical Mitigation":[3.84],"Evade Chance":[4.78],"Resist Chance":[5.95],"Fire EDB":[0,15.36],"Cold EDB":[0,15.36],"Elec EDB":[0,15.36],"Wind EDB":[0,15.36],"Holy EDB":[0,15.36],"Dark EDB":[0,15.36],"Crushing":[2.26],"Fire MIT":[0,23.61],"Cold MIT":[0,23.61],"Elec MIT":[0,23.61],"Wind MIT":[0,23.61],"Holy MIT":[0,23.61],"Dark MIT":[0,23.61],"Agility":[5.46],"Intelligence":[6.42],"Wisdom":[6.42]},
	"Phase Pants" : {"Attack Accuracy":[5.04],"Magic Damage":[0,4.56],"Casting Speed":[0,3.77],"Magic Accuracy":[5.94],"Spell Crit Damage":[0,4.29],"Mana Conservation":[0,3.91],"Physical Mitigation":[3.7],"Magical Mitigation":[4.64],"Evade Chance":[5.78],"Resist Chance":[7.08],"Fire EDB":[0,18.58],"Cold EDB":[0,18.58],"Elec EDB":[0,18.58],"Wind EDB":[0,18.58],"Holy EDB":[0,18.58],"Dark EDB":[0,18.58],"Crushing":[2.73],"Fire MIT":[0,28.61],"Cold MIT":[0,28.61],"Elec MIT":[0,28.61],"Wind MIT":[0,28.61],"Holy MIT":[0,28.61],"Dark MIT":[0,28.61],"Agility":[6.6],"Intelligence":[7.77],"Wisdom":[7.77]},
	"Phase Shoes" : {"Attack Accuracy":[3.83],"Magic Damage":[0,3.57],"Casting Speed":[0,2.89],"Magic Accuracy":[4.47],"Spell Crit Damage":[0,3.15],"Mana Conservation":[0,3.11],"Physical Mitigation":[2.75],"Magical Mitigation":[3.44],"Evade Chance":[4.28],"Resist Chance":[5.39],"Fire EDB":[0,13.75],"Cold EDB":[0,13.75],"Elec EDB":[0,13.75],"Wind EDB":[0,13.75],"Holy EDB":[0,13.75],"Dark EDB":[0,13.75],"Crushing":[2.03],"Fire MIT":[0,21.11],"Cold MIT":[0,21.11],"Elec MIT":[0,21.11],"Wind MIT":[0,21.11],"Holy MIT":[0,21.11],"Dark MIT":[0,21.11],"Agility":[4.89],"Intelligence":[5.73],"Wisdom":[5.73]},

	"Cotton Cap" : {"Attack Accuracy":[4.62],"Casting Speed":[0,3.47],"Magic Accuracy":[4.23],"Mana Conservation":[0,3.61],"Physical Mitigation":[4.43,6.74],"Magical Mitigation":[4.24,9.07],"Evade Chance":[4.03],"Resist Chance":[6.11],"Proficiency":[0,8.29],"Crushing":[3.27],"Fire MIT":[0,26.11],"Cold MIT":[0,26.11],"Elec MIT":[0,26.11],"Wind MIT":[0,26.11],"Holy MIT":[0,26.11],"Dark MIT":[0,26.11],"Agility":[4.83],"Intelligence":[6.33],"Wisdom":[6.33]},
	"Cotton Robe" : {"Attack Accuracy":[5.41],"Casting Speed":[0,4.06],"Magic Accuracy":[4.96],"Mana Conservation":[0,4.11],"Physical Mitigation":[5.27,8.04],"Magical Mitigation":[5.05,10.83],"Evade Chance":[4.78],"Resist Chance":[7.16],"Proficiency":[0,9.89],"Crushing":[3.89],"Fire MIT":[0,31.11],"Cold MIT":[0,31.11],"Elec MIT":[0,31.11],"Wind MIT":[0,31.11],"Holy MIT":[0,31.11],"Dark MIT":[0,31.11],"Agility":[5.73],"Intelligence":[7.53],"Wisdom":[7.53]},
	"Cotton Gloves" : {"Attack Accuracy":[4.25],"Casting Speed":[0,3.18],"Magic Accuracy":[3.88],"Mana Conservation":[0,3.41],"Physical Mitigation":[4.01,6.09],"Magical Mitigation":[3.84,8.18],"Evade Chance":[3.65],"Resist Chance":[5.63],"Proficiency":[0,7.5],"Crushing":[2.96],"Fire MIT":[0,23.61],"Cold MIT":[0,23.61],"Elec MIT":[0,23.61],"Wind MIT":[0,23.61],"Holy MIT":[0,23.61],"Dark MIT":[0,23.61],"Agility":[4.38],"Intelligence":[5.73],"Wisdom":[5.73]},
	"Cotton Pants" : {"Attack Accuracy":[5.04],"Casting Speed":[0,3.77],"Magic Accuracy":[4.62],"Mana Conservation":[0,3.91],"Physical Mitigation":[4.85,7.39],"Magical Mitigation":[4.64,9.95],"Evade Chance":[4.4],"Resist Chance":[6.68],"Proficiency":[0,9.09],"Crushing":[3.58],"Fire MIT":[0,28.61],"Cold MIT":[0,28.61],"Elec MIT":[0,28.61],"Wind MIT":[0,28.61],"Holy MIT":[0,28.61],"Dark MIT":[0,28.61],"Agility":[5.28],"Intelligence":[6.93],"Wisdom":[6.93]},
	"Cotton Shoes" : {"Attack Accuracy":[3.83],"Casting Speed":[0,2.89],"Magic Accuracy":[3.49],"Mana Conservation":[0,3.11],"Physical Mitigation":[3.59,5.44],"Magical Mitigation":[3.44,7.3],"Evade Chance":[3.28],"Resist Chance":[5.07],"Proficiency":[0,6.7],"Crushing":[2.65],"Fire MIT":[0,21.11],"Cold MIT":[0,21.11],"Elec MIT":[0,21.11],"Wind MIT":[0,21.11],"Holy MIT":[0,21.11],"Dark MIT":[0,21.11],"Agility":[3.93],"Intelligence":[5.13],"Wisdom":[5.13]},

	"Shade Helmet" : {"Attack Damage":[11.25],"Attack Speed":[0,3.69],"Attack Accuracy":[6.78],"Attack Crit Chance":[0,2.75],"Attack Crit Damage":[0,3.12],"Magic Accuracy":[0,7.01],"Physical Mitigation":[6.97],"Magical Mitigation":[5.26],"Evade Chance":[4.42,6.67],"Resist Chance":[14.21,21.44],"Interference":[8.4,2.1],"Crushing":[5.99],"Slashing":[5.92],"Fire MIT":[0,26.17],"Cold MIT":[0,26.17],"Elec MIT":[0,26.17],"Wind MIT":[0,26.17],"Holy MIT":[0,26.17],"Dark MIT":[0,26.17],"Strength":[4.1],"Dexterity":[4.85],"Endurance":[4.1],"Agility":[4.85],"Intelligence":[0,3.38],"Wisdom":[0,3.38]},
	"Shade Breastplate" : {"Attack Damage":[13.3],"Attack Speed":[0,4.31],"Attack Accuracy":[7.99],"Attack Crit Chance":[0,3.27],"Attack Crit Damage":[0,3.72],"Magic Accuracy":[0,8.29],"Physical Mitigation":[8.31],"Magical Mitigation":[6.27],"Evade Chance":[5.24,7.94],"Resist Chance":[16.86,25.54],"Interference":[10.08,2.52],"Crushing":[7.16],"Slashing":[7.06],"Fire MIT":[0,31.17],"Cold MIT":[0,31.17],"Elec MIT":[0,31.17],"Wind MIT":[0,31.17],"Holy MIT":[0,31.17],"Dark MIT":[0,31.17],"Strength":[4.85],"Dexterity":[5.75],"Endurance":[4.85],"Agility":[5.75],"Intelligence":[0,3.98],"Wisdom":[0,3.98]},
	"Shade Gauntlets" : {"Attack Damage":[10.22],"Attack Speed":[0,3.4],"Attack Accuracy":[6.17],"Attack Crit Chance":[0,2.49],"Attack Crit Damage":[0,2.82],"Magic Accuracy":[0,6.37],"Physical Mitigation":[6.29],"Magical Mitigation":[4.76],"Evade Chance":[4.02,6.04],"Resist Chance":[12.92,19.43],"Interference":[7.56,1.89],"Crushing":[5.42],"Slashing":[5.35],"Fire MIT":[0,23.67],"Cold MIT":[0,23.67],"Elec MIT":[0,23.67],"Wind MIT":[0,23.67],"Holy MIT":[0,23.67],"Dark MIT":[0,23.67],"Strength":[3.74],"Dexterity":[4.4],"Endurance":[3.74],"Agility":[4.4],"Intelligence":[0,3.08],"Wisdom":[0,3.08]},
	"Shade Leggings" : {"Attack Damage":[12.27],"Attack Speed":[0,4.03],"Attack Accuracy":[7.39],"Attack Crit Chance":[0,3.01],"Attack Crit Damage":[0,3.42],"Magic Accuracy":[0,7.65],"Physical Mitigation":[7.64],"Magical Mitigation":[5.76],"Evade Chance":[4.84,7.32],"Resist Chance":[15.57,23.53],"Interference":[9.24,2.31],"Crushing":[6.58],"Slashing":[6.5],"Fire MIT":[0,28.67],"Cold MIT":[0,28.67],"Elec MIT":[0,28.67],"Wind MIT":[0,28.67],"Holy MIT":[0,28.67],"Dark MIT":[0,28.67],"Strength":[4.49],"Dexterity":[5.3],"Endurance":[4.49],"Agility":[5.3],"Intelligence":[0,3.68],"Wisdom":[0,3.68]},
	"Shade Boots" : {"Attack Damage":[9.2],"Attack Speed":[0,3.06],"Attack Accuracy":[5.57],"Attack Crit Chance":[0,2.22],"Attack Crit Damage":[0,2.52],"Magic Accuracy":[0,5.73],"Physical Mitigation":[5.62],"Magical Mitigation":[4.26],"Evade Chance":[3.59,5.39],"Resist Chance":[11.55,17.34],"Interference":[6.72,1.68],"Crushing":[4.83],"Slashing":[4.77],"Fire MIT":[0,21.17],"Cold MIT":[0,21.17],"Elec MIT":[0,21.17],"Wind MIT":[0,21.17],"Holy MIT":[0,21.17],"Dark MIT":[0,21.17],"Strength":[3.35],"Dexterity":[3.95],"Endurance":[3.35],"Agility":[3.95],"Intelligence":[0,2.78],"Wisdom":[0,2.78]},

	// Reinforced Leather Gauntlets of Deflection [Piercing]
	"Leather Helmet" : {"Attack Speed":[0,3.69],"Physical Mitigation":[8.12,11.17],"Magical Mitigation":[6.67,11.97],"Evade Chance":[2.54],"Resist Chance":[10.59],"Burden":[3.5],"Interference":[7],"Crushing":[7.93,11.03,14.91,18.01],"Slashing":[7.83,10.89,14.71,17.77],"Piercing":[3.93,6.93,10.68,13.68],"Fire MIT":[0,26.17],"Cold MIT":[0,26.17],"Elec MIT":[0,26.17],"Wind MIT":[0,26.17],"Holy MIT":[0,26.17],"Dark MIT":[0,26.17],"Strength":[4.85],"Dexterity":[4.85],"Endurance":[4.1],"Agility":[4.1]},
	"Leather Breastplate" : {"Attack Speed":[0,4.31],"Physical Mitigation":[9.7,13.35],"Magical Mitigation":[7.95,14.33],"Evade Chance":[2.99],"Resist Chance":[12.52],"Burden":[4.2],"Interference":[8.4],"Crushing":[9.48,13.2,17.85,21.57],"Slashing":[9.36,13.03,17.62,21.29],"Piercing":[4.68,8.28,12.78,16.38],"Fire MIT":[0,31.17],"Cold MIT":[0,31.17],"Elec MIT":[0,31.17],"Wind MIT":[0,31.17],"Holy MIT":[0,31.17],"Dark MIT":[0,31.17],"Strength":[5.75],"Dexterity":[5.75],"Endurance":[4.85],"Agility":[4.85]},
	"Leather Gauntlets" : {"Attack Speed":[0,3.4],"Physical Mitigation":[7.34,10.09],"Magical Mitigation":[6.02,10.81],"Evade Chance":[2.32],"Resist Chance":[9.62],"Burden":[3.15],"Interference":[6.3],"Crushing":[7.16,9.95,13.43,16.22],"Slashing":[7.06,9.82,13.26,16.01],"Piercing":[3.55,6.25,9.63,"12.33"],"Fire MIT":[0,23.67],"Cold MIT":[0,23.67],"Elec MIT":[0,23.67],"Wind MIT":[0,23.67],"Holy MIT":[0,23.67],"Dark MIT":[0,23.67],"Strength":[4.4],"Dexterity":[4.4],"Endurance":[3.74],"Agility":[3.74]},
	"Leather Leggings" : {"Attack Speed":[0,4.03],"Physical Mitigation":[8.92,12.28],"Magical Mitigation":[7.31,13.14],"Evade Chance":[2.77],"Resist Chance":[11.55],"Burden":[3.85],"Interference":[7.7],"Crushing":[8.71,12.12,16.38,19.79],"Slashing":[8.59,11.96,16.17,19.53],"Piercing":[4.3,7.6,11.73,15.03],"Fire MIT":[0,28.67],"Cold MIT":[0,28.67],"Elec MIT":[0,28.67],"Wind MIT":[0,28.67],"Holy MIT":[0,28.67],"Dark MIT":[0,28.67],"Strength":[5.3],"Dexterity":[5.3],"Endurance":[4.49],"Agility":[4.49]},
	"Leather Boots" : {"Attack Speed":[0,3.06],"Physical Mitigation":[6.55,8.98],"Magical Mitigation":[5.38,9.62],"Evade Chance":[2.09],"Resist Chance":[8.66],"Burden":[2.8],"Interference":[5.6],"Crushing":[6.38,8.86,11.96,14.44],"Slashing":[6.3,8.75,11.81,14.26],"Piercing":[3.18,5.58,8.58,10.98],"Fire MIT":[0,21.17],"Cold MIT":[0,21.17],"Elec MIT":[0,21.17],"Wind MIT":[0,21.17],"Holy MIT":[0,21.17],"Dark MIT":[0,21.17],"Strength":[3.95],"Dexterity":[3.95],"Endurance":[3.35],"Agility":[3.35]},

	"Power Helmet" : {"Attack Damage":[18.04,25.73],"Attack Accuracy":[6.15,21.02],"Attack Crit Chance":[1.43,5.68],"Attack Crit Damage":[1.36,4.36],"Physical Mitigation":[8.11,11.16],"Magical Mitigation":[6.26,11.48],"Burden":[10.5,7.91],"Interference":[17.5],"Crushing":[4.82],"Slashing":[7.82],"Piercing":[7.67],"Fire MIT":[0,26.13],"Cold MIT":[0,26.13],"Elec MIT":[0,26.13],"Wind MIT":[0,26.13],"Holy MIT":[0,26.13],"Dark MIT":[0,26.13],"Strength":[7.09],"Dexterity":[6.34],"Endurance":[4.84]},
	"Power Armor" : {"Attack Damage":[21.46,30.68],"Attack Accuracy":[7.24,25.09],"Attack Crit Chance":[1.69,6.8],"Attack Crit Damage":[1.61,5.21],"Physical Mitigation":[9.69,13.34],"Magical Mitigation":[7.46,13.73],"Burden":[12.6,9.45],"Interference":[21],"Crushing":[5.75],"Slashing":[9.35],"Piercing":[9.17],"Fire MIT":[0,31.13],"Cold MIT":[0,31.13],"Elec MIT":[0,31.13],"Wind MIT":[0,31.13],"Holy MIT":[0,31.13],"Dark MIT":[0,31.13],"Strength":[8.44],"Dexterity":[7.54],"Endurance":[5.74]},
	"Power Gauntlets" : {"Attack Damage":[16.33,23.25],"Attack Accuracy":[5.6,19.02],"Attack Crit Chance":[1.3,5.14],"Attack Crit Damage":[1.24,3.94],"Physical Mitigation":[7.33,10.09],"Magical Mitigation":[5.65,10.36],"Burden":[9.45,7.07],"Interference":[15.75],"Crushing":[4.36],"Slashing":[7.06],"Piercing":[6.92],"Fire MIT":[0,23.63],"Cold MIT":[0,23.63],"Elec MIT":[0,23.63],"Wind MIT":[0,23.63],"Holy MIT":[0,23.63],"Dark MIT":[0,23.63],"Strength":[6.43],"Dexterity":[5.74],"Endurance":[4.39]},
	"Power Leggings" : {"Attack Damage":[19.75,28.20],"Attack Accuracy":[6.69,23.08],"Attack Crit Chance":[1.57,6.25],"Attack Crit Damage":[1.49,4.79],"Physical Mitigation":[8.91,12.27],"Magical Mitigation":[6.86,12.61],"Burden":[11.55,8.68],"Interference":[19.25],"Crushing":[5.29],"Slashing":[8.59],"Piercing":[8.42],"Fire MIT":[0,28.63],"Cold MIT":[0,28.63],"Elec MIT":[0,28.63],"Wind MIT":[0,28.63],"Holy MIT":[0,28.63],"Dark MIT":[0,28.63],"Strength":[7.78],"Dexterity":[6.94],"Endurance":[5.29]},
	"Power Boots" : {"Attack Damage":[14.62,20.77],"Attack Accuracy":[5.05,16.95],"Attack Crit Chance":[1.17,4.57],"Attack Crit Damage":[1.11,3.51],"Physical Mitigation":[6.54,8.97],"Magical Mitigation":[5.05,9.23],"Burden":[8.4,6.3],"Interference":[14],"Crushing":[3.89],"Slashing":[6.29],"Piercing":[6.17],"Fire MIT":[0,21.13],"Cold MIT":[0,21.13],"Elec MIT":[0,21.13],"Wind MIT":[0,21.13],"Holy MIT":[0,21.13],"Dark MIT":[0,21.13],"Strength":[5.74],"Dexterity":[5.14],"Endurance":[3.94]},

	"Plate Helmet" : {"Physical Mitigation":[10.73,14.3],"Magical Mitigation":[7.76,13.29],"Block Chance":[0,6.09],"Burden":[14,10.5],"Interference":[14],"Crushing":[5.98,12.96],"Slashing":[9.73,16.62],"Piercing":[9.54,16.29],"Fire MIT":[0,26.11],"Cold MIT":[0,26.11],"Elec MIT":[0,26.11],"Wind MIT":[0,26.11],"Holy MIT":[0,26.11],"Dark MIT":[0,26.11],"Strength":[4.83],"Dexterity":[4.83],"Endurance":[6.33]},
	"Plate Cuirass" : {"Physical Mitigation":[12.83,17.12],"Magical Mitigation":[9.27,15.9],"Block Chance":[0,7.09],"Burden":[16.8,12.6],"Interference":[16.8],"Crushing":[7.15,15.52],"Slashing":[11.64,19.91],"Piercing":[11.42,19.52],"Fire MIT":[0,31.11],"Cold MIT":[0,31.11],"Elec MIT":[0,31.11],"Wind MIT":[0,31.11],"Holy MIT":[0,31.11],"Dark MIT":[0,31.11],"Strength":[5.73],"Dexterity":[5.73],"Endurance":[7.53]},
	"Plate Gauntlets" : {"Physical Mitigation":[9.68,12.9],"Magical Mitigation":[7.02,12],"Block Chance":[0,5.59],"Burden":[12.6,9.45],"Interference":[12.6],"Crushing":[5.41,11.69],"Slashing":[8.78,14.98],"Piercing":[8.61,14.69],"Fire MIT":[0,23.61],"Cold MIT":[0,23.61],"Elec MIT":[0,23.61],"Wind MIT":[0,23.61],"Holy MIT":[0,23.61],"Dark MIT":[0,23.61],"Strength":[4.38],"Dexterity":[4.38],"Endurance":[5.73]},
	"Plate Greaves" : {"Physical Mitigation":[11.78,15.71],"Magical Mitigation":[8.52,14.61],"Block Chance":[0,6.59],"Burden":[15.4,11.55],"Interference":[15.4],"Crushing":[6.57,14.25],"Slashing":[10.7,18.27],"Piercing":[10.49,17.91],"Fire MIT":[0,28.61],"Cold MIT":[0,28.61],"Elec MIT":[0,28.61],"Wind MIT":[0,28.61],"Holy MIT":[0,28.61],"Dark MIT":[0,28.61],"Strength":[5.28],"Dexterity":[5.28],"Endurance":[6.93]},
	"Plate Sabatons" : {"Physical Mitigation":[8.63,11.49],"Magical Mitigation":[6.25,10.67],"Block Chance":[0,5.09],"Burden":[11.2,8.4],"Interference":[11.2],"Crushing":[4.82,10.4],"Slashing":[7.82,13.33],"Piercing":[7.67,13.07],"Fire MIT":[0,21.11],"Cold MIT":[0,21.11],"Elec MIT":[0,21.11],"Wind MIT":[0,21.11],"Holy MIT":[0,21.11],"Dark MIT":[0,21.11],"Strength":[3.93],"Dexterity":[3.93],"Endurance":[5.13]}
};

$equip.ppxp = {
	"Axe":375, "Club":375, "Rapier":377, "Shortsword":377, "Wakizashi":378,
	"Estoc":377, "Katana":375, "Longsword":375, "Mace":375,
	"Katalox Staff":368, "Oak Staff":371, "Redwood Staff":371, "Willow Staff":371,
	"Buckler":374, "Force Shield":374, "Kite Shield":374,
	"Phase":377, "Cotton":377,
	"Arcanist":421, "Shade":394, "Leather":393,
	"Power":382, " Plate":377,
};

$equip.parse = {

name : function(name,eq) {
	eq = eq || {info:{},data:{},node:{}};
	if(!eq.info.name) {
		eq.info.name = name;
	}

	var exec = $equip.reg.name.exec(name);
	if(exec) {
		if(!eq.info.category) {
			eq.info.category = exec[4]?"One-handed Weapon" : exec[5]?"Two-handed Weapon" : exec[6]?"Staff" : exec[7]?"Shield" : exec[8]?"Cloth Armor" : exec[9]?"Light Armor" : exec[10]?"Heavy Armor" : "Obsolete";
		}
		eq.info.quality = exec[1];
		eq.info.prefix = exec[2] || exec[3];
		eq.info.type = exec[4] || exec[5] || exec[6] || exec[7] || exec[8] || exec[9] || exec[10];
		eq.info.slot = exec[11];
		eq.info.suffix = exec[12];

	} else if(!eq.info.category) {
		eq.info.category = "Obsolete";
	}

	return eq;
},

div : function(div) {
	var eid = /equips\.set\((\d+),/.test(div.getAttribute("onmouseover")) && RegExp.$1;
	if(!eid) {
		return {error:"invalid div"};
	}
	var dynjs = $equip.dynjs_equip[eid] || $equip.dynjs_eqstore[eid] || $equip.dynjs_loaded[eid];
	if(!dynjs) {
		return {error:"no data"};
	}
	var exec = $equip.reg.html.exec(dynjs.d);
	if(!exec) {
		return {error:"parse error"};
	}

	var eq = {
		info : {
			name : dynjs.t,
			category : exec[1],
			level : parseInt(exec[2])||0,
			unassigned : exec[2]==="Unassigned",
			tradeable : exec[3]==="Tradeable",
			soulbound : exec[3]==="Soulbound",
			cdt : exec[4]/exec[5],
			condition : parseInt(exec[4]),
			durability : parseInt(exec[5]),
			tier : parseInt(exec[6]),
			pxp1 : parseInt(exec[7]),
			pxp2 : parseInt(exec[8]),
			pab : (dynjs.d.match($equip.reg.pab)||[]).map(p=>p[0]).join(""),
			eid : eid,
			key : dynjs.k
		},
		data : {
			html : dynjs.d,
			value : $equip.eqvalue[eid]
		},
		node : {
			div : div
		}
	};

	var equipname = $equip.names[eq.info.eid];
	if(equipname && equipname !== eq.info.name) {
		eq.info.customname = eq.info.name;
		eq.info.name = equipname;
	}
	$equip.parse.name(eq.info.name,eq);

	eq.info.pxp = $equip.getpxp(eq);
	div.dataset.eid = eq.info.eid;
	div.dataset.key = eq.info.key;
	return eq;
},

extended : function(extended) {
	var stats_div = extended.children[0],
		exec = $equip.reg.html.exec( stats_div.children[0].outerHTML+stats_div.children[1].outerHTML );

	var eq = {
		info:{
			category : exec[1],
			level : parseInt(exec[2])||0,
			unassigned : exec[2]==="Unassigned",
			tradeable : exec[3]==="Tradeable",
			soulbound : exec[3]==="Soulbound",
			cdt : exec[4]/exec[5],
			condition : parseInt(exec[4]),
			durability : parseInt(exec[5]),
			tier : parseInt(exec[6]),
			pxp1 : parseInt(exec[7]),
			pxp2 : parseInt(exec[8]),
		},
		data:{
		},
		node:{
			extended : extended
		}
	};

	var name_div = extended.previousElementSibling;
	if(!name_div.textContent.trim()) { // "RENAME" button in Forge/Upgrade
		name_div = name_div.previousElementSibling;
	}
	eq.info.name = Array.from(name_div.firstElementChild.children).map(d=>d.textContent.trim()||" ").join("").replace(/\b(Of|The)\b/,s=>s.toLowerCase());
	$equip.parse.name(eq.info.name,eq);

	var custom_div = name_div.previousElementSibling;
	if(custom_div) {
		eq.info.customname = Array.from(custom_div.firstElementChild.children).map(d=>d.textContent.trim()||" ").join("");
	}

	eq.info.pxp = $equip.getpxp(eq);
	$equip.parse.stats(eq);
	$equip.parse.upgrades(eq);
	return eq;
},

stats : function(eq) {
	var div;
	if(eq.node.extended) {
		div = eq.node.extended.children[0];
	} else if(eq.data.html) {
		div = $element("template",null,["/"+eq.data.html]).content.firstElementChild;
	} else {
		console.log("No Equipment Data");
		return;
	}
	eq.stats = {};

	var level = eq.info.level || _player.level,
		magics = ["Fire","Cold","Elec","Wind","Holy","Dark"];
	Array.from(div.children).forEach(function(child){
		if(child.classList.contains("ex")) {
			Array.from(child.children).forEach(function(div) {
				if(!div.childElementCount) {
					return;
				}
				let name = div.firstElementChild.textContent,
					span = div.children[1].firstElementChild,
					value = parseFloat(span.textContent),
					base = div.title ? parseFloat(div.title.slice(6)) : (value / (1 + level / $equip.stats[name].scale));
				eq.stats[name] = {value,base,span};
			});
		} else if(child.classList.contains("ep")) {
			let type = child.firstElementChild.textContent;
			Array.from(child.children).slice(1).forEach(function(div){
				let text = div.firstChild.nodeValue.slice(0,-2),
					name = magics.includes(text) ? text + (type==="Damage Mitigations"?" MIT":" EDB") : text,
					span = div.firstElementChild,
					value = parseFloat(span.textContent),
					base = div.title ? parseFloat(div.title.slice(6)) : (value / (1 + level / $equip.stats[name].scale));
				eq.stats[name] = {value,base,span};
			});
		} else if(/\+(\d+) (.+) Damage/.test(child.textContent)) {
			let name = "Attack Damage",
				span = child.firstElementChild,
				value = parseFloat(RegExp.$1),
				base = child.title ? parseFloat(child.title.slice(6)) : (value / (1 + level / $equip.stats[name].scale));
			eq.stats[name] = {value,base,span};
		}
	});
	//$equip.parse.pmax(eq);
	//console.log(eq.stats);
},

pmax : function(eq) {
	var pmax = (eq.info.slot ? $equip.pmax[eq.info.type+" "+eq.info.slot] : $equip.pmax[eq.info.type]) || {},
		pmax_offset = {};
	if($equip.prefix[eq.info.prefix]) {
		$equip.prefix[eq.info.prefix].forEach(function(e){
			if(!pmax_offset[e]) {
				pmax_offset[e] = 0;
			}
			pmax_offset[e]++;
		});
	}
	if($equip.suffix[eq.info.suffix]) {
		$equip.suffix[eq.info.suffix].forEach(function(e){
			if(!pmax_offset[e]) {
				pmax_offset[e] = 0;
			}
			pmax_offset[e]++;
		});
	}
	if(eq.info.type === "Leather" || eq.info.type === "Kite Shield") {
		if( ["Dampening","Stoneskin","Deflection"].includes(eq.info.suffix) ) {
			$equip.suffix[eq.info.suffix].forEach(function(e){
				pmax_offset[e]++;
			});
		}
	}
	if(eq.info.category === "Staff") {
		if( ["Surtr","Niflheim","Mjolnir","Freyr","Heimdall","Fenrir"].includes(eq.info.suffix) ) {
			$equip.suffix[eq.info.suffix].forEach(function(e){
				pmax_offset[e]++;
			});
		}
	}
	Object.entries(eq.stats).forEach(function([name,stat]){
		stat.pmax = pmax[name] ? pmax[name][pmax_offset[name]||0] : null;
	});
},

upgrades : function(eq) {
	var div = eq.node.extended.children[1];
	eq.upgrades = {};
	$qsa("#eu>span",div).forEach(function(span){
		if(/(.+) Lv\.(\d+)/.test(span.textContent)) {
			eq.upgrades[RegExp.$1] = parseFloat(RegExp.$2);
		}
	});
	$qsa("#ep>span",div).forEach(function(span){
		if(/(.+) Lv\.(\d+)/.test(span.textContent)) {
			eq.upgrades[RegExp.$1] = parseFloat(RegExp.$2);
		} else {
			eq.upgrades[span.textContent] = true;
		}
	});
	$qsa("#ee>span",div).forEach(function(span){
		if(/(.+) \[(\d+)m\]/.test(span.textContent)) {
			eq.upgrades[RegExp.$1] = parseFloat(RegExp.$2);
		}
	});
	Object.entries(eq.stats).forEach(function([name,stat]){
		stat.unforged = $equip.unforge(name,stat.base,eq.upgrades,eq.info.pxp,eq.info.level||_player.level,eq.upgrades);
	});
},

};

$equip.unforge = function(name,base,upgrade,pxp,level,iw) {
	var stat = $equip.stats[name];
	upgrade = (upgrade && typeof upgrade==="object" ? upgrade[stat.forge] : upgrade) || 0;
	iw = (iw && typeof iw==="object" ? iw[stat.potency] : iw) || 0;
	var iwcoeff = 1,
		iwbonus = 0;
	if(iw) {
		if(name==="Attack Damage" || name==="Magic Damage") {
			iwcoeff += iw * stat.plus;
		} else {
			iwbonus += iw * stat.plus;
		}
	}
	var fluc = stat.fluc,
		bonus = ((pxp - 100) / 25) * fluc,
		factor = (name==="Attack Damage" || name==="Magic Damage") ? 0.279575 : 0.2,
		coeff = 1 + factor * Math.log(0.1 * upgrade + 1),
		value = (base - bonus - iwbonus) /  coeff / iwcoeff + bonus; // this base is a forged base
	return value;
};

$equip.forge = function(name,base,upgrade,pxp,level,iw) {
	var stat = $equip.stats[name];
	upgrade = (upgrade && typeof upgrade==="object" ? upgrade[stat.forge] : upgrade) || 0;
	iw = (iw && typeof iw==="object" ? iw[stat.potency] : iw) || 0;
	var iwcoeff = 1,
		iwbonus = 0;
	if(iw) {
		if(name==="Attack Damage" || name==="Magic Damage") {
			iwcoeff += iw * stat.plus;
		} else {
			iwbonus += iw * stat.plus;
		}
	}
	var fluc = stat.fluc,
		bonus = ((pxp - 100) / 25) * fluc,
		factor = (name==="Attack Damage" || name==="Magic Damage") ? 0.279575 : 0.2,
		coeff = 1 + factor * Math.log(0.1 * upgrade + 1),
		forged = (base - bonus) * coeff * iwcoeff + bonus + iwbonus,
		scale = stat.scale,
		value = forged * (1 + level / scale);
	return value;
};

$equip.getpxp = function(eq) {
	var pxp;
	if(eq.info.tier === 0) {
		pxp = eq.info.pxp2;
	} else if(eq.info.tier === 10) {
		var ppxp = (Object.entries($equip.ppxp).find(([n])=>eq.info.name.includes(n))||[null,400])[1];
		if(eq.info.quality === "Peerless") {
			pxp = ppxp;
		} else if(eq.info.quality === "Legendary") {
			pxp = Math.round(ppxp * 0.95);
		} else if(eq.info.quality === "Magnificent") {
			pxp = Math.round(ppxp * 0.89);
		} else {
			pxp = Math.round(ppxp * 0.8);
		}
	} else {
		pxp = $equip.calcpxp(eq.info.pxp2,eq.info.tier);
	}
	return pxp;
};

$equip.calcpxp = function(pxpN,n) {
	let pxp0Est = 300;
	for (let i = 1; i < 15; i++){
		const sumPxpNextLevel = Math.ceil(1000 * (Math.pow(1 + pxp0Est / 1000, n + 1) - 1));
		const sumPxpThisLevel = Math.ceil(1000 * (Math.pow(1 + pxp0Est / 1000, n) - 1));
		const estimate = sumPxpNextLevel - sumPxpThisLevel;
		if (estimate > pxpN)
			pxp0Est -= 300 / Math.pow(2, i);
		else
			pxp0Est += 300 / Math.pow(2, i);
	}
	return Math.ceil(pxp0Est);
};

$equip.filter = function(name,filter) {
	if(!filter) {
		return false;
	}
	var n = name.toLowerCase();
	return filter.some(function(f){
		var c = f.toLowerCase().replace(/[a-z\- ]+/g,function(s){s=s.trim();return !s?"":n.includes(s);});
		try {
			return eval(c);
		} catch(e) {
			alert("Invalid Equip Filter: "+f);
			return false;
		}
	});
};

$equip.list = function(node,sort,append) {

if(!node) {
	return;
}

var list = Array.from($qsa("div[onmouseover*='equips.set']",node)).map(function(div){
	var eq = $equip.parse.div(div);
	eq.node.wrapper = div.parentNode;

	if(eq.info.customname) {
		div.classList.add("hvut-eq-name");
		div.addEventListener("mouseenter",function(){div.textContent=eq.info.name;});
		div.addEventListener("mouseleave",function(){div.textContent=eq.info.customname;});
	}
	div.classList.add("hvut-eq-"+eq.info.quality);

	return eq;
});

if(settings.equipSort && sort) {

	list.sort(function(a,b){
		var sorter = $equip.sort;
		if(a.info.category !== b.info.category) {
			return sorter.category[a.info.category] - sorter.category[b.info.category];
		} else if(a.info.category === "Obsolete") {
			return a.info.name>b.info.name?1 : a.info.name<b.info.name?-1 : 0;
		} else if(a.info.type !== b.info.type) {
			return (sorter.type[a.info.type]||99) - (sorter.type[b.info.type]||99);
		}

		var sortkey =
			a.info.category==="One-handed Weapon" || a.info.category==="Two-handed Weapon" ? ["suffix","quality","prefix"] :
			a.info.category==="Staff" ? ["prefix","suffix","quality"] :
			a.info.type==="Buckler" ? ["suffix","quality","prefix"] :
			a.info.category==="Shield" ? ["quality","suffix","prefix"] :
			a.info.category==="Cloth Armor" ? ["suffix","slot","quality","prefix"] :
											["slot","suffix","quality","prefix"];

		var r = 0;
		sortkey.some(function(e){
			if(e in sorter) {
				r = (sorter[e][a.info[e]]||99) - (sorter[e][b.info[e]]||99);
			} else {
				r = a.info[e]>b.info[e]?1 : a.info[e]<b.info[e]?-1 : 0;
			}

			if(r) {
				return true;
			} else {
				return false;
			}
		});

		return r || (b.info.eid-a.info.eid);
	});

	var frag = $element();
	list.forEach(function(e,i,a){
		var p = a[i-1] || {info:{}};
		if(e.info.category !== p.info.category && sort === 2) {
			$element("h4",frag,[" "+e.info.category,".hvut-eq-category"]);
		}

		switch(e.info.category) {

		case "Obsolete":
			break;

		case "One-handed Weapon":
		case "Two-handed Weapon":
			if(e.info.type !== p.info.type) {
				$element("h5",frag,[" "+e.info.type,".hvut-eq-type"]);
			} else if(e.info.suffix !== p.info.suffix) {
				e.node.wrapper.classList.add("hvut-eq-border");
			}
			break;

		case "Staff":
			if(e.info.type !== p.info.type) {
				$element("h5",frag,[" "+e.info.type,".hvut-eq-type"]);
			} else if(e.info.prefix !== p.info.prefix) {
				e.node.wrapper.classList.add("hvut-eq-border");
			}
			break;

		case "Shield":
			if(e.info.type !== p.info.type) {
				$element("h5",frag,[" "+e.info.type,".hvut-eq-type"]);
			} else if(e.info.suffix !== p.info.suffix && e.info.type === "Buckler") {
				e.node.wrapper.classList.add("hvut-eq-border");
			}
			break;

		case "Cloth Armor":
			if(e.info.type !== p.info.type || e.info.suffix !== p.info.suffix) {
				$element("h5",frag,[" "+(e.info.suffix||"*"),".hvut-eq-type"]);
			} else if(e.info.slot !== p.info.slot) {
				e.node.wrapper.classList.add("hvut-eq-border");
			}
			break;

		case "Light Armor":
		case "Heavy Armor":
			if(e.info.type !== p.info.type || e.info.slot !== p.info.slot) {
				$element("h5",frag,[" "+e.info.type+" "+e.info.slot,".hvut-eq-type"]);
			} else if(e.info.suffix !== p.info.suffix && (e.info.type === "Shade" || e.info.type === "Power")) {
				e.node.wrapper.classList.add("hvut-eq-border");
			}
			break;

		}

		frag.appendChild(e.node.wrapper);
	});

	if(!append) {
		append = node;
		if(!append.classList.contains("equiplist")) {
			append = $qs(".equiplist",append);
		}
	}
	append.innerHTML = "";
	append.appendChild(frag);
}

return list;

};

$equip.bbcode = function(eq) {
	function rainbow(text) {
		var c = ["#f00","#f90","#fc0","#0c0","#09f","#00c","#c0f"], l = c.length;
		return text.split("").map((t,i)=>"[color="+c[i%l]+"]"+t+"[/color]").join("");
	}

	var all = {},
		quality = {code:eq.info.quality},
		prefix = {code:eq.info.prefix},
		type = {code:eq.info.type},
		slot = {code:eq.info.slot},
		suffix = {code:eq.info.suffix},
		color1 = "#f00",
		color2 = "#f90";

	switch(eq.info.category) {

	case "One-handed Weapon":
		if(eq.info.type==="Rapier" && eq.info.suffix==="Slaughter") {
			prefix.color = ({"Ethereal":color1,"Hallowed":color1,"Demonic":color1})[eq.info.prefix];
			type.bold = true;
			suffix.bold = true;
		} else if((eq.info.type==="Rapier" || eq.info.type==="Wakizashi") && (eq.info.suffix==="the Nimble" || eq.info.suffix==="Balance")) {
			prefix.color = ({"Ethereal":color1})[eq.info.prefix];
			type.bold = true;
		} else if((eq.info.type==="Club" || eq.info.type==="Shortsword" || eq.info.type==="Axe") && eq.info.suffix==="Slaughter") {
			prefix.color = ({"Ethereal":color1})[eq.info.prefix];
			type.bold = true;
			suffix.bold = true;
		}
		break;

	case "Two-handed Weapon":
		if(eq.info.suffix==="Slaughter") {
			prefix.color = ({"Ethereal":color1})[eq.info.prefix];
			type.bold = true;
			suffix.bold = true;
		}
		break;

	case "Staff":

		switch(eq.info.type) {

		case "Oak Staff":
			if(eq.info.prefix==="Hallowed" && eq.info.suffix==="Heimdall") {
				prefix.bold = true;
				type.bold = true;
				suffix.bold = true;
			}
			break;

		case "Willow Staff":
			if(["Shocking","Tempestuous","Demonic"].includes(eq.info.prefix) && eq.info.suffix==="Destruction") {
				prefix.bold = true;
				type.bold = true;
				suffix.bold = true;
			}
			break;

		case "Katalox Staff": {
			let pre = ({"Hallowed":5,"Demonic":6})[eq.info.prefix],
				suf = ({"Destruction":-1,"Heimdall":5,"Fenrir":6,"the Heaven-sent":8,"the Demon-fiend":9})[eq.info.suffix];
			if(pre && suf) {
				if(suf === -1) {
					prefix.bold = true;
					type.bold = true;
					suffix.bold = true;
				} else if(suf === pre) {
					prefix.bold = true;
					type.bold = true;
				} else if(suf === pre+3) {
					prefix.bold = true;
					type.bold = true;
				}
			}
			break;
		}
		case "Redwood Staff": {
			let pre = ({"Fiery":1,"Arctic":2,"Shocking":3,"Tempestuous":4})[eq.info.prefix],
				suf = ({"Destruction":-1,"Surtr":1,"Niflheim":2,"Mjolnir":3,"Freyr":4,"the Elementalist":7})[eq.info.suffix];
			if(pre && suf) {
				if(suf === -1) {
					prefix.bold = true;
					type.bold = true;
					suffix.bold = true;
				} else if(suf === pre) {
					prefix.bold = true;
					type.bold = true;
				} else if(suf === 7) {
					prefix.bold = true;
					type.bold = true;
				}
			}
			break;
		}

		}

		break;

	case "Shield":
		if(eq.info.type === "Force Shield") {
			type.bold = true;
			suffix.bold = ["Protection","Dampening","Deflection"].includes(eq.info.suffix);
		} else if(eq.info.type==="Buckler" && ["the Barrier","the Battlecaster"].includes(eq.info.suffix)) {
			prefix.color = ({"Reinforced":color1})[eq.info.prefix];
			type.bold = true;
			suffix.bold = true;
		}
		break;

	case "Cloth Armor":
		if(eq.info.type === "Phase") {
			prefix.color = ({"Radiant":color1,"Charged":color1,"Mystic":color2,"Frugal":color2})[eq.info.prefix];
			type.bold = true;
		} else if(eq.info.type === "Cotton" && ["the Elementalist","the Heaven-sent","the Demon-fiend"].includes(eq.info.suffix)) {
			prefix.color = ({"Charged":color1,"Frugal":color2})[eq.info.prefix];
			suffix.bold = true;
		}
		break;

	case "Light Armor":
		if(eq.info.type === "Shade") {
			prefix.color = ({"Savage":color1,"Agile":color2})[eq.info.prefix];
			type.bold = true;
			suffix.bold = eq.info.suffix === "the Shadowdancer";
		} else if(eq.info.type === "Leather") {
			prefix.color = ({"Reinforced":color1})[eq.info.prefix];
		}
		break;

	case "Heavy Armor":
		if(eq.info.type === "Power") {
			prefix.color = ({"Savage":color1})[eq.info.prefix];
			type.bold = true;
			suffix.bold = eq.info.suffix === "Slaughter";
		} else if(eq.info.type==="Plate") {
			prefix.color = ({"Shielding":color1})[eq.info.prefix];
			suffix.bold = eq.info.suffix === "Protection";
		}
		break;

	default:
		all.code = eq.info.name;

	}

	if(eq.info.quality === "Peerless") {quality.code = rainbow(quality.code);quality.bold = true;}
	else if(eq.info.quality === "Legendary") {quality.bold = true;}

	if(quality.color) {quality.code = "[color="+quality.color+"]"+quality.code+"[/color]";}
	if(quality.bold) {quality.code = "[b]"+quality.code+"[/b]";}

	if(prefix.color) {prefix.code = "[color="+prefix.color+"]"+prefix.code+"[/color]";}
	if(prefix.bold) {prefix.code = "[b]"+prefix.code+"[/b]";}

	if(type.color) {type.code = "[color="+type.color+"]"+type.code+"[/color]";}
	if(type.bold) {type.code = "[b]"+type.code+"[/b]";}

	if(slot.color) {slot.code = "[color="+slot.color+"]"+slot.code+"[/color]";}
	if(slot.bold) {slot.code = "[b]"+slot.code+"[/b]";}

	if(suffix.color) {suffix.code = "[color="+suffix.color+"]"+suffix.code+"[/color]";}
	if(suffix.bold) {suffix.code = "[b]"+suffix.code+"[/b]";}

	all.code = all.code || quality.code + (prefix.code?" "+prefix.code:"") + " " + type.code + (slot.code?" "+slot.code:"") + (suffix.code?" of "+suffix.code:"");

	if(all.color) {all.code = "[color="+all.color+"]"+all.code+"[/color]";}
	if(all.bold) {all.code = "[b]"+all.code+"[/b]";}

	return all.code;
};
/***** [MODULE] Equip Parser *****/


/***** [MODULE] Item Price *****/
var $prices = {

json : null,
desc : {
	"WTS": " price is used for calculating the CoD when sending through [MoogleMail]",
	"WTB": " price is used for calculating the CoD on [MoogleMail] that you receive",
	"Materials": " price is used for calculating the profits in [Monster Lab], the total cost of upgrading equipment in [Upgrade] and what salvaging in [Equipment Shop] is worth",
},
init : function() {
	if($prices.json) {
		return;
	}
	$prices.keys = Object.keys(settings.itemPrice);
	$prices.json = Object.assign(JSON.parse(JSON.stringify(settings.itemPrice)),getValue("prices"));
},
get : function(tag) {
	$prices.init();
	if($prices.keys.includes(tag)) {
		return $prices.json[tag];
	} else {
		alert("Invalid Prices: ["+tag+"]");
		return {};
	}
},
set : function(tag,json) {
	$prices.init();
	$prices.json[tag] = json;
	Object.keys($prices.json).forEach(function(k){
		if(!$prices.keys.includes(k)) {
			delete $prices.json[k];
		}
	});
	setValue("prices",$prices.json);
},
edit : function(tag,callback) {
	$prices.init();
	if(!tag) {
		return;
	}
	var prev = $prices.get(tag);
	popup_text($prices.json2str(tag),"width:300px;height:500px",
	[{value:"Save",click:function(w,t){
		var json = $prices.str2json(t.value),
			error = json["\nERROR"];
		if(error) {
			alert("!!! ERROR\n\n"+error);
			return;
		}
		$prices.set(tag,json);
		w.remove();
		if(callback && JSON.stringify(prev)!==JSON.stringify(json)) {
			callback();
		}
	}},
	{value:"Default",click:function(w,t){
		var a = ["// ["+tag+"]"+($prices.desc[tag]||"")+"\n"];
		Object.entries(settings.itemPrice[tag]).forEach(function([n,p]){
			a.push(n+" @ "+p);
		});
		t.value = a.join("\n");
	}}]);
},
selector : function(size) {
	$prices.init();
	var selector = $element("select",null,null,{change:function(){selector.blur();$prices.edit(selector.value);}});
	if(size) {
		selector.size = $prices.keys.length + 1;
		selector.classList.add("hvut-scrollbar-none");
	}
	$element("option",selector,{text:"[Edit Price]",value:""});
	$prices.keys.forEach(function(k){
		$element("option",selector,{text:k,value:k});
	});
	return selector;
},
str2json : function(str) {
	var json = {};
	str.split("\n").some(function(s,i){
		s = s.trim();
		if(!s || s.startsWith("//")) {
			return;
		}
		if(/^(.+?)\s*@\s*(\d+)$/.test(s)) {
			json[RegExp.$1] = parseInt(RegExp.$2);
		} else {
			json["\nERROR"] = "#"+(i+1)+": "+s;
			return true;
		}
	});
	return json;
},
json2str : function(tag) {
	var a = ["// ["+tag+"]"+($prices.desc[tag]||"")+"\n"];
	Object.entries($prices.get(tag)).forEach(function([n,p]){
		a.push(n+" @ "+p);
	});
	return a.join("\n");
}

};
/***** [MODULE] Item Price *****/


// Player Data
var _player = {
	stamina : /Stamina: (\d+)/.test($id("stamina_readout").textContent) && parseInt(RegExp.$1),
	accuracy : $qs("#stamina_readout > div:nth-child(2)").title,
	condition : $qs("#stamina_readout img[title^='Stamina']").title,
	dfct : /^(.+) Lv\.(\d+)/.test($id("level_readout").textContent.trim()) && RegExp.$1,
	level : parseInt(RegExp.$2),
	warn : [],
};

if(isNaN(_player.level)) { // check font settings
	alert("To use HVUT, You need to set [Custom Font].");
	if(_query.ss === "se") {
		scrollIntoView($id("settings_cfont").parentNode,$id("settings_outer"));

		_se.form = $qs("#settings_outer form");
		_se.form.fontlocal.required = true;
		_se.form.fontface.required = true;
		_se.form.fontsize.required = true;
		_se.form.fontface.placeholder = "Tahoma, Arial";
		_se.form.fontsize.placeholder = "10";
		_se.form.fontoff.placeholder = "0";

	} else {
		location.href = "?s=Character&ss=se";
	}
	return;
}


// Basic CSS
GM_addStyle(`
	input, select, textarea {box-sizing:border-box; vertical-align:top}
	input[type='text'], input[type='number'] {font-size:9pt; margin:0 5px; padding:2px 5px; border-width:1px; line-height:16px}
	input[type='text'][readonly], input[type='number'][readonly] {color:#666}
	input[type='number'] {padding-right:1px}
	input[type='button'] {font-size:9pt; font-weight:bold; margin:0 5px; padding:1px 5px; border-width:2px; border-radius:5px; line-height:16px}
	input[type='checkbox'] {width:16px; height:16px; margin:0 2px; position:relative; top:0}
	select {font-size:9pt; margin:0 5px; padding:2px; border-width:1px; height:calc(4em/3 + 6px)}
	select[size] {height:auto}
	select[size] option:checked {background-color:revert; color:revert}
	option {font-size:inherit}
	textarea {margin:5px; padding:5px; font-size:10pt; line-height:20px}

	#mainpane {width:auto}
	.csps {visibility:hidden}
	.cspp {overflow-y:auto}
	.fc2, .fc4 {display:inline}
	.hvut-none {display:none !important}
	.hvut-hide-on .hvut-hide {display:none}
	.hvut-cphu, .hvut-cphu-sub > * {cursor:pointer}
	.hvut-cphu:hover, .hvut-cphu-sub > *:hover {text-decoration:underline}
	.hvut-spaceholder {flex-grow:1}

	.hvut-scrollbar-none {padding:0; scrollbar-width:none}
	.hvut-scrollbar-none::-webkit-scrollbar {display:none}
	.hvut-scrollbar-none option {margin:0; border:0; padding:3px}

	.equiplist {font-weight:normal}
	.eqp {margin:5px; width:auto}
	.eqp:hover {background-color:#ddd}
	.eqp > div:last-child {padding:1px 5px; line-height:20px; white-space:nowrap}
	.hvut-eq-name::before {content:'[ '}
	.hvut-eq-name::after {content:' ]'}
	.hvut-eq-category {margin:10px 0 5px; padding:2px 5px; border:2px solid; background-color:#edb; font-size:12pt; text-align:left}
	.hvut-eq-type {margin:10px 5px 5px; padding:2px 5px; border:1px solid; font-size:10pt}
	div + .hvut-eq-border {margin-top:11px}
	div + .hvut-eq-border::before {content:''; position:absolute; margin-top:-6px; width:100%; border-top:1px solid}

	.hvut-it-Consumable {color:#00B000}
	.hvut-it-Artifact {color:#0000FF}
	.hvut-it-Trophy {color:#461B7E}
	.hvut-it-Token {color:#254117}
	.hvut-it-Crystal {color:#BA05B4}
	.hvut-it-Monster_Food {color:#489EFF}
	.hvut-it-Material {color:#f00}
	.hvut-it-Collectable {color:#0000FF}
`);

if(settings.equipColor) {
	GM_addStyle(`
		.eqp > div:last-child:not([onclick]) {color:#966}
		.eqp > div:last-child[style*='color'] {box-shadow:0 0 0 2px inset}
		.hvut-eq-Superior {background-color:#ccc}
		.hvut-eq-Exquisite {background-color:#ce9}
		.hvut-eq-Magnificent {background-color:#bdf}
		.hvut-eq-Legendary {background-color:#fd8}
		.hvut-eq-Peerless {background-color:#fbb}
	`);
}

if($id("stats_pane")) {
	GM_addStyle(`
		#stats_header, #eqch_stats .csps {display:none}
		#stats_pane {height:650px !important; white-space:nowrap}
		.stats_page .spc {width:auto; padding:10px 0 0 10px}
		.stats_page .spc > .fal > div {font-weight:bold}
		.stats_page .far {color:#c00}
		.stats_page .st2 > div:nth-child(2n) {width:100px}
		.hvut-ch-expand #eqch_left {width:660px}
		.hvut-ch-expand #eqch_stats {width:560px}
		.hvut-ch-expand #stats_pane {overflow:hidden !important}
		.hvut-ch-expand .stats_page {float:left; height:100%; overflow:hidden}
		.hvut-ch-expand .stats_page:hover {overflow:visible}
		.hvut-ch-expand .stats_page:nth-of-type(1) {width:250px; border-right:1px dotted}
		.hvut-ch-expand .stats_page:nth-of-type(2) {width:300px; margin-left:3px}
		.hvut-ch-expand .st1 > div:nth-child(2n+1) {width:45px; padding-left:5px; clear:left}
		.hvut-ch-expand .st1 > div:nth-child(2n) {width:200px}
		.hvut-ch-expand .st2 > div:nth-child(2n+1) {width:45px; padding-left:5px}
		.hvut-ch-expand .st2 > div:nth-child(2n) {width:100px}
		.hvut-ch-expand .st3 > div:nth-child(2n+1) {width:45px; padding-left:5px}
		.hvut-ch-expand .st3 > div:nth-child(2n) {width:200px}
	`);

	$qs("#stats_pane > div:last-of-type").prepend( ...$qsa("#stats_pane > div:first-of-type > div:nth-last-of-type(-n+2)") );

	toggle_button( $element("input",$id("stats_pane"),{type:"button",style:"position:absolute;top:12px;right:20px;width:70px;padding:1px 0"}), "Collapse","Expand",$id("eqch_outer"),"hvut-ch-expand",settings.characterExpandStatsByDefault);
}


// Equipment Key Functions
if(settings.equipmentKeyFunctions) {
	document.addEventListener("keydown",function(e){
		if(e.target.nodeName==="INPUT" || e.target.nodeName==="TEXTAREA") {
			return;
		}
		var div = $qs("div[data-eid]:hover");
		if(div) {
			if(e.which === 67) { // C
				if(e.getModifierState("CapsLock")) {
					div.dispatchEvent(new MouseEvent("mouseover"));
					document.dispatchEvent(new KeyboardEvent("keypress",{which:99,keyCode:99}));
				}
			} else if(e.which === 76) { // L
				prompt("Forum Link:","[url="+location.origin+location.pathname+"equip/"+div.dataset.eid+"/"+div.dataset.key+"]"+div.textContent.replace(/(^\[\d+\] )|( \[[SDEAIW]+\]$)/g,"")+"[/url]");
			} else if(e.which === 86) { // V
				window.open("equip/"+div.dataset.eid+"/"+div.dataset.key,"_blank");
			}
		}
	});
}

// Equipment Mouse Functions
if(settings.equipmentMouseFunctions) {
	document.addEventListener("dblclick",function(){
		var div = $qs("div[data-eid]:hover");
		if(div) {
			window.open("equip/"+div.dataset.eid+"/"+div.dataset.key,"_blank");
		}
	});
}

/***** Top Menu *****/
GM_addStyle(`
	#navbar {display:none}

	#hvut-top {display:flex; position:relative; height:22px; padding:2px 0; border-bottom:1px solid; font-size:10pt; line-height:22px; font-weight:bold; z-index:10; white-space:nowrap; cursor:default}
	#hvut-top > div {position:relative; height:22px; margin:0 5px}
	#hvut-top a {text-decoration:none}

	.hvut-top-warn {background-color:#fd9}
	.hvut-top-message {position:absolute !important; top:100%; left:-1px; width:100%; margin:0 !important; padding:2px 0; border:1px solid #5C0D11; background-color:#fd9c; color:#e00; z-index:-1; pointer-events:none}

	.hvut-top-sub {display:none; position:absolute; top:22px; left:-6px; padding:5px; border-style:solid; border-width:0 1px 1px; background-color:#EDEBDF; opacity:0.95}
	div:hover > .hvut-top-sub {display:block}
	.hvut-top-sub select {display:block; margin:0}
	.hvut-top-stamina > div {width:220px; margin-top:3px; padding-top:3px; border-top:1px solid #5C0D11; white-space:normal}
	.hvut-top-stamina > div:first-child {border-top:none}
	.hvut-top-exp {position:relative; width:299px; height:8px; margin:0 auto; border:1px solid; background:linear-gradient(to right, #930 0, transparent 1px) repeat -1px 0 / 30px}
	.hvut-top-exp::before {content:''; position:absolute; top:0; left:0; width:100%; height:100%; background-color:#fff; z-index:-1}
	.hvut-top-exp > div {position:absolute; top:0; left:0; height:100%; background-color:#9cf; z-index:-1}
	.hvut-top-server {margin-left:auto !important}
	.hvut-top-server > span {color:#930}
	.hvut-top-server > div {left:auto; right:-6px}
	.hvut-top-server a {display:block}

	.hvut-top-menu {display:flex}
	.hvut-top-menu > div {position:relative; margin:0 5px}
	.hvut-top-menu span {font-size:12pt; color:#930}
	.hvut-top-menu .hvut-top-sub {width:max-content}
	.hvut-top-menu ul {float:left; margin:0 0 0 5px; padding:0; list-style:none; text-align:left; line-height:20px}
	.hvut-top-menu ul:first-child {margin-left:0}
	.hvut-top-menu a {display:block; margin:3px 0; padding:0 5px}
	.hvut-top-menu a:hover {background-color:#fff}
	.hvut-top-menu-s {padding:0 5px; background-color:#5C0D11; color:#fff}

	.hvut-top-quick {margin:0 15px !important}
	.hvut-top-quick > a {display:inline-block; position:relative; margin:0 1px; width:28px; font-size:12pt; border-radius:2px}
	.hvut-top-quick > a:hover {background-color:#fff}
	.hvut-top-quick > a::after {content:attr(data-desc); display:none; position:absolute; top:100%; left:0; margin-top:2px; margin-left:0; line-height:20px; padding:1px 4px; background-color:#fff; color:#930; border:1px solid; font-size:10pt; font-weight:normal; pointer-events:none}
	.hvut-top-quick > a:hover::after {display:block}

	.hvut-top-ygm {color:transparent !important; background:url("/y/mmail/ygm.png") no-repeat center center; animation:ygm 0.5s ease-in-out 10 alternate; filter:brightness(200%)}
	.hvut-top-ygm:hover {color:#e00 !important; background-image:none; animation:none; filter:none}
	@keyframes ygm { from {opacity:1} to {opacity:0.3} }
`);

_top.menu = {
	"Character" : {s:"Character",ss:"ch"},
	"Equipment" : {s:"Character",ss:"eq"},
	"Abilities" : {s:"Character",ss:"ab"},
	"Training" : {s:"Character",ss:"tr",isekai:false},
	"Item Inventory" : {s:"Character",ss:"it"},
	"Equip Inventory" : {s:"Character",ss:"in"},
	"Settings" : {s:"Character",ss:"se"},
	"Switch to Isekai" : {s:"Character",href:"/isekai/",isekai:false,text:"Pst"},
	"Switch to Persistent" : {s:"Character",href:"/",isekai:true,text:"Isk"},

	"Equipment Shop" : {s:"Bazaar",ss:"es"},
	"Item Shop" : {s:"Bazaar",ss:"is"},
	"Item Backorder" : {s:"Bazaar",ss:"ib",isekai:false},
	"Monster Lab" : {s:"Bazaar",ss:"ml",isekai:false},
	"The Shrine" : {s:"Bazaar",ss:"ss"},
	"The Market" : {s:"Bazaar",ss:"mk"},
	"MoogleMail" : {s:"Bazaar",ss:"mm"},
	"Weapon Lottery" : {s:"Bazaar",ss:"lt",isekai:false},
	"Armor Lottery" : {s:"Bazaar",ss:"la",isekai:false},

	"The Arena" : {s:"Battle",ss:"ar"},
	"The Tower" : {s:"Battle",ss:"tw",isekai:true},
	"Ring of Blood" : {s:"Battle",ss:"rb"},
	"GrindFest" : {s:"Battle",ss:"gr"},
	"Item World" : {s:"Battle",ss:"iw"},

	"Repair" : {s:"Forge",ss:"re"},
	"Upgrade" : {s:"Forge",ss:"up"},
	"Enchant" : {s:"Forge",ss:"en"},
	"Salvage" : {s:"Forge",ss:"sa"},
	"Reforge" : {s:"Forge",ss:"fo"},
	"Soulfuse" : {s:"Forge",ss:"fu"},
};

_top.init = function() {
	if(_top.inited) {
		return;
	}
	_top.inited = true;

	var ul = {};
	if(settings.topMenuIntegrate) {
		let sub = $element("div",_top.node.menu_div["MENU"],[".hvut-top-sub"]);
		["Character","Bazaar","Battle","Forge"].forEach(function(m){
			ul[m] = $element("ul",sub);
			$element("li",ul[m],[" "+m,".hvut-top-menu-s"]);
		});
	} else {
		["Character","Bazaar","Battle","Forge"].forEach(function(m){
			let sub = $element("div",_top.node.menu_div[m],[".hvut-top-sub"]);
			ul[m] = $element("ul",sub);
		});
	}
	Object.entries(_top.menu).forEach(function([k,m]){
		if(!_isekai && m.isekai===true || _isekai && m.isekai===false) {
			return;
		}
		$element("a", $element("li",ul[m.s]), {textContent:k,href:m.href||"?s="+m.s+"&ss="+m.ss});
	});
	/* monsterbation */
	if($id("mbsettings")) {
		$element("li",ul["Character"]).appendChild($element("a")).appendChild($id("mbsettings"));
		$id("mbsettings").firstElementChild.className = "";
		GM_addStyle(`
			#mbsettings {position:relative}
			#mbprofile {top:100%; left:0; min-width:100%; box-sizing:border-box; font-weight:normal}
		`);
	}
	/* monsterbation */

	let sub = $element("div",_top.node.stamina,[".hvut-top-sub hvut-top-stamina"]);
	if(!_isekai) {
		let form = $element("form",sub,{method:"POST"},{submit:function(e){if(settings.confirmStaminaRestorative&&!confirm("Are you sure you want to use a Caffeinated Candy or Energy Drink?")){e.preventDefault();}}});
		$element("input",form,{type:"hidden",name:"recover",value:"stamina"});
		$element("input",form,{type:"submit",value:"USE RESTORATIVE",disabled:_player.stamina>settings.disableStaminaRestorative,style:"width:200px"});

		_top.node.stamina.addEventListener("mouseenter",function init(){
			if(init.inited) {
				return;
			}
			init.inited = true;

			let div = $element("div",form,"Loading...");
			let inventory = {};
			$ajax.add("GET","?s=Character&ss=it",null,function(r){
				var doc = $doc(r.responseText);
				Array.from($qs(".itemlist",doc).rows).forEach(function(tr){
					inventory[ tr.cells[0].textContent.trim() ] = parseInt(tr.cells[1].textContent);
				});
				if( ["Caffeinated Candy","Energy Drink"].filter(e=>{var n=inventory[e];if(n){$element("div",form,e+" ("+n+")");}return n;}).length ) {
					div.remove();
				} else {
					div.textContent = "No items available";
				}
			});
		});
	}
	$element("div",sub,_player.condition);
	if(_player.accuracy) {
		$element("div",sub,[" "+_player.accuracy,"!color:#e00"]);
	}

	if(_player.level !== 500) {
		let exp = {};
		exp.exec = /([0-9,]+) \/ ([0-9,]+)\s*Next: ([0-9,]+)/.exec($id("level_details").textContent);
		exp.now = parseInt(exp.exec[1].replace(/,/g,""));
		exp.up = parseInt(exp.exec[2].replace(/,/g,""));
		exp.next = parseInt(exp.exec[3].replace(/,/g,""));
		exp.bottom = Math.round(Math.pow(_player.level+3, Math.pow(2.850263212287058, (1+_player.level/1000))));
		exp.level_up = exp.up - exp.bottom;
		exp.level_now = exp.now - exp.bottom;
		exp.p = Math.floor(exp.level_now/exp.level_up*100);

		let sub = $element("div",_top.node.level,[".hvut-top-sub"]);
		$element("div",sub,"Total: "+exp.now.toLocaleString()+" / "+exp.up.toLocaleString());
		$element("div",sub,"Next: "+exp.next.toLocaleString());
		$element("div",sub,"Level: "+exp.level_now.toLocaleString()+" / "+exp.level_up.toLocaleString()+" ("+exp.p+"%)");
		$element("div",sub,[".hvut-top-exp","/<div style='width:"+exp.p+"%'></div>"]);
	}

	if(_top.node.server) {
		let sub = $element("div",_top.node.server,[".hvut-top-sub"]);
		$element("a",sub,{innerHTML:"Currently playing on "+(_isekai?"Isekai":"Persistent")+(_isekai?"<br>"+_isekai:"")+"<br>Click to switch to "+(_isekai?"Persistent":"Isekai"),href:_isekai?"/":"/isekai/"});
	}
};


_top.node = {};
_top.node.div = $element("div",null,["#hvut-top","!justify-content:"+settings.topMenuAlign],{mouseenter:_top.init});

_top.node.menu = $element("div",_top.node.div,[".hvut-top-menu"]);
_top.node.quick = $element("div",_top.node.div,[".hvut-top-quick"]);

_top.node.menu_div = {};
if(settings.topMenuIntegrate) {
	_top.node.menu_div["MENU"] = $element("div",_top.node.menu,["/<span>MENU</span>"]);
} else {
	["Character","Bazaar","Battle","Forge"].forEach(function(t){
		_top.node.menu_div[t] = $element("div",_top.node.menu,["/<span>"+t.toUpperCase()+"</span>"]);
	});
}

_top.mm = $id("nav_mail") && $id("nav_mail").textContent.trim();
if(_top.mm && !settings.topMenuLinks.includes("MoogleMail")) {
	settings.topMenuLinks.push("MoogleMail");
}
settings.topMenuLinks = settings.topMenuLinks.filter(function(t){
	var m = _top.menu[t];
	if(!m || !_isekai && m.isekai===true || _isekai && m.isekai===false) {
		return false;
	} else {
		return true;
	}
});
settings.topMenuLinks.forEach(function(t){
	var m = _top.menu[t],
		text = m.text || m.ss.toUpperCase(),
		href = m.href || "?s="+m.s+"&ss="+m.ss,
		cn;
	if(t === "MoogleMail" && _top.mm) {
		text = "["+_top.mm+"]";
		cn = "hvut-top-ygm";
	}
	var a = $element("a",_top.node.quick,{textContent:text,dataset:{desc:t},href:href});
	if(cn) {
		a.className = cn;
	}
});

_top.node.stamina = $element("div",_top.node.div,["!width:90px","/<span>Stamina: "+_player.stamina+"</span>"]);
_top.node.level = $element("div",_top.node.div,["!width:60px","/<span>Lv." + _player.level+"</span>"]);
_top.node.dfct = $element("div",_top.node.div,["!width:80px","/<span>"+_player.dfct+"</span>"]);
_top.node.persona = $element("div",_top.node.div,["!width:110px","/<span>Persona</span>"]);

if(settings.randomEncounter) {
	_top.node.re = $element("div",_top.node.div,["!width:80px;cursor:pointer"]);
	$re.clock(_top.node.re);
}
if(!settings.topMenuIntegrate*9 + settings.topMenuLinks.length <= 19) {
	_top.node.server = $element("div",_top.node.div,["!width:80px",".hvut-top-server","/<span>"+(_isekai?"Isekai":"Persistent")+"</span>"]);
}

$id("navbar").after(_top.node.div);


// Difficulty
var $dfct = {};
$dfct.list = ["Normal","Hard","Nightmare","Hell","Nintendo","IWBTH","PFUDOR"];

$dfct.hover = function(){
	if(!$dfct.sub) {
		$dfct.sub = $element("div",$dfct.div,[".hvut-top-sub"]);

		$dfct.selector = $element("select",$dfct.sub,{size:$dfct.list.length,className:"hvut-scrollbar-none",style:"width:80px"},{change:function(){
			$dfct.selector.disabled = true;
			$dfct.change($dfct.selector.value);
		}});
		$dfct.selector.append( ...$dfct.list.map((d,i)=>$element("option",null,{value:i+1,text:d,selected:d===_player.dfct})).reverse() );
	}
};

$dfct.change = function(value){
	$dfct.button.textContent = "(D1...)";

	$ajax.add("GET","?s=Character&ss=se",null,function(r){
		$dfct.button.textContent = "(D2...)";
		var doc = $doc(r.responseText),
			input = $qs(`input[name="difflevel"][value^="${value}"]`,doc);
		if(input) {
			input.checked = true;
			var post = {};
			Array.from(input.form.elements).forEach(function(e){
				if(e.disabled || (e.type==="checkbox"||e.type==="radio") && !e.checked) {
					return;
				}
				post[e.name] = e.value;
			});
			$ajax.add("POST","?s=Character&ss=se",post,function(r){var doc=$doc(r.responseText);$dfct.set_button(doc);},function(){$dfct.button.textContent="(D:ERROR)";});

		} else {
			$dfct.button.textContent = "(D:ERROR)";
		}
	});
};

$dfct.set_button = function(doc) {
	var value = /^(.+) Lv\.(\d+)/.test($id("level_readout",doc).textContent.trim()) && RegExp.$1;
	if(!value) {
		$dfct.button.textContent = "(D:ERROR)";
		return;
	}

	_player.dfct = value;
	$dfct.button.textContent = value;
	if($dfct.selector) {
		$dfct.selector.value = $dfct.list.indexOf(value) + 1;
		$dfct.selector.disabled = false;
	}

	var ch_style = getValue("ch_style",{});
	ch_style.difficulty = value;
	setValue("ch_style",ch_style);
};

$dfct.button = _top.node.dfct.firstChild;
$dfct.div = $dfct.button.parentNode;
$dfct.div.addEventListener("mouseenter",$dfct.hover);


// Persona
var $persona = {};

$persona.check_p = function(doc) {
	var changed;
	Array.from($qs("select[name='persona_set']",doc).options).forEach(function(o){
		var pidx = parseInt(o.value);
		if(!$persona.json.plist[pidx]) {
			$persona.json.plist[pidx] = {elist:[null]};
		}
		if($persona.json.plist[pidx].name !== o.text) {
			$persona.json.plist[pidx].name = o.text;
			changed = true;
		}
	});
	if(changed) {
		$persona.set_value();
	}
};

$persona.check_e = function(length) {
	var json = $persona.json,
		p = json.plist[json.pidx];
	if(p.elist.length !== length + 1) {
		for(let i=p.elist.length; i<length+1; i++) {
			p.elist.push({});
		}
		p.elist.length = length + 1;
	}
};

$persona.change_p = function(pidx) {
	$persona.button.textContent = "(P1...)";
	$dfct.button.textContent = "(D...)";
	$ajax.add("POST","?s=Character&ss=ch",pidx?"persona_set="+pidx:null,function(r){
		var doc = $doc(r.responseText);
		$persona.set_p(doc);
	});
};

$persona.set_p = function(doc) {
	$persona.button.textContent = "(P2...)";
	$persona.check_p(doc);
	$persona.set_value("j","pidx",parseInt($qs("select[name='persona_set']",doc).value));

	if($persona.selector_p) {
		$persona.selector_p.value = $persona.json.pidx;
		$persona.selector_p.disabled = false;
	}
	$persona.change_e();
	$dfct.set_button(doc);
};

$persona.change_e = function(eidx) {
	$persona.button.textContent = "(E1...)";
	$ajax.add("POST","?s=Character&ss=eq",eidx?"equip_set="+eidx:null,function(r){
		var doc = $doc(r.responseText);
		$persona.set_e(doc);
	});
};

$persona.set_e = function(doc) {
	$persona.button.textContent = "(E2...)";
	$persona.check_e($qsa("#eqsl>div",doc).length);
	$persona.set_value("j","eidx",parseInt($qs("img[src$='_on.png']",doc).getAttribute("src").slice(-8,-7)));

	var json = $persona.json;
	if($persona.selector_e) {
		$persona.selector_e.innerHTML = "";
		json.plist[json.pidx].elist.forEach(function(e,i){if(!i){return;}$element("option",$persona.selector_e,{value:i,text:e.name||"Set "+i});});
		$persona.selector_e.value = json.eidx;
		$persona.selector_e.disabled = false;
	}

	$ajax.add("GET",$qs("script[src*='/dynjs/']",doc).getAttribute("src"),null,function(r){
		var html = r.responseText;
		$equip.dynjs_loaded = JSON.parse(html.slice(16,-1));
		$persona.parse_stats_pane(doc);
		$persona.set_button();
		setValue("eq_set",$qsa(".eqb",doc).map(div=>{var slot=div.children[0].textContent,eq=$equip.parse.div(div.children[1]);return eq.info?{slot:slot,category:eq.info.category,name:eq.info.name,eid:eq.info.eid,key:eq.info.key}:{slot:slot};}));
		if(_query.s === "Battle") {
			$battle.init();
		} else if(["eq","ab","it","se"].includes(_query.ss)) {
			location.href = location.href;
		}
	});

	$persona.check_warning(doc);
};

$persona.check_warning = function(doc) {
	if(_top.node.message) {
		_top.node.message.remove();
	}
	_top.node.div.classList.remove("hvut-top-warn");

	_top.node.persona.firstChild.style.color = "";
	_player.warn = $qsa("#stamina_restore .fcr",doc).map(d=>d.textContent.trim()); // Repair weapon, Repair armor, Check equipment, Check attributes
	if(_player.warn.length) {
		if(_query.s === "Battle") {
			_top.node.message = _top.node.message || $element("div",null,[".hvut-top-message"]);
			_top.node.message.textContent = "[WARNING] " + _player.warn.join(", ");
			_top.node.div.appendChild(_top.node.message);
		}
		_top.node.div.classList.add("hvut-top-warn");
		_top.node.persona.firstChild.style.color = "#e00";
	}

	_top.node.stamina.firstChild.style.color = "";
	if(_player.condition.includes("Stamina: Exhausted") || _player.accuracy || _player.stamina < settings.warnLowStamina) {
		_top.node.div.classList.add("hvut-top-warn");
		_top.node.stamina.firstChild.style.color = "#e00";
	} else if(_player.condition.includes("Stamina: Great")) {
		_top.node.stamina.firstChild.style.color = "#03c";
	}
};

$persona.parse_stats_pane = function(doc) {
	var stats_pane = {};
	$qsa("#stats_pane .st1 > div:nth-child(2n), #stats_pane .st2 > div:nth-child(2n)",doc).forEach(function(div){
		var type = div.parentNode.previousElementSibling.textContent,
			text = div.textContent.trim(),
			number = parseFloat(div.previousElementSibling.textContent);
		if(text.startsWith("% ")) {
			text = text.slice(2);
		}
		if(text === "hit chance") {
			let p = type==="Physical Attack"?"Attack " : type==="Magical Attack"?"Magic " : "";
			text = p + "hit chance"; // equipment: Attack Accuracy, Magic Accuracy
		} else if(text.match(/crit chance \/ \+([0-9.]+) % damage/)) {
			let p = type==="Physical Attack"?"Attack " : type==="Magical Attack"?"Magic " : "";
			text = p + "crit chance";
			stats_pane[p+"Crit Damage"] = parseFloat(RegExp.$1); // equipment: Attack Crit Damage, Spell Crit Damage
		}
		text = text.replace(/\b[a-z]/g,s=>s.toUpperCase());
		if( ["Fire","Cold","Elec","Wind","Holy","Dark"].includes(text) ) {
			text += type==="Specific Mitigation" ? " MIT" : " EDB";
		}
		stats_pane[text] = number;
	});

	var fighting_style = /(Unarmed|One-Handed|Two-Handed|Dualwield|Niten Ichiryu|Staff)/.test($qs(".spn",doc).textContent) && RegExp.$1,
		spell_type = ["Fire","Cold","Elec","Wind","Holy","Dark"].sort((a,b)=>stats_pane[b+" EDB"]-stats_pane[a+" EDB"])[0],
		spell_damage = stats_pane[spell_type+" EDB"],
		prof_factor = Math.max(0, Math.min(1, stats_pane[({"Holy":"Divine","Dark":"Forbidden"})[spell_type]||"Elemental"]/_player.level-1)),
		ch_style = {difficulty:_player.dfct};

	stats_pane["Fighting Style"] = fighting_style;
	ch_style["Fighting Style"] = fighting_style;
	if(fighting_style === "Staff" || spell_damage>=100) {
		stats_pane["Spell Type"] = spell_type;
		stats_pane["Proficiency Factor"] = prof_factor;
		ch_style["Spell Type"] = spell_type;
		ch_style["Proficiency Factor"] = Math.round(prof_factor*1000)/1000;
	}
	setValue("ch_style",ch_style);

	return stats_pane;
};

$persona.set_button = function() {
	$persona.button.textContent = ($persona.json.plist[$persona.json.pidx].name||"Persona").slice(0,12).trim() + " ["+$persona.json.eidx+"]";
};

$persona.set_value = function(type,name,value) {
	var json = $persona.json,
		p = json.plist[json.pidx],
		e = p && p.elist[json.eidx];
	if(type === "j") {
		json[name] = value;
	} else if(type === "p") {
		p[name] = value;
	} else if(type === "e") {
		e[name] = value;
	}
	setValue("persona",$persona.json);
};

$persona.get_value = function(type,name) {
	var json = $persona.json,
		p = json.plist[json.pidx],
		e = p && p.elist[json.eidx];
	if(type === "j") {
		return json[name];
	} else if(type === "p") {
		return p[name];
	} else if(type === "e") {
		return e[name];
	}
};

$persona.hover = function(){
	var json = $persona.json;
	if(!json.pidx || !json.eidx) {
		return;
	}
	if(!$persona.sub) {
		$persona.sub = $element("div",$persona.div,[".hvut-top-sub"]);

		$persona.selector_p = $element("select",$persona.sub,{size:json.plist.length-1,className:"hvut-scrollbar-none",style:"width:110px"},{change:function(){$persona.selector_p.disabled=true;$persona.change_p($persona.selector_p.value);}});
		json.plist.forEach(function(p,i){if(!i){return;}$element("option",$persona.selector_p,{value:i,text:p.name});});
		$persona.selector_p.value = json.pidx;

		$persona.selector_e = $element("select",$persona.sub,{size:json.plist[json.pidx].elist.length-1,className:"hvut-scrollbar-none",style:"width:110px"},{change:function(){$persona.selector_e.disabled=true;$persona.change_e($persona.selector_e.value);}});
		json.plist[json.pidx].elist.forEach(function(e,i){if(!i){return;}$element("option",$persona.selector_e,{value:i,text:e.name||"Set "+i});});
		$persona.selector_e.value = json.eidx;
	}
};

$persona.json = getValue("persona");
if(!$persona.json || !$persona.json.pidx) {
	$persona.json = {pidx:0, eidx:0, plist:[]};
}
$persona.button = _top.node.persona.firstChild;
$persona.div = $persona.button.parentNode;
$persona.div.addEventListener("mouseenter",$persona.hover);
$persona.check_warning();

if($id("persona_form")) {
	$persona.check_p();
	if($qs("select[name='persona_set']").value != $persona.json.pidx) {
		$persona.set_p();
	} else {
		$persona.set_button();
	}
} else if(!$persona.json.pidx || !$persona.json.eidx) {
	$persona.change_p();
} else {
	$persona.set_button();
}


// Check Equipment
if(settings.equipment && _query.s==="Character" && _query.ss==="eq" && !_query.equip_slot && $persona.json.pidx) {
	$persona.check_e($qsa("#eqsl>div").length);
	$persona.set_value("j","eidx",parseInt($qs("img[src$='_on.png']").src.slice(-8,-7)));

	$persona.name_header = $element("div",[$id("eqch_left"),"afterbegin"],["!width:650px;height:0;margin:0 auto;text-align:right"]);
	$persona.name_input = $element("input",$persona.name_header,{type:"text",value:$persona.json.plist[$persona.json.pidx].elist[$persona.json.eidx].name||"Set "+$persona.json.eidx,style:"width:100px;text-align:center"});
	$element("input",$persona.name_header,{type:"button",value:"Set"},function(){$persona.set_value("e","name",$persona.name_input.value);});
}
/***** Top Menu *****/


/***** Bottom Menu *****/
GM_addStyle(`
	#hvut-bottom {position:absolute; display:flex; top:100%; left:-1px; width:100%; border:1px solid; font-size:10pt; line-height:20px}
	#hvut-bottom:empty {display:none}
	#hvut-bottom > div {margin:-1px 0 -1px -1px; border:1px solid #5C0D11; padding:0 10px}
	#hvut-bottom > .hvut-spaceholder ~ div {margin:-1px -1px -1px 0}
	#hvut-bottom > .hvut-spaceholder {margin:0; border:0; padding:0}
	#hvut-bottom a {color:inherit}
	.hvut-bottom-warn {background-color:#5C0D11; color:#fff}
`);

_bottom.div = $element("div",$id("csp"),["#hvut-bottom"]);

// Credits Counter
if(settings.showCredits) {
	_bottom.credits_div = $element("div",_bottom.div,"Loading...");
	if($id("networth")) {
		$id("networth").style.display = "none";
		_bottom.credits_div.textContent = $id("networth").textContent;

	} else {
		$ajax.add("GET","?s=Bazaar&ss=is",null,function(r){
			var html = r.responseText;
			_bottom.credits_div.textContent = />Credits: ([0-9,]+)</.test(html) ? "Credits: " + RegExp.$1 : "Failed to load";
		});
	}
}

// Equip Counter
if(settings.showEquipSlots===2 || settings.showEquipSlots===1 && _query.s==="Battle") {
	_bottom.equip_div = $element("div",_bottom.div,"Loading...");

	$ajax.add("GET","?s=Character&ss=in",null,function(r){
		var html = r.responseText,
			exec = />Equip Slots: (\d+)(?: \+ (\d+))? \/ (\d+)</.exec(html),
			inventory = parseInt(exec[1]),
			storage = parseInt(exec[2]||0),
			slots = parseInt(exec[3]),
			free = slots - inventory - storage;
		_bottom.equip_div.textContent = "Equip Slots: "+inventory+(storage?" + "+storage:"")+" / "+slots;
		if(free < slots/10) {
			_bottom.equip_div.classList.add("hvut-bottom-warn");
		} else if(free < slots/2) {
			_bottom.equip_div.style.color = "#c00";
		}
	});
}

// Training Timer
if(settings.trainingTimer) {

_tr.clock = function() {
	var remain = _tr.json.current_end - Date.now();
	if(remain > 0) {
		_tr.timer.node.clock.textContent = time_format(remain);
		setTimeout(_tr.clock,1000);

	} else {
		_tr.timer.node.link.textContent = "Loading...";
		_tr.timer.node.clock.textContent = "";
		$ajax.add("GET","?s=Character&ss=tr",null,_tr.parse,function(){setTimeout(_tr.clock,10000);});
	}
};

_tr.parse = function(r) {
	var html = r.responseText,
		doc = $doc(html),
		error = get_message(doc);
	if(!$id("train_outer",doc)) {
		_tr.timer.node.link.textContent = "Waiting...";
		setTimeout(_tr.clock,60000);
		return;
	}

	var level = {};
	Array.from($id("train_table",doc).rows).slice(1).forEach(function(tr){
		level[tr.cells[0].textContent] = parseInt(tr.cells[4].textContent);
	});
	_tr.json.error = "";

	if($id("train_progress",doc)) {
		_tr.json.current_name = $id("train_progcnt",doc).previousElementSibling.textContent;
		_tr.json.current_level = level[_tr.json.current_name];
		_tr.json.current_end = /var end_time = (\d+);/.test(html) && parseInt(RegExp.$1)*1000;

		_tr.timer.node.link.textContent = _tr.json.current_name + " ["+(_tr.json.current_level+1)+"]";
		_tr.clock();

	} else if(_tr.json.next_name) {
		if(error) {
			_tr.json.error = error;
			_tr.timer.node.link.textContent = _tr.json.error;
			setTimeout(_tr.clock,60000);

		} else if(level[_tr.json.next_name] < _tr.json.next_level) {
			if($qs("img[onclick*='training.start_training("+_tr.json.next_id+")']",doc)) {
				$ajax.add("POST","?s=Character&ss=tr","start_train="+_tr.json.next_id,_tr.parse,function(){setTimeout(_tr.clock,10000);});
			} else {
				_tr.json.error = "Can't start Training";
				_tr.timer.node.link.textContent = _tr.json.error;
				setTimeout(_tr.clock,60000);
			}

		} else {
			_tr.timer.node.link.textContent = "Training completed!";
		}

	} else {
		_tr.timer.node.link.textContent = "Training completed!";
	}

	setValue("tr_save",_tr.json);
};

_tr.json = getValue("tr_save",{});
if(_tr.json.current_name || _tr.json.next_name || _tr.json.error) {
	_tr.timer = {node:{}};
	_tr.timer.node.div = $element("div",_bottom.div);
	_tr.timer.node.link = $element("a",_tr.timer.node.div,{href:"?s=Character&ss=tr",textContent:"Initializing...",style:"margin-right:5px"});
	_tr.timer.node.clock = $element("span",_tr.timer.node.div,{style:"display:inline-block;width:60px"});
	if(_tr.json.error) {
		_tr.timer.node.link.textContent = _tr.json.error;
	} else if(_tr.json.current_name) {
		_tr.timer.node.link.textContent = _tr.json.current_name + " ["+(_tr.json.current_level+1)+"]";
	}
	_tr.clock();
}

}

// Lottery
if(settings.showLottery) {
	GM_addStyle(`
		.hvut-lt-div > a {margin-right:5px}
		.hvut-lt-div > span:nth-child(2) {display:inline-block; width:40px}
		.hvut-lt-div > span:nth-child(3) {display:none; width:40px; cursor:pointer}
		.hvut-lt-div:hover > span:nth-child(2) {display:none}
		.hvut-lt-div:hover > span:nth-child(3) {display:inline-block}
		.hvut-lt-check {background-color:#fd9}
	`);
	$element("div",_bottom.div,[".hvut-spaceholder"]);

	["lt","la"].forEach(function(ss){
		if(_query.ss === ss) {
			return;
		}
		var json = getValue(ss+"_show",{},"hvut_"),
			now = Date.now();

		if(json.date > now && json.hide) {
			return;
		}

		var div = $element("div",_bottom.div,[".hvut-lt-div"]),
			equip_span = $element("a",div,{textContent:"Loading...",href:"/?s=Bazaar&ss="+ss,target:!_isekai?"_self":"_hentaiverse"}),
			time_span = $element("span",div,"--:--"),
			hide_span = $element("span",div,"close",function(){json.hide=true;setValue(ss+"_show",json);div.remove();});

		if(json.date > now) {
			if(json.date-now < 3600000) {
				div.classList.add("hvut-bottom-warn");
			} else if(json.check) {
				div.classList.add("hvut-lt-check");
			}
			equip_span.textContent = json.equip;
			time_span.textContent = time_format(json.date-now,1);
			return;
		}

		div.classList.add("hvut-bottom-warn");

		$ajax.add("GET","/?s=Bazaar&ss="+ss,null,function(r){
			var doc = $doc(r.responseText);

			var eqname = $id("lottery_eqname",doc);
			if(!eqname) {
				if($id("battle_top",doc)) {
					equip_span.textContent = "In Battle";
				} else {
					equip_span.textContent = "Failed to load";
				}
				return;
			}
			if(!$qs("img[src*='lottery_today_d.png']",doc)) {
				return;
			}

			var text = $id("rightpane",doc).lastElementChild.textContent,
				date = Date.now(),
				margin = 0;
			if(/Today's drawing is in (?:(\d+) hours?)?(?: and )?(?:(\d+) minutes?)?/.test(text)) {
				date += (60*parseInt(RegExp.$1||0) + parseInt(RegExp.$2||0)) * 60000;
				margin = 2;
			} else if(text.includes("Today's ticket sale is closed")) {
				margin = 10;
			} else {
				date = null;
			}
			var m = (new Date(date)).getUTCMinutes();
			if(date && (m<1 || 60-m<=margin)) {
				date = Math.round(date/3600000)*3600000;
			}
			json.id = $qs("img[src*='lottery_prev_a.png']",doc).getAttribute("onclick").match(/lottery=(\d+)/) && parseInt(RegExp.$1)+1;
			json.equip = eqname.textContent;
			json.date = date;
			json.check = $equip.filter(json.equip,settings.lotteryCheck);
			json.hide = !settings.showLottery;
			setValue(ss+"_show",json,"hvut_");

			equip_span.textContent = json.equip;
			time_span.textContent = time_format(json.date-now,1);

			if(json.check) {
				popup("<span>"+eqname.previousElementSibling.textContent+"</span><br><span style='color:#f00;font-weight:bold'>"+json.equip+"</span>");
			}
		});
	});
}
/***** Bottom Menu *****/


//* [1] Character - Character
if(settings.character && (_query.s==="Character" && _query.ss==="ch" || $id("persona_outer"))) {

if(settings.characterExpCalculator) {

_ch.persona = $qs("select[name='persona_set']").value;
_ch.total_exp = _window.total_exp;
_ch.exp = [null,{total:0}];
_ch.prof = {};
_ch.ass = getValue("tr_level",{})["Assimilator"] || 0;
_ch.attr_keys = ["str","dex","agi","end","int","wis"];

_ch.init_simulate = function() {
	_ch.node.div.innerHTML = "";
	$qs("img[onclick*='do_attr_post']").style.visibility = "hidden";
	$id("prof_outer").classList.add("hvut-ch-prof");

	Object.values(_ch.prof).forEach(function(p){
		p.current = parseFloat(p.tr.cells[1].textContent);
		p.exp = _ch.get_exp(p.current);
		p.tr.cells[1].textContent = p.current;
		$element("td",p.tr);
		$element("td",p.tr);
	});

	_ch.node.level = $element("input",$element("label",_ch.node.div,"Level"),{type:"number",value:_player.level,min:1,max:600,style:"width:70px"},{input:_ch.calc_prof});
	_ch.node.ass = $element("input",$element("label",_ch.node.div,"Assimilator"),{type:"number",value:_ch.ass,min:0,max:25,style:"width:50px"},{input:_ch.calc_prof});
	_ch.calc_prof();
};

_ch.calc_prof = function() {
	var level = parseFloat(_ch.node.level.value),
		ass = parseInt(_ch.node.ass.value);
	if(isNaN(level) || level<1 || level>600 || isNaN(ass) || ass<0 || ass>25) {
		return;
	}

	_window.total_exp = _ch.get_exp(level);
	_window.update_usable_exp();
	_window.update_display("str");

	var exp_gain = _window.total_exp - _ch.total_exp,
		prof_gain = exp_gain * 4 * (1+ass*0.1);
	if(prof_gain < 0) {
		prof_gain = 0;
	}
	Object.values(_ch.prof).forEach(function(p){
		p.level = _ch.get_level(p.exp+prof_gain,p.current);
		p.tr.cells[2].textContent = "+" + (p.level-p.current).toFixed(3);
		p.tr.cells[3].textContent = p.level.toFixed(3);
	});
};

_ch.get_exp = function(level) {
	var num = parseInt(level),
		dec = level%1;
	if(!_ch.exp[num]) {
		_ch.exp[num] = {total:Math.round(Math.pow(num+3, Math.pow(2.850263212287058, (1+num/1000))))};
	}
	var exp = _ch.exp[num].total;
	if(dec) {
		if(!_ch.exp[num].next) {
			_ch.exp[num].next = _ch.get_exp(num+1) - exp;
		}
		exp += Math.round(_ch.exp[num].next * dec);
	}
	return exp;
};

_ch.get_level = function(exp,level) {
	level = parseInt(level) || 1;
	while(exp >= _ch.exp[level].total) {
		level++;
		if(!_ch.exp[level]) {
			_ch.exp[level] = {total:_ch.get_exp(level)};
		}
	}
	level--;
	if(!_ch.exp[level].next) {
		_ch.exp[level].next = _ch.exp[level+1].total - _ch.exp[level].total;
	}
	return level + (exp-_ch.exp[level].total) / _ch.exp[level].next;
};

GM_addStyle(`
	#attr_table tr:last-child > td {padding-top:10px !important}
	.hvut-ch-div {position:absolute; margin:-25px 0 0 40px; line-height:22px; text-align:left}
	.hvut-ch-div label {margin:0 5px; font-size:10pt}
	.hvut-ch-div label > input {text-align:right}
	.hvut-ch-prof {width:640px !important}
	.hvut-ch-prof > div {width:310px !important; margin:0 5px}
	.hvut-ch-prof td:nth-child(1) {width:105px !important}
	.hvut-ch-prof td:nth-child(2) {width:60px !important; font-size:10pt}
	.hvut-ch-prof td:nth-child(3) {width:65px; font-size:10pt; color:#c00}
	.hvut-ch-prof td:nth-child(4) {width:60px; font-size:10pt; font-weight:bold}
`);

_ch.node = {};
_ch.node.div = $element("div",$id("attr_outer"),[".hvut-ch-div"]);
$element("input",_ch.node.div,{type:"button",value:"Simulator"},_ch.init_simulate);

$qsa("#prof_outer tr").forEach(function(tr){
	_ch.prof[tr.cells[0].textContent] = {tr:tr};
});

_window.common.get_dynamic_digit_string = function(n){return '<div class="fc4 far fcb"><div>'+n.toLocaleString()+'</div></div>';};

}

$persona.parse_stats_pane();

} else
// [END 1] Character - Character */


//* [2] Character - Equipment
if(settings.equipment && _query.s==="Character" && _query.ss==="eq") {

_eq.mage_stats = function() {
	// to get exact numbers
	var stats_pane = _eq.stats_pane,
		stats_equip = {};

	_eq.list.forEach(function(eq){
		$equip.parse.stats(eq);
		Object.entries(eq.stats).forEach(function([s,v]){
			if(!stats_equip[s]) {
				stats_equip[s] = 0;
			}
			if($equip.stats[s].multi) {
				stats_equip[s] = (1 - (1 - stats_equip[s]/100) * (1 - v.value/100)) * 100;
			} else {
				stats_equip[s] += v.value;
			}
		});
	});

	var spell_type = stats_pane["Spell Type"],
		prof_factor = stats_pane["Proficiency Factor"],
		edb_infusion = $persona.get_value("e","infusion") ? 0.25 : 0,
		edb = stats_pane[spell_type+" EDB"] / 100 + edb_infusion,
		magic_damage = stats_pane["Magic Base Damage"],
		magic_crit_chance = stats_pane["Magic Crit Chance"] / 100,
		magic_crit_damage = 0.5 + (stats_equip["Spell Crit Damage"]||0) / 100, // stats_equip["Spell Crit Damage"] is more accurate than stats_pane["Magic Crit Damage"]
		magic_score = magic_damage * (1 + edb) * (1 + magic_crit_chance * magic_crit_damage),
		arcane_crit_chance = 1 - (1 - magic_crit_chance) * (1 - 0.1),
		arcane_crit_damage = magic_crit_damage + (_player.level>=405?0.15 : _player.level>=365?0.14 : _player.level>=325?0.12 : _player.level>=285?0.10 : _player.level>=245?0.08 : _player.level>=205?0.06 : _player.level>=175?0.03 : 0),
		arcane_score = magic_damage * 1.25 * (1 + edb) * (1 + arcane_crit_chance * arcane_crit_damage);

	var cr_staff = stats_equip["Counter-Resist"]||0,
		cr_spell = prof_factor/2 + cr_staff/100,
		prof_dep = Math.max(0, Math.min(1, stats_pane["Deprecating"] / _player.level - 1)),
		cr_dep = prof_dep/2 + cr_staff/100,
		prof_sup = Math.min(1, stats_pane["Supportive"] / _player.level - 1),
		cure_bonus = prof_sup / (prof_sup>0?2:5),
		mit_imperil = $persona.get_value("e","imperil") ? (spell_type==="Holy"||spell_type==="Dark"? 0.25 : 0.4) : 0,
		mit_reduce = Math.pow(prof_factor,1.5)*0.5 + mit_imperil,
		mit_day = $persona.get_value("e","day") ? 0.1 :0,
		resist_dfct = _player.dfct==="PFUDOR" ? 0.1 : 0;

	_eq.node.mage.innerHTML = "";
	var div = $element("div",_eq.node.mage,[".hvut-eq-chart"]),
		ul;

	ul = $element("div",div,[".hvut-eq-options"]);
	$element("input",[$element("label",ul," PFUDOR"),"afterbegin"],{type:"checkbox",checked:resist_dfct},function(){_eq.mage_stats();});
	$element("input",[$element("label",ul," Infusion"),"afterbegin"],{type:"checkbox",checked:edb_infusion},function(){$persona.set_value("e","infusion",this.checked);_eq.mage_stats();});
	$element("input",[$element("label",ul," Imperil"),"afterbegin"],{type:"checkbox",checked:mit_imperil},function(){$persona.set_value("e","imperil",this.checked);_eq.mage_stats();});
	$element("input",[$element("label",ul," Day of the Week"),"afterbegin"],{type:"checkbox",checked:mit_day},function(){$persona.set_value("e","day",this.checked);_eq.mage_stats();});

	ul = $element("ul",div,[".hvut-eq-stats"]);
	$element("li",ul,"Mage Statistics");
	$element("li",ul,["/<span>"+Math.round(magic_score).toLocaleString()+"</span><span>Magic Score</span>"]);
	$element("li",ul,["/<span>"+Math.round(arcane_score).toLocaleString()+"</span><span>Arcane Score</span>"]);
	$element("li",ul,["/<span>"+(prof_factor).toFixed(3)+"</span><span>Proficiency Factor</span>"]);
	$element("li",ul,["/<span>"+(mit_reduce*100).toFixed(2)+"%</span><span>Mitigation Reduction</span>"]);
	$element("li",ul,["/<span>"+(cr_staff).toFixed(2)+"%</span><span>Base Counter-Resist</span>"]);
	$element("li",ul,["/<span>"+(cr_spell*100).toFixed(2)+"%</span><span>"+spell_type+" CR</span>"]);
	$element("li",ul,["/<span>"+(cr_dep*100).toFixed(2)+"%</span><span>Deprecating CR</span>"]);
	$element("li",ul,["/<span>"+(cure_bonus*100).toFixed(2)+"%</span><span>Cure Bonus</span>"]);

	ul = $element("ul",div,[".hvut-eq-monster"]);
	$element("li",ul,{textContent:"Monster Resist [?]",title:"Stats Resist + Token Resist"+(resist_dfct?" + PFUDOR Resist":"")});
	$element("li",ul,"Base Resist");
	$element("li",ul,{textContent:"Deprecating Resist",title:"Base Resist * (1 - Deprecating CR)"});
	$element("li",ul,{textContent:spell_type+" Resist",title:"Base Resist * (1 - "+spell_type+" CR)"});
	$element("li",ul,{textContent:"Damage Reduction",title:"https://ehwiki.org/wiki/Damage#Resist_Mechanics"},function(){window.open("https://ehwiki.org/wiki/Damage#Resist_Mechanics");});

	var mits = [0, 0.5, 0.62, 0.75];
	mits.forEach(function(mit){
		$element("li",ul,"Mitigation "+(mit*100)+"%");
	});

	[{n:"Schoolgirl",s:10,t:0},{n:"Average",s:8.5,t:5},{n:"Maximum",s:10,t:10}].forEach(function(r){
		var rb = 1 - (1-r.s/100) * (1-r.t/100) * (1-resist_dfct), // base resist
			rs = rb * (1-cr_spell), // spell resist
			rd = 0.15*rs*rs*rs - 0.75*rs*rs + 1.5*rs, // damage resist
							//Math.pow(rs,1) * Math.pow(1-rs,2) * 3C1 * 0.5 +
							//Math.pow(rs,2) * Math.pow(1-rs,1) * 3C2 * 0.75 +
							//Math.pow(rs,3) * 1 * 0.9;
			r_dep = rb * (1-cr_dep);

		var ul = $element("ul",div,[".hvut-eq-damage"]);
		$element("li",ul,{textContent:r.n,title:r.s+"%+"+r.t+"%"+(resist_dfct?"+"+resist_dfct*100+"%":"")});
		$element("li",ul,(rb*100).toFixed(2)+"%");
		$element("li",ul,(r_dep*100).toFixed(2)+"%");
		$element("li",ul,(rs*100).toFixed(2)+"%");
		$element("li",ul,(rd*100).toFixed(2)+"%");

		mits.forEach(function(mit){
			mit -= mit_day;
			$element("li",ul,Math.round(arcane_score * (1 - rd) * (1 - (mit<0?mit:mit<mit_reduce?0:mit-mit_reduce))).toLocaleString());
		});
	});
};

_eq.equipset_code = function() {
	popup_text(_eq.list.map(eq=>"[url="+location.origin+location.pathname+"equip/"+eq.info.eid+"/"+eq.info.key+"]"+eq.info.name+"[/url]"),"width:900px;max-height:500px;white-space:pre");
};

_eq.equip_popups = function() {
	if(_eq.node.popups) {
		_eq.node.popups.classList.toggle("hvut-none");
		return;
	}

	GM_addStyle(`
		.hvut-eq-popups {position:relative;width:1238px;padding:10px 0;line-height:0;text-align:left;background-color:inherit}
		.hvut-eq-popups > iframe {width:372px;height:445px;border:1px solid;margin:0 -1px -1px 0;overflow:hidden}
	`);
	_eq.node.popups = $element("div",document.body,[".hvut-eq-popups",(_eq.list.length>6?"!width:1500px":"")]);
	_eq.list.forEach(function(eq){
		$element("iframe",_eq.node.popups,{src:"equip/"+eq.info.eid+"/"+eq.info.key,scrolling:"no"});
	});
};

_eq.prof = {
	node : {},
	equip_data : {
		"Oak Staff" : {base:6.45,pxp:371},
		"Willow Staff" : {base:6.14,pxp:371},
		"Redwood Staff" : {base:8.29,pxp:371},
		"Redwood Staff+" : {base:16.24,pxp:371},
		"Katalox Staff" : {base:8.28,pxp:368},
		"Katalox Staff+" : {base:16.24,pxp:368},
		"Cotton Cap" : {base:8.29,pxp:377},
		"Cotton Robe" : {base:9.89,pxp:377},
		"Cotton Gloves" : {base:7.5,pxp:377},
		"Cotton Pants" : {base:9.09,pxp:377},
		"Cotton Shoes" : {base:6.7,pxp:377},
	},
	init : function() {
		if(_eq.prof.inited) {
			return;
		}
		_eq.prof.inited = true;

		GM_addStyle(`
			.hvut-eq-prof {position:absolute; top:0; left:0; width:100%; height:100%; overflow:auto; padding-left:100px; box-sizing:border-box; font-size:10pt; text-align:left; background-color:#EDEBDF}
			.hvut-eq-prof input[type='number'] {text-align:right}
			.hvut-eq-prof input[type='checkbox'] {top:3px}
			.hvut-eq-prof input:invalid {color:#e00}
			.hvut-eq-prof span {display:inline-block}
			.hvut-eq-prof > h4 {margin:5px 0; text-decoration:underline}
			.hvut-eq-prof > h4 > span {min-width:265px; margin-right:20px; text-decoration:underline}
			.hvut-eq-prof > div {margin:5px 0 15px; max-width:530px}
			.hvut-eq-prof > div > div {margin:1px 0; height:22px; line-height:22px}

			.hvut-eq-side {position:absolute; top:0; left:5px; width:80px; display:flex; flex-direction:column}
			.hvut-eq-side input {margin:3px 0}
			.hvut-eq-side input:nth-child(2) {margin-bottom:20px}
			.hvut-eq-current {color:#03c !important; border-color:#03c !important}

			.hvut-eq-summary > div > span {margin-right:50px}
			.hvut-eq-summary span > span:first-child {width:110px}
			.hvut-eq-summary span > span:last-child {width:60px; font-weight:bold; text-align:right}

			.hvut-eq-player > div > *:nth-child(1) {width:110px}
			.hvut-eq-player > div > *:nth-child(2) {width:70px}
			.hvut-eq-player > div > *:nth-child(3) {margin-left:5px}

			.hvut-eq-equip {min-height:93px}
			.hvut-eq-equip input {margin:0}
			.hvut-eq-equip > div:first-child {border:1px solid}
			.hvut-eq-equip > div > *:nth-child(1) {width:130px; padding-left:5px}
			.hvut-eq-equip > div > *:nth-child(2) {width:16px; margin:0 5px}
			.hvut-eq-equip > div > *:nth-child(3) {width:55px}
			.hvut-eq-equip > div > *:nth-child(4) {width:55px}
			.hvut-eq-equip > div > *:nth-child(5) {width:60px}
			.hvut-eq-equip > div > *:nth-child(6) {width:10px; text-align:right}
			.hvut-eq-equip > div > *:nth-child(7) {width:40px; text-align:right}
			.hvut-eq-equip > div > *:nth-child(8) {width:45px; margin-left:10px}
			.hvut-eq-equip > div > *:nth-child(9) {width:55px; text-align:right}
			.hvut-eq-equip > div > *:nth-child(10) {width:22px; margin-left:10px; padding:1px}

			.hvut-eq-equiplist span {padding:0 5px}
			.hvut-eq-equiplist > div:first-child {border:1px solid}
			.hvut-eq-equiplist > div > *:nth-child(1) {width:120px}
			.hvut-eq-equiplist > div > *:nth-child(2) {width:40px; text-align:right}
			.hvut-eq-equiplist > div > *:nth-child(3) {width:50px; text-align:right}
		`);

		_eq.prof.data = getValue("eq_prof",{});
		_eq.prof.current = null;
		_eq.prof.noname = 0;

		var div = _eq.node.prof,
			node = _eq.prof.node,
			li;

		node.side = $element("div",div,[".hvut-eq-side"]);
		$element("input",node.side,{type:"button",value:"[CLOSE]"},function(){_eq.prof.toggle();});
		$element("input",node.side,{type:"button",value:"[NEW]"},function(){_eq.prof.load();});

		var h4 = $element("h4",div);
		node.name = $element("span",h4);
		$element("input",h4,{type:"button",value:"Save"},function(){_eq.prof.save();});
		$element("input",h4,{type:"button",value:"Delete"},function(){_eq.prof.remove();});

		var summary = $element("div",div,[".hvut-eq-summary"]);
		li = $element("div",summary);
		node.proficiency = $element("span",$element("span",li,["/<span>Total Proficiency</span>"]));
		node.mit_reduction = $element("span",$element("span",li,["/<span>MIT Reduction</span>"]));
		li = $element("div",summary);
		node.prof_factor = $element("span",$element("span",li,["/<span>Proficiency Factor</span>"]));
		node.counter_resist = $element("span",$element("span",li,["/<span>Counter-Resist</span>"]));

		$element("h4",div,"Configuration");
		var player = $element("div",div,[".hvut-eq-player"],{input:function(){_eq.prof.change("player");}});
		li = $element("div",player);
		$element("span",li,"Level");
		node.level = $element("input",li,{type:"number",min:0,max:500,step:1,required:true});
		li = $element("div",player);
		$element("span",li,"Base Proficiency");
		node.base = $element("input",li,{type:"number",min:0,step:0.1});
		node.base_factor = $element("span",li);
		li = $element("div",player);
		node.hathperk = $element("input",[$element("label",$element("span",li)," Hath Perk"),"afterbegin"],{type:"checkbox"});
		node.hath_bonus = $element("input",li,{type:"number",step:0.001,readOnly:true});

		$element("h4",div,"Equipment");
		node.equip = $element("div",div,[".hvut-eq-equip"]);
		$element("div",node.equip,["/<span>Type</span><span>sb</span><span>level</span><span>pxp</span><span>base</span><span></span><span>pmax</span><span>upgrade</span><span>scaled</span><span></span>"]);

		$element("h4",div,"Add Equipment");
		var equip_list = $element("div",div,[".hvut-eq-equiplist"]);
		$element("div",equip_list,["/<span>Type</span><span>pxp</span><span>pmax</span>"]);
		Object.entries(_eq.prof.equip_data).forEach(function([k,e]){
			var peerless = {
				type : k,
				soulbound : true,
				level : _player.level,
				pxp : e.pxp,
				base : e.base,
				upgrade : 0,
				desc : e.desc
			};
			var legendary = Object.assign({},peerless);
			legendary.pxp = Math.round(peerless.pxp*0.95);
			legendary.base = Math.round(peerless.base*0.95*100)/100;

			$element("div",equip_list).append(
				$element("span",null,peerless.type),
				$element("span",null,peerless.pxp),
				$element("span",null,peerless.base.toFixed(2)),
				$element("input",null,{type:"button",value:"Legendary"},function(){_eq.prof.add_equip(legendary);}),
				$element("input",null,{type:"button",value:"Peerless"},function(){_eq.prof.add_equip(peerless);})
			);
		});

		Object.entries(_eq.prof.data).forEach(function([k,d]){
			d.saved = true;
			d.name = k;
			d.node = {};
			_eq.prof.add_button(d);
		});
		_eq.prof.load(Object.keys(_eq.prof.data)[0]);
	},
	toggle : function() {
		_eq.prof.init();
		_eq.node.prof.classList.toggle("hvut-none");
	},
	add_equip : function(eq) {
		eq = Object.assign({},eq);
		_eq.prof.current.equip.push(eq);
		_eq.prof.create_equip(eq);
	},
	create_equip : function(eq) {
		if(!eq.node) {
			eq.node = {};
			eq.node.div = $element("div",null,null,{input:function(){_eq.prof.change("equip",eq);}});
			$element("span",eq.node.div,eq.type);
			eq.node.soulbound = $element("input",eq.node.div,{type:"checkbox",checked:eq.soulbound});
			eq.node.level = $element("input",eq.node.div,{type:"number",min:1,max:500,step:1,required:true,value:eq.level||""});
			eq.node.pxp = $element("input",eq.node.div,{type:"number",min:200,max:_eq.prof.equip_data[eq.type].pxp,step:1,required:true,value:eq.pxp||""});
			eq.node.base = $element("input",eq.node.div,{type:"number",min:1,max:_eq.prof.equip_data[eq.type].base,step:0.01,required:true,value:eq.base||""});
			$element("span",eq.node.div," / ");
			$element("span",eq.node.div,_eq.prof.equip_data[eq.type].base.toFixed(2));
			eq.node.upgrade = $element("input",eq.node.div,{type:"number",min:0,max:50,step:1,value:eq.upgrade||""});
			eq.node.scaled = $element("span",eq.node.div);
			$element("input",eq.node.div,{type:"button",value:"x",tabIndex:-1},function(){_eq.prof.del_equip(eq);});
		}
		_eq.prof.node.equip.appendChild(eq.node.div);
		_eq.prof.change("equip",eq);
	},
	del_equip : function(eq) {
		eq.node.div.remove();
		_eq.prof.current.equip.splice(_eq.prof.current.equip.indexOf(eq),1);
		_eq.prof.calc();
	},
	change : function(type,eq) {
		var current = _eq.prof.current,
			player = current.player,
			equip = current.equip,
			node = _eq.prof.node;
		if(type === "player") {
			var prev_level = player.level;
			player.valid = true;
			["level","base","hathperk"].forEach(function(e){
				if(node[e].type === "number") {
					player[e] = parseFloat(node[e].value) || 0;
				} else if(node[e].type === "checkbox") {
					player[e] = node[e].checked;
				} else {
					player[e] = node[e].value;
				}
				if(!node[e].validity.valid) {
					player.valid = false;
				}
			});
			if(!player.valid) {
				//return;
			}

			if(player.level !== prev_level) {
				equip.forEach(function(eq){
					eq.node.level.max = player.level;
					if(eq.soulbound) {
						eq.level = player.level;
						eq.scaled = $equip.forge("Elemental",eq.base,eq.upgrade,eq.pxp,eq.level);
						eq.node.level.value = eq.level;
						eq.node.scaled.textContent = eq.scaled.toFixed(2);
					}
				});
				node.base.max = (player.level*1.2*10) / 10;
			}
			node.base_factor.textContent = " = Level * " + (player.base / player.level).toFixed(3);
			node.hath_bonus.value = player.hathperk ? (player.base*0.1).toFixed(2) : 0;
		}
		if(type === "equip") {
			eq.valid = true;
			["soulbound","level","pxp","base","upgrade"].forEach(function(e){
				if(eq.node[e].type === "number") {
					eq[e] = parseFloat(eq.node[e].value) || 0;
				} else if(eq.node[e].type === "checkbox") {
					eq[e] = eq.node[e].checked;
				} else {
					eq[e] = eq.node[e].value;
				}
				if(!eq.node[e].validity.valid) {
					eq.valid = false;
				}
			});
			if(!eq.valid) {
				//return;
			}

			eq.node.level.disabled = eq.soulbound;
			if(eq.soulbound) {
				eq.level = player.level;
				eq.node.level.value = eq.level;
			}
			eq.scaled = $equip.forge("Elemental",eq.base,eq.upgrade,eq.pxp,eq.level);
			eq.node.scaled.textContent = eq.scaled.toFixed(2);
		}
		_eq.prof.calc();
	},
	calc : function() {
		var current = _eq.prof.current,
			player = current.player,
			equip = current.equip,
			node = _eq.prof.node;
		player.proficiency = player.base;
		if(player.hathperk) {
			player.proficiency += player.base * 0.1;
		}
		equip.forEach(function(eq){
			//if(eq.valid) {}
			player.proficiency += eq.scaled;
		});
		player.prof_factor = Math.max(0, Math.min(1, player.proficiency / player.level - 1));
		player.mit_reduction = Math.pow(player.prof_factor,1.5) / 2;
		player.counter_resist = player.prof_factor / 2;

		node.proficiency.textContent = player.proficiency.toFixed(3);
		node.prof_factor.textContent = player.prof_factor.toFixed(3);
		node.mit_reduction.textContent = (player.mit_reduction*100).toFixed(2) + "%";
		node.counter_resist.textContent = (player.counter_resist*100).toFixed(2) + "%";
	},
	save : function() {
		var current = _eq.prof.current,
			name = prompt("Enter the name of this setting",current.saved?current.name:"");
		if(!name || !name.trim()) {
			return;
		}
		name = name.trim();

		var json = getValue("eq_prof",{});
		json[current.name] = {
			player : current.player,
			equip : current.equip.map(eq=>({type:eq.type,soulbound:eq.soulbound,level:eq.level,pxp:eq.pxp,base:eq.base,upgrade:eq.upgrade}))
		};
		var data = _eq.prof.data;
		if(current.name !== name && data[name]) {
			data[name].node.button.remove();
			delete data[name];
		}
		current.saved = true;
		current.name = name;
		current.node.button.value = name;
		_eq.prof.node.name.textContent = name;

		var json_new = {},
			data_new = {};
		Object.entries(data).forEach(function([k,d]){
			var key_new = d.name;
			data_new[key_new] = d;
			if(d.saved) {
				json_new[key_new] = json[k];
			}
		});
		_eq.prof.data = data_new;
		setValue("eq_prof",json_new);
	},
	load : function(data) {
		if(typeof data === "string") {
			data = _eq.prof.data[data];
		}
		if(data === _eq.prof.current) {
			return;
		}
		if(_eq.prof.current) {
			_eq.prof.current.node.button.classList.remove("hvut-eq-current");
			_eq.prof.current.equip.forEach(eq=>eq.node.div.remove());
		}
		if(!data) {
			_eq.prof.noname++;
			data = {
				name : "\n*Noname " + _eq.prof.noname,
				player : {level:_player.level,base:_player.level,hathperk:false},
				equip : [],
				node : {}
			};
			_eq.prof.data[data.name] = data;
			_eq.prof.add_button(data);
		}
		_eq.prof.current = data;

		var current = _eq.prof.current,
			player = current.player,
			equip = current.equip;

		current.node.button.classList.add("hvut-eq-current");
		_eq.prof.node.name.textContent = current.name;
		_eq.prof.node.level.value = player.level;
		_eq.prof.node.base.value = player.base;
		_eq.prof.node.base.max = (player.level*1.2*10) / 10;
		_eq.prof.node.hathperk.checked = player.hathperk;
		_eq.prof.change("player");

		equip.forEach(function(eq){
			_eq.prof.create_equip(eq);
		});
		//_eq.prof.calc();
	},
	remove : function() {
		var current = _eq.prof.current;
		current.node.button.remove();
		delete _eq.prof.data[current.name];
		var json = getValue("eq_prof",{});
		delete json[current.name];
		setValue("eq_prof",json);
		_eq.prof.load(Object.keys(_eq.prof.data)[0]);
	},
	add_button : function(data) {
		data.node.button = $element("input",_eq.prof.node.side,{type:"button",value:data.name.trim()},function(){_eq.prof.load(data);});
	}
};


_eq.node = {};

if(_query.equip_slot) {
	GM_addStyle(`
		#eqch_left .eqb {padding:0; height:auto; font-size:10pt; line-height:20px; text-align:center; overflow:hidden}
		#eqch_left .eqb > div:last-child {padding-top:2px}
	`);

	$equip.list($id("equip_pane"),1);

} else {
	GM_addStyle(`
		#popup_box.hvut-eq-popupbox {margin-top:15px}
		#eqch_left {height:647px; padding-top:10px}
		#eqsh {display:none}
		#eqsl {margin-top:35px}
		#eqsb .eqb {padding:0; height:auto; font-size:10pt; line-height:20px; text-align:center; overflow:hidden}
		#eqsb .eqb > div:last-child {padding-top:2px}

		.hvut-eq-info {position:absolute; top:0; right:0; font-size:9pt}
		.hvut-eq-info > span {display:inline-block; margin:0 3px}
		.hvut-eq-info > span:nth-child(2) {width:35px}
		.hvut-eq-info > span:nth-child(3) {width:35px}
		.hvut-eq-untradeable {color:#c00}
		.hvut-eq-cdt1 {color:#c00}
		.hvut-eq-cdt2 {color:#fff; background-color:#c00}

		.hvut-eq-buttons {width:650px; height:0; margin:0 auto; text-align:left}

		.hvut-eq-mage {position:absolute; bottom:0; left:0; width:100%}
		.hvut-eq-chart {position:relative; display:flex; flex-wrap:wrap; justify-content:space-between; width:620px; margin:0 auto; padding:10px 15px; overflow:hidden; border:1px solid; font-size:10pt; text-align:left; white-space:nowrap}
		.hvut-eq-options {width:100%; margin-bottom:5px; padding-bottom:5px; border-bottom:1px solid}
		.hvut-eq-options > label {margin-right:10px}
		.hvut-eq-chart > ul {list-style:none; margin:0; padding:0}
		.hvut-eq-chart li {padding:0 10px; line-height:20px; border-bottom:1px dotted transparent}
		.hvut-eq-chart li:first-child {font-weight:bold; margin-bottom:3px; border-bottom:1px dotted}
		.hvut-eq-chart li[title] {cursor:help}
		.hvut-eq-chart span:first-child {display:inline-block; width:50px; text-align:right; margin-right:5px}
		.hvut-eq-stats {width:210px}
		.hvut-eq-monster {width:140px; text-align:right}
		.hvut-eq-damage {width:80px; text-align:right}
		.hvut-eq-stats > li:nth-child(3), .hvut-eq-stats > li:nth-child(5), .hvut-eq-monster > li:nth-child(5), .hvut-eq-damage > li:nth-child(5) {border-bottom-color:currentColor}

	`);
	$id("popup_box").classList.add("hvut-eq-popupbox");

	setValue("eq_set",$qsa(".eqb").map(div=>{var slot=div.children[0].textContent,eq=$equip.parse.div(div.children[1]);return eq.info?{slot:slot,category:eq.info.category,name:eq.info.name,eid:eq.info.eid,key:eq.info.key}:{slot:slot};}));

	_eq.stats_pane = $persona.parse_stats_pane();
	_eq.list = $equip.list($id("eqch_left"));
	_eq.list.forEach(function(eq){
		eq.node.div.textContent = eq.node.div.textContent;
		$element("div",eq.node.wrapper.firstElementChild,[".hvut-eq-info"]).append(
			$element("span",null,[" "+(eq.info.soulbound?"Soulbound":"Lv."+eq.info.level),(eq.info.soulbound||!eq.info.tradeable?".hvut-eq-untradeable":"")])," : ",
			$element("span",null,"IW "+eq.info.tier)," : ",
			$element("span",null,[" "+Math.ceil(eq.info.cdt*100)+"%",(eq.info.cdt<=0.5?".hvut-eq-cdt2":eq.info.cdt<=0.6?".hvut-eq-cdt1":"")])
		);
	});

	_eq.node.buttons = $element("div",[$id("eqch_left"),"afterbegin"],[".hvut-eq-buttons"]);
	_eq.node.mage = $element("div",$id("eqch_left"),[".hvut-eq-mage"]);
	_eq.node.prof = $element("div",$id("eqch_left"),[".hvut-eq-prof hvut-none"]);

	$element("input",_eq.node.buttons,{type:"button",value:"Equip Set Code"},_eq.equipset_code);
	$element("input",_eq.node.buttons,{type:"button",value:"Equip Popups"},_eq.equip_popups);

	if(_eq.stats_pane["Spell Type"]) {
		$element("input",_eq.node.buttons,{type:"button",value:"Proficiency Calculator"},_eq.prof.toggle);
		if(settings.equipmentStatsAnalyzer) {
			_eq.mage_stats();
		}
	}

	$ajax.add("GET","?s=Character&ss=ch",null,function(r){
		var doc = $doc(r.responseText);
		var base = {};
		$qsa("#attr_table tr:nth-last-child(n+2), #prof_outer tr",doc).forEach(function(tr){
			base[tr.children[0].textContent.toLowerCase()] = tr.children[1].textContent;
		});
		var stats = ["strength","dexterity","agility","endurance","intelligence","wisdom", "elemental","divine","forbidden","deprecating","supportive"];
		$qsa(".st2:nth-last-child(-n+3) .fal > div").forEach(function(d){
			var s = d.textContent.trim();
			if(stats.includes(s)) {
				d.innerHTML = "&nbsp;["+Math.round(base[s])+"]"+ d.textContent;
			}
		});
	});

	$persona.set_button();
}

} else
// [END 2] Character - Equipment */


//* [3] Character - Abilities
if(settings.abilities && _query.s==="Character" && _query.ss==="ab") {

_ab.data = {
	"HP Tank": {"category":"General","img":"3.png","pos":0,"tier":[0,25,50,75,100,120,150,200,250,300],"point":[1,2,3,3,4,4,4,5,5,5]},
	"MP Tank": {"category":"General","img":"3.png","pos":-34,"tier":[0,30,60,90,120,160,210,260,310,350],"point":[1,2,3,3,4,4,4,5,5,5]},
	"SP Tank": {"category":"General","img":"3.png","pos":-68,"tier":[0,40,80,120,170,220,270,330,390,450],"point":[1,2,3,3,4,4,4,5,5,5]},
	"Better Health Pots": {"category":"General","img":"1.png","pos":0,"tier":[0,100,200,300,400],"point":[1,2,3,4,5]},
	"Better Mana Pots": {"category":"General","img":"1.png","pos":-34,"tier":[0,80,140,220,380],"point":[2,3,5,7,9]},
	"Better Spirit Pots": {"category":"General","img":"1.png","pos":-68,"tier":[0,90,160,240,400],"point":[2,3,5,7,9]},

	"1H Damage": {"category":"One-handed","img":"e.png","pos":-68,"tier":[0,100,200],"point":[2,3,5]},
	"1H Accuracy": {"category":"One-handed","img":"e.png","pos":-34,"tier":[50,150],"point":[1,2]},
	"1H Block": {"category":"One-handed","img":"e.png","pos":0,"tier":[250],"point":[3]},

	"2H Damage": {"category":"Two-handed","img":"k.png","pos":-34,"tier":[0,100,200],"point":[2,3,5]},
	"2H Accuracy": {"category":"Two-handed","img":"k.png","pos":0,"tier":[50,150],"point":[1,2]},
	"2H Parry": {"category":"Two-handed","img":"e.png","pos":-102,"tier":[250],"point":[3]},

	"DW Damage": {"category":"Dual-wielding","img":"j.png","pos":0,"tier":[0,100,200],"point":[2,3,5]},
	"DW Accuracy": {"category":"Dual-wielding","img":"k.png","pos":-68,"tier":[50,150],"point":[1,2]},
	"DW Crit": {"category":"Dual-wielding","img":"k.png","pos":-102,"tier":[250],"point":[3]},

	"Staff Spell Damage": {"category":"Staff","img":"9.png","pos":-68,"tier":[0,100,200],"point":[2,3,5]},
	"Staff Accuracy": {"category":"Staff","img":"v.png","pos":0,"tier":[50,150],"point":[1,2]},
	"Staff Damage": {"category":"Staff","img":"k.png","pos":-136,"tier":[0],"point":[3]},

	"Cloth Spellacc": {"category":"Cloth Armor","img":"5.png","pos":0,"tier":[120],"point":[5]},
	"Cloth Spellcrit": {"category":"Cloth Armor","img":"5.png","pos":-34,"tier":[0,40,90,130,190],"point":[1,2,3,5,7]},
	"Cloth Castspeed": {"category":"Cloth Armor","img":"5.png","pos":-68,"tier":[150,250],"point":[2,5]},
	"Cloth MP": {"category":"Cloth Armor","img":"u.png","pos":-136,"tier":[0,60,110,170,230,290,350],"point":[1,2,3,3,4,4,5]},

	"Light Acc": {"category":"Light Armor","img":"7.png","pos":-34,"tier":[0],"point":[3]},
	"Light Crit": {"category":"Light Armor","img":"7.png","pos":0,"tier":[0,40,90,130,190],"point":[1,2,3,5,7]},
	"Light Speed": {"category":"Light Armor","img":"6.png","pos":-68,"tier":[150,250],"point":[2,5]},
	"Light HP/MP": {"category":"Light Armor","img":"5.png","pos":-102,"tier":[0,60,110,170,230,290,350],"point":[1,2,3,3,4,4,5]},

	"Heavy Crush": {"category":"Heavy Armor","img":"j.png","pos":-34,"tier":[0,75,150],"point":[3,5,7]},
	"Heavy Prcg": {"category":"Heavy Armor","img":"a.png","pos":-102,"tier":[0,75,150],"point":[3,5,7]},
	"Heavy Slsh": {"category":"Heavy Armor","img":"j.png","pos":-68,"tier":[0,75,150],"point":[3,5,7]},
	"Heavy HP": {"category":"Heavy Armor","img":"u.png","pos":-102,"tier":[0,60,110,170,230,290,350],"point":[1,2,3,3,4,4,5]},

	"Better Weaken": {"category":"Deprecating 1","img":"4.png","pos":0,"tier":[70,100,130,190,250],"point":[1,2,3,5,7]},
	"Faster Weaken": {"category":"Deprecating 1","img":"b.png","pos":-68,"tier":[80,165,250],"point":[3,5,7]},
	"Better Imperil": {"category":"Deprecating 1","img":"a.png","pos":-68,"tier":[130,175,230,285,330],"point":[1,2,3,4,5]},
	"Faster Imperil": {"category":"Deprecating 1","img":"r.png","pos":0,"tier":[140,225,310],"point":[3,5,7]},
	"Better Blind": {"category":"Deprecating 1","img":"r.png","pos":-34,"tier":[110,130,160,190,220],"point":[1,2,3,4,5]},
	"Faster Blind": {"category":"Deprecating 1","img":"9.png","pos":-102,"tier":[120,215,275],"point":[1,2,3]},
	"Mind Control": {"category":"Deprecating 1","img":"9.png","pos":-136,"tier":[80,130,170],"point":[1,3,5]},

	"Better Silence": {"category":"Deprecating 2","img":"c.png","pos":-170,"tier":[120,170,215],"point":[3,5,7]},
	"Better MagNet": {"category":"Deprecating 2","img":"u.png","pos":0,"tier":[250,295,340,370,400],"point":[1,2,3,4,5]},
	"Better Slow": {"category":"Deprecating 2","img":"c.png","pos":0,"tier":[30,50,75,105,135],"point":[1,2,3,4,5]},
	"Better Drain": {"category":"Deprecating 2","img":"2.png","pos":0,"tier":[20,50,90],"point":[2,3,5]},
	"Faster Drain": {"category":"Deprecating 2","img":"n.png","pos":0,"tier":[30,70,110,150,200],"point":[1,2,3,4,5]},
	"Ether Theft": {"category":"Deprecating 2","img":"2.png","pos":-34,"tier":[150],"point":[5]},
	"Spirit Theft": {"category":"Deprecating 2","img":"2.png","pos":-68,"tier":[150],"point":[5]},

	"Better Haste": {"category":"Supportive 1","img":"9.png","pos":-34,"tier":[60,75,90,110,130],"point":[1,2,3,4,5]},
	"Better Shadow Veil": {"category":"Supportive 1","img":"6.png","pos":-34,"tier":[90,105,120,135,155],"point":[1,2,3,5,7]},
	"Better Absorb": {"category":"Supportive 1","img":"c.png","pos":-34,"tier":[40,60,80],"point":[1,2,3]},
	"Stronger Spirit": {"category":"Supportive 1","img":"a.png","pos":0,"tier":[200,220,240,265,285],"point":[1,2,3,4,5]},
	"Better Heartseeker": {"category":"Supportive 1","img":"6.png","pos":0,"tier":[140,185,225,265,305,345,385],"point":[1,2,3,4,5,6,7]},
	"Better Arcane Focus": {"category":"Supportive 1","img":"q.png","pos":0,"tier":[175,205,245,285,325,365,405],"point":[1,2,3,4,5,6,7]},
	"Better Regen": {"category":"Supportive 1","img":"b.png","pos":-34,"tier":[50,70,95,145,195,245,295,375,445,500],"point":[1,2,3,4,5,6,7,8,9,10]},
	"Better Cure": {"category":"Supportive 1","img":"i.png","pos":-102,"tier":[0,35,65],"point":[2,3,5]},

	"Better Spark": {"category":"Supportive 2","img":"q.png","pos":-170,"tier":[100,125,150],"point":[2,3,5]},
	"Better Protection": {"category":"Supportive 2","img":"o.png","pos":0,"tier":[40,55,75,95,120],"point":[1,2,3,4,5]},
	"Flame Spike Shield": {"category":"Supportive 2","img":"s.png","pos":0,"tier":[10,65,140,220,300],"point":[3,1,2,3,4]},
	"Frost Spike Shield": {"category":"Supportive 2","img":"p.png","pos":0,"tier":[10,65,140,220,300],"point":[3,1,2,3,4]},
	"Shock Spike Shield": {"category":"Supportive 2","img":"g.png","pos":0,"tier":[10,65,140,220,300],"point":[3,1,2,3,4]},
	"Storm Spike Shield": {"category":"Supportive 2","img":"a.png","pos":-34,"tier":[10,65,140,220,300],"point":[3,1,2,3,4]},

	"Conflagration": {"category":"Elemental","img":"h.png","pos":0,"tier":[50,100,150,200,250,300,400],"point":[3,4,5,6,8,10,12]},
	"Cryomancy": {"category":"Elemental","img":"i.png","pos":-34,"tier":[50,100,150,200,250,300,400],"point":[3,4,5,6,8,10,12]},
	"Havoc": {"category":"Elemental","img":"9.png","pos":0,"tier":[50,100,150,200,250,300,400],"point":[3,4,5,6,8,10,12]},
	"Tempest": {"category":"Elemental","img":"i.png","pos":-68,"tier":[50,100,150,200,250,300,400],"point":[3,4,5,6,8,10,12]},
	"Sorcery": {"category":"Elemental","img":"c.png","pos":-68,"tier":[70,140,210,280,350],"point":[1,2,3,4,5]},
	"Elementalism": {"category":"Elemental","img":"c.png","pos":-136,"tier":[85,170,255,340,425],"point":[2,3,5,7,9]},
	"Archmage": {"category":"Elemental","img":"i.png","pos":0,"tier":[90,180,270,360,450],"point":[5,7,9,12,15]},

	"Better Corruption": {"category":"Forbidden","img":"t.png","pos":0,"tier":[75,150],"point":[3,5]},
	"Better Disintegrate": {"category":"Forbidden","img":"t.png","pos":-34,"tier":[175,250],"point":[5,7]},
	"Better Ragnarok": {"category":"Forbidden","img":"u.png","pos":-68,"tier":[250,325,400],"point":[7,9,12]},
	"Ripened Soul": {"category":"Forbidden","img":"u.png","pos":-34,"tier":[150,300,450],"point":[7,10,15]},
	"Dark Imperil": {"category":"Forbidden","img":"t.png","pos":-68,"tier":[175,225,275,325,375],"point":[2,3,5,7,9]},

	"Better Smite": {"category":"Divine","img":"q.png","pos":-136,"tier":[75,150],"point":[3,5]},
	"Better Banish": {"category":"Divine","img":"q.png","pos":-34,"tier":[175,250],"point":[5,7]},
	"Better Paradise": {"category":"Divine","img":"q.png","pos":-68,"tier":[250,325,400],"point":[7,9,12]},
	"Soul Fire": {"category":"Divine","img":"l.png","pos":0,"tier":[150,300,450],"point":[7,10,15]},
	"Holy Imperil": {"category":"Divine","img":"v.png","pos":-34,"tier":[175,225,275,325,375],"point":[2,3,5,7,9]},
};

_ab.preset = {
	"CURRENT": [],
	"One-handed": ["HP Tank","MP Tank","SP Tank","Better Health Pots","Better Mana Pots","Better Spirit Pots","1H Damage","1H Accuracy","1H Block","Heavy Crush","Heavy Prcg","Heavy Slsh","Heavy HP","Better Haste","Better Shadow Veil","Stronger Spirit","Better Heartseeker","Better Regen","Better Cure","Better Spark","Better Protection","Flame Spike Shield"],
	"Two-handed": ["HP Tank","MP Tank","SP Tank","Better Health Pots","Better Mana Pots","Better Spirit Pots","2H Damage","2H Accuracy","2H Parry","Light Acc","Light Crit","Light Speed","Light HP/MP","Better Haste","Better Shadow Veil","Stronger Spirit","Better Heartseeker","Better Regen","Better Cure","Better Spark","Better Protection","Flame Spike Shield"],
	"Dual-wielding": ["HP Tank","MP Tank","SP Tank","Better Health Pots","Better Mana Pots","Better Spirit Pots","DW Damage","DW Accuracy","DW Crit","Light Acc","Light Crit","Light Speed","Light HP/MP","Better Haste","Better Shadow Veil","Stronger Spirit","Better Heartseeker","Better Regen","Better Cure","Better Spark","Better Protection","Flame Spike Shield"],
	"Niten Ichiryu": ["HP Tank","MP Tank","SP Tank","Better Health Pots","Better Mana Pots","Better Spirit Pots","2H Damage","2H Parry","DW Accuracy","DW Crit","Light Acc","Light Crit","Light Speed","Light HP/MP","Better Haste","Better Shadow Veil","Stronger Spirit","Better Heartseeker","Better Regen","Better Cure","Better Spark","Better Protection","Flame Spike Shield"],
	"Elemental mage": ["HP Tank","MP Tank","SP Tank","Better Health Pots","Better Mana Pots","Better Spirit Pots","Staff Spell Damage","Staff Accuracy","Cloth Spellacc","Cloth Spellcrit","Cloth Castspeed","Cloth MP","Better Imperil","Faster Imperil","Better Haste","Better Shadow Veil","Stronger Spirit","Better Arcane Focus","Better Regen","Better Cure","Better Spark","Better Protection","Flame Spike Shield","Conflagration","Sorcery","Elementalism","Archmage"],
	"Dark mage": ["HP Tank","MP Tank","SP Tank","Better Health Pots","Better Mana Pots","Better Spirit Pots","Staff Spell Damage","Staff Accuracy","Cloth Spellacc","Cloth Spellcrit","Cloth Castspeed","Cloth MP","Better Imperil","Faster Imperil","Better Haste","Better Shadow Veil","Stronger Spirit","Better Arcane Focus","Better Regen","Better Cure","Better Spark","Better Protection","Flame Spike Shield","Better Corruption","Better Disintegrate","Better Ragnarok","Ripened Soul","Dark Imperil"],
	"Holy mage": ["HP Tank","MP Tank","SP Tank","Better Health Pots","Better Mana Pots","Better Spirit Pots","Staff Spell Damage","Staff Accuracy","Cloth Spellacc","Cloth Spellcrit","Cloth Castspeed","Cloth MP","Better Imperil","Faster Imperil","Better Haste","Better Shadow Veil","Stronger Spirit","Better Arcane Focus","Better Regen","Better Cure","Better Spark","Better Protection","Flame Spike Shield","Better Smite","Better Banish","Better Paradise","Soul Fire","Holy Imperil"],
};

_ab.point = /Ability Points: (\d+)/.test($id("ability_top").children[3].textContent) && parseInt(RegExp.$1);
_ab.level = {};


_ab.unlock = function(ab,to) {
	var level = ab.level,
		i = to - level;
	while(i-- > 0){
		$ajax.add("POST",location.href,"unlock_ability="+ab.id,function(){
			level++;
			var button = ab.div.children[2].children[level-1];
			button.style.opacity = 0.5;
			button.style.backgroundImage = button.style.backgroundImage.replace("u.png","f.png");
			if(level === to) {
				location.href = location.href;
			}
		});
	}
};

_ab.calc = {

node : {},
array : [],
list : [],
reverse : false,

init : function() {
	if(_ab.calc.inited) {
		return;
	}
	_ab.calc.inited = true;

	GM_addStyle(`
		.hvut-ab-calc {display:flex; position:absolute; top:27px; left:0; width:100%; height:675px; justify-content:center; align-items:center; background-color:#EDEBDF; z-index:9; text-align:left}
		.hvut-ab-calc > div {margin:0 10px; height:616px}
		.hvut-ab-calc > div:nth-child(3) {overflow:hidden auto}

		.hvut-ab-side {width:130px; display:flex; flex-direction:column}
		.hvut-ab-side input {margin:3px 0}
		.hvut-ab-side input:first-child {margin-bottom:20px}

		.hvut-ab-icon {display:inline-block; position:relative; width:30px; margin:2px; height:32px; vertical-align:middle; background-position-y:-2px; user-select:none; cursor:default}
		.hvut-ab-off {filter:grayscale(100%); box-shadow:0 0 0 20px #fff9 inset}
		.hvut-ab-off:hover {filter:none} /* grayscale(0) */
		.hvut-ab-point {position:absolute; top:0; right:0; width:14px; padding:1px; text-align:center; background-color:#333; color:#fff; font-size:9pt}
		.hvut-ab-tooltip {display:none; position:absolute; bottom:32px; left:0; padding:0 3px; border:1px solid; background-color:#fff; font-size:9pt; line-height:16px; white-space:nowrap; z-index:1; pointer-events:none}
		.hvut-ab-icon:hover > .hvut-ab-tooltip {display:inline-block}

		.hvut-ab-ul {width:450px; margin:0; padding:0; border:1px solid; list-style:none}
		.hvut-ab-ul > li {padding:2px; border-bottom:1px solid}
		.hvut-ab-ul > li:last-child {border-bottom:none}
		.hvut-ab-category {display:inline-block; width:130px; margin-left:10px; font-size:12pt; font-weight:bold; vertical-align:middle}
		.hvut-ab-ul .hvut-ab-icon {cursor:pointer}

		.hvut-ab-table {table-layout:fixed; width:400px; border-collapse:collapse; position:relative; font-size:10pt; text-align:right}
		.hvut-ab-table tr:first-child > td {height:36px; font-weight:bold; text-align:center; cursor:pointer}
		.hvut-ab-table td {border:1px solid; padding:2px 5px}
		.hvut-ab-table td:nth-child(1) {width:50px}
		.hvut-ab-table td:nth-child(2) {width:50px}
		.hvut-ab-table td:nth-child(3) {width:50px}
		.hvut-ab-table td:nth-child(4) {width:205px; text-align:left}
		.hvut-ab-nolevel {background-color:#edb}
		.hvut-ab-noab > span {color:#999}
		.hvut-ab-hr {margin:2px 0; border-top:1px dotted}
	`);

	var node = _ab.calc.node;

	node.side = $element("div",node.div,[".hvut-ab-side"]);
	$element("input",node.side,{type:"button",value:"[CLOSE]"},function(){_ab.calc.toggle();});
	Object.entries(_ab.preset).forEach(function([k,p]){
		$element("input",node.side,{type:"button",value:k},function(){_ab.calc.run(p);});
	});

	node.ul = $element("ul",$element("div",node.div),[".hvut-ab-ul"],_ab.calc.click);
	var category,li;
	Object.entries(_ab.data).forEach(function([n,ab]){
		if(category !== ab.category) {
			category = ab.category;
			li = $element("li",node.ul);
			$element("span",li,[" "+category,".hvut-ab-category"]);
		}
		node[n] = $element("div",li,[".hvut-ab-icon hvut-ab-off","!background-image:url('/y/t/"+ab.img+"');background-position-x:"+(ab.pos-2)+"px"]);
		node[n].dataset.ab = n;
		$element("span",node[n],[" "+n,".hvut-ab-tooltip"]);
	});
	node.table = $element("table",$element("div",node.div),[".hvut-ab-table"]);

	var index = 0;
	Object.entries(_ab.data).forEach(function([n,ab]){
		ab.tier.forEach(function(lv,i){
			_ab.calc.array.push({index:index,ability:n,level:lv,tier:i+1,point:ab.point[i]});
		});
		index++;
	});
	_ab.calc.array.sort((a,b)=>a.level-b.level||a.index-b.index);

	_ab.calc.run();
},

table : function() {
	var array = _ab.calc.array,
		list = _ab.calc.list,
		reverse = _ab.calc.reverse,
		filter = [],
		current = {level:-1,sum:0};

	array.forEach(function(d){
		if(!list.includes(d.ability)) {
			return;
		}
		if(current.level !== d.level) {
			filter.push( (current={level:d.level,sum:current.sum,ability:[]}) );
		}
		current.sum += d.point;
		current.ab = current.sum - current.level;
		current.ability.push(d);
	});
	if(reverse) {
		filter.reverse();
	}

	var frag = $element();
	$element("tr",frag,["/<td>"+(reverse?"&#x25BC;":"&#x25B2;")+" Level</td><td>Ability Points</td><td>Ability Boost</td><td>Abilities</td>"],function(){_ab.calc.reverse=!reverse;_ab.calc.table();});
	filter.forEach(function(d){
		var tr = $element("tr",frag,[_player.level<d.level?".hvut-ab-nolevel":""]);
		$element("td",tr,d.level);
		$element("td",tr,d.sum);
		$element("td",tr,["/<span>"+d.ab+"</span>",d.ab<=0?".hvut-ab-noab":""]);
		var td = $element("td",tr);
		d.ability.forEach(function(ab,i){
			if(i%6===0 && i) {
				$element("div",td,[".hvut-ab-hr"]);
			}
			var data = _ab.data[ab.ability];
			$element("div",td,[".hvut-ab-icon","!background-image:url('/y/t/"+data.img+"');background-position-x:"+(data.pos-2)+"px"]).append(
				$element("span",null,[" "+ab.point,".hvut-ab-point"]),
				$element("span",null,[" "+ab.ability+" Lv."+ab.tier,".hvut-ab-tooltip"])
			);
		});
	});
	_ab.calc.node.table.innerHTML = "";
	_ab.calc.node.table.appendChild(frag);
},

run : function(list=_ab.preset.CURRENT) {
	_ab.calc.init();
	_ab.calc.list.forEach(function(e){
		_ab.calc.node[e].classList.add("hvut-ab-off");
	});
	list.forEach(function(e){
		_ab.calc.node[e].classList.remove("hvut-ab-off");
	});
	_ab.calc.list = list.slice();
	_ab.calc.table();
},

click : function(e) {
	var btn = e.target,
		ab = btn.dataset.ab,
		list = _ab.calc.list;
	if(!ab) {
		return;
	}
	if(list.includes(ab)) {
		list.splice(list.indexOf(ab),1);
		btn.classList.add("hvut-ab-off");
	} else {
		list.push(ab);
		btn.classList.remove("hvut-ab-off");
	}
	_ab.calc.table();
},

toggle : function() {
	_ab.calc.init();
	_ab.calc.node.div.classList.toggle("hvut-none");
}

};


GM_addStyle(`
	.hvut-ab-slot {position:absolute; bottom:-5px; left:2px; width:30px; font-size:9pt; color:#fff}
	.hvut-ab-max {background-color:#333}
	.hvut-ab-limit {background-color:#03c}
	.hvut-ab-up {background-color:#c00}
	.hvut-ab-tree > img[src*='/td'] {filter:brightness(250%)}

	.hvut-ab-bar {line-height:30px; font-size:10pt}
	.hvut-ab-bu {color:#333; display:block}
	.hvut-ab-bux {color:#999; display:block; cursor:not-allowed}
	.hvut-ab-bx {color:#999}

	#ability_treepane > div > div:nth-child(1) {padding-top:13px}
	.hvut-ab-unslotted {display:block; margin-top:-6px}
	.hvut-ab-unslotted::before {content:'unslotted'; display:inline-block; margin-bottom:2px; padding:1px 3px; border-radius:2px; background-color:#c00; color:#fff; font-size:10pt}
	.hvut-ab-unleveled {display:block; margin-top:-6px}
	.hvut-ab-unleveled::before {content:'level up'; display:inline-block; margin-bottom:2px; padding:1px 3px; border-radius:2px; background-color:#c00; color:#fff; font-size:10pt}
`);

$qsa("#ability_top div[onmouseover*='overability']").forEach(function(div){
	var exec = /overability\(\d+, '([^']+)'.+?(?:(Not Acquired)|Requires <strong>Level (\d+))/.exec(div.getAttribute("onmouseover")),
		name = exec[1],
		ab = _ab.data[name];

	ab.slotted = true;
	ab.level = exec[2] ? 0 : ab.tier.indexOf(parseInt(exec[3])) + 1;
	ab.max = ab.tier.length;
	ab.limit = ab.tier.findIndex(e=>e>_player.level);
	if(ab.limit === -1) {
		ab.limit = ab.max;
	}

	_ab.preset.CURRENT.push(name);
	if(ab.level) {
		_ab.level[name] = ab.level;
	}

	var span = $element("span",div,[".hvut-ab-slot"]);
	if(ab.level === ab.max) {
		span.textContent = "max";
		span.classList.add("hvut-ab-max");
	} else if(ab.level === ab.limit) {
		span.textContent = ab.level+"/"+ab.max;
		span.classList.add("hvut-ab-limit");
	} else {
		span.textContent = ab.level+"/"+ab.max;
		span.classList.add("hvut-ab-up");
		$qsa("#ability_treelist > div")[ ["General","One-handed","Two-handed","Dual-wielding","","Staff","Cloth Armor","Light Armor","Heavy Armor","Deprecating 1","Deprecating 2","Supportive 1","Supportive 2","Elemental","Forbidden","Divine"].indexOf(ab.category) ].classList.add("hvut-ab-tree");
	}
});
setValue("ab_level",_ab.level);

$qsa("#ability_treepane > div").forEach(function(div){
	var name = div.firstElementChild.textContent,
		ab = _ab.data[name],
		point = _ab.point;

	ab.div = div;
	ab.id = /do_unlock_ability\((\d+)\)/.test(div.children[2].getAttribute("onclick")||"") && RegExp.$1;
	ab.level = 0;

	Array.from(div.children[2].children).forEach(function(button,i){
		var type = /(.)\.png/.test(button.style.backgroundImage) && RegExp.$1;
		if(!type) {
			return;
		}
		button.classList.add("hvut-ab-bar");

		if(type === "f") {
			ab.level++;
		} else if(type === "u") {
			point -= ab.point[i];
			if(point < 0) {
				$element("span",button,[".hvut-ab-bux"," "+ab.point[i]]);
			} else {
				$element("span",button,[".hvut-ab-bu"," "+ab.point[i]]);
				button.addEventListener("click",function(e){e.stopPropagation();_ab.unlock(ab,i+1);},true);
			}
		} else if(type === "x") {
			$element("span",button,[".hvut-ab-bx"," "+ab.point[i]+" ("+ab.tier[i]+")"]);
		}
	});

	if(ab.level) {
		if(!ab.slotted) {
			div.firstElementChild.firstElementChild.classList.add("hvut-ab-unslotted");
		} else if(ab.level !== ab.limit) {
			div.firstElementChild.firstElementChild.classList.add("hvut-ab-unleveled");
		}
	}
});

$element("input",$id("ability_outer"),{type:"button",value:"Ability Calculator",style:"position:absolute;top:20px;left:-80px;width:80px;white-space:normal"},function(){_ab.calc.toggle();});

_ab.calc.node.div = $element("div",$id("mainpane"),[".hvut-ab-calc hvut-none"]);

} else
// [END 3] Character - Abilities */


//* [4] Character - Training
if(settings.training && _query.s==="Character" && _query.ss==="tr") {

_tr.data = {
	"Adept Learner":{id:50,b:100,l:50,e:0.000417446},
	"Assimilator":{id:51,b:50000,l:50000,e:0.0057969565},
	"Ability Boost":{id:80,b:100,l:100,e:0.0005548607},
	"Manifest Destiny":{id:81,b:1000000,l:1000000,e:0},
	"Scavenger":{id:70,b:500,l:500,e:0.0088310825},
	"Luck of the Draw":{id:71,b:2000,l:2000,e:0.0168750623},
	"Quartermaster":{id:72,b:5000,l:5000,e:0.017883894},
	"Archaeologist":{id:73,b:25000,l:25000,e:0.030981982},
	"Metabolism":{id:84,b:1000000,l:1000000,e:0},
	"Inspiration":{id:85,b:2000000,l:2000000,e:0},
	"Scholar of War":{id:90,b:30000,l:10000,e:0,disabled:true},
	"Tincture":{id:91,b:30000,l:10000,e:0,disabled:true},
	"Pack Rat":{id:98,b:10000,l:10000,e:0,disabled:true},
	"Dissociation":{id:88,b:1000000,l:1000000,e:0},
	"Set Collector":{id:96,b:12500,l:12500,e:0}
};

_tr.change = function(name,level) {
	var data = _tr.data[name];
	if(!data || data.disabled) {
		_tr.node.select.value = "";
		_tr.node.level.value = "";
		_tr.node.level.disabled = true;
		_tr.node.cost.value = "";
		return;
	}
	if(!level) {
		level = data.level;
	}
	_tr.node.select.value = name;
	_tr.node.level.value = level;
	_tr.node.level.min = data.level;
	_tr.node.level.max = data.max;
	_tr.node.level.disabled = false;
	_tr.calc();
};

_tr.calc = function() {
	var name = _tr.node.select.value,
		to = parseInt(_tr.node.level.value);
	if(!name || !to) {
		return;
	}

	var data = _tr.data[name],
		level = data.level,
		b = data.b,
		l = data.l,
		e = data.e,
		c = 0;

	if(name === _tr.current) {
		level++;
	}

	while(level < to) {
		c += Math.round(Math.pow(b+l*level,1+e*level));
		level++;
	}

	_tr.node.cost.value = c.toLocaleString();
};

_tr.set = function(reload) {
	if(_tr.node.select.value) {
		_tr.json.next_name = _tr.node.select.value;
		_tr.json.next_level = parseInt(_tr.node.level.value);
		_tr.json.next_id = _tr.data[_tr.node.select.value].id;
	} else {
		_tr.json.next_name = "";
		_tr.json.next_level = 0;
		_tr.json.next_id = 0;
	}
	setValue("tr_save",_tr.json);

	if(reload) {
		location.href = location.href;
	}
};

_tr.cancel = function(reload) {
	_tr.node.select.value = "";
	_tr.set(reload);
};

GM_addStyle(`
	#train_table > tbody > tr > td:last-child {width:100px; padding-right:10px}
	#train_table > tbody > tr:last-child > td {font-weight:bold}
`);

_tr.node = {};
_tr.node.div = $element("div",[$id("train_outer"),"afterbegin"],["!margin:5px"+(settings.trainingTimer?"":";display:none")]);
_tr.node.select = $element("select",_tr.node.div,null,{change:function(){_tr.change(this.value);}});
_tr.node.level = $element("input",_tr.node.div,{type:"number",disabled:true,style:"width:50px;text-align:right"},{input:_tr.calc});
$element("input",_tr.node.div,{type:"button",value:"Set"},function(){_tr.set(true);});
_tr.node.cost = $element("input",_tr.node.div,{type:"text",readOnly:true,style:"width:80px;text-align:right"});
$element("input",_tr.node.div,{type:"button",value:"Cancel Reservation"},function(){_tr.cancel(true);});

_tr.json = getValue("tr_save",{});
_tr.current = $id("train_progress") && $qs("#train_progress > div:nth-child(2) > :first-child").textContent;
_tr.level = {};
_tr.spent = 0;

$element("option",_tr.node.select,{text:"[RESERVE TRAINING]",value:""});

if($id("train_progress")) {
	confirm_event($qs("img[src$='/canceltrain.png']"),"click","Are you sure you want to cancel the current training?",null,function(){_tr.cancel();});
}

Array.from($id("train_table").rows).forEach(function(tr,i){
	if(!i) {
		$element("th",tr);
		$element("th",tr,["/<div class='fc2 fac fcb'><div>Spent Credits</div></div>"]);
		return;
	}
	var cells = tr.cells,
		name = cells[0].textContent.trim(),
		level = parseInt(cells[4].textContent),
		max = parseInt(cells[6].textContent),
		td = $element("td",tr);

	_tr.level[name] = level;

	var data = _tr.data[name];
	if(!data) {
		return;
	}
	data.level = level;
	data.max = max;
	if(!data.disabled) {
		$element("option",_tr.node.select,{text:name,value:name});
		cells[0].firstElementChild.firstElementChild.classList.add("hvut-cphu");
		cells[0].firstElementChild.firstElementChild.addEventListener("click",function(){_tr.change(name);});
	}

	var spent = 0;
	while(level--) {
		spent += Math.round(Math.pow(data.b+data.l*level,1+data.e*level));
	}
	_tr.spent += spent;
	td.innerHTML = "<div class='fc4 far fcb'><div>" + spent.toLocaleString() + "</div></div>";
});
setValue("tr_level",_tr.level);
$element("td",$element("tr",$id("train_table").tBodies[0]),{innerHTML:"<div class='fc4 far fcb'><div>Total " + _tr.spent.toLocaleString() + "</div></div>",colSpan:9});

if(_tr.current && _tr.data[_tr.current]) {
	_tr.json.current_name = _tr.current;
	_tr.json.current_level = _tr.data[_tr.current].level;
	_tr.json.current_end = _window.end_time*1000;
} else {
	_tr.json.current_name = "";
	_tr.json.current_level = 0;
	_tr.json.current_end = 0;
}
if(_tr.json.next_name) {
	if(_tr.data[_tr.json.next_name].level < _tr.json.next_level) {
		_tr.change(_tr.json.next_name,_tr.json.next_level);
	} else {
		_tr.json.next_name = "";
		_tr.json.next_level = 0;
		_tr.json.next_id = 0;
	}
}
_tr.json.error = "";
setValue("tr_save",_tr.json);

} else
// [END 4] Character - Training */


//* [5] Character - Item Inventory
if(settings.itemInventory && _query.s==="Character" && _query.ss==="it") {

_it.inventory = {};
_it.node = {};
_it.template = getValue("it_template",settings.itemCodeTemplate.trim());

_it.edit_template = function() {
	popup_text(_it.template,"width:600px;height:500px;white-space:pre",
	[{value:"Save Template",click:function(w,t){
		_it.template = t.value || settings.itemCodeTemplate.trim();
		setValue("it_template",_it.template);
		w.remove();
		_it.node.wts.remove();
		_it.wts();
	}},
	{value:"Default",click:function(w,t){t.value=settings.itemCodeTemplate.trim();}}]);
};

_it.wts = function() {
	var prices = $prices.get("WTS"),
		code = [],
		option = {
			"itemCode": "{$zero:[color=transparent]$zero[/color]}{$count} x {$name}{$price: @ $price}",
			"unpricedCode": "{$zero}{$count} x {$name} (offer)",
			"stockoutCode": "[color=#999][s]{$zero}{$count} x {$name}{$price: @ $price}[/s][/color]",
		};

	_it.template.split("\n").forEach(function(s,i,a){
		if(s.startsWith("//")) {
			if(/^\/\/\s*set (\w+)\s*=\s*([^;]*)/.test(s)) {
				var o = RegExp.$1,
					v = RegExp.$2;
				if(o==="keepStock" && /^min\((.+)\)/.test(v)) {
					v = Math.min(...RegExp.$1.split(",").map(n=>_it.inventory[n.trim()]||0));
				} else if(o==="stockDigits" && v==="block") {
					var array = a.slice(i+1),
						next = array.findIndex(s=>/^\/\/\s*set stockDigits/.test(s));
					if(next !== -1) {
						array = array.slice(0,next);
					}
					v = Math.max(...array.map(s=>(/\{([^{}]+)\}/.test(s)&&_it.inventory[RegExp.$1]||0).toString().length));
				}
				option[o] = v&&!isNaN(v) ? Number(v) : v;
			}
			return;
		}

		if(/\{([^{}]+)\}/.test(s)) {
			var name = RegExp.$1,
				count = _it.inventory[name] || 0,
				zero,
				price = prices[name];

			if(option.keepStock) {
				count = Math.max(0,count-option.keepStock);
			}
			if(!option.unpricedCode && !(name in prices) || !option.stockoutCode && !count) {
				code.push("__SKIP__");
				return;
			}
			if((zero=option.stockDigits-count.toString().length) > 0) {
				zero = "0".repeat(zero);
			} else {
				zero = undefined;
			}
			if(price >= 1000000) {
				price = price/1000000 + "m";
			} else if(price >= 1000) {
				price = price/1000 + "k";
			}

			var replace = {name:name,count:count,zero:zero,price:price},
				itemcode = !count?option.stockoutCode: !(name in prices)?option.unpricedCode: option.itemCode,
				text = itemcode.toString().replace(/\{\$(\w+)(?::(.*?))?\}/g,function(s,a,b){var r=replace[a];return r===undefined?"" : b===undefined?r : b.replace(/\$(\w+)/g,(s,a)=>{var r=replace[a];return r===undefined?"":r;});}).trim();
			code.push(s.replace("{"+name+"}",text));

		} else {
			code.push(s);
		}
	});
	code = code.join("\n").replace(/(?:__SKIP__|\n)+/g,s=>s.split("__SKIP__").sort().pop());

	_it.node.wts = popup_text(code,"width:600px;height:500px;white-space:pre",[{value:"Edit Template",click:function(){_it.edit_template();}}]);
};

_it.crystal = function() {
	var pa = ["Crystal of Vigor","Crystal of Finesse","Crystal of Swiftness","Crystal of Fortitude","Crystal of Cunning","Crystal of Knowledge"],
		er = ["Crystal of Flames","Crystal of Frost","Crystal of Lightning","Crystal of Tempest","Crystal of Devotion","Crystal of Corruption"],
		pa_min = Math.min(...pa.map(n=>_it.inventory[n])),
		er_min = Math.min(...er.map(n=>_it.inventory[n])),
		pa_extra = pa.map(n=>(_it.inventory[n]-pa_min)+" x "+n),
		er_extra = er.map(n=>(_it.inventory[n]-er_min)+" x "+n);

	popup_text([
		"[Primary attributes]","",
		...pa_extra,"",
		"[Elemental mitigation]","",
		...er_extra
	],"width:300px;max-height:500px;white-space:pre");
};


GM_addStyle(`
	#item_outer {margin-left:120px}
	#item_left {width:405px}
	#item_left .cspp {overflow-y:scroll}
	#item_list .itemlist > tbody > tr > td:last-child {width:65px}
	#item_right {width:605px}
	#item_slots {height:572px; margin-top:19px}
	#item_slots > div {width:300px}
	.sa {height:30px; margin:8px auto; line-height:20px}
	.sa > div:first-child {padding:5px 15px}
	.sa > div:last-child {height:30px}
	.sa > div:last-child > div {padding:5px}

	.hvut-it-side {position:absolute; top:53px; left:-100px; width:100px; display:flex; flex-direction:column}
	.hvut-it-side input {margin:3px 0; white-space:normal}
	.hvut-it-side select {margin:3px 0}
`);

Array.from($qs(".itemlist").rows).forEach(function(tr){
	var div = tr.cells[0].firstElementChild,
		type = /'([^']+)'\)/.test(div.getAttribute("onmouseover")) && RegExp.$1.replace(/\W/g,"_") || "Consumable",
		name = div.textContent.trim(),
		count = parseInt(tr.cells[1].textContent);
	_it.inventory[name] = count;
	tr.classList.add("hvut-it-"+type);
});
_it.inventory["Crystal Pack"] = Math.floor(Math.min(...["Crystal of Vigor","Crystal of Finesse","Crystal of Swiftness","Crystal of Fortitude","Crystal of Cunning","Crystal of Knowledge","Crystal of Flames","Crystal of Frost","Crystal of Lightning","Crystal of Tempest","Crystal of Devotion","Crystal of Corruption"].map(n=>{if(!_it.inventory[n]){_it.inventory[n]=0;}return _it.inventory[n];}))/1000);

_it.node.side = $element("div",$id("item_outer"),[".hvut-it-side"]);
$element("input",_it.node.side,{type:"button",value:"WTS Code"},_it.wts);
$element("input",_it.node.side,{type:"button",value:"Edit WTS Price"},function(){$prices.edit("WTS");});
_it.node.side.appendChild($prices.selector());

if(settings.itemInventoryExtraCrystals) {
	$element("input",_it.node.side,{type:"button",value:"Extra Crystals"},_it.crystal);
}

} else
// [END 5] Character - Item Inventory */


//* [6] Character - Equip Inventory
if(settings.equipInventory && _query.s==="Character" && _query.ss==="in") {

_in.filter = _query.filter || "1handed";
_in.uid = _window.uid;
_in.simple_token = _window.simple_token;
_in.json = getValue("in_json",{});
_in.group = {"1handed":{},"2handed":{},"staff":{},"shield":{},"acloth":{},"alight":{},"aheavy":{}};
_in.node = {};
_in.equipcode = getValue("in_equipcode",settings.equipCode);
_in.list = [];

_in.get_list = function(outer,g) {
	var list = $equip.list(outer,1,_in.group[g].div);
	_in.group[g].list = list;
	_in.list = _in.list.concat(list);
	_in.check_name(list);
	var popup_reg = /(equips\.set\(\d+),'(?:inv_equip|inv_eqstor)',\d+,\d+\)/,
		popup_replace = ["acloth","alight","aheavy"].includes(g) ? "$1,'inv_eqstor',295,200)" : "$1,'inv_equip',498,200)";

	list.forEach(function(eq){
		eq.json = _in.json[eq.info.eid] || {};
		if(!eq.node.div.previousElementSibling) {
			$element("div",[eq.node.wrapper,"afterbegin"],[".hvut-in-is"]);
		}
		eq.node.lock = eq.node.wrapper.firstElementChild;
		eq.node.sub = $element("div",[eq.node.div,"beforebegin"],[".hvut-in-sub"+(eq.info.tradeable?"":" hvut-in-untrade")]);
		eq.node.eid = $element("span",eq.node.sub,[" "+eq.info.eid,".hvut-in-eid"]);
		eq.node.check = $element("input",eq.node.sub,{type:"checkbox",className:"hvut-in-check",checked:eq.json.checked});
		eq.node.price = $element("input",eq.node.sub,{type:"text",className:"hvut-in-price",placeholder:"$price",value:eq.json.price||""});
		eq.node.note = $element("input",eq.node.sub,{type:"text",className:"hvut-in-note",placeholder:"$note",value:eq.json.note||""});
		eq.node.lpr = $element("input",eq.node.sub,{type:"button",className:"hvut-in-lpr",value:"<"},function(){_in.get_pr(eq);});

		eq.node.div.setAttribute("onmouseover", eq.node.div.getAttribute("onmouseover").replace(popup_reg,popup_replace));
	});
	return list;
};

_in.sort_list = function(key) {
	if(key === "category") {
		Object.values(_in.group).forEach(function(g){
			$equip.list(g.div,1);
		});
		_in.set_rangeselect();
		return;
	}
	var func =
		key==="name" ? function(a,b){return ($equip.sort.quality[a.info.quality]-$equip.sort.quality[b.info.quality]) || (a.info.name>b.info.name?1:a.info.name<b.info.name?-1:b.info.eid-a.info.eid);} :
		key==="eid" ? function(a,b){return b.info.eid-a.info.eid;} :
		null;
	Object.values(_in.group).forEach(function(g){
		var div = g.div;
		div.style.display = "none";
		div.innerHTML = "";
		g.list.sort(func);
		g.list.forEach(function(eq){
			eq.node.wrapper.classList.remove("hvut-eq-border");
			div.appendChild(eq.node.wrapper);
		});
		div.style.display = "";
	});
	_in.set_rangeselect();
};

_in.check_list = function(t,c) {
	if(t === "all") {
		_in.list.forEach(eq=>{eq.node.check.checked=c;});
	} else if(t === "tradeable") {
		_in.list.forEach(eq=>{if(eq.info.tradeable){eq.node.check.checked=c;}});
	} else if(t === "reverse") {
		_in.list.forEach(eq=>{eq.node.check.checked=!eq.node.check.checked;});
	}
};

_in.get_pr = function(eq) {
	eq.node.lpr.value = "...";
	var ifr = $element("iframe",document.body,{style:"display:none"},{load:function(){
		var d = this.contentDocument;
		if(!d.location.href.includes("/equip/")) {
			return;
		}
		var value = eq.node.note.value,
			count = 0,
			tid = setInterval(function(){
				var div = $id("summaryDiv",d);
				if(div) {
					var summary = div.textContent;
					eq.node.note.value = summary;
					if(summary.match(/^(Soulbound|Unassigned|\d+)/)) {
						clearInterval(tid);
						eq.node.lpr.value = "<";
						ifr.remove();
						return;
					} else if(summary.match(/Response Error|Request Error|No data available|Unknown equip type/)) {
						count = 9999;
					} else if(summary.match(/Getting ranges/)) {
						count++;
					} else {
						count++;
					}
				} else {
					count++;
				}
				if(count > 100) {
					clearInterval(tid);
					eq.node.lpr.value = "<";
					eq.node.note.style.width = "250px";
					if(!div) {
						eq.node.note.value = "Couldn't load Percentile Range script";
					}
					setTimeout(function(){
						eq.node.note.value = value;
						eq.node.note.style.width = "";
						ifr.remove();
					},3000);
				}
			},100);
	}});
	ifr.src = "equip/"+eq.info.eid+"/"+eq.info.key;
};

_in.set_rangeselect = function() {
	var inv_equip = [],
		inv_eqstor = [];
	$qsa("#eqinv_outer .eqp").forEach(function(w){
		if((w.firstElementChild.getAttribute("onclick")||"").includes("equips.lock")) {
			inv_equip.push(w.lastElementChild.dataset.eid);
		} else {
			inv_eqstor.push(w.lastElementChild.dataset.eid);
		}
	});
	_window.rangeselect.inv_equip = inv_equip;
	_window.rangeselect.inv_eqstor = inv_eqstor;
};

_in.wts = function() {
	var list = _in.list.filter(e=>e.node.check.checked),
		eid_maxlen = Math.max(...list.map(e=>e.info.eid.toString().length)),
		code = "",
		code_new = "",
		code_featured = "";

	list.sort(function(a,b){
		var sorter = $equip.sort;
		if(a.info.category !== b.info.category) {
			return sorter.category[a.info.category] - sorter.category[b.info.category];
		} else if(a.info.category === "Obsolete") {
			return a.info.name>b.info.name?1 : a.info.name<b.info.name?-1 : 0;
		} else if(a.info.type !== b.info.type) {
			return sorter.type[a.info.type] - sorter.type[b.info.type];
		}

		var sortkey =
			a.info.category==="One-handed Weapon" || a.info.category==="Two-handed Weapon" ? ["suffix","quality","prefix"] :
			a.info.category==="Staff" ? ["prefix","suffix","quality"] :
			a.info.type==="Buckler" ? ["suffix","quality","prefix"] :
			a.info.category==="Shield" ? ["quality","suffix","prefix"] :
			a.info.category==="Cloth Armor" ? ["suffix","slot","quality","prefix"] :
											["slot","suffix","quality","prefix"];

		var r = 0;
		sortkey.some(function(e){
			if(e in sorter) {
				r = (sorter[e][a.info[e]]||99) - (sorter[e][b.info[e]]||99);
			} else {
				r = a.info[e]>b.info[e]?1 : a.info[e]<b.info[e]?-1 : 0;
			}
			if(r) {
				return true;
			} else {
				return false;
			}
		});

		return r || (b.info.eid-a.info.eid);
	});

	list.forEach(function(eq,i,a){
		var p = a[i-1] || {info:{}};
		if(eq.info.category !== p.info.category) {
			code += "\n\n\n[size=3][b][" + eq.info.category + "][/b][/size]\n";
		}

		switch(eq.info.category) {

		case "Obsolete":
			break;

		case "One-handed Weapon":
		case "Two-handed Weapon":
			if(eq.info.type !== p.info.type) {
				code += "\n\n[size=2][b][" + eq.info.type + "][/b][/size]\n\n";
			} else if(eq.info.suffix !== p.info.suffix) {
				code += "\n";
			}
			break;

		case "Staff":
			if(eq.info.type !== p.info.type) {
				code += "\n\n[size=2][b][" + eq.info.type + "][/b][/size]\n\n";
			} else if(eq.info.prefix !== p.info.prefix) {
				code += "\n";
			}
			break;

		case "Shield":
			if(eq.info.type !== p.info.type) {
				code += "\n\n[size=2][b][" + eq.info.type + "][/b][/size]\n\n";
			}
			break;

		case "Cloth Armor":
			if(eq.info.type !== p.info.type || eq.info.suffix !== p.info.suffix) {
				code += "\n\n[size=2][b][" + (eq.info.suffix||"No suffix") + "][/b][/size]\n\n";
			} else if(eq.info.slot !== p.info.slot) {
				//code += "\n";
			}
			break;

		case "Light Armor":
		case "Heavy Armor":
			if(eq.info.type !== p.info.type) {
				code += "\n\n[size=2][b][" + eq.info.type + "][/b][/size]\n\n";
			} else if(eq.info.slot !== p.info.slot) {
				code += "\n";
			}
			break;

		}

		var eid_len = eq.info.eid.toString().length;
		eq.data._eid = (eid_maxlen>eid_len?"[color=transparent]"+"_".repeat(eid_maxlen-eid_len)+"[/color]":"") + eq.info.eid;
		if(!eq.data.url) {
			eq.data.url = location.origin+location.pathname+"equip/"+eq.info.eid+"/"+eq.info.key;
		}
		if(!eq.data.bbcode) {
			eq.data.bbcode = $equip.bbcode(eq);
		}
		if(!eq.info.level) {
			eq.data.level = "-";
		}
		eq.data.price = eq.node.price.value;
		eq.data.note = eq.node.note.value;

		if(eq.data.note.includes("$featured;")) {
			eq.data["featured"] = true;
			eq.data.note = eq.data.note.replace("$featured;","");
		} else {
			eq.data["featured"] = false;
		}
		if(eq.data.note.includes("$new;")) {
			eq.data["new"] = true;
			eq.data.note = eq.data.note.replace("$new;","");
		} else {
			eq.data["new"] = false;
		}

		var equipcode = _in.equipcode.replace(/\{\$(\w+)(?::(.*?))?\}/g,function(s,a,b){var v=(a in eq.data)?eq.data[a]:(a in eq.info)?eq.info[a]:"";return !v?"" : !b?v : b.replace(/\$(\w+)/g,(s,a)=>{var v=(a in eq.data)?eq.data[a]:(a in eq.info)?eq.info[a]:"";return v;});}).trim();

		code += equipcode + "\n";
		if(eq.data["new"]) {
			code_new += equipcode + "\n";
		}
		if(eq.data["featured"]) {
			code_featured += equipcode + "\n";
		}
	});

	if(code_featured) {
		code = "\n\n\n[size=3][b][Featured][/b][/size]\n\n" + code_featured + code;
	}
	if(code_new) {
		code = "\n\n\n[size=3][b][Newly Added][/b][/size]\n\n" + code_new + code;
	}
	_in.node.wts = popup_text(code.trim()||"No equipment selected.","width:1000px;height:500px;white-space:pre",[{value:"Edit Equip Code",click:function(){_in.edit_code();}}]);
};

_in.edit_code = function() {
	var comment = `
		// {$name}     equipment name
		// {$bbcode}   equipment name in colors/bold
		// {$url}      equipment url
		// {$eid}      equipment id
		// {$_eid}     it pads $eid with spaces for layout
		// {$level}    equipment level
		// {$tier}     potency tier (iw level)
		// {$price}    the value of the 'price' input field
		// {$note}     the value of the 'note' input field
		//             if it contains '$featured;', the equip code will be added to 'Featured' section
		//             if it contains '$new;', the equip code will be added to 'Newly Added' section
		// {$condition:expression}
		//             if $condition is a valid value, it prints the expression
		//             for example, {$tier: (IW $tier)} prints ' (IW 9)' if $tier is 9
		//             while it does nothing if $tier is 0
	`.replace(/\t/g,"").trim();
	popup_text(comment+"\n\n"+_in.equipcode,"width:800px;max-height:500px;white-space:pre",
	[{value:"Save Equip Code",click:function(w,t){
		_in.equipcode = (t.value||settings.equipCode).split("\n").filter(e=>!e.startsWith("//")).join("\n").trim();
		setValue("in_equipcode",_in.equipcode);
		w.remove();
		_in.node.wts.remove();
		_in.wts();
	}},
	{value:"Default",click:function(w,t){t.value=comment+"\n\n"+settings.equipCode;}}]);
};

_in.save = function() {
	_in.json = {};
	_in.list.forEach(function(eq){
		_in.json[eq.info.eid] = {checked:eq.node.check.checked,price:eq.node.price.value,note:eq.node.note.value};
	});
	setValue("in_json",_in.json);
};

_in.load = function() {
	_in.list.forEach(function(eq){
		var j = _in.json[eq.info.eid] || {};
		eq.node.check.checked = j.checked;
		eq.node.price.value = j.price || "";
		eq.node.note.value = j.note || "";
	});
};

_in.check_name = function(list) {
	var count = list.length;
	list.forEach(function(eq){
		if(eq.info.tier < 10 || $equip.names[eq.info.eid]) {
			count--;
		} else {
			$ajax.add("GET","equip/"+eq.info.eid+"/"+eq.info.key,null,function(r){
				var doc = $doc(r.responseText),
					equipname = Array.from($id("equip_extended",doc).previousElementSibling.firstElementChild.children).map(d=>d.textContent.trim()||" ").join("").replace(/\b(Of|The)\b/,s=>s.toLowerCase());
				if(equipname && equipname !== eq.info.name) {
					eq.info.customname = eq.info.name;
					eq.info.name = equipname;
				}
				$equip.names[eq.info.eid] = eq.info.name;
        setValue('equipnames', $equip.names);
			});
		}
	});
};


if(settings.equipInventoryIntegration) {
  let filterbar = $id("filterbar");
  if (filterbar.firstElementChild.nodeName === 'DIV') {
    GM_addStyle(`
      #filterbar > div {width:150px !important}
    `);
    $element("div",[filterbar,"afterbegin"],[".cfbs","!cursor:pointer","/<div class='fc4 fac fcb'><div>All</div></div>"],function(){location.href="?s=Character&ss=in";});
    if(_query.filter) {
      filterbar.children[0].classList.remove("cfbs");
      filterbar.children[0].classList.add("cfb");
      filterbar.children[0].children[0].classList.remove("fc4");
      filterbar.children[0].children[0].classList.add("fc2");
    } else {
      filterbar.children[1].classList.remove("cfbs");
      filterbar.children[1].classList.add("cfb");
      filterbar.children[1].children[0].classList.remove("fc4");
      filterbar.children[1].children[0].classList.add("fc2");
    }
  } else if (filterbar.firstElementChild.nodeName === 'A') {
    $element('a', [filterbar, 'afterbegin'], { href: '?s=Character&ss=in', innerHTML: '<div>All</div>' });
    if (_query.filter) {
      filterbar.children[0].children[0].classList.add('cfb');
    } else {
      filterbar.children[0].children[0].classList.add('cfbs');
      filterbar.children[1].children[0].classList.remove('cfbs');
      filterbar.children[1].children[0].classList.add('cfb');
    }
  }
}

if(_query.filter || !settings.equipInventoryIntegration) {
	$equip.list($id("inv_equip"),1);
	$equip.list($id("inv_eqstor"),1);
	_in.set_rangeselect();

} else {

	GM_addStyle(`
		#eqinv_outer {position:relative; width:1120px; margin:0 0 0 100px}
		.eqinv_pane {margin:0 5px}
		.eqinv_pane > div:first-child {display:none}
		.eqinv_pane .cspp {margin-top:15px; overflow-y:scroll}

		.hvut-in-side {position:absolute; top:44px; left:-95px; width:80px; display:flex; flex-direction:column}
		.hvut-in-side input[type='button'] {margin:3px 0; white-space:normal}
		.hvut-in-side label {margin:2px 0; line-height:16px; white-space:nowrap; text-align:left}
		.hvut-in-header {position:absolute; top:5px; left:0; width:100%; text-align:center; font-size:10pt; font-weight:bold}
		.hvut-in-header > span {margin:10px; padding:3px 5px; border:1px solid}
		.hvut-eq-category > span {margin-left:10px; font-size:10pt; font-weight:normal}
		.hvut-in-is {position:absolute; top:4px; left:4px; width:10px; height:10px; border:1px dashed}
		.il, .iu, .hvut-in-is {margin-top:1px}
		.hvut-in-ii {display:inline-block; position:relative; width:22px; height:20px; padding:1px 0; vertical-align:middle}

		.hvut-in-sub {position:absolute; right:0; z-index:1}
		.hvut-in-untrade {color:#c00}
		.hvut-in-untrade * {color:inherit !important}
		.hvut-in-eid {display:none; padding:0 3px; border:1px solid; line-height:20px; background-color:#fff}
		.eqp:hover .hvut-in-eid {display:inline-block}
		.hvut-in-sub .hvut-in-check {top:3px}
		.hvut-in-sub .hvut-in-price {width:45px; margin:0 1px; text-align:right}
		.hvut-in-sub .hvut-in-note {width:75px; margin:0 1px}
		.hvut-in-sub .hvut-in-lpr {display:none; width:22px; margin:0 1px; padding:1px 0; border-radius:0; border-color:currentColor !important}
		.hvut-in-sub:hover > .hvut-in-note {width:51px}
		.hvut-in-sub:hover > .hvut-in-lpr {display:inline-block}
	`);

	$id("eqinv_bot").firstElementChild.firstElementChild.firstElementChild.prepend(
		$element("div",null,[".hvut-in-ii","/<div class='il'></div>"]),
		$element("div",null,[".hvut-in-ii","/<div class='iu'></div>"])
	);
	$id("eqinv_bot").lastElementChild.firstElementChild.firstElementChild.prepend(
		$element("div",null,[".hvut-in-ii","/<div class='hvut-in-is'></div>"])
	);

	_in.node.side = $element("div",$id("eqinv_outer"),[".hvut-in-side"]);
	$element("input",_in.node.side,{type:"button",value:"sort by category"},function(){_in.sort_list("category");});
	$element("input",_in.node.side,{type:"button",value:"sort by name"},function(){_in.sort_list("name");});
	$element("input",_in.node.side,{type:"button",value:"sort by eid"},function(){_in.sort_list("eid");});
	$element("input",_in.node.side,{type:"button",value:"WTS Code"},_in.wts);
	$element("input",[$element("label",_in.node.side,"Select All"),"afterbegin"],{type:"checkbox"},function(){_in.check_list("all",this.checked);});
	$element("input",[$element("label",_in.node.side,"Tradeables"),"afterbegin"],{type:"checkbox"},function(){_in.check_list("tradeable",this.checked);});
	$element("input",[$element("label",_in.node.side,"Reverse"),"afterbegin"],{type:"checkbox"},function(){_in.check_list("reverse");});
	$element("input",_in.node.side,{type:"button",value:"Save"},_in.save);
	$element("input",_in.node.side,{type:"button",value:"Load"},_in.load);

	_in.node.inven_header = $element("div",[$id("inv_equip").parentNode,"afterbegin"],[".hvut-in-header hvut-cphu-sub"]);
	_in.node.storage_header = $element("div",[$id("inv_eqstor").parentNode,"afterbegin"],[".hvut-in-header hvut-cphu-sub"]);

	_in.group["1handed"].scroll = $element("span",_in.node.inven_header,$equip.alias["1handed"],function(){scrollIntoView(_in.group["1handed"].header);});
	_in.group["1handed"].header = $element("h4",$id("inv_equip"),[" "+$equip.alias["1handed"],".hvut-eq-category"]);
	_in.group["1handed"].status = $element("span",_in.group["1handed"].header);
	_in.group["1handed"].div = $element("div",$id("inv_equip"),[".equiplist nosel"]);

	["2handed","staff","shield"].forEach(function(g){
		_in.group[g].scroll = $element("span",_in.node.inven_header,$equip.alias[g],function(){scrollIntoView(_in.group[g].header);});
		_in.group[g].header = $element("h4",$id("inv_equip"),[" "+$equip.alias[g],".hvut-eq-category"]);
		_in.group[g].status = $element("span",_in.group[g].header,"Loading...");
		_in.group[g].div = $element("div",$id("inv_equip"),[".equiplist nosel"]);
	});

	["acloth","alight","aheavy"].forEach(function(g){
		_in.group[g].scroll = $element("span",_in.node.storage_header,$equip.alias[g],function(){scrollIntoView(_in.group[g].header);});
		_in.group[g].header = $element("h4",$id("inv_eqstor"),[" "+$equip.alias[g],".hvut-eq-category"]);
		_in.group[g].status = $element("span",_in.group[g].header,"Loading...");
		_in.group[g].div = $element("div",$id("inv_eqstor"),[".equiplist nosel"]);
	});

	_in.tab_loaded = 1;

	_in.get_list($id("eqinv_outer"),"1handed");
	["2handed","staff","shield","acloth","alight","aheavy"].forEach(function(g){
		$ajax.add("GET","?s=Character&ss=in&filter="+g,null,
			function(r){
				var html = r.responseText,
					doc = $doc(html);

				Object.assign($equip.dynjs_eqstore, JSON.parse(/var dynjs_eqstore = (\{.*\});/.test(html) && RegExp.$1));
				_in.get_list($id("eqinv_outer",doc), g);
				_in.group[g].status.textContent = "";

				if(++_in.tab_loaded === 7) {
					_in.set_rangeselect();
				}
			},
			function(){_in.group[g].status.textContent = "ERROR";}
		);
	});
}

} else
// [END 6] Character - Equip Inventory */


//* [7] Character - Settings
if(settings.settings && _query.s==="Character" && _query.ss==="se") {

_se.form = $qs("#settings_outer form");
_se.elements = Array.from(_se.form.elements);
_se.data = getValue("se_data",{});
_se.node = {};

_se.save = function() {
	var name = prompt("Enter the title of settings");
	if(!name || !name.trim()) {
		return;
	}
	name = name.trim();

	var json = {};
	_se.elements.forEach(function(e){
		if(e.disabled || e.type==="button" || e.type==="reset" || e.type==="image" || e.type==="submit") {
			return;
		}

		var value = e.value;
		if(e.type === "checkbox") {
			if(!e.checked) {
				return;
			}
			value = e.checked;

		} else if(e.type === "radio") {
			if(!e.checked) {
				return;
			}
		}

		json[e.name] = value;
	});

	if(!_se.data[name]) {
		_se.add(name);
	}
	_se.data[name] = json;
	setValue("se_data",_se.data);
};
_se.change = function() {
	var name = this.dataset.se,
		json = _se.data[name];
	_se.elements.forEach(function(e){
		if(e.type==="button" || e.type==="reset" || e.type==="image" || e.type==="submit") {
			return;
		}
		if(e.type === "checkbox") {
			e.checked = json[e.name];
		} else if(e.type === "radio") {
			e.checked = json[e.name] === e.value;
		} else {
			e.value = json[e.name];
		}
	});
};
_se.add = function(name) {
	_se.node[name] = $element("input",_se.div,{type:"button",value:name,dataset:{se:name},className:"hvut-se-button"},_se.change);
	$element("input",_se.div,{type:"button",value:"x",dataset:{se:name},className:"hvut-se-remove"},_se.remove);
};
_se.remove = function() {
	var name = this.dataset.se;
	delete _se.data[name];
	setValue("se_data",_se.data);
	_se.node[name].nextElementSibling.remove();
	_se.node[name].remove();
};


GM_addStyle(`
	.hvut-se-div {margin-top:20px; padding:20px 0; border-top:3px double; text-align:left}
	.hvut-se-div .hvut-se-button {min-width:50px; margin:0 30px 10px 10px}
	.hvut-se-div .hvut-se-remove {visibility:hidden; margin-left:-30px}
	.hvut-se-button:hover + .hvut-se-remove, .hvut-se-remove:hover {visibility:visible}
`);

_se.div = $element("div",_se.form,[".hvut-se-div"]);
$element("input",_se.div,{type:"button",value:"Store Current Settings",style:"margin-bottom:15px"},_se.save);
$element("br",_se.div);

Object.keys(_se.data).forEach(function(p){
	_se.add(p);
});

_se.elements.forEach(function(e){
	if(e.nodeName === "SELECT") {
		var value = e.value,
			options = Array.from(e.options),
			frag = $element();
		options.sort((a,b)=>a.value-b.value);
		options.forEach(function(o){
			frag.appendChild(o);
		});
		e.appendChild(frag);
		e.value = value;
	}
});


_se.form.fontlocal.required = true;
_se.form.fontface.required = true;
_se.form.fontsize.required = true;
_se.form.fontface.placeholder = "Tahoma, Arial";
_se.form.fontsize.placeholder = "10";
_se.form.fontoff.placeholder = "0";

} else
// [END 7] Character - Settings */


//* [8] Bazaar - Equipment Shop
if(settings.equipmentShop && _query.s==="Bazaar" && _query.ss==="es") {

_es.filter = _query.filter || "1handed";
_es.rare_type = {"Force Shield":true,"Phase":true,"Shade":true,"Power":true};
_es.mat_type = {"1handed":"Metals","2handed":"Metals","staff":"Wood","shield":"Wood","acloth":"Cloth","alight":"Leather","aheavy":"Metals"};
_es.core_type = {"1handed":"Weapon","2handed":"Weapon","staff":"Staff","shield":"Armor","acloth":"Armor","alight":"Armor","aheavy":"Armor"};
_es.quality = {"Flimsy":1,"Crude":2,"Fair":3,"Average":4,"Fine":5,"Superior":6,"Exquisite":7,"Magnificent":8,"Legendary":9,"Peerless":10};

_es.item_pane = [];
_es.shop_pane = [];
_es.group = {};

_es.uid = _window.uid;
_es.simple_token = _window.simple_token;
_es.storetoken = $id("shopform").elements.storetoken.value;

_es.protect_filter = getValue("es_protect",settings.equipmentShopProtect);
_es.bazaar_filter = getValue("es_bazaar",settings.equipmentShopBazaar);

_es.edit_filter = function(e) {
	var comment = ({protect:"// prevent YOUR valuable equipment from being selected by \"Select All\" button",bazaar:"// check valuable equipment in BAZZAR, then hide all other trash"})[e];
	popup_text(comment+"\n\n"+_es[e+"_filter"].join("\n"),"width:750px;height:500px;white-space:pre",
	[{value:"Save Filter",click:function(w,t){
		var filter = t.value.split("\n").filter(f=>f&&!f.startsWith("//")),
			failed = [];
		filter.forEach(function(f,i){
			try {
				eval(f.toLowerCase().replace(/[a-z\- ]+/g,function(s){s=s.trim();return !s?"":"true";}));
			} catch(e) {
				failed.push("#"+(i+1)+": "+f);
			}
		});
		if(failed.length) {
			alert(failed.join("\n"));
			return;
		}
		_es[e+"_filter"] = filter;
		setValue("es_"+e,filter);
		w.remove();
	}},
	{value:"Default",click:function(w,t){t.value=comment+"\n\n"+settings["equipmentShop"+e[0].toUpperCase()+e.slice(1)].join("\n");}}]);
};

_es.showequip = function(eq) {
	var box = $id("popup_box");
	box.innerHTML = "";
	box.style.cssText = "top:110px;left:234px;width:380px;height:522px;visibility:visible";
	$element("iframe",box,{src:"equip/"+eq.info.eid+"/"+eq.info.key,style:"border:0; width:100%; height:100%; overflow:hidden"});
};

_es.transfer = function(eq,equipgroup="inv_equip") {
	if(eq.data.sold) {
		return;
	}
	eq.data.sold = true;
	eq.node.div.classList.add("hvut-es-disabled");
	eq.node.sub.classList.add("hvut-es-disabled");
	// "inv_equip": to storage, "inv_eqstor": to inventory
	$ajax.add("POST","?s=Character&ss=in","equiplist="+eq.info.eid+"&equipgroup="+equipgroup,function(){eq.node.wrapper.remove();});
};


_es.sell = function(eq,skip) {
	if(eq.data.sold) {
		return;
	}
	if(eq.node.div.dataset.locked != "0") {
		alert("This item is locked.");
		return;
	}
	if(!skip && (settings.equipmentShopConfirm===2 || settings.equipmentShopConfirm===1&&eq.data.need_salvage || settings.equipmentShopConfirm>0&&eq.data.valuable) && !confirm("["+eq.info.name+"]"+"\n\nAre you sure you want to SELL this for "+eq.data.value.toLocaleString()+" credits?"+(eq.data.need_salvage?"\nSALVAGE would give you more credit value.":""))) {
		return;
	}
	eq.data.sold = true;
	eq.node.div.classList.add("hvut-es-disabled");
	eq.node.sub.classList.add("hvut-es-disabled");
	$ajax.add("POST","?s=Bazaar&ss=es","storetoken="+_es.storetoken+"&select_group=item_pane&select_eids="+eq.info.eid,function(){eq.node.wrapper.remove();});
};

_es.salvage = function(eq,skip) {
	if(eq.data.sold) {
		return;
	}
	if(eq.node.div.dataset.locked != "0") {
		alert("This item is locked.");
		return;
	}
	if(!skip && (settings.equipmentShopConfirm===2 || settings.equipmentShopConfirm===1&&!eq.data.need_salvage || settings.equipmentShopConfirm>0&&eq.data.valuable) && !confirm("["+eq.info.name+"]"+"\n\nAre you sure you want to SALVAGE this?"+(!eq.data.need_salvage?"\nSELL would give you more credit value.":""))) {
		return;
	}
	eq.data.sold = true;
	eq.node.div.classList.add("hvut-es-disabled");
	eq.node.sub.classList.add("hvut-es-disabled");
	$ajax.add("POST","?s=Forge&ss=sa&filter="+eq.data.filter,"select_item="+eq.info.eid,function(){eq.node.wrapper.remove();});
};

_es.is_selected = function(eq) {
	return /rgb\(0,\s*48,\s*203\)|#0030CB/i.test(eq.node.div.style.color); // later, may need hex2rgb
};

_es.select_all = function(v) {
	var s = v==="salvage"?true : v==="sell"?false : "all",
		ctrlclick = new MouseEvent("click",{ctrlKey:true});
	_es.item_pane.forEach(function(eq){
		if(eq.node.div.dataset.locked != "0") {
			return;
		}
		if(eq.data.valuable) {
			if(_es.is_selected(eq)) {
				eq.node.div.dispatchEvent(ctrlclick);
			}
			return;
		}
		if(_es.is_selected(eq) !== (eq.data.need_salvage===s || s==="all")) {
			eq.node.div.dispatchEvent(ctrlclick);
		}
	});
};

_es.sell_all = function() {
	var selected = [],
		valuable = [],
		salvage = [],
		sum = 0;

	_es.item_pane.forEach(function(eq){
		if(eq.data.sold || !_es.is_selected(eq) || eq.node.div.dataset.locked != "0") {
			return;
		}
		selected.push(eq);
		if(eq.data.valuable) {
			valuable.push(eq.info.name);
		} else if(eq.data.need_salvage) {
			salvage.push(eq.info.name);
		}
		sum += eq.data.value;
	});

	if(!selected.length || (settings.equipmentShopConfirm===2 || settings.equipmentShopConfirm===1&&salvage.length || valuable.length) && !confirm("Are you sure you wish to sell "+selected.length+" equipment piece"+(selected.length>1?"s":"")+" for "+(sum.toLocaleString())+" credits?"+(valuable.length?"\n\n[Protected Equipment]\n"+valuable.join("\n"):"")+(salvage.length?"\n\n[Better to Salvage]\n"+salvage.join("\n"):""))) {
		return;
	}

	$ajax.add("POST","?s=Bazaar&ss=es","storetoken="+_es.storetoken+"&select_group=item_pane&select_eids="+selected.map(eq=>eq.info.eid).join(","),function(){location.href=location.href;});
};

_es.salvage_all = function() {
	var selected = [],
		valuable = [],
		sell = [];

	_es.item_pane.forEach(function(eq){
		if(eq.data.sold || !_es.is_selected(eq) || eq.node.div.dataset.locked != "0") {
			return;
		}
		selected.push(eq);
		if(eq.data.valuable) {
			valuable.push(eq.info.name);
		} else if(!eq.data.need_salvage) {
			sell.push(eq.info.name);
		}
	});

	if(!selected.length || (settings.equipmentShopConfirm===2 || settings.equipmentShopConfirm===1&&sell.length || valuable.length) && !confirm("Are you sure you wish to salvage "+selected.length+" equipment piece"+(selected.length>1?"s":"")+"?"+(valuable.length?"\n\n[Protected Equipment]\n"+valuable.join("\n"):"")+(sell.length?"\n\n[Better to Sell]\n"+sell.join("\n"):""))) {
		return;
	}

	selected.forEach(function(eq){
		_es.salvage(eq,true);
	});
};

_es.salvage_calc = function(eq,d) {
	var q = _es.quality[eq.info.quality],
		t = _es.mat_type[eq.data.filter],
		s = ["",0,0],
		g = ["",0,0],
		e = ["",0,0],
		c = ["",0,0],
		value = eq.data.value,
		prices = $prices.get("Materials");

	if(d) {
		value = Math.ceil(value/d);
	}
	if(!q) { // obsolete
	} else if(q < 6) {
		s[0] = "Scrap " + t.replace("Metals","Metal");
		s[1] = Math.min(Math.ceil(value/100),10);
		s[2] = prices[s[0]] || 0;
	} else {
		g[0] = (q===6?"Low-Grade " : q===7?"Mid-Grade " : "High-Grade ") + t;
		g[1] = !_isekai?1 : q===6?3 : q===7?2 : 1;
		g[2] = prices[g[0]] || 0;
	}
	if(q >= 9) {
		c[0] = (q===9?"Legendary ":"Peerless ") + _es.core_type[eq.data.filter] + " Core";
		c[1] = _es.rare_type[eq.info.type] ? 5 : 1;
		c[2] = prices[c[0]] || 0;
	}
	if(_es.rare_type[eq.info.type]) {
		e[0] = "Energy Cell";
		e[1] = Math.max(s[1]/2,1);
		e[2] = prices[e[0]] || 0;
	}
	eq.data.salvage = s[1]*s[2] + g[1]*g[2] + e[1]*e[2] + c[1]*c[2];
};


_es.shop_buy = function(eq,skip) {
	if(eq.data.sold || !skip && !confirm('Are you sure you wish to buy "'+eq.info.name+'" for '+eq.data.value.toLocaleString()+' credits?')) {
		return;
	}
	eq.data.sold = true;
	$ajax.add("POST","?s=Bazaar&ss=es","storetoken="+_es.storetoken+"&select_group=shop_pane&select_eids="+eq.info.eid,function(){eq.node.wrapper.remove();});
};

_es.shop_salvage = function(eq,skip) {
	if(eq.data.sold || !skip && !confirm('Are you sure you wish to buy "'+eq.info.name+'" and then salvage it?')) {
		return;
	}
	eq.data.sold = true;
	eq.node.div.classList.add("hvut-es-disabled");
	eq.node.sub.classList.add("hvut-es-disabled");

	$ajax.add("POST","?s=Bazaar&ss=es","storetoken="+_es.storetoken+"&select_group=shop_pane&select_eids="+eq.info.eid,
		function(){
			$ajax.add("JSON","json",{type:"simple",method:"lockequip",uid:_es.uid,token:_es.simple_token,eid:eq.info.eid,lock:0},
				function(){
					$ajax.add("POST","?s=Forge&ss=sa&filter="+eq.data.filter,"select_item="+eq.info.eid,
						function(){eq.node.wrapper.remove();},
						function(){alert("Failed to salvage the item");}
					);
				},
				function(){alert("Failed to unlock the item");}
			);
		},
		function(){alert("Failed to purchase the item");}
	);
};

_es.get_item_pane = function(pane,filter,append) {
	var list = $equip.list(pane);
	list.forEach(function(eq){
		eq.data.filter = filter;
		eq.data.valuable = $equip.filter(eq.info.name,_es.protect_filter);
		_es.salvage_calc(eq);

		eq.node.wrapper.classList.add("hvut-es-expand");
		if(settings.equipmentShopShowLevel && eq.info.level) {
			$element("span",[eq.node.div,"afterbegin"],[" ["+eq.info.level+"] ",".hvut-es-level"]);
		}
		if(settings.equipmentShopShowPAB && eq.info.pab) {
			$element("span",eq.node.div,["  ["+eq.info.pab+"]",".hvut-es-pab"]);
		}
		$element("span",eq.node.div,[".hvut-es-check"]);

		eq.node.lock = eq.node.wrapper.firstElementChild;
		eq.node.sub = $element("div",[eq.node.div,"beforebegin"],[".hvut-es-sub hvut-cphu-sub"]);
		eq.node.sell = $element("span",eq.node.sub,"Sell "+eq.data.value,function(){_es.sell(eq);});
		eq.node.salvage = $element("span",eq.node.sub,"Salvage "+eq.data.salvage,function(){_es.salvage(eq);});
		$element("span",eq.node.sub,[" Transfer to Storage",".hvut-es-transfer"],function(){_es.transfer(eq);});

		if(eq.info.tier) {
			eq.data.valuable = true;
			$element("span",eq.node.sub,[" IW "+eq.info.tier,".hvut-es-tier"]);
		}
		if(eq.data.salvage > eq.data.value) {
			eq.data.need_salvage = true;
			eq.node.salvage.classList.add("hvut-es-bold");
		} else {
			eq.data.need_salvage = false;
			eq.node.sell.classList.add("hvut-es-bold");
		}

		if(eq.data.valuable) {
			_es.group["valuable"].item_div.appendChild(eq.node.wrapper);
			if(settings.equipmentShopAutoLock && eq.node.div.dataset.locked == "0") {
				eq.node.lock.click();
			}
		} else if(append) {
			append.appendChild(eq.node.wrapper);
		}
	});
	_es.item_pane = _es.item_pane.concat(list);
};

_es.get_shop_pane = function(pane,filter,append) {
	var list = $equip.list(pane,append?0:1);
	list.forEach(function(eq){
		eq.data.filter = filter;
		eq.node.sub = $element("div",null,[".hvut-es-sub hvut-cphu-sub"]);
		_es.salvage_calc(eq,5);

		if(settings.equipmentShopShowLevel && eq.info.level) {
			$element("span",[eq.node.div,"afterbegin"],[" ["+eq.info.level+"] ",".hvut-es-level"]);
		}
		if(settings.equipmentShopShowPAB && eq.info.pab) {
			$element("span",eq.node.div,["  ["+eq.info.pab+"]",".hvut-es-pab"]);
		}

		var display;
		if($equip.filter(eq.info.name,_es.bazaar_filter)) {
			display = true;
		}
		if(!eq.info.tradeable) {
			display = false;
		}
		$element("span",eq.node.sub,"Buy "+eq.data.value,function(){_es.shop_buy(eq);});
		if(settings.equipmentShopCheckIW && eq.info.tier) {
			display = true;
			$element("span",eq.node.sub,[" IW "+eq.info.tier,".hvut-es-tier"],function(){_es.showequip(eq);});
		}
		if(eq.data.salvage > eq.data.value) {
			display = true;
			$element("span",eq.node.sub,[" Salvage "+eq.data.salvage,".hvut-es-bold"],function(){_es.shop_salvage(eq);});
		}

		if(display) {
			_es.shop_pane.push(eq);
			$element("span",eq.node.div,[".hvut-es-check"]);
			eq.node.div.insertAdjacentElement("beforebegin",eq.node.sub);
			eq.node.wrapper.classList.add("hvut-es-expand");
			if(append) {
				append.appendChild(eq.node.wrapper);
			}
		} else {
			eq.node.wrapper.classList.add("hvut-hide");
		}
	});
};

_es.set_rangeselect = function() {
	_window.rangeselect.item_pane = $qsa("#item_pane .eqp").map(w=>w.lastElementChild.dataset.eid);
	_window.rangeselect.shop_pane = $qsa("#shop_pane .eqp").map(w=>w.lastElementChild.dataset.eid);
};

GM_addStyle(`
	.eqshop_pane > div:first-child {display:none}
	.eqshop_pane .cspp {margin-top:15px; overflow-y:scroll}
	#eqshop_sellall {display:none}

	.hvut-es-buttons {position:absolute; margin-top:1px; z-index:1}
	.hvut-es-buttons input {margin-right:20px}
	.hvut-es-buttons input:nth-child(1), .hvut-es-buttons input:nth-child(3) {margin-right:0}
	.hvut-es-tab {margin:0; padding:10px; border-top:1px solid; font-size:12pt; text-align:left}
	.hvut-es-tab > span {margin-left:10px; font-size:10pt; font-weight:normal}
	.hvut-es-valuable {background-color:#fff}

	.eqshop_pane .equiplist {padding:1px 0 5px}
	.eqshop_pane .equiplist:empty {display:none}
	.eqp.hvut-es-expand {height:44px}

	.hvut-es-level {display:inline-block; width:40px}
	.hvut-es-pab {position:absolute; left:490px; width:0; direction:rtl; pointer-events:none}
	.hvut-es-check {position:absolute; top:25px; left:5px; width:14px; height:14px; border:1px solid; color:#930; background-color:#fff}
	.eqp > div:last-child[style*='color'] > .hvut-es-check::before {content:''; position:absolute; top:1px; left:4px; width:3px; height:7px; border-right:3px solid; border-bottom:3px solid; transform:rotate(45deg)}
	.hvut-es-sub {position:absolute; top:24px; left:20px; font-size:8pt; line-height:18px; padding-left:45px}
	.hvut-es-sub > * {margin-right:10px}
	.hvut-es-bold {font-weight:bold; color:#c00}
	.hvut-es-tier {font-weight:bold; color:#03c}
	.hvut-es-valuable .hvut-es-transfer {font-weight:bold}
	.hvut-es-disabled {text-decoration:line-through}
`);

_es.buttons = $element("div",[$id("eqshop_outer"),"afterbegin"],[".hvut-es-buttons"]);
$element("input",_es.buttons,{type:"button",value:"Select :"},function(){_es.select_all("sell");});
$element("input",_es.buttons,{type:"button",value:"Sell"},_es.sell_all);
$element("input",_es.buttons,{type:"button",value:"Select :"},function(){_es.select_all("salvage");});
$element("input",_es.buttons,{type:"button",value:"Salvage"},_es.salvage_all);
$element("input",_es.buttons,{type:"button",value:"Edit Materials Price"},function(){$prices.edit("Materials",()=>{if(confirm("You need to refresh the page.")){location.href=location.href;}});});
$element("input",_es.buttons,{type:"button",value:"Edit Protection Filter"},function(){_es.edit_filter("protect");});
$element("input",_es.buttons,{type:"button",value:"Edit Bazaar Filter"},function(){_es.edit_filter("bazaar");});

_es.group["valuable"] = {};
_es.group["valuable"].item_tab = $element("h4",$id("item_pane"),[" [Protected: Valuable Equipment]",".hvut-es-tab hvut-es-valuable"]);
_es.group["valuable"].item_status = $element("span",_es.group["valuable"].item_tab);
_es.group["valuable"].item_div = $element("div",$id("item_pane"),[".equiplist nosel hvut-es-valuable"]);

if(settings.equipmentShopIntegration) {
  let filterbar = $id("filterbar");
  if (filterbar.firstElementChild.nodeName === 'DIV') {
    GM_addStyle(`
      #filterbar > div {width:150px !important}
    `);
    $element("div",[filterbar,"afterbegin"],[".cfbs","!cursor:pointer","/<div class='fc4 fac fcb'><div>All</div></div>"],function(){location.href="?s=Bazaar&ss=es";});
    if(_query.filter) {
      filterbar.children[0].classList.remove("cfbs");
      filterbar.children[0].classList.add("cfb");
      filterbar.children[0].children[0].classList.remove("fc4");
      filterbar.children[0].children[0].classList.add("fc2");
    } else {
      filterbar.children[1].classList.remove("cfbs");
      filterbar.children[1].classList.add("cfb");
      filterbar.children[1].children[0].classList.remove("fc4");
      filterbar.children[1].children[0].classList.add("fc2");
    }
  } else if (filterbar.firstElementChild.nodeName === 'A') {
    $element('a', [filterbar, 'afterbegin'], { href: '?s=Bazaar&ss=es', innerHTML: '<div>All</div>' });
    if (_query.filter) {
      filterbar.children[0].children[0].classList.add('cfb');
    } else {
      filterbar.children[0].children[0].classList.add('cfbs');
      filterbar.children[1].children[0].classList.remove('cfbs');
      filterbar.children[1].children[0].classList.add('cfb');
    }
  }
}

if(_query.filter || !settings.equipmentShopIntegration) {
	_es.get_item_pane($id("item_pane"),_es.filter);
	_es.get_shop_pane($id("shop_pane"),_es.filter);
	_es.set_rangeselect();
	$element("h4",$id("item_pane"),[" "+$equip.alias[_query.filter],".hvut-es-tab"]);
	$id("item_pane").appendChild($qs("#item_pane > .equiplist"));
	toggle_button( $element("input",_es.buttons,{type:"button",style:"width:135px"}), "Show Equipment","Hide Equipment",$id("shop_pane"),"hvut-hide-on",false);

} else {

	_es.group["1handed"] = {};
	_es.group["1handed"].item_tab = $element("h4",$id("item_pane"),[" "+$equip.alias["1handed"],".hvut-es-tab"]);
	_es.group["1handed"].item_status = $element("span",_es.group["1handed"].item_tab);
	_es.group["1handed"].item_div = $element("div",$id("item_pane"),[".equiplist nosel"]);
	_es.group["1handed"].shop_tab = $element("h4",$id("shop_pane"),[" "+$equip.alias["1handed"],".hvut-es-tab"]);
	_es.group["1handed"].shop_status = $element("span",_es.group["1handed"].shop_tab);
	_es.group["1handed"].shop_div = $element("div",$id("shop_pane"),[".equiplist nosel"]);

	let item_default = $qs("#item_pane > .equiplist"),
		shop_default = $qs("#shop_pane > .equiplist");
	_es.get_item_pane(item_default,"1handed",_es.group["1handed"].item_div);
	_es.get_shop_pane(shop_default,"1handed",_es.group["1handed"].shop_div);
	item_default.remove();
	shop_default.remove();

	_es.group["valuable"].item_status.textContent = "Loading...";
	_es.tab_loaded = 1;

	["2handed","staff","shield","acloth","alight","aheavy"].forEach(function(filter){
		var tab = _es.group[filter] = {};

		tab.item_tab = $element("h4",$id("item_pane"),[" "+$equip.alias[filter],".hvut-es-tab"]);
		tab.item_status = $element("span",tab.item_tab,"Loading...");
		tab.item_div = $element("div",$id("item_pane"),[".equiplist nosel"]);

		tab.shop_tab = $element("h4",$id("shop_pane"),[" "+$equip.alias[filter],".hvut-es-tab"]);
		tab.shop_status = $element("span",tab.shop_tab,"Loading...");
		tab.shop_div = $element("div",$id("shop_pane"),[".equiplist nosel"]);

		$ajax.add("GET","?s=Bazaar&ss=es&filter="+filter,null,
			function(r){
				var html = r.responseText,
					doc = $doc(html);

				Object.assign($equip.dynjs_eqstore, JSON.parse(/var dynjs_eqstore = (\{.*\});/.test(html) && RegExp.$1));
				Object.assign($equip.eqvalue, JSON.parse(/var eqvalue = (\{.*\});/.test(html) && RegExp.$1));

				tab.item_status.textContent = "";
				tab.item_div.innerHTML = "";
				_es.get_item_pane($id("item_pane",doc),filter,tab.item_div);
				tab.shop_status.textContent = "";
				tab.shop_div.innerHTML = "";
				_es.get_shop_pane($id("shop_pane",doc),filter,tab.shop_div);

				if(++_es.tab_loaded === 7) {
					_es.group["valuable"].item_status.textContent = "";
					_es.set_rangeselect();
				}
			},
			function(){tab.item_status.textContent=tab.shop_status.textContent="ERROR";}
		);
	});
}

} else
// [END 8] Bazaar - Equipment Shop */


//* [9] Bazaar - Item Shop
if(settings.itemShop && _query.s==="Bazaar" && _query.ss==="is") {

Array.from($qs("#item_pane .itemlist").rows).forEach(function(tr){
	var div = tr.cells[0].firstElementChild,
		name = div.textContent.trim(),
		type = /'([^']+)'\)/.test(div.getAttribute("onmouseover")) && RegExp.$1.replace(/\W/g,"_") || "Consumable";
	tr.classList.add("hvut-it-"+type);
	if( settings.itemShopHideInventory.some(h=>name.includes(h)) ) {
		tr.classList.add("hvut-hide");
	}
});

Array.from($qs("#shop_pane .itemlist").rows).forEach(function(tr){
	var div = tr.cells[0].firstElementChild,
		name = div.textContent.trim(),
		type = /'([^']+)'\)/.test(div.getAttribute("onmouseover")) && RegExp.$1.replace(/\W/g,"_") || "Consumable";
	tr.classList.add("hvut-it-"+type);
	if( settings.itemShopHideStore.some(h=>name.includes(h)) ) {
		tr.classList.add("hvut-hide");
	}
});

GM_addStyle(`
	.itshop_pane .cspp {margin-top:15px; overflow-y:scroll}
`);

toggle_button( $element("input",$id("itshop_outer"),{type:"button",style:"position:absolute;top:75px;right:220px;width:100px;z-index:3"}), "Show Items","Hide Items",$id("itshop_outer"),"hvut-hide-on",true);

} else
// [END 9] Bazaar - Item Shop */


//* [10] Bazaar - Item Backorder
if(settings.itemBackorder && _query.s==="Bazaar" && _query.ss==="ib") {

_ib.dynjs_itemc = _window.dynjs_itemc;
_ib.biddata = _window.biddata;

GM_addStyle(`
	#itembot_outer {width:1140px}
	#active_pane .itemlist td:nth-child(1) {width:220px !important}
	#active_pane .itemlist td:nth-child(2) {width:60px}
	#active_pane .itemlist td:nth-child(3) {width:80px}

	.hvut-ib-bot {float:right; width:300px; height:611px; margin:20px 0 0; padding:0; overflow-y:scroll; list-style:none; font-size:9pt; line-height:18px; text-align:left; white-space:nowrap}
	.hvut-ib-bot > li {border-width:1px; border-bottom-style:solid; overflow:hidden; text-overflow:ellipsis; cursor:default}
	.hvut-ib-bot > li:hover {background-color:#fff}
	.hvut-ib-category {margin-top:10px; border-style:solid; font-weight:bold; background-color:#edb; text-align:center}
	.hvut-ib-category:first-child {margin-top:0}
	.hvut-ib-margin {margin-top:1px; border-top-style:solid}
	.hvut-ib-bot span:first-child {float:left; width:70px; height:18px; margin-right:5px; padding-right:5px; border-right:1px solid; text-align:right}
`);

_ib.ul = $element("ul",$id("itembot_outer"),[".hvut-ib-bot"]);
_ib.selector = $id("bot_item");

Array.from(_ib.selector.options).forEach(function(o){
	if(!o.value || o.value == "0"){return;}
	var bid = _ib.biddata[o.value],
		name = o.text,
		category = !o.previousElementSibling && o.parentNode.label;

	if(category) {
		$element("li",_ib.ul,[" "+category,".hvut-ib-category"]);
	}
	var li = $element("li",_ib.ul,["/<span>"+(bid.cb?bid.cb.toLocaleString():"")+"</span><span>"+name+"</span>"],function(){_window.itembot.set_item(o.value);});
	if(category || ["Infusion of Flames","Scroll of Swiftness","Wirts Leg","ManBearPig Tail","Platinum Coupon","Crystallized Phazon","Binding of Slaughter","Legendary Weapon Core","Voidseeker Shard"].includes(name)) {
		li.classList.add("hvut-ib-margin");
	}
});

$qsa("#active_pane .itemlist tr").forEach(function(tr){
	var bid = _ib.biddata[ /itembot\.set_item\((\d+)\)/.test(tr.cells[0].firstElementChild.getAttribute("onclick")) && RegExp.$1 ];
	$element("td",tr,bid.cb.toLocaleString());
});

if($qs("#active_pane .itemlist")) {
	$element("tr",[$qs("#active_pane .itemlist"),"afterbegin"],["/<td>Item</td><td>Count</td><td>Bid</td>"]);
}

} else
// [END 10] Bazaar - Item Backorder */


//* [11] Bazaar - Monster Lab
if(settings.monsterLab && _query.s==="Bazaar" && _query.ss==="ml") {

if(_query.create) {
} else if(_query.slot) {
	if(_query.pane === "skills") {
		_ml.prev_button = $qs("img[src$='/monster/prev.png']");
		_ml.prev_button.setAttribute("onclick",_ml.prev_button.getAttribute("onclick").replace("ss=ml","ss=ml&pane=skills"));
		_ml.next_button = $qs("img[src$='/monster/next.png']");
		_ml.next_button.setAttribute("onclick",_ml.next_button.getAttribute("onclick").replace("ss=ml","ss=ml&pane=skills"));
	}
} else {

GM_addStyle(`
	#monster_outer {margin:5px 0 0 120px; font-weight:normal}
	#monster_list .cspp {margin-top:15px; overflow-y:scroll}
	#slot_pane {white-space:nowrap}
	#slot_pane > div {position:relative; display:flex; height:26px; line-height:26px}
	#slot_pane > div > div {margin-left:10px; padding:0}
	#slot_pane .fc4 {font-size:10pt}
	#slot_pane > div > div:nth-child(1) {order:1; width:20px}
	#slot_pane > div > div:nth-child(2) {order:2; width:210px; overflow:hidden}
	#slot_pane > div > div:nth-child(4) {order:3; width:70px}
	#slot_pane > div > div:nth-child(3) {order:4; width:40px; text-align:right}
	#slot_pane > div > div:nth-child(7) {order:5; width:90px}
	#slot_pane > div > div:nth-child(8) {order:6; width:25px}
	#slot_pane > div > div:nth-child(9) {order:7; width:50px}
	#slot_pane > div > div:nth-child(6) {order:8; width:200px}
	#slot_pane > div > div:nth-child(5) {order:9; width:200px}

	#monster_list {width:auto}
	#monster_actions {width:auto}

	.hvut-ml-sort {position:absolute; display:flex; top:10px; left:22px}
	.hvut-ml-sort > div {margin:0 5px; border:1px solid; box-sizing:border-box}
	.hvut-ml-sort > .hvut-ml-sort-current {font-weight:bold; border-width:2px}

	.hvut-ml-side {position:absolute; top:38px; left:-100px; width:100px; display:flex; flex-direction:column}
	.hvut-ml-side input {margin:3px 0; white-space:normal}
	.hvut-ml-side input:nth-child(5), .hvut-ml-side input:nth-child(8) {margin-top:20px}

	#slot_pane .hvut-ml-feed {position:absolute; top:7px; left:62px; width:124px; height:12px; font-size:8pt; line-height:12px; text-align:center; background:none}
	#slot_pane div:hover > .hvut-ml-feed {background-color:#fff9}

	.hvut-ml-new {background-color:#edb}
	.hvut-ml-gains {position:relative}
	.hvut-ml-gains > span {display:inline-block; width:25px; line-height:22px; border-radius:2px; background-color:#5C0D11; color:#fff}
	.hvut-ml-gains > ul {display:none; position:absolute; top:2px; right:35px; margin:0; padding:5px 10px; border:1px solid; list-style:none; font-size:9pt; line-height:20px; white-space:nowrap; background-color:#EDEBDF; z-index:1}
	#slot_pane > div:nth-of-type(n+15):nth-last-of-type(-n+5) > .hvut-ml-gains > ul {top:auto; bottom:2px}
	.hvut-ml-gains:hover > ul {display:block}
	.hvut-ml-failed {color:#c00; font-weight:bold}

	.hvut-ml-summary {position:absolute; top:38px; left:10px; max-height:500px; min-width:250px; margin:0; padding:10px; overflow:auto; border:1px solid; list-style:none; background-color:#EDEBDF; font-size:10pt; line-height:20px; text-align:left; white-space:nowrap; z-index:3}
	.hvut-ml-summary:empty {display:none}
	.hvut-ml-summary > li:first-child {margin-bottom:5px; font-weight:bold}
	.hvut-ml-summary > li {margin:0 5px}

	.hvut-ml-log {position:absolute; top:38px; left:610px; margin:0; padding:10px; width:460px; height:560px; column-count:2; column-gap:10px; border:1px solid; list-style:none; background-color:#EDEBDF; font-size:9pt; line-height:16px; text-align:left; white-space:nowrap; z-index:3}
	.hvut-ml-log > li {overflow:hidden; text-overflow:ellipsis}
	.hvut-ml-log > li:nth-child(-n+3) {column-span:all; font-weight:bold}
	.hvut-ml-log > li:nth-child(3) {margin-bottom:16px}
	.hvut-ml-margin {margin-top:16px !important}
	.hvut-ml-break {break-after:column}
`);

_ml.materials = ["Low-Grade Cloth","Mid-Grade Cloth","High-Grade Cloth","Low-Grade Leather","Mid-Grade Leather","High-Grade Leather","Low-Grade Metals","Mid-Grade Metals","High-Grade Metals","Low-Grade Wood","Mid-Grade Wood","High-Grade Wood","Crystallized Phazon","Shade Fragment","Repurposed Actuator","Defense Matrix Modulator","Binding of Slaughter","Binding of Balance","Binding of Isaac","Binding of Destruction","Binding of Focus","Binding of Friendship","Binding of Protection","Binding of Warding","Binding of the Fleet","Binding of the Barrier","Binding of the Nimble","Binding of Negation","Binding of the Ox","Binding of the Raccoon","Binding of the Cheetah","Binding of the Turtle","Binding of the Fox","Binding of the Owl","Binding of Surtr","Binding of Niflheim","Binding of Mjolnir","Binding of Freyr","Binding of Heimdall","Binding of Fenrir","Binding of the Elementalist","Binding of the Heaven-sent","Binding of the Demon-fiend","Binding of the Curse-weaver","Binding of the Earth-walker","Binding of Dampening","Binding of Stoneskin","Binding of Deflection","Binding of the Fire-eater","Binding of the Frost-born","Binding of the Thunder-child","Binding of the Wind-waker","Binding of the Thrice-blessed","Binding of the Spirit-ward"];

_ml.mobs = [];
_ml.gains = {};
_ml.now = Date.now();
_ml.log = getValue("ml_log",[]);

_ml.parse = function(mob,doc) {
	mob.pl = mob.log.pl = parseInt($qs(".msl>div:nth-child(3)",doc).textContent.slice(4));
	mob.hunger = parseInt($qs(".msl>div:nth-child(5) img",doc).style.width) * 200;
	mob.morale = parseInt($qs(".msl>div:nth-child(6) img",doc).style.width) * 200;
	mob.wins = parseInt($qs("#monsterstats_right>div:nth-child(2)>div:nth-child(2)",doc).textContent);
	mob.kills = parseInt($qs("#monsterstats_right>div:nth-child(3)>div:nth-child(2)",doc).textContent);

	var pa = $qsa("#monsterstats_top td:nth-child(2)",doc).map(td=>parseInt(td.textContent)),
		er = pa.splice(6,6);
	mob.pa.forEach(function(e,i){
		e.value = mob.log.pa[i].value = pa[i];
	});
	mob.er.forEach(function(e,i){
		e.value = mob.log.er[i].value = er[i];
	});

	$qsa("#chaosupg td:nth-child(2)",doc).forEach(function(td,i){
		mob.ct[i].value = mob.log.ct[i].value = $qsa(".mcu2",td).length;
		mob.ct[i].max = mob.log.ct[i].max = 20 - $qsa(".mcu0",td).length;
	});
};

_ml.price2str = function(price) {
	if(price > 1000000) {
		price = Math.round(price/10000)/100 + "m";
	} else if(price > 1000) {
		price = Math.round(price/10)/100 + "k";
	} else {
		price = Math.round(price);
	}
	return price;
};


_ml.main = {

node : {},

sort : function(key) {
	if(!["#","index","name","class","pl","wins","kills","+","gains","gifts","morale","hunger"].includes(key)) {
		return;
	}
	if(key === "#") {
		key = "index";
	}
	if(key === "+") {
		key = "gains";
	}
	var order = ["pl","wins","kills","gains","gifts"].includes(key)?-1:1;
	if(key === _ml.main.sort.key) {
		order = _ml.main.sort.order * -1;
	}
	if(_ml.main.sort.key) {
		_ml.main.node.sort[_ml.main.sort.key].classList.remove("hvut-ml-sort-current");
	}
	_ml.main.node.sort[key].classList.add("hvut-ml-sort-current");
	_ml.main.sort.key = key;
	_ml.main.sort.order = order;

	if(!_ml.main.sort.list) {
		_ml.main.sort.list = _ml.mobs.filter(m=>m).concat( $qsa("#slot_pane > div[onclick*='&create=new']").map(d=>({node:{div:d},index:+d.firstElementChild.textContent})) );
	}
	_ml.main.sort.list.sort((a,b)=>(a[key]==b[key]?0: a[key]==undefined?1: b[key]==undefined?-1: (a[key]>b[key]?1:-1)*order));
	var slot_pane = $id("slot_pane"),
		frag = $element();
	slot_pane.style.display = "none";
	_ml.main.sort.list.forEach(function(m){frag.appendChild(m.node.div);});
	slot_pane.prepend(frag);
	slot_pane.style.display = "";
},

feed : function(mob,t) {
	if(!mob.status) {
		return;
	}
	mob.status = 0;
	mob.node.wins.textContent = "...";
	$ajax.add("POST","?s=Bazaar&ss=ml&slot="+mob.index,t?"food_action="+t:"",function(r){var doc=$doc(r.responseText);_ml.main.update(mob,doc);},function(){_ml.main.update(mob,false);});
},
feedall : function(stat,value,food) {
	_ml.mobs.forEach(function(mob){
		_ml.main.feed(mob, !value||value>=mob[stat]?food:null);
	});
},
update : function(mob,doc) {
	if(doc) {
		_ml.parse(mob,doc);
		mob.status = 1;
		mob.node.wins.classList.remove("hvut-ml-failed");
		mob.node.wins.textContent = mob.wins + " / " + mob.kills;
		mob.node.hunger.textContent = mob.hunger;
		mob.node.hungerbar.style.width = (mob.hunger/200)+"px";
		mob.node.morale.textContent = mob.morale;
		mob.node.moralebar.style.width = (mob.morale/200)+"px";
	} else {
		mob.status = -1;
		mob.node.wins.classList.add("hvut-ml-failed");
		mob.node.wins.textContent = "failed";
	}
},

change_price : function() {
	_ml.main.make_summary();
	if(_ml.mobs.sum.node.log) {
		_ml.main.make_log(_ml.mobs.sum);
	}
	_ml.mobs.forEach(function(mob){
		if(mob.node.log) {
			_ml.main.make_log(mob);
		}
	});
},

toggle_summary : function() {
	_ml.main.node.summary.classList.toggle("hvut-none");
},
make_summary : function() {
	_ml.main.node.summary.innerHTML = "";

	var mobs = Object.keys(_ml.gains);
	if(!mobs.length) {
		return;
	}
	var summary = {},
		count = 0,
		prices = $prices.get("Materials"),
		profits = 0;
	mobs.forEach(function(m){
		_ml.gains[m].forEach(function(g){
			if(!summary[g]) {
				summary[g] = 0;
			}
			summary[g]++;
			count++;
			profits += (prices[g] || 0);
		});
	});
	$element("li",_ml.main.node.summary,mobs.length+" monster(s), "+count+" gift(s), "+_ml.price2str(profits)+" credits");
	_ml.materials.forEach(function(g){
		if(summary[g]) {
			$element("li",_ml.main.node.summary,summary[g]+" x "+g);
		}
	});
},

toggle_log : function(mob) {
	if(mob.node.log && mob.node.log.parentNode) {
		_ml.main.hide_log(mob);
	} else {
		_ml.main.show_log(mob);
	}
},
show_log : function(mob) {
	if(!mob.node.log) {
		_ml.main.make_log(mob);
	}
	$id("monster_outer").appendChild(mob.node.log);
},
hide_log : function(mob) {
	if(mob.node.log) {
		mob.node.log.remove();
	}
},
make_log : function(mob) {
	if(!mob.node.log) {
		mob.node.log = $element("ul",null,[".hvut-ml-log"]);
	}
	mob.node.log.innerHTML = "";

	var date = mob.log.date,
		hours = (_ml.now-date) / (1000*60*60),
		daily = Math.max(hours,24),
		round = Math.round(hours),
		prices = $prices.get("Materials"),
		count = 0,
		sum = 0;

	_ml.materials.forEach(function(mat,i){
		var li = $element("li",mob.node.log,mob.log.gift[i]+" x "+mat);
		if(i===12 || i===16 || i===22 || i===28 || i===34 || i===40 || i===45 || i===48) {
			li.classList.add("hvut-ml-margin");
		}
		if(i===27) {
			li.classList.add("hvut-ml-break");
		}
		count += mob.log.gift[i];
		sum += mob.log.gift[i] * (prices[mat]||0);
	});

	mob.node.log.prepend(
		$element("li",null,"For "+Math.floor(round/24)+" days "+(round%24)+" hours / Since "+(new Date(date)).toLocaleString()),
		$element("li",null,"- Total: "+count+" gifts, "+_ml.price2str(sum)+" credits"),
		$element("li",null,"- Daily: "+(Math.round(count/daily*24*10)/10)+" gifts, "+_ml.price2str(sum/daily*24)+" credits")
	);
},
reset_log : function() {
	if(confirm("Monster gift log will be deleted.\n\nContinue?")) {
		deleteValue("ml_log");
		location.href = location.href;
	}
}

};


if($id("messagebox_outer")) {
	let monster,
		gift;
	get_message(null,true).forEach(function(msg){
		if(!msg) {
			return;
		} else if(/^(.+) brought you (?:a gift|some gifts)!$/.test(msg)) {
			monster = RegExp.$1;
			_ml.gains[monster] = [];
		} else {
			gift = /^Received (?:a|some) (.+)$/.test(msg) ? RegExp.$1 : msg;
			_ml.gains[monster].push(gift);
		}
	});
	if(settings.monsterLabCloseDefaultPopup) {
		$id("messagebox_outer").style.display = "none";
	}
}

_ml.mobs.sum = {log:{date:_ml.now,gift:new Array(54).fill(0)},node:{}};
$id("monster_outer").style.display = "none";
$qsa("#slot_pane > div").forEach(function(div,i){
	var index = i + 1;
	if(div.getAttribute("onclick").includes("&create=new")) {
		_ml.log[index] = null;
		return;
	}

	var log = _ml.log[index];
	if(!log) {
		log = _ml.log[index] = {date:_ml.now,pl:null,selected:true,pa:[],er:[],ct:[],gift:[]};
		for(let i=0; i<6; i++) {
			log.pa[i] = {};
			log.er[i] = {};
		}
		for(let i=0; i<12; i++) {
			log.ct[i] = {};
		}
		for(let i=0; i<54; i++) {
			log.gift[i] = 0;
		}
	}
	if(_ml.mobs.sum.log.date > log.date) {
		_ml.mobs.sum.log.date = log.date;
	}

	var mob = {index:index,status:-1,pa:[],er:[],ct:[],log:_ml.log[index],node:{div:div}};
	_ml.mobs[mob.index] = mob;

	mob.name = div.children[1].textContent;
	mob.class = div.children[3].textContent;
	mob.pl = div.children[2].textContent = parseInt(div.children[2].textContent.slice(4));
	if(mob.pl !== mob.log.pl) {
		mob.need_update = true;
	}
	mob.visible = true;
	mob.selected = true;

	for(let i=0; i<6; i++) {
		mob.pa[i] = {value:log.pa[i].value};
		mob.er[i] = {value:log.er[i].value};
	}
	for(let i=0; i<12; i++) {
		mob.ct[i] = {value:log.ct[i].value,max:log.ct[i].max};
	}

	var hungerdiv = div.children[4],
		moralediv = div.children[5];
	hungerdiv.addEventListener("click",function(e){_ml.main.feed(mob,"food");e.stopPropagation();});
	moralediv.addEventListener("click",function(e){_ml.main.feed(mob,"drugs");e.stopPropagation();});
	mob.node.hungerbar = hungerdiv.firstElementChild.firstElementChild;
	mob.node.moralebar = moralediv.firstElementChild.firstElementChild;
	mob.hunger = parseInt(mob.node.hungerbar.style.width) * 200;
	mob.morale = parseInt(mob.node.moralebar.style.width) * 200;
	mob.node.hunger = $element("div",hungerdiv,[" "+mob.hunger,".hvut-ml-feed"]);
	mob.node.morale = $element("div",moralediv,[" "+mob.morale,".hvut-ml-feed"]);
	mob.node.wins = $element("div",div,"-",function(e){_ml.main.feed(mob);e.stopPropagation();});
	mob.node.gains = $element("div",div,[".hvut-ml-gains"]);
	mob.node.gifts = $element("div",div,null,{mouseenter:function(){_ml.main.show_log(mob);},mouseleave:function(){_ml.main.hide_log(mob);}});

	var gains = _ml.gains[mob.name];
	if(gains) {
		mob.gains = gains.length;
		div.classList.add("hvut-ml-new");
		$element("span",mob.node.gains,gains.length);
		var ul = $element("ul",mob.node.gains);
		gains.forEach(function(g){
			$element("li",ul,g);
			mob.log.gift[_ml.materials.indexOf(g)]++;
		});
	}

	for(let i=0; i<54; i++) {
		_ml.mobs.sum.log.gift[i] += mob.log.gift[i];
	}
	mob.gifts = mob.log.gift.reduce((p,c)=>p+c,0);
	mob.node.gifts.textContent = mob.gifts;
});
$id("monster_outer").style.display = "";

setValue("ml_log",_ml.log);

_ml.main.node.sort = {};
_ml.main.node.sort.div = $element("div",[$id("slot_pane"),"beforebegin"],[".hvut-ml-sort hvut-cphu-sub"]);
_ml.main.node.sort.index = $element("div",_ml.main.node.sort.div,[" #","!width:30px"],function(){_ml.main.sort("index");});
_ml.main.node.sort.name = $element("div",_ml.main.node.sort.div,[" Name","!width:210px"],function(){_ml.main.sort("name");});
_ml.main.node.sort.class = $element("div",_ml.main.node.sort.div,[" Class","!width:70px"],function(){_ml.main.sort("class");});
_ml.main.node.sort.pl = $element("div",_ml.main.node.sort.div,[" PL","!width:40px"],function(){_ml.main.sort("pl");});
_ml.main.node.sort.wins = $element("div",_ml.main.node.sort.div,[" Wins","!width:40px"],function(){_ml.main.sort("wins");});
_ml.main.node.sort.kills = $element("div",_ml.main.node.sort.div,[" Kills","!width:40px"],function(){_ml.main.sort("kills");});
_ml.main.node.sort.gains = $element("div",_ml.main.node.sort.div,[" +","!width:25px"],function(){_ml.main.sort("gains");});
_ml.main.node.sort.gifts = $element("div",_ml.main.node.sort.div,[" Gifts","!width:50px"],function(){_ml.main.sort("gifts");});
_ml.main.node.sort.morale = $element("div",_ml.main.node.sort.div,[" Morale","!width:200px"],function(){_ml.main.sort("morale");});
_ml.main.node.sort.hunger = $element("div",_ml.main.node.sort.div,[" Hunger","!width:200px"],function(){_ml.main.sort("hunger");});

if(settings.monsterLabDefaultSort !== "#") {
	_ml.main.sort(settings.monsterLabDefaultSort);
} else {
	_ml.main.sort.key = "index";
	_ml.main.sort.order = 1;
	_ml.main.node.sort.index.classList.add("hvut-ml-sort-current");
}

_ml.main.node.side = $element("div",$id("monster_outer"),[".hvut-ml-side"]);
$element("input",_ml.main.node.side,{type:"button",value:"Gift Summary"},function(){_ml.main.toggle_summary();});
$element("input",_ml.main.node.side,{type:"button",value:"Accumulated Log"},function(){_ml.main.toggle_log(_ml.mobs.sum);});
$element("input",_ml.main.node.side,{type:"button",value:"Edit Materials Price"},function(){$prices.edit("Materials",_ml.main.change_price);});
$element("input",_ml.main.node.side,{type:"button",value:"Reset Log"},function(){_ml.main.reset_log();});
$element("input",_ml.main.node.side,{type:"button",value:"Check Wins/Kills"},function(){_ml.main.feedall();});
$element("input",_ml.main.node.side,{type:"button",value:"Drug Morale<"+settings.monsterLabMorale},function(){_ml.main.feedall("morale",settings.monsterLabMorale,"drugs");});
$element("input",_ml.main.node.side,{type:"button",value:"Feed Hunger<"+settings.monsterLabHunger},function(){_ml.main.feedall("hunger",settings.monsterLabHunger,"food");});
$element("input",_ml.main.node.side,{type:"button",value:"Monster Upgrader",id:"hvut-ml-up-button"},function(){_ml.upgrade.toggle();});
$element("input",_ml.main.node.side,{type:"button",value:"Power Level Calculator"},function(){_ml.lab.toggle();});

_ml.main.node.summary = $element("ul",$id("monster_outer"),[".hvut-ml-summary"]);
_ml.main.make_summary();

// Monster Upgrader
_ml.upgrade = {

pa : [
	{query:"pa_str",text:"STR",crystal:"Crystal of Vigor"},
	{query:"pa_dex",text:"DEX",crystal:"Crystal of Finesse"},
	{query:"pa_agi",text:"AGI",crystal:"Crystal of Swiftness"},
	{query:"pa_end",text:"END",crystal:"Crystal of Fortitude"},
	{query:"pa_int",text:"INT",crystal:"Crystal of Cunning"},
	{query:"pa_wis",text:"WIS",crystal:"Crystal of Knowledge"}
],
er : [
	{query:"er_fire",text:"FIRE",crystal:"Crystal of Flames"},
	{query:"er_cold",text:"COLD",crystal:"Crystal of Frost"},
	{query:"er_elec",text:"ELEC",crystal:"Crystal of Lightning"},
	{query:"er_wind",text:"WIND",crystal:"Crystal of Tempest"},
	{query:"er_holy",text:"HOLY",crystal:"Crystal of Devotion"},
	{query:"er_dark",text:"DARK",crystal:"Crystal of Corruption"}
],
ct : [
	{query:"affect",text:"Scavenging",desc:"Increases the gift factor by 2.5%"},
	{query:"health",text:"Fortitude",desc:"Increases monster health by 5%"},
	{query:"damage",text:"Brutality",desc:"Increases monster damage by 2.5%"},
	{query:"accur",text:"Accuracy",desc:"Increases monster accuracy by 5%"},
	{query:"cevbl",text:"Precision",desc:"Decreases effective target evade/block by 1%"},
	{query:"cpare",text:"Overpower",desc:"Decreases effective target parry/resist by 1%"},
	{query:"parry",text:"Interception",desc:"Increases monster parry by 0.5%"},
	{query:"resist",text:"Dissipation",desc:"Increases monster resist by 0.5%"},
	{query:"evade",text:"Evasion",desc:"Increases monster evade by 0.5%"},
	{query:"phymit",text:"Defense",desc:"Increases monster physical mitigation by 1%"},
	{query:"magmit",text:"Warding",desc:"Increases monster magical mitigation by 1%"},
	{query:"atkspd",text:"Swiftness",desc:"Increases monster attack speed by 2.5%"}
],

pa_pl : [0],
er_pl : [0],
pa_crystal : [0],
er_crystal : [0],
pa_morale : [0],
er_morale : [0],

node : {
	div : $element("div",$id("mainpane"),[".hvut-ml-up hvut-none"]),
	button : $id("hvut-ml-up-button")
},

status : 0, // 0:inactive, 1:init_updating, 2:init_updated, 3:ok, 4:working

init : function() {
	if(!_ml.inventory) {
		_ml.upgrade.node.button.disabled = true;
		_ml.inventory = {};
		$ajax.add("GET","?s=Character&ss=it",null,function(r){
			var doc = $doc(r.responseText);
			Array.from($qs(".itemlist",doc).rows).forEach(function(tr){
				_ml.inventory[ tr.cells[0].textContent.trim() ] = parseInt(tr.cells[1].textContent);
			});
			_ml.upgrade.pa.forEach(function(e){
				e.stock = _ml.inventory[e.crystal] || 0;
			});
			_ml.upgrade.er.forEach(function(e){
				e.stock = _ml.inventory[e.crystal] || 0;
			});
			_ml.upgrade.ct.stock = _ml.inventory["Chaos Token"] || 0;
			_ml.upgrade.node.button.disabled = false;
			_ml.upgrade.init();
		});
		return;
	}

	if(_ml.upgrade.status === 0) {
		_ml.upgrade.status = 1;
		_ml.upgrade.update();
		return;

	} else if(_ml.upgrade.status === 2){ // do process

	} else {
		return;
	}
	_ml.upgrade.status = 3;

	GM_addStyle(`
		.hvut-ml-up {position:absolute; top:27px; left:0; width:100%; height:675px; z-index:9; background-color:#EDEBDF; text-align:left}
		.hvut-ml-up-header {margin:10px; padding:3px 8px; border:1px solid; font-size:8pt; line-height:16px; user-select:none}

		.hvut-ml-up-list {height:462px; margin:10px; padding:0; overflow:auto; font-size:10pt; line-height:16px; list-style:none; user-select:none}
		.hvut-ml-up-list > li {margin:3px; padding:3px 5px; border:1px solid; color:#999}
		.hvut-ml-up-list > li:hover {background-color:#fff}
		.hvut-ml-up-list > .hvut-ml-up-selected {background-color:#edb; color:inherit}

		.hvut-ml-up-select {cursor:pointer}
		.hvut-ml-up-select > span {display:inline-block; margin:0 3px; vertical-align:top}
		.hvut-ml-up-index {width:25px; text-align:center}
		.hvut-ml-up-selected .hvut-ml-up-index {color:#c00}
		.hvut-ml-up-name {width:210px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis}
		.hvut-ml-up-class {width:70px}
		.hvut-ml-up-pl {width:35px; text-align:right}
		.hvut-ml-up-morale {width:30px; text-align:right}
		.hvut-ml-up-change {color:#c00}

		.hvut-ml-up-reset {display:inline-block; width:15px; margin:0 10px; text-align:center; border-radius:2px; cursor:pointer}
		.hvut-ml-up-reset:hover {background-color:#c00; color:#fff}

		.hvut-ml-up-span {border-left:1px solid; padding:0 2px; position:relative}
		.hvut-ml-up-span > span {display:inline-block; margin:0 2px; width:21px; text-align:center; cursor:pointer; border-radius:2px}
		.hvut-ml-up-span > span:hover {background-color:#edb}
		.hvut-ml-up-span > span[data-desc]::after {content:attr(data-desc); position:absolute; width:100%; top:25px; left:-4px; white-space:nowrap; padding:2px 3px; background-color:#fff; border:1px solid; z-index:1; display:none}
		.hvut-ml-up-span > span[data-desc]:hover::after {display:block}

		.hvut-ml-up-bottom {margin:10px; font-size:9pt}
		.hvut-ml-up-bottom > ul {float:left; margin:0 5px; padding:5px; list-style:none; border:1px solid}
		.hvut-ml-up-bottom li {margin:5px}
		.hvut-ml-up-bottom li::after {content:''; display:block; clear:both}
		.hvut-ml-up-bottom li.hvut-ml-up-nostock {color:#c00}
		.hvut-ml-up-bottom li > span {float:left; text-align:right}

		.hvut-ml-up-crystal span:nth-child(1) {width:70px}
		.hvut-ml-up-crystal span:nth-child(2) {width:90px}
		.hvut-ml-up-crystal span:nth-child(3) {width:100px}
		.hvut-ml-up-crystal span:nth-child(4) {width:80px}
		.hvut-ml-up-token span:nth-child(1) {width:60px}
		.hvut-ml-up-token span:nth-child(2) {width:60px}

		.hvut-ml-up-buttons {float:right; width:250px; display:flex; flex-wrap:wrap; justify-content:space-between}
		.hvut-ml-up-buttons input {width:32%; margin:0 0 5px}
	`);

	var header = $element("div",_ml.upgrade.node.div,[".hvut-ml-up-header"],{mousedown:_ml.upgrade.mousedown,contextmenu:function(e){e.preventDefault();}}),
		ol = $element("ol",_ml.upgrade.node.div,[".hvut-ml-up-list"],{mousedown:_ml.upgrade.mousedown,contextmenu:function(e){e.preventDefault();}}),
		bottom = $element("div",_ml.upgrade.node.div,[".hvut-ml-up-bottom"]),
		span,ul;

	span = $element("span",header,[".hvut-ml-up-select"]);

	$element("span",span,[" #",".hvut-ml-up-index"],function(){_ml.upgrade.sort("index");});
	$element("span",span,[".hvut-ml-up-name"]).appendChild( $element("input",null,{type:"checkbox",checked:true},function(){_ml.upgrade.exec("all","select",null,this.checked);}) );
	$element("span",span,[" class",".hvut-ml-up-class"],function(){_ml.upgrade.sort("class");});
	$element("span",span,[" pl",".hvut-ml-up-pl"],function(){_ml.upgrade.sort("pl");});
	$element("span",span,{textContent:"mor",className:"hvut-ml-up-morale"},function(){_ml.upgrade.sort("morale");});

	_ml.upgrade.sort.key = "index";
	_ml.upgrade.sort.order = 1;

	$element("span",header,{textContent:"*",className:"hvut-ml-up-reset",dataset:{index:"selected",type:"reset"}});

	span = $element("span",header,[".hvut-ml-up-span"]);
	$element("span",span,{textContent:"+",dataset:{index:"selected",type:"pa",item:"all",desc:"Increase / Decrease Primary Attributes"}});
	$element("span",span,{textContent:"=",dataset:{index:"selected",type:"pa",item:"equal",desc:"Equalize Primary Attributes"}});
	_ml.upgrade.pa.forEach(function(pa,i){
		$element("span",span,{textContent:pa.text.toLowerCase(),dataset:{index:"selected",type:"pa",item:i,desc:pa.crystal}});
	});

	span = $element("span",header,[".hvut-ml-up-span"]);
	$element("span",span,{textContent:"+",dataset:{index:"selected",type:"er",item:"all",desc:"Increase / Decrease Elemental Mitigations"}});
	$element("span",span,{textContent:"=",dataset:{index:"selected",type:"er",item:"equal",desc:"Equalize Elemental Mitigations"}});
	_ml.upgrade.er.forEach(function(er,i){
		$element("span",span,{textContent:er.text.toLowerCase(),dataset:{index:"selected",type:"er",item:i,desc:er.crystal}});
	});

	span = $element("span",header,[".hvut-ml-up-span"]);
	$element("span",span,{textContent:"+",dataset:{index:"selected",type:"ct",item:"all",desc:"Increase / Decrease Token Stats"}});
	_ml.upgrade.ct.forEach(function(ct,i){
		$element("span",span,{textContent:ct.text.slice(0,3).toLowerCase(),dataset:{index:"selected",type:"ct",item:i,desc:ct.text+" : "+ct.desc}});
	});


	ul = $element("ul",bottom,[".hvut-ml-up-crystal"]);
	_ml.upgrade.pa.forEach(function(e){
		e.li = $element("li",ul);
	});

	ul = $element("ul",bottom,[".hvut-ml-up-crystal"]);
	_ml.upgrade.er.forEach(function(e){
		e.li = $element("li",ul);
	});

	_ml.upgrade.ct.ul = $element("ul",bottom,[".hvut-ml-up-token"]);

	var buttons = $element("div",bottom,[".hvut-ml-up-buttons"]);
	$element("input",buttons,{type:"button",value:"Save"},_ml.upgrade.save);
	$element("input",buttons,{type:"button",value:"Load"},_ml.upgrade.load);
	$element("input",buttons,{type:"button",value:"Update"},_ml.upgrade.force_update);
	_ml.upgrade.node.run = $element("input",buttons,{type:"button",value:"Run"},_ml.upgrade.run);
	$element("input",buttons,{type:"button",value:"Close"},_ml.upgrade.toggle);

	for(let i=0;i<25;i++) {
		_ml.upgrade.pa_pl[i+1] = _ml.upgrade.pa_pl[i] + (3+i*0.5);
		_ml.upgrade.pa_crystal[i+1] = _ml.upgrade.pa_crystal[i] + Math.round(50 * Math.pow(1.555079154,i));
		_ml.upgrade.pa_morale[i+1] = _ml.upgrade.pa_morale[i] + (Math.ceil(i*0.5)+3)*1000;
	}
	for(let i=0;i<50;i++) {
		_ml.upgrade.er_pl[i+1] = _ml.upgrade.er_pl[i] + Math.floor(1+i*0.1);
		_ml.upgrade.er_crystal[i+1] = _ml.upgrade.er_crystal[i] + Math.round(10 * Math.pow(1.26485522,i));
		_ml.upgrade.er_morale[i+1] = _ml.upgrade.er_morale[i] + (Math.floor(i*0.1)+1)*2000;
	}
	_ml.upgrade.pa.forEach(function(e){e.used=0;e.require=0;});
	_ml.upgrade.er.forEach(function(e){e.used=0;e.require=0;});

	var ct_slot = $qsa("#slot_pane > div.msl").length,
		ct_gs = false,
		ct_next = /Cost: (\d+) Chaos Token/.test($id("monster_actions").textContent) && parseInt(RegExp.$1);
	if(ct_next === Math.ceil(1+Math.pow(ct_slot,1.2))) {
	} else if(ct_next === Math.ceil(1+Math.pow(ct_slot/2,1.2))) {
		ct_slot = ct_slot/2;
		ct_gs = true;
	} else {
		ct_slot = 0;
	}

	_ml.upgrade.ct.slot = ct_slot;
	_ml.upgrade.ct.gs = ct_gs;
	_ml.upgrade.ct.unlock = 0;
	while(ct_slot--) {
		_ml.upgrade.ct.unlock += Math.ceil(1+Math.pow(ct_slot,1.2));
	}
	_ml.upgrade.ct.used = 0;
	_ml.upgrade.ct.require = 0;

	// create mob item (li) now
	_ml.mobs.forEach(function(mob){
		var li = mob.node.li = $element("li",ol,[".hvut-ml-up-selected"]),
			span;

		span = $element("span",li,{className:"hvut-ml-up-select",dataset:{index:mob.index,type:"select"}});

		$element("span",span,[" "+mob.index,".hvut-ml-up-index"]);
		$element("span",span,[" "+mob.name,".hvut-ml-up-name"]);
		$element("span",span,[" "+mob.class,".hvut-ml-up-class"]);
		mob.node.pl = $element("span",span,[" "+mob.pl,".hvut-ml-up-pl"]);
		mob.node.morale = $element("span",span,[" "+(mob.morale/100),".hvut-ml-up-morale"+(mob.morale<settings.monsterLabMorale?" hvut-ml-up-change":"")]);
		$element("span",li,{textContent:"*",className:"hvut-ml-up-reset",dataset:{index:mob.index,type:"reset"}});

		span = $element("span",li,[".hvut-ml-up-span"]);
		$element("span",span,{textContent:"+",dataset:{index:mob.index,type:"pa",item:"all"}});
		$element("span",span,{textContent:"=",dataset:{index:mob.index,type:"pa",item:"equal"}});
		mob.pa.forEach(function(e,i){
			e.node = $element("span",span,{textContent:e.value,dataset:{index:mob.index,type:"pa",item:i}});
			e.to = e.value;
			e.used = _ml.upgrade.pa_crystal[e.value];
			_ml.upgrade.pa[i].used += e.used;
			e.require = 0;
		});

		span = $element("span",li,[".hvut-ml-up-span"]);
		$element("span",span,{textContent:"+",dataset:{index:mob.index,type:"er",item:"all"}});
		$element("span",span,{textContent:"=",dataset:{index:mob.index,type:"er",item:"equal"}});
		mob.er.forEach(function(e,i){
			e.node = $element("span",span,{textContent:e.value,dataset:{index:mob.index,type:"er",item:i}});
			e.to = e.value;
			e.used = _ml.upgrade.er_crystal[e.value];
			_ml.upgrade.er[i].used += e.used;
			e.require = 0;
		});

		mob.ct.used = 0;
		mob.ct.require = 0;
		span = $element("span",li,[".hvut-ml-up-span"]);
		$element("span",span,{textContent:"+",dataset:{index:mob.index,type:"ct",item:"all"}});
		mob.ct.forEach(function(e,i){
			e.node = $element("span",span,{textContent:e.value,dataset:{index:mob.index,type:"ct",item:i}});
			e.to = e.value;
			mob.ct.used += (1+e.value) * e.value/2;
		});
		_ml.upgrade.ct.used += mob.ct.used;
	});

	_ml.upgrade.sum(true);
},

update : function() {
	if(_ml.upgrade.status !== 1 && _ml.upgrade.status !== 3) {
		return;
	}
	var total = 0,
		count = 0;

	_ml.mobs.forEach(function(mob){
		if(mob.need_update) {
			total++;
			$ajax.add("GET","?s=Bazaar&ss=ml&slot="+mob.index,null,
				function(r){
					var doc = $doc(r.responseText);
					count++;
					mob.need_update = false;
					_ml.parse(mob,doc);
					_ml.upgrade.node.button.value = "Updating... ("+count+"/"+total+")";

					var status = _ml.upgrade.status;
					if(status === 1) {
					} else if(status === 4) {
						_ml.upgrade.node.run.value = count+" / "+total;
					}
					if(total === count) {
						setValue("ml_log",_ml.log);
						_ml.upgrade.node.button.disabled = false;
						_ml.upgrade.node.button.value = "Monster Upgrader";
						if(status === 1) {
							_ml.upgrade.status = 2;
							_ml.upgrade.init();
						} else if(status === 4) {
							// _ml.upgrade.status = 3; // should refresh the page after feed
							_ml.upgrade.node.run.value = "Completed";
						}
					}
				},
				function(){
					popup("<span>Failed to Update #"+mob.index+" <span style='font-weight:bold'>"+mob.name+"</span></span><br><span>Try again</span>");
					mob.log.pl = -1;
					setValue("ml_log",_ml.log);
				}
			);
		}
	});

	if(!total) {
		if(_ml.upgrade.status === 1) {
			_ml.upgrade.status = 2;
			_ml.upgrade.init();
		} else if(_ml.upgrade.status === 3) {
		}
		return;
	}

	_ml.upgrade.node.button.disabled = true;
	_ml.upgrade.node.button.value = "Updating... (0/0)";

	if(_ml.upgrade.status === 1) {
	} else if(_ml.upgrade.status === 3) { // status 3 -> 4
		_ml.upgrade.status = 4;
		_ml.upgrade.node.run.disabled = true;
		_ml.upgrade.node.run.value = "Updating...";
	}
},

force_update : function() {
	if(_ml.upgrade.status !== 3) {
		alert("Wait a second...");
		return;
	}
	_ml.mobs.forEach(function(mob){
		mob.log.pl = -1;
	});
	setValue("ml_log",_ml.log);
	location.href = location.href;
},

sort : function(key) {
	if(!["#","index","name","class","pl","wins","kills","+","gains","gifts","morale","hunger"].includes(key)) {
		return;
	}
	if(key === "#") {
		key = "index";
	}
	if(key === "+") {
		key = "gains";
	}
	var order = ["wins","kills","gains","gifts"].includes(key)?-1:1;
	if(key === _ml.upgrade.sort.key) {
		order = _ml.upgrade.sort.order * -1;
	}
	_ml.upgrade.sort.key = key;
	_ml.upgrade.sort.order = order;

	if(!_ml.upgrade.sort.list) {
		_ml.upgrade.sort.list = _ml.mobs.filter(m=>m);
	}
	_ml.upgrade.sort.list.sort((a,b)=>(a[key]==b[key]?0: a[key]==undefined?1: b[key]==undefined?-1: (a[key]>b[key]?1:-1)*order));
	var ol = $qs(".hvut-ml-up-list"),
		frag = $element();
	ol.style.display = "none";
	_ml.upgrade.sort.list.forEach(function(m){frag.appendChild(m.node.li);});
	ol.prepend(frag);
	ol.style.display = "";
},

toggle : function() {
	if($id("messagebox_outer")) {
		$id("messagebox_outer").style.display = "none";
	}
	_ml.upgrade.init();
	_ml.upgrade.node.div.classList.toggle("hvut-none");
},

mousedown : function(e) {
	var target = e.target;
	while(!target.dataset.type) {
		if(target === e.currentTarget) {
			return;
		} else {
			target = target.parentNode;
		}
	}

	var dataset = target.dataset,
		index = dataset.index,
		type = dataset.type,
		item = dataset.item,
		param = e.which===1?1 : e.which===3?-1 : 0;

	if(type==="select" && index!=="all") {
		if(!e.shiftKey) {
			_ml.upgrade.mousedown.selectIndex = parseInt(index);
			_ml.upgrade.mousedown.selectValue = !_ml.mobs[index].selected;
		} else if(_ml.upgrade.mousedown.selectIndex) {
			var from = _ml.upgrade.mousedown.selectIndex,
				to = parseInt(index),
				inc = from<to?1:-1;
			index = [to];
			for(;from!==to;from+=inc) {
				index.push(from);
			}
			param = _ml.upgrade.mousedown.selectValue;
		}
	}

	_ml.upgrade.exec(index,type,item,param);
},

exec : function(index,type,item,param) {
	if(_ml.upgrade.status !== 3) {
		return;
	}

	var mobs = _ml.mobs;
	if(!isNaN(index)) {
		mobs = [mobs[index]];
	} else if(Array.isArray(index)) {
		mobs = mobs.filter(mob=>index.includes(mob.index));
	} else if(index === "selected") {
		mobs = mobs.filter(mob=>mob.visible&&mob.selected);
	} else if(index === "all") {
	}

	// type : select, reset, pa, er, ct
	if(type === "select") {

		var selected;
		if(typeof param === "boolean") {
			selected = param;
		} else {
			selected = !_ml.mobs[index].selected;
		}

		mobs.forEach(function(mob){
			if(mob.visible) {
				mob.selected = selected;
				mob.node.li.classList[selected?"add":"remove"]("hvut-ml-up-selected");
			}
		});

	} else if(type === "reset") {

		mobs.forEach(function(mob){
			mob.pa.forEach(function(e){
				e.to = e.value;
			});
			mob.er.forEach(function(e){
				e.to = e.value;
			});
			mob.ct.forEach(function(e){
				e.to = e.value;
			});
			_ml.upgrade.exec(mob.index,"pa","all",0);
			_ml.upgrade.exec(mob.index,"er","all",0);
			_ml.upgrade.exec(mob.index,"ct","all",0);

			_ml.upgrade.exec_calc(mob);
		});

	} else if(type==="pa" || type==="er" || type==="ct") {

		mobs.forEach(function(mob){
			var items;
			if(item === "equal") {
				var max = Math.max(...mob[type].map(e=>e.to));
				mob[type].forEach(e=>{e.to=max;});
				items = mob[type];
				param = 0;
			} else if(item === "all") {
				items = mob[type];
			} else {
				items = [mob[type][item]];
			}
			items.forEach(function(e){
				var value = e.value,
					to = e.to + param,
					max = type==="pa"?25 : type==="er"?50 : type==="ct"?e.max : 0;
				if(to < value) {
					to = value;
				} else if(to > max) {
					to = max;
				}
				e.to = to;
				e.node.textContent = to;
				if(to > value) {
					e.node.classList.add("hvut-ml-up-change");
				} else {
					e.node.classList.remove("hvut-ml-up-change");
				}
			});
			_ml.upgrade.exec_calc(mob);

			mob.node.pl.textContent = mob.pl_to;
			if(mob.pl === mob.pl_to) {
				mob.node.pl.classList.remove("hvut-ml-up-change");
			} else {
				mob.node.pl.classList.add("hvut-ml-up-change");
			}
			mob.node.morale.textContent = mob.morale_to/100;
			if(mob.morale_to < settings.monsterLabMorale) {
				mob.node.morale.classList.add("hvut-ml-up-change");
			} else {
				mob.node.morale.classList.remove("hvut-ml-up-change");
			}
		});

	}

	_ml.upgrade.sum();
},

exec_calc : function(mob) {
	mob.pa.forEach(function(e){
		e.require = _ml.upgrade.pa_crystal[e.to] - _ml.upgrade.pa_crystal[e.value];
	});
	mob.er.forEach(function(e){
		e.require = _ml.upgrade.er_crystal[e.to] - _ml.upgrade.er_crystal[e.value];
	});

	mob.ct.require = mob.ct.reduce((p,c)=>p+(c.value+1+c.to)*(c.to-c.value)/2,0);
	mob.pl_to = Math.round( mob.pa.reduce((p,c)=>p+_ml.upgrade.pa_pl[c.to],0) + mob.er.reduce((p,c)=>p+_ml.upgrade.er_pl[c.to],0) );
	mob.morale_to = Math.min(24000, mob.morale + mob.pa.reduce((p,c)=>p+(_ml.upgrade.pa_morale[c.to]-_ml.upgrade.pa_morale[c.value]),0) + mob.er.reduce((p,c)=>p+(_ml.upgrade.er_morale[c.to]-_ml.upgrade.er_morale[c.value]),0) );
},

sum : function(skip) {
	if(!skip) {
		_ml.upgrade.pa.forEach(function(e){e.require=0;});
		_ml.upgrade.er.forEach(function(e){e.require=0;});
		_ml.upgrade.ct.require = 0;

		_ml.mobs.forEach(function(mob){
			if(!mob.visible || !mob.selected) {
				return;
			}
			mob.pa.forEach(function(e,i){
				_ml.upgrade.pa[i].require += e.require;
			});
			mob.er.forEach(function(e,i){
				_ml.upgrade.er[i].require += e.require;
			});
			_ml.upgrade.ct.require += mob.ct.require;
		});
	}

	_ml.upgrade.pa.forEach(function(e){
		e.li.innerHTML =
			"<span>"+e.crystal.slice(11)+"</span>"+
			"<span>"+e.used.toLocaleString()+"</span>"+
			"<span>+"+e.require.toLocaleString()+"</span>"+
			"<span>("+e.stock.toLocaleString()+")</span>";

		if(e.require > e.stock) {
			e.li.classList.add("hvut-ml-up-nostock");
		} else {
			e.li.classList.remove("hvut-ml-up-nostock");
		}
	});

	_ml.upgrade.er.forEach(function(e){
		e.li.innerHTML =
			"<span>"+e.crystal.slice(11)+"</span>"+
			"<span>"+e.used.toLocaleString()+"</span>"+
			"<span>+"+e.require.toLocaleString()+"</span>"+
			"<span>("+e.stock.toLocaleString()+")</span>";

		if(e.require > e.stock) {
			e.li.classList.add("hvut-ml-up-nostock");
		} else {
			e.li.classList.remove("hvut-ml-up-nostock");
		}
	});

	var ul = _ml.upgrade.ct.ul;
	ul.innerHTML = "";
	$element("li",ul,"Chaos Tokens");
	$element("li",ul,["/<span>Slots</span><span>"+_ml.upgrade.ct.slot.toLocaleString()+(_ml.upgrade.ct.gs?" * 2":"")+"</span>"]);
	$element("li",ul,["/<span>Unlocked</span><span>"+_ml.upgrade.ct.unlock.toLocaleString()+"</span>"]);
	$element("li",ul,["/<span>Upgraded</span><span>"+_ml.upgrade.ct.used.toLocaleString()+"</span>"]);
	$element("li",ul,["/<span>Requires</span><span>"+_ml.upgrade.ct.require.toLocaleString()+"</span>"]);
	$element("li",ul,[(_ml.upgrade.ct.require>_ml.upgrade.ct.stock?".hvut-ml-up-nostock":""),"/<span>Stock</span><span>"+_ml.upgrade.ct.stock.toLocaleString()+"</span>"]);

	_ml.upgrade.stock = !$qs(".hvut-ml-up-nostock");
	_ml.upgrade.node.run.disabled = !_ml.upgrade.stock;
},

run : function() {
	if(!_ml.upgrade.stock) {
		alert("Not enough Crystals or Chaos Tokens");
		return;
	}
	if(_ml.upgrade.status !== 3) {
		return false;
	}
	if(!confirm("Are you sure you wish to upgrade monsters?")) {
		return;
	}

	_ml.upgrade.status = 4;
	_ml.upgrade.succeeded = 0;
	_ml.upgrade.failed = 0;
	_ml.upgrade.node.run.disabled = true;

	var requests = 0;

	_ml.mobs.forEach(function(mob){
		if(!mob.visible || !mob.selected) {
			return;
		}
		var need_update;

		mob.pa.forEach(function(e,i){
			var r = e.to - e.value;
			if(r < 1) {
				return;
			}
			need_update = true;
			requests += r;
			while(r--) {
				$ajax.add("POST","?s=Bazaar&ss=ml&slot="+mob.index,"crystal_upgrade="+_ml.upgrade.pa[i].query,_ml.upgrade.feed_succeed,_ml.upgrade.feed_error);
			}
		});

		mob.er.forEach(function(e,i){
			var r = e.to - e.value;
			if(r < 1) {
				return;
			}
			need_update = true;
			requests += r;
			while(r--) {
				$ajax.add("POST","?s=Bazaar&ss=ml&slot="+mob.index,"crystal_upgrade="+_ml.upgrade.er[i].query,_ml.upgrade.feed_succeed,_ml.upgrade.feed_error);
			}
		});

		mob.ct.forEach(function(e,i){
			var r = e.to - e.value;
			if(r < 1) {
				return;
			}
			need_update = true;
			requests += r;
			while(r--) {
				$ajax.add("POST","?s=Bazaar&ss=ml&slot="+mob.index,"chaos_upgrade="+_ml.upgrade.ct[i].query,_ml.upgrade.feed_succeed,_ml.upgrade.feed_error);
			}
		});

		if(need_update) {
			mob.need_update = true;
			mob.log.pl = -1;
		}
	});

	_ml.upgrade.requests = requests;
	setValue("ml_log",_ml.log);
},

feed_succeed : function() {
	_ml.upgrade.succeeded++;
	if(!_ml.upgrade.requests) {
		return;
	}

	var completed = _ml.upgrade.succeeded + _ml.upgrade.failed;
	_ml.upgrade.node.run.value = completed + " / " + _ml.upgrade.requests;

	if(completed === _ml.upgrade.requests) {
		_ml.upgrade.status = 3;
		_ml.upgrade.update();
	}
},

feed_error : function() {
	if($ajax.error && !_ml.upgrade.error) {
		_ml.upgrade.error = $ajax.error;
		return;
	}

	_ml.upgrade.failed++;
	if(!_ml.upgrade.requests) {
		return;
	}

	var completed = _ml.upgrade.succeeded + _ml.upgrade.failed;
	_ml.upgrade.node.run.value = completed + " / " + _ml.upgrade.requests;

	if(completed === _ml.upgrade.requests) {
		_ml.upgrade.status = 3;
		_ml.upgrade.update();
	}
},

save : function() {
	_ml.mobs.forEach(function(mob){
		var log = mob.log;
		log.selected = mob.visible && mob.selected;

		log.pa.forEach(function(e,i){
			e.to = mob.pa[i].to;
		});
		log.er.forEach(function(e,i){
			e.to = mob.er[i].to;
		});
		log.ct.forEach(function(e,i){
			e.to = mob.ct[i].to;
		});
	});

	setValue("ml_log",_ml.log);
},

load : function() {
	_ml.mobs.forEach(function(mob,i){
		mob.visible = true;
		mob.node.li.classList.remove("hvut-none");

		if(mob.selected !== mob.log.selected) {
			_ml.upgrade.exec(i,"select");
		}

		mob.pa.forEach(function(e,j){
			e.to = mob.log.pa[j].to || e.value;
		});
		mob.er.forEach(function(e,j){
			e.to = mob.log.er[j].to || e.value;
		});
		mob.ct.forEach(function(e,j){
			e.to = mob.log.ct[j].to || e.value;
		});
	});

	_ml.upgrade.exec("all","pa","all",0);
	_ml.upgrade.exec("all","er","all",0);
	_ml.upgrade.exec("all","ct","all",0);
}

};

// PL-Crystal Calculator
_ml.lab = {

preset : {
	"250": {count:1, pa_lv:5, pa_up:4, er_lv:14, er_up:0},
	"500": {count:1, pa_lv:9, pa_up:3, er_lv:21, er_up:4},
	"750": {count:1, pa_lv:12, pa_up:3, er_lv:27, er_up:1},
	"1000": {count:1, pa_lv:15, pa_up:1, er_lv:32, er_up:0},
	"1250": {count:1, pa_lv:17, pa_up:2, er_lv:36, er_up:3},
	"1500": {count:1, pa_lv:19, pa_up:3, er_lv:40, er_up:2},
	"1750": {count:1, pa_lv:21, pa_up:2, er_lv:43, er_up:5},
	"2250": {count:1, pa_lv:25, pa_up:0, er_lv:50, er_up:0},
},
data : {
	pa_crystal: [0],
	pa_pl: [0],
	er_crystal: [0],
	er_pl: [0],
},
list : [],
node : {
	div : $element("div",$id("mainpane"),[".hvut-ml-lab hvut-none"])
},

init : function() {
	if(_ml.lab.inited) {
		return;
	}
	_ml.lab.inited = true;

	GM_addStyle(`
		.hvut-ml-lab {display:flex; position:absolute; top:27px; left:0; width:100%; height:675px; justify-content:center; align-items:center; z-index:9; background-color:#EDEBDF; font-size:10pt; text-align:left; white-space:nowrap}

		.hvut-ml-lab-right {height:635px; margin-left:20px}
		.hvut-ml-lab-buttons {display:flex; flex-wrap:wrap; justify-content:space-between; width:250px}
		.hvut-ml-lab-buttons input {margin:0 0 4px}
		.hvut-ml-lab-buttons input:nth-child(-n+3) {width:32%}
		.hvut-ml-lab-buttons input:nth-child(4) {width:100%; margin-top:16px}
		.hvut-ml-lab-buttons input:nth-child(n+5) {width:24%}
		.hvut-ml-lab-table {margin-top:20px; width:480px; border-collapse:collapse}
		.hvut-ml-lab-table tr:first-child {font-weight:bold}
		.hvut-ml-lab-table td {border:1px solid; padding:2px 5px}
		.hvut-ml-lab-table td:first-child {text-align:right}

		.hvut-ml-lab-left {width:600px; height:530px; margin-top:105px; overflow:auto; line-height:26px}
		.hvut-ml-lab-left > div {display:flex; width:572px; margin:5px 0; padding:5px 0; border:1px solid}
		.hvut-ml-lab-left > div:first-child {position:absolute; margin-top:-105px; outline:1px solid}
		.hvut-ml-lab-left > div > div {width:240px; padding:5px; border-left:1px solid}
		.hvut-ml-lab-left > div > div:first-child {width:60px; border-left:none}

		.hvut-ml-lab-left input {vertical-align:initial}
		.hvut-ml-lab-left input[type='number'] {width:50px; text-align:right}
		.hvut-ml-lab-del {width:22px; margin:0 10px 0 0 !important; padding:1px 0 !important}
		.hvut-ml-lab-btn {display:inline-block; width:140px; text-align:center}
		.hvut-ml-lab-btn > span {display:inline-block; width:18px; line-height:18px; border:1px solid; margin:0 1px; text-align:center; background-color:#fff; border-radius:3px; cursor:default}
		.hvut-ml-lab-btn > input {width:25px; padding:2px 0; border-width:1px; border-radius:0}
		.hvut-ml-lab-btn > .hvut-ml-lab-up {background-color:#edb}
		.hvut-ml-lab-crystal {display:inline-block; width:95px; text-align:right}
	`);

	var data = _ml.lab.data;
	for(let i=0;i<26;i++) {
		data.pa_pl[i+1] = data.pa_pl[i] + (3+i*0.5);
		data.pa_crystal[i+1] = data.pa_crystal[i] + Math.round(50 * Math.pow(1.555079154,i));
	}
	for(let i=0;i<51;i++) {
		data.er_pl[i+1] = data.er_pl[i] + Math.floor(1+i*0.1);
		data.er_crystal[i+1] = data.er_crystal[i] + Math.round(10 * Math.pow(1.26485522,i));
	}

	var node = _ml.lab.node;

	node.left = $element("div",node.div,[".hvut-ml-lab-left"]);

	var total = $element("div",node.left);
	$element("div",total).append(
		$element("span",null,"Monsters"),$element("br"),$element("br"),
		node.count = $element("input",null,{type:"number",min:0,max:200,readOnly:true})
	);
	$element("div",total).append(
		$element("span",null,"Primary Attributes"),$element("br"),
		$element("span",null,[" Crystal",".hvut-ml-lab-btn"]),
		node.pa_total = $element("span",null,[".hvut-ml-lab-crystal"]),$element("br"),
		$element("span",null,[" Difference",".hvut-ml-lab-btn"]),
		node.pa_total_diff = $element("span",null,[".hvut-ml-lab-crystal"])
	);
	$element("div",total).append(
		$element("span",null,"Elemental Mitigations"),$element("br"),
		$element("span",null,[" Crystal",".hvut-ml-lab-btn"]),
		node.er_total = $element("span",null,[".hvut-ml-lab-crystal"]),$element("br"),
		$element("span",null,[" Difference",".hvut-ml-lab-btn"]),
		node.er_total_diff = $element("span",null,[".hvut-ml-lab-crystal"])
	);

	node.right = $element("div",node.div,[".hvut-ml-lab-right"]);

	var buttons = $element("div",node.right,[".hvut-ml-lab-buttons"]);
	$element("input",buttons,{type:"button",value:"Save"},function(){_ml.lab.save();});
	$element("input",buttons,{type:"button",value:"Load"},function(){_ml.lab.load();});
	$element("input",buttons,{type:"button",value:"Close"},function(){_ml.lab.toggle();});
	$element("input",buttons,{type:"button",value:"Add Monster"},function(){_ml.lab.add();});
	Object.entries(_ml.lab.preset).forEach(([k,p])=>{
		$element("input",buttons,{type:"button",value:k},function(){_ml.lab.add(p);});
	});

	$element("table",node.right,[".hvut-ml-lab-table","/"+
		`<tbody>
		<tr><td>Power<br> Level</td><td>Effects</td></tr>
		<tr><td>25</td><td>Unlocks naming and becomes active in battles once named</td></tr>
		<tr><td>200</td><td>Unlocks second Skill Attack</td></tr>
		<tr><td>250</td><td>Can no longer be deleted<br>Morale drain reduced by 2x</td></tr>
		<tr><td>251</td><td>Requires Monster Edibles instead of Monster Chow as Food</td></tr>
		<tr><td>400</td><td>Unlocks Spirit Attack</td></tr>
		<tr><td>499</td><td>Gifts may now include High-Grade materials</td></tr>
		<tr><td>750</td><td>Morale drain reduced by 3x<br>Low-Grade materials can no longer be gifts</td></tr>
		<tr><td>751</td><td>Requires Monster Cuisine instead of Monster Edibles as Food</td></tr>
		<tr><td>1000</td><td>Will never be deactivated</td></tr>
		<tr><td>1005</td><td>All Chaos Upgrades are available</td></tr>
		<tr><td>1250</td><td>Morale drain reduced by 4x</td></tr>
		<tr><td>1499</td><td>Mid-Grade materials can no longer be gifts (100% are High-Grade)</td></tr>
		<tr><td>1750</td><td>Morale drain reduced by 5x</td></tr>
		<tr><td>2250</td><td>Power Level cap reached<br>Morale drain reduced by 6x</td></tr>
		</tbody>`]);

	_ml.lab.load();
},

save : function() {
	setValue("ml_lab",_ml.lab.list.map(m=>m.json));
},
load : function() {
	_ml.lab.list.forEach(m=>m.node.div.remove());
	_ml.lab.list.length = 0;
	getValue("ml_lab",[_ml.lab.preset["250"]]).forEach(function(m){
		_ml.lab.add(m);
	});
},
toggle : function() {
	_ml.lab.init();
	_ml.lab.node.div.classList.toggle("hvut-none");
},
remove : function(m) {
	if(m) {
		m.node.div.remove();
		_ml.lab.list.splice(_ml.lab.list.indexOf(m),1);
		_ml.lab.list.forEach(function(m,i){m.node.index.textContent="#"+(i+1);});
	}
	_ml.lab.calc();
},
select : function() {
	this.select();
},

add : function(j) {
	var m = {json:{count:1,pa_lv:0,pa_up:0,er_lv:0,er_up:0},node:{}};
	if(j) {
		Object.assign(m.json,j);
	}
	m.node.div = $element("div",_ml.lab.node.left);
	var sub,span;

	sub = $element("div",m.node.div);
	$element("input",sub,{type:"button",className:"hvut-ml-lab-del",value:"x"},function(){_ml.lab.remove(m);});
	m.node.index = $element("span",sub,"#"+(_ml.lab.list.length+1));
	$element("br",sub);
	m.node.pl = $element("span",sub);
	$element("br",sub);
	m.node.count = $element("input",sub,{type:"number",value:m.json.count,min:0,max:200},{input:function(){_ml.lab.change(m,"count");},focus:_ml.lab.select});

	sub = $element("div",m.node.div);
	$element("span",sub,"Primary Attributes");
	$element("br",sub);

	span = $element("span",sub,[".hvut-ml-lab-btn"]);
	m.node.pa = [];
	for(let i=0;i<6;i++) {
		m.node.pa.push($element("span",span));
	}
	m.node.pa_avg = $element("span",sub,[".hvut-ml-lab-crystal"]);
	$element("br",sub);

	span = $element("span",sub,[".hvut-ml-lab-btn"]);
	$element("input",span,{type:"button",value:"-6"},function(){_ml.lab.change(m,"pa","-");});
	$element("input",span,{type:"button",value:"-1"},function(){_ml.lab.change(m,"pa",-1);});
	$element("input",span,{type:"button",value:"+1"},function(){_ml.lab.change(m,"pa",+1);});
	$element("input",span,{type:"button",value:"+6"},function(){_ml.lab.change(m,"pa","+");});
	m.node.pa_diff = $element("span",sub,[".hvut-ml-lab-crystal"]);

	sub = $element("div",m.node.div);
	$element("span",sub,"Elemental Mitigations");
	$element("br",sub);

	span = $element("span",sub,[".hvut-ml-lab-btn"]);
	m.node.er = [];
	for(let i=0;i<6;i++) {
		m.node.er.push($element("span",span));
	}
	m.node.er_avg = $element("span",sub,[".hvut-ml-lab-crystal"]);
	$element("br",sub);

	span = $element("span",sub,[".hvut-ml-lab-btn"]);
	$element("input",span,{type:"button",value:"-6"},function(){_ml.lab.change(m,"er","-");});
	$element("input",span,{type:"button",value:"-1"},function(){_ml.lab.change(m,"er",-1);});
	$element("input",span,{type:"button",value:"+1"},function(){_ml.lab.change(m,"er",+1);});
	$element("input",span,{type:"button",value:"+6"},function(){_ml.lab.change(m,"er","+");});
	m.node.er_diff = $element("span",sub,[".hvut-ml-lab-crystal"]);

	_ml.lab.list.push(m);
	_ml.lab.change(m);
},

change : function(m,p,v) {
	if(!p) {
	} else if(p === "count") {
		m.json[p] = (v===undefined?parseInt(m.node[p].value):parseInt(v)) || 0;
	} else {
		var lv = m.json[p+"_lv"],
			up = m.json[p+"_up"],
			max = p==="pa"?25:p==="er"?50:null;
		if(v === "+") {
			lv++;
			up = 0;
		} else if(v === "-") {
			if(up === 0) {
				lv--;
			}
			up = 0;
		} else {
			up += v;
			if(up >= 6) {
				lv++;
				up -= 6;
			} else if(up < 0) {
				lv--;
				up += 6;
			}
		}
		if(lv < 0) {
			lv = 0;
			up = 0;
		} else if(lv >= max) {
			lv = max;
			up = 0;
		}
		m.json[p+"_lv"] = lv;
		m.json[p+"_up"] = up;
	}

	if(m.node.count.validity.valid) {
		var data = _ml.lab.data,
			pa_lv = m.json.pa_lv,
			pa_up = m.json.pa_up,
			er_lv = m.json.er_lv,
			er_up = m.json.er_up;
		m.count = m.json.count;
		m.pl = data.pa_pl[pa_lv] * (6-pa_up) + data.pa_pl[pa_lv+1] * (pa_up) + data.er_pl[er_lv] * (6-er_up) + data.er_pl[er_lv+1] * (er_up);
		m.pa_avg = (data.pa_crystal[pa_lv] * (6-pa_up) + data.pa_crystal[pa_lv+1] * (pa_up)) / 6;
		m.er_avg = (data.er_crystal[er_lv] * (6-er_up) + data.er_crystal[er_lv+1] * (er_up)) / 6;
		m.diff = m.pa_avg - m.er_avg;

		m.node.pl.textContent = "PL " + m.pl;
		m.node.pa.forEach(function(span,i){
			if(i + pa_up >= 6) {
				span.textContent = pa_lv + 1;
				span.classList.add("hvut-ml-lab-up");
			} else {
				span.textContent = pa_lv;
				span.classList.remove("hvut-ml-lab-up");
			}
		});
		m.node.er.forEach(function(span,i){
			if(i + er_up >= 6) {
				span.textContent = er_lv + 1;
				span.classList.add("hvut-ml-lab-up");
			} else {
				span.textContent = er_lv;
				span.classList.remove("hvut-ml-lab-up");
			}
		});

		m.node.pa_avg.textContent = Math.round(m.pa_avg).toLocaleString();
		m.node.pa_diff.textContent = m.diff>0?"(+"+Math.round(m.diff).toLocaleString()+")":"";
		m.node.er_avg.textContent = Math.round(m.er_avg).toLocaleString();
		m.node.er_diff.textContent = m.diff<0?"(+"+Math.round(-m.diff).toLocaleString()+")":"";

		m.valid = true;
	} else {
		m.valid = false;
	}

	_ml.lab.calc();
},

calc : function() {
	var total_count=0, total_pa=0, total_er=0, total_diff;
	_ml.lab.list.forEach(m=>{
		if(!m.valid) {
			return;
		}
		total_count += m.count;
		total_pa += m.pa_avg * m.count;
		total_er += m.er_avg * m.count;
	});
	total_diff = total_pa - total_er;

	_ml.lab.node.count.value = total_count;
	_ml.lab.node.pa_total.textContent = Math.round(total_pa).toLocaleString();
	_ml.lab.node.pa_total_diff.textContent = total_diff>0?"(+"+Math.round(total_diff).toLocaleString()+")":"";
	_ml.lab.node.er_total.textContent = Math.round(total_er).toLocaleString();
	_ml.lab.node.er_total_diff.textContent = total_diff<0?"(+"+Math.round(-total_diff).toLocaleString()+")":"";
}

};

}

} else
// [END 11] Bazaar - Monster Lab */


//* [12] Bazaar - The Shrine
if(settings.shrine && _query.s==="Bazaar" && _query.ss==="ss") {

_ss.log = getValue("ss_log",{});
_ss.node = {};
_ss.equip = {selected:null,capacity:0,current:0,request:0,received:0,sold:0,salvaged:0,total:0};
_ss.items = {};
_ss.trophies = {
	"ManBearPig Tail": {tier:2,value:1000},
	"Holy Hand Grenade of Antioch": {tier:2,value:1000},
	"Mithra's Flower": {tier:2,value:1000},
	"Dalek Voicebox": {tier:2,value:1000},
	"Lock of Blue Hair": {tier:2,value:1000},
	"Bunny-Girl Costume": {tier:3,value:2000},
	"Hinamatsuri Doll": {tier:3,value:2000},
	"Broken Glasses": {tier:3,value:2000},
	"Black T-Shirt": {tier:4,value:4000},
	"Sapling": {tier:4,value:4000},
	"Unicorn Horn": {tier:5,value:5000},
	"Noodly Appendage": {value:5000},
};
_ss.index_items = [
	"Precursor Artifact",
	"Trophy Tier 2","Trophy Tier 3","Trophy Tier 4","Trophy Tier 5",
	"ManBearPig Tail","Holy Hand Grenade of Antioch","Mithra's Flower","Dalek Voicebox","Lock of Blue Hair","Bunny-Girl Costume","Hinamatsuri Doll","Broken Glasses","Black T-Shirt","Sapling","Unicorn Horn","Noodly Appendage","Stocking Stuffers","Tenbora's Box","Peerless Voucher",
	"Mysterious Box","Solstice Gift","Shimmering Present","Potato Battery","RealPervert Badge","Raptor Jesus","Rainbow Egg","Colored Egg","Gift Pony","Faux Rainbow Mane Cap","Pegasopolis Emblem","Fire Keeper Soul","Crystalline Galanthus","Sense of Self-Satisfaction","Six-Lock Box","Golden One-Bit Coin","USB ASIC Miner","Reindeer Antlers","Ancient Porn Stash","VPS Hosting Coupon","Heart Locket","Holographic Rainbow Projector","Pot of Gold","Dinosaur Egg","Precursor Smoothie Blender","Rainbow Smoothie","Mysterious Tooth","Grammar Nazi Armband","Abstract Wire Sculpture","Delicate Flower","Assorted Coins","Coin Collector's Guide","Iron Heart","Shrine Fortune","Plague Mask","Festival Coupon","Annoying Gun",
	"Platinum Coupon","Golden Coupon","Silver Coupon","Bronze Coupon",
];

_ss.toggle_results = function() {
	_ss.node.results.classList.toggle("hvut-none");
};

_ss.offer = function(name,count) {
	if(_ss.error) {
		popup(_ss.error);
		return;
	}
	var item = _ss.items[name];
	if(count > item.max) {
		count = item.max;
	}
	if(!count) {
		return;
	}

	var select_reward_type,
		select_reward_slot;
	if(item.type === "Trophy") {
		if(_ss.equip.selected && !_ss.equip.selected.disabled) {
			select_reward_type = _ss.equip.selected.dataset.select_reward_type;
			select_reward_slot = _ss.equip.selected.dataset.select_reward_slot;
		} else {
			popup("Select the major class of the item");
			return;
		}
		_ss.equip.request += count;
	} else { // Artifact, Collectable
		select_reward_type = "";
		select_reward_slot = "";
	}

	if(!_ss.log[item.log]) {
		_ss.log[item.log] = {};
	}
	if(!item.results) {
		item.results = _ss.create_list(item.type);
		item.node.header = $element("h4",_ss.node.results,[" "+name+(item.upgrade?" => Tier "+item.upgrade:"")+" ",".hvut-ss-h4"]).appendChild($element("span"));
		item.node.ul = $element("ul",_ss.node.results,[".hvut-ss-ul"]);
		Object.values(item.results).forEach(function(r){
			item.node.ul.appendChild(r.li).classList.add("hvut-none");
		});
		scrollIntoView(item.node.ul);
	}
	_ss.node.results.classList.remove("hvut-none");

	item.request += count;
	item.stock -= count * item.bulk;
	item.max -= count;
	item.node.stock.textContent = item.stock;
	item.node.max.textContent = item.max;

	while(count--) {
		$ajax.add("POST","?s=Bazaar&ss=ss","select_item="+item.id+"&select_reward_type="+select_reward_type+"&select_reward_slot="+select_reward_slot,_ss.response,null,{name:name});
	}
};

_ss.response = function(r) {
	var doc = $doc(r.responseText),
		name = r.context.name,
		item = _ss.items[name],
		results = item.results,
		rewards = [],
		reg = /Received (.+)|(Your .+ has increased by one)|((?:Crude|Fair|Average|Superior|Exquisite|Magnificent|Legendary|Peerless) .+)/;

	get_message(doc,true).forEach(function(msg){
		if(!msg || msg==="Snowflake has blessed you with some of her power!" || msg==="Snowflake has blessed you with an item!" || msg==="Received:" || msg==="Hit Space Bar to offer another item like this.") {
			return;
		} else if(msg.includes("Peerless Voucher")) { // "Received 1x Peerless Voucher!"
			popup("<span style='color:#f00;font-weight:bold'>"+msg+"</span>");
		} else if(reg.test(msg)) {
			rewards.push(RegExp.$1||RegExp.$2||RegExp.$3);
		} else if(msg.includes("Sold it for")) {
			_ss.equip.sold++;
		} else if(msg.includes("Salvaged it for")) {
			_ss.equip.salvaged++;
		} else if(msg === "Your equipment inventory is full") {
			if(!_ss.error) {
				_ss.error = msg;
				popup(_ss.error);
			}
		} else {
			rewards.push(msg);
		}
	});

	item.recieved++;
	item.node.header.textContent = "("+item.recieved+"/"+item.request+")";

	rewards.forEach(function(n){
		var r = item.type==="Trophy"?n.split(" ")[0]:n;
		if(!_ss.log[item.log][r]) {
			_ss.log[item.log][r] = 0;
		}
		_ss.log[item.log][r]++;

		if(!results[r]) {
			results[r] = _ss.create_listitem(r);
			item.node.ul.appendChild(results[r].li);
		}
		if(!results[r].count) {
			results[r].li.classList.remove("hvut-none");
			if(results[r].group) {
				results[results[r].group].li.classList.remove("hvut-none");
			}
		}

		results[r].count++;
		if(results[r].group) {
			results[results[r].group].count++;
		}
		Object.keys(results).forEach(function(k){
			results[k].sp.textContent = (results[k].count*100/item.recieved).toFixed(1)+" %";
			results[k].sc.textContent = " ["+results[k].count+"] ";
		});

		if(item.type === "Trophy") {
			if($equip.filter(n,settings.shrineTrackEquip)) {
				$element("li",[results[r].li,"afterend"],[" "+n,".hvut-ss-equip"]);
			}
			_ss.equip.received++;
			_ss.equip.total = _ss.equip.current + _ss.equip.received - _ss.equip.sold - _ss.equip.salvaged;
			_ss.node.equip.value = "Equip Slots: "+_ss.equip.total+" / "+_ss.equip.capacity+(_ss.equip.sold?", Sold: "+_ss.equip.sold:"")+(_ss.equip.salvaged?", Salvaged: "+_ss.equip.salvaged:"");
			if(_ss.equip.total >= _ss.equip.capacity) {
				if(!_ss.error) {
					_ss.error = "Your equipment inventory is full";
					popup(_ss.error);
				}
			}
		}
	});

	if(item.recieved%10===0 || item.recieved===item.request || _ss.error) {
		setValue("ss_log",_ss.log);
	}
};

_ss.toggle_log = function() {
	if(_ss.node.log.classList.contains("hvut-none")) {
		_ss.view_log();
		_ss.node.log.classList.remove("hvut-none");
	} else {
		_ss.node.log.innerHTML = "";
		_ss.node.log.classList.add("hvut-none");
	}
};

_ss.view_log = function() {
	var div = _ss.node.log;
	div.innerHTML = "";

	var type_artifact = /Energy Drink|Hath|Last Elixir|1000x Crystal of|has increased by one"/,
		type_trophy = /Peerless|Legendary|Magnificent|Exquisite|Superior|Average/,
		type_collectable = /High-Grade|Binding of/;

	object_sort(_ss.log,_ss.index_items);
	Object.entries(_ss.log).forEach(function([n,log]){
		let keys = Object.keys(log),
			type = keys.some(k=>type_artifact.test(k)) ? "Artifact" : keys.some(k=>type_trophy.test(k)) ? "Trophy" : keys.some(k=>type_collectable.test(k)) ? "Collectable" : null,
			list = _ss.create_list(type),
			total = Object.values(log).reduce((p,c)=>p+c);
		if(type === "Collectable") {
			total /= 2;
		}

		$element("h4",div,[" "+n+" ("+total+")",".hvut-ss-h4"]);
		let ul = $element("ul",div,[".hvut-ss-ul"]);

		Object.entries(log).forEach(function([r,c]){
			if(!list[r]) {
				list[r] = _ss.create_listitem(r);
			}
			list[r].count = c;
			if(list[r].group) {
				list[list[r].group].count += c;
			}
		});

		Object.keys(list).forEach(function(r){
			if(!list[r].count) {
				return;
			}
			list[r].sp.textContent = (list[r].count*100/total).toFixed(1)+" %";
			list[r].sc.textContent = " ["+list[r].count+"] ";
			ul.appendChild(list[r].li);
		});
	});

	$element("input", $element("div",div,["!margin-top:20px;padding:10px 0;border-top:1px solid"]), {type:"button",value:"Reset Log"},function(){if(confirm("Shrine reward log will be deleted.\n\nContinue?")){deleteValue("ss_log");location.href=location.href;}});
};

_ss.create_list = function(type) {
	var array;
	if(type === "Artifact") {
		array = ["Energy Drink","2 Hath","1 Hath","Last Elixir",{group:"1000x Crystal",member:["1000x Crystal of Vigor","1000x Crystal of Finesse","1000x Crystal of Swiftness","1000x Crystal of Fortitude","1000x Crystal of Cunning","1000x Crystal of Knowledge","1000x Crystal of Flames","1000x Crystal of Frost","1000x Crystal of Lightning","1000x Crystal of Tempest","1000x Crystal of Devotion","1000x Crystal of Corruption"]},{group:"Primary Attributes Bonus",member:["Your strength has increased by one","Your dexterity has increased by one","Your agility has increased by one","Your endurance has increased by one","Your intelligence has increased by one","Your wisdom has increased by one"]}];
	} else if(type === "Trophy") {
		array = ["Peerless","Legendary","Magnificent","Exquisite","Superior","Average"];
	} else if(type === "Collectable") {
		array = [{group:"3x High-Grade Material",member:["3x High-Grade Cloth","3x High-Grade Leather","3x High-Grade Metals","3x High-Grade Wood"]},{group:"2x High-Grade Material",member:["2x High-Grade Cloth","2x High-Grade Leather","2x High-Grade Metals","2x High-Grade Wood"]},{group:"1x High-Grade Material",member:["1x High-Grade Cloth","1x High-Grade Leather","1x High-Grade Metals","1x High-Grade Wood"]},{group:"Binding",member:["Binding of Slaughter","Binding of Balance","Binding of Isaac","Binding of Destruction","Binding of Focus","Binding of Friendship","Binding of Protection","Binding of Warding","Binding of the Fleet","Binding of the Barrier","Binding of the Nimble","Binding of Negation","Binding of the Ox","Binding of the Raccoon","Binding of the Cheetah","Binding of the Turtle","Binding of the Fox","Binding of the Owl","Binding of Surtr","Binding of Niflheim","Binding of Mjolnir","Binding of Freyr","Binding of Heimdall","Binding of Fenrir","Binding of the Elementalist","Binding of the Heaven-sent","Binding of the Demon-fiend","Binding of the Earth-walker","Binding of the Curse-weaver","Binding of Dampening","Binding of Deflection","Binding of Stoneskin","Binding of the Fire-eater","Binding of the Frost-born","Binding of the Thunder-child","Binding of the Wind-waker","Binding of the Thrice-blessed","Binding of the Spirit-ward"]}];
	} else {
		array = [];
	}
	var list = {};
	array.forEach(function(r){
		if(typeof r === "string") {
			list[r] = _ss.create_listitem(r);
		} else {
			let g = r.group;
			list[g] = _ss.create_listitem(g);
			r.member.forEach(function(m){
				list[m] = _ss.create_listitem(m,g);
			});
		}
	});
	return list;
};

_ss.create_listitem = function(r,g) {
	var item = {count:0};
	item.li = $element("li");
	item.sp = $element("span",item.li);
	item.sc = $element("span",item.li);
	$element("span",item.li,r);
	if(g) {
		item.group = g;
		item.li.classList.add("hvut-ss-group");
	}
	return item;
};

_ss.select_equip = function(e) {
	var selected = e.target;
	if(!_ss.equip.selected) {
		selected.classList.add("hvut-ss-selected");
		_ss.equip.selected = selected;
	} else if(_ss.equip.selected === selected) {
		selected.classList.remove("hvut-ss-selected");
		_ss.equip.selected = null;
	} else {
		_ss.equip.selected.classList.remove("hvut-ss-selected");
		selected.classList.add("hvut-ss-selected");
		_ss.equip.selected = selected;
	}
	e.preventDefault();
};

_ss.show_trophies = function() {
	popup_text(_ss.trophies_text,"width:500px;max-height:500px;white-space:pre");
};

GM_addStyle(`
	#shrine_outer {position:relative; width:1010px}
	#shrine_left {width:504px}
	#shrine_left .cspp {margin-top:15px; overflow-y:scroll}

	#shrine_left .itemlist td:nth-child(1) {width:170px !important}
	#shrine_left .itemlist td:nth-child(2) {width:60px}
	#shrine_left .itemlist td:nth-child(3) {width:30px; padding-left:5px; text-align:left; font-size:8pt; color:#966}
	#shrine_left .itemlist td:nth-child(4) {width:50px}
	#shrine_left .itemlist td:nth-child(5) {width:140px; padding-left:5px; text-align:left}
	#shrine_left .itemlist input {margin:0 1px}
	#shrine_left .itemlist input:nth-child(1) {width:45px; text-align:right}
	#shrine_left .itemlist input:nth-child(2) {width:50px}
	#shrine_left .itemlist input:nth-child(3) {width:35px}

	.hvut-ss-selected:not([disabled]) {color:#c00 !important; border-color:#c00 !important; outline:1px solid}

	.hvut-ss-buttons {position:absolute; top:15px; white-space:nowrap}
	.hvut-ss-log {position:absolute; top:52px; left:0; width:482px; height:552px; margin:0; padding:10px; border:1px solid; text-align:left; overflow:auto; background-color:#EDEBDF}
	.hvut-ss-results {position:absolute; top:52px; left:534px; width:452px; height:552px; margin:0; padding:10px; border:1px solid; text-align:left; overflow:auto; background-color:#EDEBDF}
	.hvut-ss-h4 {margin:10px 5px 5px; font-size:12pt}
	.hvut-ss-ul {margin:10px; padding:0; list-style:none; font-size:10pt; line-height:18px}
	.hvut-ss-ul span:first-child {display:inline-block; width:60px; text-align:right; color:#930}
	.hvut-ss-ul span:last-child {font-weight:bold}
	.hvut-ss-group {color:#966}
	.hvut-ss-group > span:first-child {visibility:hidden}
	.hvut-ss-equip {margin-left:65px; color:#930}
`);


_ss.node.left_buttons = $element("div",[$id("shrine_left").firstElementChild,"afterbegin"],[".hvut-ss-buttons"]);
$element("input",_ss.node.left_buttons,{type:"button",value:"Log"},_ss.toggle_log);
toggle_button( $element("input",_ss.node.left_buttons,{type:"button",style:"width:100px"}), "Show Items","Hide Items",$id("inv_item"),"hvut-hide-on",true);

_ss.node.right_buttons = $element("div",[$id("shrine_right").firstElementChild,"afterbegin"],[".hvut-ss-buttons"]);
$element("input",_ss.node.right_buttons,{type:"button",value:"Results"},_ss.toggle_results);
_ss.node.equip = $element("input",_ss.node.right_buttons,{type:"button",value:"Equip Slots",style:"min-width:300px"});

_ss.node.log = $element("div",$id("shrine_outer"),[".hvut-ss-log hvut-none"]);
_ss.node.results = $element("div",$id("shrine_outer"),[".hvut-ss-results hvut-none"]);

$ajax.add("GET","?s=Character&ss=in",null,function(r){
	var html = r.responseText,
		exec = />Equip Slots: (\d+)(?: \+ (\d+))? \/ (\d+)</.exec(html);
	_ss.equip.current = parseInt(exec[1]) + parseInt(exec[2]||0);
	_ss.equip.capacity = parseInt(exec[3]);
	_ss.node.equip.value = "Equip Slots: "+_ss.equip.current+" / "+_ss.equip.capacity;
});

_ss.trophies_value = 0;
_ss.trophies_text = [];

Array.from($qs(".itemlist").rows).forEach(function(tr){
	var div = tr.cells[0].firstElementChild,
		name = div.textContent,
		type = /'([^']+)'\)/.test(div.getAttribute("onmouseover")) && RegExp.$1.replace(/\W/g,"_") || "Consumable",
		exec = /snowflake\.set_shrine_item\((\w+),(?:(\d+),(\d+),)?'(.+?)'\);/.exec(div.getAttribute("onclick")),
		id = exec[1],
		stock = parseInt(exec[2]),
		bulk = parseInt(exec[3]),
		max = Math.floor(stock/bulk),
		item = {type:type,id:id,log:name,stock:stock,bulk:bulk,max:max,request:0,recieved:0,node:{}};

	_ss.items[name] = item;

	item.node.stock = tr.cells[1];
	item.node.bulk = $element("td",tr);
	item.node.max = $element("td",tr);
	var td = $element("td",tr);
	item.node.count = $element("input",td,{type:"text"});
	item.node.button = $element("input",td,{type:"button",value:"Offer"},function(){_ss.offer(name,Number(item.node.count.value));});

	if(item.type === "Trophy") {
		if(_ss.trophies[name]) {
			item.tier = _ss.trophies[name].tier;
			item.value = _ss.trophies[name].value;
			if(item.tier) {
				let t = item.tier,
					b = item.bulk;
				while(b > 1) {
					b /= t===2?4 : t===3?2 : t===4?4 : 1;
					t++;
				}
				item.value *= t===item.tier?1 : t===3?1.1 : t===4?1.2 : t===5?1.3 : 1;
				item.upgrade = t;
				item.log = "Trophy Tier " + t;
			}
			if(item.value) {
				let a = item.stock - item.stock%item.bulk;
				if(a) {
					_ss.trophies_value += a * item.value;
					_ss.trophies_text.push(a.toLocaleString()+" x "+name+" @ "+item.value.toLocaleString()+" = "+(a*item.value).toLocaleString());
				}
			}
		}
		item.node.bulk.textContent = "/"+item.bulk;
		item.node.max.textContent = item.max;
		$element("input",td,{type:"button",value:"All"},function(){_ss.offer(name,item.max);});
	}
	if( settings.shrineHideItems.some(h=>name.includes(h)) ) {
		tr.classList.add("hvut-hide");
	}
});

$element("input",$id("shrine_trophy"),{type:"button",value:"You have "+_ss.trophies_value.toLocaleString()+" credits worth of trophies in the inventory.",style:"margin:5px"},function(){_ss.show_trophies();});

$qsa("#accept_equip input[type='submit']").forEach(function(s){
	[,s.dataset.select_reward_type,s.dataset.select_reward_slot] = /submit_shrine_reward\('(.*?)','(.*?)'\)/.exec(s.getAttribute("onclick"));
	s.addEventListener("click",_ss.select_equip);
	s.removeAttribute("onclick");
});

} else
// [END 12] Bazaar - The Shrine */


//* [13] Bazaar - MoogleMail
if(settings.moogleMail && _query.s==="Bazaar" && _query.ss==="mm") {

_mm.node = {};

// MM SEND
_mm.send = function(queue) {
	var s = _mm.send,
		b;
	if(queue) {
		if(!queue.length) {
			return;
		}
		if(s.error) {
			return;
		}
		if(s.batch) {
			alert("Now sending other mail");
			return;
		}
		if(!_mm.node.to_name.value) {
			alert("No recipient");
			return;
		}

		s.index = 0;
		s.batch = [];

		b = null;
		queue.forEach(function(c,i){ // c={mail:{pane,to_name,subject,body,count,cod,price,atext},info:{name,eid,iid},node:{}}
			if(!b || c.mail.to_name && c.mail.to_name!==b.to_name || b.length >= _mm.max || !c.mail.pane || !c.mail.count) { // next batch
				b = null;
			}
			if(!b) {
				b = [];
				b.count = 0;
				b.cod = 0;
				b.deduction = 0;
				b.rcod = 0;
				b.pcod = _isekai && _mm.node.persistent.checked;
				b.atext = "";
				b.to_name = c.mail.to_name || _mm.node.to_name.value;
				b.subject = c.mail.subject || _mm.node.subject.value;
				b.body = _mm.node.body.value + (c.mail.body?"\n\n"+c.mail.body:"");
				s.batch.push(b);
			}
			b.push(c);

			c.mail.index = i;
			if(c.mail.pane && c.mail.count) { // attach
				if(c.mail.cod) {
					b.cod += c.mail.cod;
				}
				if(c.mail.atext) {
					b.atext += c.mail.atext + "\n";
				}
			} else { // no attachment
				b.status = 5;
				b = null;
			}
		});

		var error;
		s.batch.forEach(function(b){
			if(queue.deduction) {
				b.deduction = Math.min(b.cod,queue.deduction);
				queue.deduction -= b.deduction;
			}
			b.rcod = b.cod - b.deduction;
			if(b.rcod !== 0 && b.rcod < 10) {
				error = "CoD must be at least 10 credits.";
			}

			var message_to_name = b.to_name,
				message_subject = b.subject,
				message_body = b.body;
			if(!message_subject) {
				if(b[0].mail.pane) {
					message_subject = (b[0].mail.count&&b[0].mail.pane!=="equip"?b[0].mail.count.toLocaleString()+" x ":"") + b[0].info.name;
					if(b.length > 1) {
						message_subject += " and "+(b.length-1)+" item"+(b.length>2?"s":"");
					}
				} else {
					message_subject = "(no subject)";
				}
			}
			if(b.atext) {
				message_body += "\n\n========== Attachment ==========\n\n" + b.atext;
				if(b.cod) {
					if(b.length > 1) {
						message_body += "\nTotal: "+b.cod.toLocaleString()+" Credits";
					}
					if(b.deduction) {
						message_body += "\nDeduction: -"+b.deduction.toLocaleString()+" Credits";
						message_body += "\nCoD: "+b.rcod.toLocaleString()+" Credits";
					}
					if(b.pcod) {
						message_body += "\n* CoD has been sent to Persistent";
					}
				}
				message_body += "\n\n================================\n\n";
			}
			b.message_data = {mmtoken:_mm.mmtoken,action:"send",message_to_name:message_to_name,message_subject:message_subject,message_body:message_body};
		});
		if(error) {
			alert(error);
			s.batch = null;
			return;
		}

		_mm.node.to_name.disabled = true;
		_mm.node.subject.disabled = true;
		_mm.node.body.disabled = true;
		if(_mm.node.item_field) {
			_mm.node.item_field.disabled = true;
			_mm.node.equip_field.disabled = true;
			_mm.node.credits_field.disabled = true;
			_mm.node.deduction.disabled = true;
		}
		_mm.log("\n========== SENDING ==========");
	}

	b = s.batch[s.index];
	// status : 1=attaching 2=attached 3=setting cod 4=done 5=no attachment 6=no cod 7=sending 8=done
	if(!b.status) { // attach
		b.status = 1;
		_mm.log("#"+(s.index+1)+": Preparing");
		b.forEach(function(c){
			$ajax.add("POST","?s=Bazaar&ss=mm&filter=new","mmtoken="+_mm.mmtoken+"&action=attach_add&select_item="+(c.info.eid||c.info.iid||"0")+"&select_count="+(c.mail.count||"")+"&select_pane="+c.mail.pane,function(r){
				if(_mm.check(r.responseText)) {
					return;
				}
				b.count++;
				_mm.log("#"+(s.index+1)+": Attaching ["+b.count+"/"+b.length+"]");
				if(b.count === b.length) {
					b.status = 2;
					_mm.send();
				}
			});
		});

	} else if(b.status === 2) {
		if(b.rcod) { // cod
			if(b.pcod) { // send items in Isekai and sendH ealth Draught with cod in Persistent
				b.status = 30;
				_mm.log("#"+(s.index+1)+": Preparing in Persistent");
				$ajax.add("GET","/?s=Bazaar&ss=mm&filter=new",null,function(r){
					if(_mm.check(r.responseText)) {
						return;
					}
					if(!r.responseText.includes('<div id="navbar">')) {
						_mm.log("!!! Error: "+"Unable to access to Persistent MoogleMail");
						return;
					}
					if(r.responseText.includes('<div id="mmail_attachremove">')) {
						_mm.log("!!! Error: "+"Something is attached to Persistent MoogleMail");
						return;
					}
					_mm.send();
				});

			} else {
				b.status = 3;
				_mm.log("#"+(s.index+1)+": Setting CoD");
				$ajax.add("POST","?s=Bazaar&ss=mm&filter=new","mmtoken="+_mm.mmtoken+"&action=attach_cod&action_value="+b.rcod,function(r){
					if(_mm.check(r.responseText)) {
						return;
					}
					b.status = 4;
					_mm.send();
				});
			}

		} else { // no cod
			b.status = 6;
			_mm.send();
		}

	//* Isekai-Persistent CoD
	} else if(b.status === 30) {
		b.status = 31;
		_mm.log("#"+(s.index+1)+": Attaching in Persistent");
		//$ajax.add("POST","/?s=Bazaar&ss=mm&filter=new","mmtoken="+_mm.mmtoken+"&action=attach_add&select_item="+"11191"+"&select_count="+1+"&select_pane="+"item",function(r){ // attach 1x Health Draught
		$ajax.add("POST","/?s=Bazaar&ss=mm&filter=new","mmtoken="+_mm.mmtoken+"&action=attach_add&select_item="+"0"+"&select_count="+1+"&select_pane="+"credits",function(r){ // attach 1 credits
			if(_mm.check(r.responseText)) {
				return;
			}
			b.status = 32;
			_mm.send();
		});

	} else if(b.status === 32) {
		b.status = 33;
		_mm.log("#"+(s.index+1)+": Setting CoD in Persistent");
		$ajax.add("POST","/?s=Bazaar&ss=mm&filter=new","mmtoken="+_mm.mmtoken+"&action=attach_cod&action_value="+b.rcod,function(r){
			if(_mm.check(r.responseText)) {
				return;
			}
			b.status = 34;
			_mm.send();
		});

	} else if(b.status === 34) {
		b.status = 37;
		_mm.log("#"+(s.index+1)+": Sending in Persistent");
		$ajax.add("POST","/?s=Bazaar&ss=mm&filter=new",b.message_data,function(r){
			if(_mm.check(r.responseText)) {
				return;
			}
			b.status = 4;
			_mm.send();
		});
	// Isekai-Persistent CoD */

	} else if(b.status === 4 || b.status === 5 || b.status === 6) { // send
		b.status = 7;
		_mm.log("#"+(s.index+1)+": Sending");
		$ajax.add("POST","?s=Bazaar&ss=mm&filter=new",b.message_data,function(r){
			if(_mm.check(r.responseText)) {
				return;
			}
			b.status = 8;
			_mm.log("#"+(s.index+1)+": Completed");
			s.index++;
			if(s.batch[s.index]) {
				_mm.send();
			} else {
				_mm.log("Redirecting");
				location.href = "?s=Bazaar&ss=mm&filter=sent";
			}
		});
	}
};

_mm.pack = function(e) {
	var queue;
	if(e && e.mail) {
		e.mail.atext = _mm.atext(e);
		queue = [e];
	} else if(Array.isArray(e)) {
		queue = e;
	} else {
		queue = [].concat( _mm.equip_selected, _mm.item_selected, _mm.credits_selected ).filter(e=>e);
	}
	if( queue.some(e=>e.mail.pane==="equip"&&e.node.div&&e.node.div.dataset.locked=="1") ) {
		alert("Some equipment are locked.");
		return;
	}
	if(!queue.length) { // no attachment
		queue.push({mail:{pane:null}});
	}
	queue.deduction = _mm.parse_price(_mm.node.deduction.value);

	_mm.userlist.add(_mm.node.to_name.value);
	_mm.send(queue);
};

_mm.check = function(html) {
	var error = get_message(html);
	if(error) {
		_mm.send.error = error;
		_mm.log("!!! Error: "+error);
		$ajax.add("POST","?s=Bazaar&ss=mm&filter=new","mmtoken="+_mm.mmtoken+"&action=discard&action_value=0");
	}
	return error;
};

_mm.atext = function(item) {
	if(!item.mail.pane || !item.mail.count) {
		return "";
	} else if(item.mail.pane === "equip") {
		return "["+item.info.eid+"] " + item.info.name + (item.mail.cod?" @ "+item.mail.cod.toLocaleString()+"c":"");
	} else {
		return item.mail.count.toLocaleString() + " x " + item.info.name + (item.mail.cod?" @ "+item.mail.price.toLocaleString()+"c = "+item.mail.cod.toLocaleString()+"c":"");
	}
};

_mm.parse_count = function(str) {
	if(!str) {
		return 0;
	}
	return parseInt(str.replace(/,/g,"")) || 0;
};

_mm.parse_price = function(str,float) {
	if(!str) {
		return 0;
	}
	if( /([0-9,]+(?:\.\d*)?)([ckm]?)/.test(str.toLowerCase()) ) {
		var price = (RegExp.$2==="k"?1000:RegExp.$2==="m"?1000000:1) * parseFloat(RegExp.$1.replace(/,/g,""));
		return float ? price : Math.round(price);
	} else {
		return 0;
	}
};


// MM WRITE
if(_query.filter === "new" && _query.hvut !== "disabled") {

if($id("mmail_attachremove")) {
	alert("Remove attached items.");
	location.href = location.href + "&hvut=disabled";
	return;
}

_mm.log = function(text,clear) {
	if(clear) {
		_mm.node.log.value = "";
	}
	_mm.node.log.value += text+"\n";
	_mm.node.log.scrollTop = _mm.node.log.scrollHeight;
};

_mm.calc = function() {
	var queue = [].concat( _mm.equip_selected, _mm.item_selected, _mm.credits_selected ),
		atext="", cod=0;
	queue.forEach(function(e){
		atext += e.mail.atext + "\n";
		cod += e.mail.cod;
	});
	if(cod) {
		if(queue.length > 1) {
			atext += "\nTotal: "+cod.toLocaleString()+" Credits";
		}
		var deduction = _mm.parse_price(_mm.node.deduction.value);
		if(deduction) {
			atext += "\nDeduction: -"+deduction.toLocaleString()+" Credits";
			atext += "\nCoD: "+(cod-deduction).toLocaleString()+" Credits";
			if(cod < deduction) {
				atext += "\n=> CoD: 0 Credit";
			}
		}
	}
	_mm.log(atext,true);
};

_mm.search = function(pane,value,set) {
	value = value.trim();
	var preset;
	if(/^\[(.+)\]$/.test(value)) {
		preset = RegExp.$1;
	} else {
		value = value.toLowerCase().replace(/\s+/g," ");
	}

	if(value === _mm.search.value) {
		return;
	}
	_mm.search.value = value;
	if(set) {
		_mm.node[pane+"_search"].value = value;
	}

	if(pane==="item" && preset) {
		if(!$prices.keys.includes(preset)) {
			return;
		}
		var prices = $prices.get(preset);
		_mm[pane].forEach(function(e){
			if(e.info.name in prices) {
				_mm[pane+"_set"](e,e.mail.count,prices[e.info.name]);
				e.node.wrapper.classList.remove("hvut-none");
			} else if(!e.node.check.checked) {
				e.node.wrapper.classList.add("hvut-none");
			}
		});
		_mm[pane+"_calc"]();

	} else if(value) {
		value = value.split(" ").map(e=>(e.charAt(0)==="-"?{s:e.slice(1),m:false}:{s:e,m:true}));
		_mm[pane].forEach(function(e){
			var lowercase = e.info.name.toLowerCase();
			if( value.every(v=>!v.s||lowercase.includes(v.s)===v.m||e.info.eid&&e.info.eid.toString().includes(v.s)===v.m) ) {
				e.node.wrapper.classList.remove("hvut-none");
			} else if(!e.node.check.checked) {
				e.node.wrapper.classList.add("hvut-none");
			}
		});

	} else {
		_mm[pane].forEach(function(e){
			e.node.wrapper.classList.remove("hvut-none");
		});
	}

	if(pane === "equip") {
		let h5,h4;
		Array.from(_mm.node.equip_list.children).forEach(function(n){
			if(n.nodeName === "DIV") {
				if(!n.classList.contains("hvut-none")) {
					h5 = false;
					h4 = false;
				}
			} else if(n.nodeName === "H5") {
				n.classList.remove("hvut-none");
				if(h5) {
					h5.classList.add("hvut-none");
				}
				h5 = n;
			} else if(n.nodeName === "H4") {
				n.classList.remove("hvut-none");
				if(h4) {
					h4.classList.add("hvut-none");
				}
				h4 = n;
			}
		});
		if(h5) {
			h5.classList.add("hvut-none");
		}
		if(h4) {
			h4.classList.add("hvut-none");
		}
	}
};

_mm.parse = function(type) {
	var text = _mm.node.body.value.split("\n"),
		textdata = {};
	text.forEach(function(t){
		if(t.includes("> Removed attachment:")) {
			return;
		}
		var exec,name,lowercase,count,price,cod;
		if( (exec=/([A-Za-z][A-Za-z0-9\-' ]*)(?:\s*@\s*([0-9,.]+[ckm]?))?(?:\s+[x*\uff0a]?\s*[\[(]?([0-9,]+)[\])]?)/i.exec(t)) ) {
			name = exec[1];
			count = exec[3];
			price = exec[2];
		} else if( (exec=/(?:[\[(]?([0-9,]+)[\])]?\s*[x*\uff0a]?\s*)([A-Za-z][A-Za-z0-9\-' ]*)(?:\s*@\s*([0-9,.]+[ckm]?))?/i.exec(t)) ) {
			name = exec[2];
			count = exec[1];
			price = exec[3];
		} else {
			return;
		}

		name = name.trim();
		lowercase = name.toLowerCase().replace(/[^A-Za-z0-9]/g,"");
		count = _mm.parse_count(count);
		price = _mm.parse_price(price,true);
		if(price) {
			cod = Math.ceil(count*price);
		}
		textdata[lowercase] = {info:{name:name},mail:{pane:"item",count:count,price:price,cod:cod}};
	});

	if(type) {
		_mm.item.forEach(function(it){
			var lowercase = it.info.lowercase;
			if(lowercase in textdata) {
				_mm.item_set(it,textdata[lowercase].mail.count,type===2?textdata[lowercase].mail.price:0);
				it.node.check.checked = true;
				it.node.wrapper.classList.remove("hvut-none");
				delete textdata[lowercase];
			} else {
				it.node.check.checked = false;
				it.node.wrapper.classList.add("hvut-none");
			}
		});
		_mm.search.value = "\n";
		_mm.item_calc();

	} else {
		var attach = "",
			count = 0,
			cod = 0;
		Object.keys(textdata).forEach(function(lowercase){
			attach += _mm.atext(textdata[lowercase]) + "\n";
			count++;
			cod += textdata[lowercase].mail.cod || 0;
		});
		_mm.log(attach+(cod&&count>1?"\nTotal: "+cod.toLocaleString()+" Credits":""),true);
	}
};

_mm.toggle = function(div) {
	if(div === _mm.toggle.current) {
		return;
	}
	if(_mm.toggle.current) {
		_mm.node[_mm.toggle.current].classList.add("hvut-none");
	}
	_mm.toggle.current = div;
	_mm.node[_mm.toggle.current].classList.remove("hvut-none");
};

_mm.userlist = {
	list : getValue("mm_userlist",[]),
	create : function() {
		_mm.node.userlist.innerHTML = "";
		_mm.userlist.list.forEach(function(user){
			$element("option",_mm.node.userlist,{value:user});
		});
	},
	add : function(user) {
		if(!user) {
			return;
		}
		_mm.userlist.list.unshift(user);
		_mm.userlist.save();
	},
	save : function() {
		_mm.userlist.list = _mm.userlist.list.filter((e,i,a)=>e&&a.indexOf(e)===i);
		setValue("mm_userlist",_mm.userlist.list);
		if(_mm.node.userlist) {
			_mm.userlist.create();
		}
	},
	popup : function() {
		popup_text(_mm.userlist.list.join("\n"),"width:300px;min-height:300px;max-height:500px",
		[{value:"Save List",click:function(w,t){
			_mm.userlist.list = t.value.split("\n");
			_mm.userlist.save();
			w.remove();
		}}]);
	}
};


_mm.max = 10;
_mm.form = $id("mailform");
_mm.mmtoken = _mm.form.elements.mmtoken.value;


GM_addStyle(`
	#mailform, #mmail_left, #mmail_right {display:none}
	#mmail_outer {text-align:left}

	#hvut-mm-left {float:left; margin-left:20px; width:600px; height:600px; font-size:10pt; line-height:27px; padding-top:20px}
	#hvut-mm-right {float:right; margin-right:20px; width:550px; height:620px; font-size:10pt}
	#mmail_outer input[type='checkbox'] {top:3px}

	#hvut-mm-left > span, #hvut-mm-left > label {display:inline-block; line-height:22px; vertical-align:top}
	#hvut-mm-left > span {text-align:right}
	#hvut-mm-left > label {margin-right:10px}
	#hvut-mm-left > :first-child {float:right}
	.hvut-mm-left-menu {float:right; width:80px; margin:2px 5px; display:flex; flex-direction:column}
	.hvut-mm-left-menu input {margin:3px 0; white-space:normal}
	.hvut-mm-toggle > input:nth-child(n+2) {display:none}
	.hvut-mm-right-menu {margin-top:10px; padding:10px 0; border-bottom:3px double; display:flex}
	.hvut-mm-right-menu input {padding:3px 10px}
	.hvut-mm-right-menu input:first-child {order:1; margin-left:auto}

	.hvut-mm-attach-menu {margin-bottom:5px; padding-top:5px; border-bottom:3px double; line-height:27px}
	.hvut-mm-field {margin:0; padding:0; border:0; height:475px; overflow:hidden scroll}
	.hvut-mm-field input {margin:0 1px}
	.hvut-mm-field input:invalid {color:#e00}
	.hvut-mm-max {color:#e00 !important}

	#hvut-mm-item .itemlist td:nth-child(1) {width:170px !important}
	#hvut-mm-item .itemlist td:nth-child(2) {width:70px; padding-right:5px}
	#hvut-mm-item .itemlist td:nth-child(3) {width:auto}

	#hvut-mm-right .hvut-mm-count {width:60px; text-align:right}
	#hvut-mm-right .hvut-mm-price {width:60px; text-align:right}
	#hvut-mm-right .hvut-mm-cod {width:80px; text-align:right}
	#hvut-mm-right .hvut-mm-send {width:40px; padding:1px 0}

	#hvut-mm-equip .hvut-mm-sub {position:absolute; right:0; z-index:1}
	.hvut-mm-eid {display:none; position:absolute; right:145px; padding:0 3px; border:1px solid; line-height:20px; background-color:#fff}
	.eqp:hover .hvut-mm-eid {display:block}

	#hvut-mm-credits div {margin-top:20px}
	#hvut-mm-credits div:last-child {border-top:3px double}
	#hvut-mm-credits span {display:inline-block; margin-right:10px; font-weight:bold; text-align:right; line-height:22px}
`);

_mm.node.frag = $element();

_mm.node.left = $element("div",_mm.node.frag,["#hvut-mm-left"]);

$element("input",_mm.node.left,{type:"button",value:"SEND",tabIndex:4,style:"width:60px;height:49px"},_mm.pack);
$element("span",_mm.node.left,[" To:","!width:60px"]);
_mm.node.to_name = $element("input",_mm.node.left,{type:"text",value:_mm.form.message_to_name.value||"",tabIndex:1,style:"width:370px;;font-weight:bold"});
$element("input",_mm.node.left,{type:"button",value:"Edit List",style:"width:80px"},_mm.userlist.popup);
$element("span",_mm.node.left,[" Subject:","!width:60px"]);
_mm.node.subject = $element("input",_mm.node.left,{type:"text",value:_mm.form.message_subject.value||"",tabIndex:2,style:"width:460px;font-weight:bold"});

_mm.node.to_name.setAttribute("list","hvut-mm-userlist");
_mm.node.userlist = $element("datalist",_mm.node.left,["#hvut-mm-userlist"]);
_mm.userlist.create();

$element("span",_mm.node.left,[" Options:","!width:60px"]);
_mm.node.deduction = $element("input",[$element("label",_mm.node.left,"CoD Deduction"),"afterbegin"],{type:"text",pattern:"(\\d+|\\d{1,3}(,\\d{3})*)(\\.\\d+)?[KMkm]?",style:"width:60px;text-align:right"},{input:_mm.calc});
if(_isekai) {
	_mm.node.persistent = $element("input",[$element("label",_mm.node.left," CoD for Persistent Credits"),"afterbegin"],{type:"checkbox",checked:true});
}
_mm.node.template = $element("span",_mm.node.left,["!float:right"]);
Object.entries(settings.moogleMailTemplate).forEach(function([k,t]){
	$element("input",_mm.node.template,{type:"button",value:k},function(){_mm.node.to_name.value=t.to;_mm.node.subject.value=t.subject;_mm.node.body.value="";_mm.parse();_mm.node.body.value=t.body;if(t.attach){_mm.parse(2);}});
});

_mm.node.body = $element("textarea",_mm.node.left,{value:_mm.form.message_body.value||"",tabIndex:3,spellcheck:false,style:"width:590px;height:250px"});
_mm.node.log = $element("textarea",_mm.node.left,{readOnly:true,spellcheck:false,style:"width:500px;height:220px"});

_mm.node.left_menu = $element("div",_mm.node.left,[".hvut-mm-left-menu hvut-mm-toggle"]);
$element("input",_mm.node.left_menu,{type:"button",value:"ATTACH from TEXT"},function(){_mm.node.left_menu.classList.toggle("hvut-mm-toggle");});
$element("input",_mm.node.left_menu,{type:"button",value:"available formats"},function(){popup_text("100x Health Potion @10\n(200) Mana Potion @90\nSpirit Potion @90 x300\nVibrant Catalyst @4.5k (100)","width:300px;white-space:pre");});
$element("input",_mm.node.left_menu,{type:"button",value:"CALC"},function(){_mm.parse();});
$element("input",_mm.node.left_menu,{type:"button",value:"ATTACH"},function(){_mm.parse(1);});
$element("input",_mm.node.left_menu,{type:"button",value:"COD"},function(){_mm.parse(2);});
$element("input",_mm.node.left_menu,{type:"button",value:"RESET"},function(){_mm.search("item","",true);});

_mm.node.right = $element("div",_mm.node.frag,["#hvut-mm-right"]);
_mm.node.right_menu = $element("div",_mm.node.right,[".hvut-mm-right-menu"]);
$element("input",_mm.node.right_menu,{type:"button",value:"Use Default MoogleMail"},function(){location.href=location.href+"&hvut=disabled";});


// MM item
_mm.item_change = function(e) {
	if(e && e.target && e.target.item) {
		var it = e.target.item,
			count = _mm.parse_count(it.node.count.value),
			price = _mm.parse_price(it.node.price.value,true);
		it.mail.count = count;
		if(it.mail.count > it.data.stock) {
			it.node.count.classList.add("hvut-mm-max");
		} else {
			it.node.count.classList.remove("hvut-mm-max");
		}
		it.mail.price = price;
		it.mail.cod = Math.ceil(count*price);
		it.node.cod.value = it.mail.cod || "";
	}
	_mm.item_calc();
};
_mm.item_calc = function() {
	_mm.item_selected.length = 0;
	_mm.item.forEach(function(it){
		if(it.node.check.checked && it.mail.count) {
			it.mail.atext = _mm.atext(it);
			_mm.item_selected.push(it);
		}
	});
	_mm.calc();
};
_mm.item_set = function(it,count,price) {
	count = parseInt(count);
	price = parseFloat(price);
	if(!isNaN(count)) {
		it.mail.count = Math.min(it.data.stock, Math.max(count,0));
		it.node.count.value = it.mail.count || "";
		if(it.mail.count > it.data.stock) {
			it.node.count.classList.add("hvut-mm-max");
		} else {
			it.node.count.classList.remove("hvut-mm-max");
		}
	}
	if(!isNaN(price)) {
		it.mail.price = Math.max(price,0);
		it.node.price.value = it.mail.price || "";
	}
	it.mail.cod = Math.ceil(it.mail.count*it.mail.price);
	it.node.cod.value = it.mail.cod || "";
};
_mm.item_count = function(num) {
	if(num !== Infinity) {
		num = parseInt(num);
		if(!Number.isInteger(num)) {
			return;
		}
	}
	_mm.item.forEach(function(it){
		if(it.node.check.checked) {
			_mm.item_set(it, num===Infinity?it.data.stock:num);
		}
	});
	_mm.item_calc();
};
_mm.item_all = function() {
	var checked = this.checked;
	_mm.item.forEach(function(it){
		if(!it.node.wrapper.classList.contains("hvut-none")) {
			it.node.check.checked = checked;
		}
	});
	_mm.item_calc();
};

_mm.node.item_div = $element("div",null,["#hvut-mm-item",".hvut-none"]);
_mm.node.item_menu = $element("div",_mm.node.item_div,[".hvut-mm-attach-menu"]);
$prices.init();
$prices.keys.forEach(function(k){
	if(["WTB","Materials"].includes(k)) {
		return;
	}
	$element("input",_mm.node.item_menu,{type:"button",value:k},function(){_mm.search("item","["+k+"]",true);});
});
$element("br",_mm.node.item_menu);
_mm.node.item_search = $element("input",_mm.node.item_menu,{type:"text",placeholder:"Search",style:"width:170px"},{input:function(e){if(e.keyCode===27){_mm.search("item","",true);}else{_mm.search("item",this.value);}}});
$element("input",_mm.node.item_menu,{type:"button",value:"Clear"},function(){_mm.search("item","",true);});
$element("input",_mm.node.item_menu,{type:"checkbox",style:"margin-left:20px"},_mm.item_all);
$element("input",_mm.node.item_menu,{type:"text",placeholder:"count",style:"width:60px;text-align:right"},{input:function(){_mm.item_count(this.value);}});
$element("input",_mm.node.item_menu,{type:"button",value:"All"},function(){_mm.item_count(Infinity);});
$element("input",_mm.node.item_menu,{type:"button",value:"0"},function(){_mm.item_count(0);});
_mm.node.item_menu.appendChild($prices.selector());

_mm.node.item_field = $element("fieldset",_mm.node.item_div,["#item",".hvut-mm-field"],{input:_mm.item_change});
_mm.node.item_list = $qs(".itemlist") || $element("table");
_mm.node.item_field.appendChild(_mm.node.item_list);

_mm.item = Array.from(_mm.node.item_list.rows).map(function(tr){
	var td = tr.cells[0].firstElementChild,
		name = td.textContent,
		lowercase = name.toLowerCase().replace(/[^A-Za-z0-9]/g,""),
		iid = /mooglemail\.set_mooglemail_item\((\d+),this\)/.test(td.getAttribute("onclick")) && RegExp.$1,
		type = /'([^']+)'\)/.test(td.getAttribute("onmouseover")) && RegExp.$1.replace(/\W/g,"_") || "Consumable";
	return ({info:{name:name,lowercase:lowercase,iid:iid,type:type},data:{stock:parseInt(tr.cells[1].textContent)},node:{wrapper:tr}});
});
_mm.item.forEach(function(it){
	it.mail = {pane:"item",count:0,price:0,cod:0};
	it.node.wrapper.classList.add("hvut-it-"+it.info.type);
	it.node.td = $element("td",null,[".hvut-mm-sub"]);
	it.node.check = $element("input",it.node.td,{type:"checkbox",className:"hvut-mm-check"});
	it.node.check.item = it;
	it.node.count = $element("input",it.node.td,{type:"text",className:"hvut-mm-count",placeholder:"count",pattern:"\\d+|\\d{1,3}(,\\d{3})*",max:it.data.stock});
	it.node.count.item = it;
	it.node.price = $element("input",it.node.td,{type:"text",className:"hvut-mm-price",placeholder:"price",pattern:"(\\d+|\\d{1,3}(,\\d{3})*)(\\.\\d+)?[KMkm]?"});
	it.node.price.item = it;
	it.node.cod = $element("input",it.node.td,{type:"text",className:"hvut-mm-cod",placeholder:"cod",readOnly:true});
	it.node.send = $element("input",it.node.td,{type:"button",value:"send",className:"hvut-mm-send"},function(){_mm.pack(it);});
	it.node.wrapper.appendChild(it.node.td);
});
_mm.item_selected = [];

if($id("mmail_attachitem")) {
	$id("item").id += "_";
	$element("input",_mm.node.right_menu,{type:"button",value:"Item"},function(){_mm.toggle("item_div");});
	_mm.node.right.appendChild(_mm.node.item_div);
}


// MM equip
_mm.equip_change = function(e) {
	if(e && e.target && e.target.equip) {
		var eq = e.target.equip;
		eq.mail.cod = _mm.parse_price(eq.node.cod.value);
	}
	_mm.equip_calc();
};
_mm.equip_calc = function() {
	_mm.equip_selected.length = 0;
	_mm.equip.forEach(function(eq){
		if(eq.node.check.checked) {
			eq.mail.atext = _mm.atext(eq);
			_mm.equip_selected.push(eq);
		}
	});
	_mm.calc();
};
_mm.equip_all = function() {
	var checked = this.checked;
	_mm.equip.forEach(function(eq){
		if(!eq.node.wrapper.classList.contains("hvut-none")) {
			eq.node.check.checked = checked;
		}
	});
	_mm.equip_calc();
};


_mm.node.equip_div = $element("div",null,["#hvut-mm-equip",".hvut-none"]);
_mm.node.equip_menu = $element("div",_mm.node.equip_div,[".hvut-mm-attach-menu"]);
_mm.node.equip_search = $element("input",_mm.node.equip_menu,{type:"text",placeholder:"Search",style:"width:290px"},{input:function(e){if(e.keyCode===27){_mm.search("equip","",true);}else{_mm.search("equip",this.value);}}});
$element("input",_mm.node.equip_menu,{type:"button",value:"Clear"},function(){_mm.search("equip","",true);});
$element("input",_mm.node.equip_menu,{type:"checkbox",style:"margin-left:20px"},_mm.equip_all);

_mm.node.equip_field = $element("fieldset",_mm.node.equip_div,["#equip",".hvut-mm-field"],{input:_mm.equip_change});
_mm.node.equip_list = $qs(".equiplist") || $element("div",null,[".equiplist"]);
_mm.node.equip_field.appendChild(_mm.node.equip_list);

_mm.equip_json = getValue("in_json",{});
_mm.equip = $equip.list(_mm.node.equip_list,2);
_mm.equip.forEach(function(eq){
	eq.mail = {pane:"equip",count:1,cod:0};
	eq.node.div.removeAttribute("onclick");
	eq.node.lock = eq.node.wrapper.firstElementChild;
	eq.node.sub = $element("div",[eq.node.div,"beforebegin"],[".hvut-mm-sub"]);
	eq.node.eid = $element("span",eq.node.sub,[" "+eq.info.eid,".hvut-mm-eid"]);
	eq.node.check = $element("input",eq.node.sub,{type:"checkbox",className:"hvut-mm-check"});
	eq.node.check.equip = eq;
	eq.node.cod = $element("input",eq.node.sub,{type:"text",className:"hvut-mm-cod",placeholder:"cod",pattern:"(\\d+|\\d{1,3}(,\\d{3})*)(\\.\\d+)?[KMkm]?"});
	eq.node.cod.equip = eq;
	eq.node.send = $element("input",eq.node.sub,{type:"button",className:"hvut-mm-send",value:"send"},function(){_mm.pack(eq);});

	var json = _mm.equip_json[eq.info.eid];
	if(json && json.price) {
		eq.node.cod.value = json.price;
		eq.mail.cod = _mm.parse_price(json.price);
	}
});
_mm.equip_selected = [];

if($id("mmail_attachequip")) {
	$id("equip").id += "_";
	$element("input",_mm.node.right_menu,{type:"button",value:"Equipment"},function(){_mm.toggle("equip_div");});
	_mm.node.right.appendChild(_mm.node.equip_div);
}


// MM credits
_mm.credits_calc = function() {
	_mm.credits_selected.length = 0;

	var credits,
		c_check = _mm.node.credits_check.checked,
		c_count = _mm.parse_price(_mm.node.credits_count.value);

	if(c_check && c_count>0) {
		if(c_count > _mm.credits_funds) {
			_mm.node.credits_count.classList.add("hvut-mm-max");
		} else {
			_mm.node.credits_count.classList.remove("hvut-mm-max");
			credits = {mail:{pane:"credits",count:c_count,cod:0},info:{name:"Credits"}};
			credits.mail.atext = _mm.atext(credits);
			_mm.credits_selected.push(credits);
		}
	}

	var hath,
		h_check = _mm.node.hath_check.checked,
		h_count = _mm.parse_count(_mm.node.hath_count.value),
		h_price = _mm.parse_price(_mm.node.hath_price.value,true),
		h_cod = Math.ceil(h_count * h_price);

	if(h_check && h_count>0) {
		if(h_count > _mm.hath_funds) {
			_mm.node.hath_count.classList.add("hvut-mm-max");
		} else {
			_mm.node.hath_count.classList.remove("hvut-mm-max");
			_mm.node.hath_cod.value = h_cod || "";

			hath = {mail:{pane:"hath",count:h_count,price:h_price,cod:h_cod},info:{name:"Hath"}};
			hath.mail.atext = _mm.atext(hath);
			_mm.credits_selected.push(hath);
		}
	}

	_mm.calc();
};

_mm.credits_funds = $id("mmail_attachcredits") ? /Current Funds: ([0-9,]+) Credits/.test($id("mmail_attachcredits").textContent) && _mm.parse_count(RegExp.$1) : 0;
_mm.hath_funds = $id("mmail_attachhath") ? /Current Funds: ([0-9,]+) Hath/.test($id("mmail_attachhath").textContent) && _mm.parse_count(RegExp.$1) : 0;

_mm.node.credits_div = $element("div",null,["#hvut-mm-credits",".hvut-none"]);
_mm.node.credits_field = $element("fieldset",_mm.node.credits_div,[".hvut-mm-field"],{input:_mm.credits_calc});

_mm.node.credits_sub = $element("div",_mm.node.credits_field);
$element("span",_mm.node.credits_sub,[" Credits","!width:75px"]);
$element("span",_mm.node.credits_sub,[" "+_mm.credits_funds.toLocaleString(),"!width:100px"]);
_mm.node.credits_check = $element("input",_mm.node.credits_sub,{type:"checkbox"});
_mm.node.credits_count = $element("input",_mm.node.credits_sub,{type:"text",className:"hvut-mm-count",placeholder:"count",pattern:"(\\d+|\\d{1,3}(,\\d{3})*)(\\.\\d+)?[KMkm]?",style:"width:80px"});

_mm.node.hath_sub = $element("div",_mm.node.credits_field);
$element("span",_mm.node.hath_sub,[" Hath","!width:75px"]);
$element("span",_mm.node.hath_sub,[" "+_mm.hath_funds.toLocaleString(),"!width:100px"]);
_mm.node.hath_check = $element("input",_mm.node.hath_sub,{type:"checkbox"});
_mm.node.hath_count = $element("input",_mm.node.hath_sub,{type:"text",className:"hvut-mm-count",placeholder:"count",pattern:"\\d+|\\d{1,3}(,\\d{3})*",style:"width:80px"});
_mm.node.hath_price = $element("input",_mm.node.hath_sub,{type:"text",className:"hvut-mm-price",placeholder:"price",pattern:"(\\d+|\\d{1,3}(,\\d{3})*)(\\.\\d+)?[KMkm]?",style:"width:60px"});
_mm.node.hath_cod = $element("input",_mm.node.hath_sub,{type:"text",className:"hvut-mm-cod",placeholder:"cod",style:"width:100px",readOnly:true});

_mm.credits_selected = [];

if($id("mmail_attachcredits")) {
	$element("input",_mm.node.right_menu,{type:"button",value:"Credits / Hath"},function(){_mm.toggle("credits_div");});
	_mm.node.right.appendChild(_mm.node.credits_div);
}


_mm.multi_send = function() {
	var queue = [],
		error = [];

	_mm.node.multi_text.value.split("\n").forEach(function(t,i){
		if(!t) {
			return;
		}
		var [n,a,s,b] = t.split(";");
		if(!n) {
			error.push("line#"+(i+1)+" is invalid: " + t);
			return;
		}
		var m = {mail:{to_name:n,subject:s?s:"",body:b?b.replace(/\|/g,"\n"):""},info:{}};
		if(!a || !(a=a.trim())) {
		} else if(/^([0-9,.]+)([ckmh])?$/.test(a.toLowerCase())) {
			var c = RegExp.$1,
				u = RegExp.$2 || "c";
			if(u === "h") {
				m.mail.pane = "hath";
				m.mail.count = _mm.parse_count(c);
				m.info.name = "Hath";
			} else {
				m.mail.pane = "credits";
				m.mail.count = _mm.parse_price(c+u);
				m.info.name = "Credits";
			}
			m.mail.price = 0;
			m.mail.cod = 0;
			m.mail.atext = _mm.atext(m);
		} else {
			error.push("line#"+(i+1)+" is invalid: " + t);
			return;
		}
		queue.push(m);
	});

	if(error.length) {
		alert(error.join("\n"));
		return;
	}
	if(!_mm.node.to_name.value) {
		_mm.node.to_name.value = " ";
	}
	_mm.send(queue);
};

_mm.node.multi_sub = $element("div",_mm.node.credits_field);

$element("input",_mm.node.multi_sub,{type:"button",value:"Multi-Send",style:"width:150px;margin:10px"},_mm.multi_send);
$element("br",_mm.node.multi_sub);
_mm.node.multi_text = $element("textarea",_mm.node.multi_sub,{readOnly:true,spellcheck:false,value:"/* user;credits;subject;text(|=new line)\nex)\nsssss2;10m\nsssss3;500k;WTB;hi|I want to buy...\nTenboro;500c\nMoogleMail;1000h;Thanks\n*/",style:"width:500px;height:300px"},function(){if(this.readOnly){this.readOnly=false;this.value="";}});


if(!["item_div","equip_div","credits_div"].some(d=>{if(_mm.node[d].parentNode){_mm.toggle(d);return true;}})) {
	$element("div",_mm.node.right,["/"+$id("mmail_right").innerHTML,"!padding:10px;font-weight:bold"]);
	_mm.node.deduction.disabled = true;
	if(_isekai) {
		_mm.node.persistent.disabled = true;
	}
}
$id("mmail_outer").appendChild(_mm.node.frag);
_mm.node.to_name.focus();


// MM LIST
} else if($id("mmail_list")) {

_mm.filter = _query.filter || "inbox";
_mm.page = parseInt(_query.page) || 0;
_mm.page_new = _mm.page;
_mm.page_old = _mm.page;

_mm.data = {};
_mm.table = {};

_mm.db = {};
_mm.db.version = 1;
_mm.db.objectStoreName = 'mm';

_mm.db.open = function(callback) {
	var request = indexedDB.open(_ns,_mm.db.version);
	request.onsuccess = function(e){
		_mm.db.obj = e.target.result;
		if(callback) {
			callback();
		}
	};
	request.onupgradeneeded = function(e){
		var db = e.target.result;
		if(!db.objectStoreNames.contains(_mm.db.objectStoreName)) {
			db.createObjectStore(_mm.db.objectStoreName, {keyPath:"mid"});
		}
	};
	request.onerror = function(e){
		console.log("open db: error",e);
	};
};

_mm.db.conn = function(mode = 'readonly', name = _mm.db.objectStoreName) {
	var transaction = _mm.db.obj.transaction(name, mode);
	transaction.onerror = function(e){
		console.log("transaction: error",e);
	};
	return transaction.objectStore(name);
};

_mm.db.clear = function() {
	_mm.db.conn("readwrite").clear();
};

_mm.load_page = function(p) {
	if(p==="new"&&_mm.page_new===false || p==="old"&&_mm.page_old===false) {
		return;
	}
	var page = p==="new"?_mm.page_new-1 : p==="old"?_mm.page_old+1 : p;
	if(_mm.table[page]) {
		return;
	}

	var table = _mm.table[page] = $element("table",[$id("mmail_outerlist"),_mm.table[page+1]],[".hvut-mm-list"]);
	$element("td",$element("tr",table),"Loading "+page+" Page");
	scrollIntoView(table);

	$ajax.add("GET","?s=Bazaar&ss=mm&filter="+_mm.filter+"&page="+page,null,function(r){
		var doc = $doc(r.responseText),
			list = $qs("#mmail_list",doc);
		_mm.kill_asshole(list);
		_mm.create_page(list,page);
		scrollIntoView(table);
	});
};

_mm.create_page = function(list,page) {
	var table = _mm.table[page],
		tbody = $element("tbody");

	$element("tr",tbody,["/"+
		"<td>"+({"inbox":"Inbox","read":"From","sent":"To"})[_mm.filter]+"</td>"+
		"<td>"+page+" Page</td>"+
		"<td>Attached</td>"+
		"<td>CoD</td>"+
		"<td>Sent</td>"+
		"<td>Read</td>"
	]);

	var conn = _mm.db.conn(),
		loaded = list.rows.length-1;
	Array.from(list.rows).slice(1).forEach(function(row){
		if(row.cells[0].id === "mmail_nnm") {
			$element("td",$element("tr",tbody),{textContent:"No New Mail",colSpan:6});
			return;
		}
		var mid = /mid=(\d+)/.test(row.getAttribute("onclick")) && parseInt(RegExp.$1),
			user = row.cells[0].textContent,
			subject = row.cells[1].textContent,
			sent = Date.parse(row.cells[2].textContent+":00.000Z")/1000,
			read = row.cells[3].textContent;
		read = read==="Never"?null : Date.parse(read+":00.000Z")/1000;

		if(!_mm.data[mid]) {
			_mm.data[mid] = {};
		}
		var data = _mm.data[mid];
		if(data.page) {
			return;
		}
		data.page = {mid:mid,filter:_mm.filter,user:user,returned:user==="MoogleMail",subject:subject,sent:sent,read:read,tr:$element("tr",tbody)};

		$element("td",data.page.tr,mid); // user
		$element("a",$element("td",data.page.tr),{href:"?s=Bazaar&ss=mm&filter="+_mm.filter+"&mid="+mid+"&page="+page},function(e){e.preventDefault();_mm.load_mail(mid,true);}); // subject
		$element("td",data.page.tr); // attach
		$element("td",data.page.tr); // cod
		$element("td",data.page.tr); // sent
		$element("td",data.page.tr); // read

		conn.get(mid).onsuccess = function(e) {
			data.db = e.target.result || null;
			if(!data.db || data.db.filter!==_mm.filter || !data.page.returned&&!data.db.user.startsWith(data.page.user) || data.db.sent!==data.page.sent || data.db.read!==data.page.read) {
				if(_mm.filter !== "inbox") {
					_mm.load_mail(mid);
				}
			}
			_mm.modify_mail(mid);
			if(!--loaded) {
				scrollIntoView(table);
			}
		};
	});
	table.innerHTML = "";
	table.appendChild(tbody);

	if(page < _mm.page_new) {
		_mm.page_new = page;
	}
	if(page > _mm.page_old) {
		_mm.page_old = page;
	}
	if(page <= 0) {
		_mm.page_new = false;
		_mm.node.page_new.disabled = true;
	}
	if(list.rows.length < 21) {
		_mm.page_old = false;
		_mm.node.page_old.disabled = true;
	}
};

_mm.load_mail = function(mid,view,post,callback) {
	var data = _mm.data[mid];
	if(view) {
		if(_mm.current===mid && !post) {
			_mm.close_mail();
			return;
		}
		_mm.close_mail();
		_mm.current = mid;
		_mm.node.view.classList.remove("hvut-none");
		$element("div",_mm.node.view,[" Loading...",".hvut-mm-loading"]);

		if(data.page) {
			data.page.tr.classList.add("hvut-mm-current");
		}
		if(data.search) {
			data.search.tr.classList.add("hvut-mm-current");
		}
	}

	$ajax.add("POST","?s=Bazaar&ss=mm&mid="+mid,post,function(r){
		var html = r.responseText,
			doc = $doc(html),
			outer = $id("mmail_outer",doc),
			error = get_message(doc);

		if(outer) {
			var form = $id("mailform",doc);
			var mail = data.mail = {
				mmtoken : form.elements.mmtoken.value,
				to : form.elements[3].value,
				from : form.elements[4].value,
				subject : form.elements[5].value,
				text : form.elements[6].value,
				attach : [],

				return : $qs("#mmail_showbuttons > img[src*='returnmail.png']",doc) ? true : false,
				recall : $qs("#mmail_showbuttons > img[src*='recallmail.png']",doc) ? true : false,
				reply : $qs("#mmail_showbuttons > img[src*='reply.png']",doc) ? true : false,
				take : $qs("#mmail_attachremove > img[src*='attach_takeall.png']",doc) ? true : false
			};

			if(mail.from === "MoogleMail") {
				mail.returned = /This message was returned from (.+), kupo!|This mail was sent to (.+), but was returned, kupo!/.test(mail.text.split("\n").reverse().join("\n")) && (RegExp.$1 || RegExp.$2);
			}
			if(mail.take) {
				mail.filter = "inbox";
				mail.user = mail.returned || mail.from;
			} else if(mail.reply) {
				mail.filter = "read";
				mail.user = mail.from;
			} else if(mail.from === "MoogleMail") {
				mail.filter = "read";
				mail.user = mail.returned;
			} else {
				mail.filter = "sent";
				mail.user = mail.to;
			}
			mail.read = mail.filter==="read" || mail.filter==="sent"&&!mail.recall;

			if($id("mmail_attachlist",doc)) {
				Object.assign($equip.dynjs_eqstore, JSON.parse(/var dynjs_eqstore = (\{.*\});/.test(html) && RegExp.$1));

				Array.from($id("mmail_attachlist",doc).children).forEach(function(div){
					var sub = div.firstElementChild.firstElementChild,
						exec;
					if(sub && sub.hasAttribute("onmouseover") && (exec=/equips\.set\((\d+)/.exec(sub.getAttribute("onmouseover")))) {
						let eid = parseInt(exec[1]),
							key = $equip.dynjs_eqstore[eid].k,
							name = $equip.dynjs_eqstore[eid].t;
						mail.attach.push({t:"e",n:name,e:eid,k:key});

					} else if((exec=/^([0-9,]+)x? (.+)$/.exec(div.textContent))) {
						let count = _mm.parse_count(exec[1]),
							name = exec[2],
							type = name==="Hath"?"h":name==="Credits"?"c":"i";
						mail.attach.push({t:type,n:name,c:count});
					} else {
						console.log(div.textContent.trim());
					}
				});

				if($id("mmail_currentcod",doc)) {
					mail.cod = /Requested Payment on Delivery: ([0-9,]+) credits/.test($id("mmail_currentcod",doc).textContent) && _mm.parse_count(RegExp.$1);
				}

			} else {
				var split = mail.text.split("\n\n").reverse(),
					attach;

				attach = split[0].split("\n").every(function(e){
					var exec = /^Removed attachment: (?:([0-9,]+)x? (.+)|(.+))$/.exec(e);
					if(!exec) {
						return false;
					}
					if(exec[3]) {
						mail.attach.unshift({t:"e",n:exec[3]});
					} else {
						let count = _mm.parse_count(exec[1]),
							name = exec[2],
							type = name==="Hath"?"h":name==="Credits"?"c":"i";
						mail.attach.unshift({t:type,n:name,c:count});
					}
					return true;
				});
				if(attach) {
					mail.cod = /^CoD Paid: ([0-9,]+) Credits$/.test(split[1]) && _mm.parse_count(RegExp.$1);
				}

				// pre 0.85
				if( (attach=/^Attached item removed: (?:([0-9,]+)x? (.+)|(.+)) \(type=([chie]) id=(\d+), CoD was ([0-9]+)C\)$/.exec(split[0])) ) {
					var type = attach[4];
					if(type === "e") {
						mail.attach.push({t:type,n:attach[3],e:attach[5]});
					} else {
						mail.attach.push({t:type,n:attach[2],c:_mm.parse_count(attach[1])});
					}
					mail.cod = _mm.parse_count(attach[6]);
				}
			}

			if(callback) {
				callback();
			}

		} else if(error) {
			data.mail = {error:error};

		} else {
			data.mail = {error:"ERROR"};
		}

		_mm.update_mail(mid,view);
	});
};

_mm.update_mail = function(mid,view) {
	var data = _mm.data[mid],
		mail = data.mail;

	if(data.db === undefined) {
		_mm.db.conn().get(mid).onsuccess = function(e) {
			data.db = e.target.result || null;
			_mm.update_mail(mid,view);
		};
		return;
	}

	if(data.db) {
		var db = data.db;
		if(mail.error) {
			if(db.error !== mail.error) {
				db.error = mail.error;
				_mm.db.conn("readwrite").put(db);
			}

		} else {
			var sent = data.page&&data.page.sent || db.sent,
				read = !mail.read?null : data.page&&data.page.read || db.read || -1;
			if(db.filter!==mail.filter || db.user!==mail.user || db.subject!==mail.subject || db.text!==mail.text || db.sent!==sent || db.read!==read || db.error) {
				db.filter = mail.filter;
				db.user = mail.user;
				db.subject = mail.subject;
				db.text = mail.text;
				db.sent = sent;
				db.read = read;
				if(mail.returned) {
					db.returned = 1;
					delete db.cod;
				}
				delete db.error;
				_mm.db.conn("readwrite").put(db);
			}
		}

	} else if(data.page && !mail.error) {
		data.db = {mid:mid,filter:mail.filter,user:mail.user,subject:mail.subject,text:mail.text,sent:data.page.sent,read:data.page.read};
		if(mail.returned) {
			data.db.returned = 1;
		}
		if(mail.attach.length) {
			data.db.attach = mail.attach;
		}
		if(mail.cod) {
			data.db.cod = mail.cod;
		}
		_mm.db.conn("readwrite").add(data.db);
	}

	_mm.modify_mail(mid);
	if(view) {
		_mm.view_mail(mid);
	}
};

_mm.modify_mail = function(mid,search) {
	var data = _mm.data[mid],
		tr,cells;

	if(data.page && !search) {
		tr = data.page.tr;
		cells = tr.cells;
		cells[0].textContent = (data.db||data.page).user;
		cells[1].firstChild.textContent = (data.db||data.page).subject;
		cells[2].innerHTML = "";
		cells[3].innerHTML = "";

		if(data.db && data.db.attach) {
			data.db.attach.forEach(function(e){
				var span = $element("span",cells[2],[".hvut-mm-attach-"+e.t]);
				if(e.t === "e") {
					if(e.e && e.k) {
						$element("a",span,{textContent:e.n,href:"equip/"+e.e+"/"+e.k,target:"_blank"});
					} else {
						span.textContent = e.n;
					}
				} else {
					span.textContent = e.c.toLocaleString()+" x "+e.n;
				}
			});
			if(data.db.cod) {
				$element("span",cells[3],data.db.cod.toLocaleString());
			}
		}
		cells[4].textContent = _mm.dts(data.page.sent);
		cells[5].textContent = data.page.read?_mm.dts(data.page.read):"";

		tr.classList[data.db?"remove":"add"]("hvut-mm-nodb");
		tr.classList[data.page.read?"remove":"add"]("hvut-mm-unread");
		tr.classList[(data.db||data.page).returned?"add":"remove"]("hvut-mm-returned");
		tr.classList[(data.db||data.page).filter!==_mm.filter?"add":"remove"]("hvut-mm-removed");
	}

	if(data.search) {
		tr = data.search.tr;
		cells = tr.cells;
		cells[0].innerHTML = "";
		$element("span",cells[0],({"inbox":"Inbox","read":"From","sent":"To"})[data.db.filter]);
		$element("",cells[0]," "+data.db.user);
		cells[1].firstChild.textContent = data.db.subject;
		cells[2].innerHTML = "";
		cells[3].innerHTML = "";

		if(data.db.attach) {
			data.db.attach.forEach(function(e){
				var span = $element("span",cells[2],[".hvut-mm-attach-"+e.t]);
				if(e.t === "e") {
					if(e.e && e.k) {
						$element("a",span,{textContent:e.n,href:"equip/"+e.e+"/"+e.k,target:"_blank"});
					} else {
						span.textContent = e.n;
					}
				} else {
					span.textContent = e.c.toLocaleString()+" x "+e.n;
				}
			});
			if(data.db.cod) {
				cells[3].textContent = data.db.cod.toLocaleString();
			}
		}
		cells[4].textContent = _mm.dts(data.db.sent);
		cells[5].textContent = data.db.read?_mm.dts(data.db.read):"";

		tr.classList[data.db.error?"add":"remove"]("hvut-mm-error");
		tr.classList[data.db.read?"remove":"add"]("hvut-mm-unread");
		tr.classList[data.db.returned?"add":"remove"]("hvut-mm-returned");
	}
};

_mm.view_mail = function(mid) {
	if(_mm.current !== mid) {
		return;
	}

	var data = _mm.data[mid],
		mail = data.mail,
		db = data.db,
		div = _mm.node.view;

	div.innerHTML = "";

	$element("dl",div,["/"+
		"<dt>"+(db.filter==="sent"?"To":"From")+"</dt>"+
		"<dd>"+(db.returned?"MoogleMail: ":"")+db.user+"</dd>"+
		"<dt>Sent</dt>"+
		"<dd>"+_mm.dts(db.sent,4)+"</dd>"+
		"<dt>Subject</dt>"+
		"<dd>"+db.subject+"</dd>"+
		"<dt>Read</dt>"+
		"<dd>"+(db.read===null?"-" : db.read===-1?"????-??-??" : _mm.dts(db.read,4))+"</dd>"
	]);

	var textarea = $element("textarea",div,{value:db.text,spellcheck:false,readOnly:true});

	var buttons = $element("div",div,[".hvut-mm-btns"]);
	$element("input",buttons,{type:"button",value:"Close"},_mm.close_mail);
	if(mail.reply) {
		$element("input",buttons,{type:"button",value:"Reply"},function(){location.href="?s=Bazaar&ss=mm&filter=new&reply="+mid;});
	}
	if(mail.take) {
		$element("input",buttons,{type:"button",value:"Take all"},function(){
			if(mail.cod && !confirm("Removing the attachments will deduct "+mail.cod.toLocaleString()+" Credits from your account, kupo! Are you sure?")) {
				return;
			}
			_mm.load_mail(mid,true,"action=attach_remove&mmtoken="+mail.mmtoken+"&action_value=0");
		});
	}
	if(mail.return) {
		$element("input",buttons,{type:"button",value:"Return"},function(){
			if(!confirm("This will return the message to the sender, kupo! Are you sure?")) {
				return;
			}
			_mm.load_mail(mid,true,"action=return_message&mmtoken="+mail.mmtoken);
		});
	}
	if(mail.recall) {
		$element("input",buttons,{type:"button",value:"Recall"},function(){
			if(!confirm("This will return the message to the sender, kupo! Are you sure?")) {
				return;
			}
			_mm.load_mail(mid,true,"action=return_message&mmtoken="+mail.mmtoken);
		});
	}
	if(db.error) {
		$element("input",buttons,{type:"button",value:db.error});
		div.classList.add("hvut-mm-failed");
	} else {
		div.classList.remove("hvut-mm-failed");
	}

	if(mail.take && !db.returned && settings.moogleMailCouponClipper && /Coupon Clipper|Item Shop/i.test(db.subject+"\n"+db.text)) {
		textarea.readOnly = false;
		textarea.addEventListener("input",function(){_mm.itemshop_parse(textarea.value);});
		$element("input",buttons,{type:"button",value:"Coupon Clipper"},function(){
			var items = _mm.itemshop_parse(textarea.value),
				credits = (db.attach.find(e=>e.t==="c")||{c:0}).c;
			if(!items.length) {
				alert("Invalid Request");
				return;
			}
			if(items.cost===credits||confirm("The total price of requested materials is "+items.cost.toLocaleString()+" credits, but the amount of attached credits is "+credits.toLocaleString()+".\n\nContinue?")) {
				_mm.itemshop(mid,items);
			}
		});
		_mm.itemshop_parse(textarea.value);
	}

	if(mail.take && !db.returned && settings.moogleMailDarkDescent && /Dark Descent|reforge/i.test(db.subject+"\n"+db.text)) {
		var equips = db.attach.filter(e=>e.t==="e").map(function(dbeq){
			var eid = dbeq.e,
				dynjs = $equip.dynjs_eqstore[eid],
				exec = $equip.reg.html.exec(dynjs.d),
				eq = {
					info : {name:dynjs.t, category:exec[1], tier:parseInt(exec[6]), eid:eid, key:dynjs.k},
					data : {html:dynjs.d},
					mail : {pane:"equip",count:1},
					node : {}
				};
			$equip.parse.name(eq.info.name,eq);
			return eq;
		});
		var c = equips.reduce((p,c)=>p+Math.ceil(c.info.tier/2),0);
		$element("input",buttons,{type:"button",value:"Dark Descent ["+c+"]"},function(){
			var a = db.attach.reduce((p,c)=>p+(c.n==="Amnesia Shard"?c.c:0),0);
			if(!equips.find(eq=>eq.info.tier)) {
				alert("Cannot reforge level zero items.");
				return;
			}
			if(!a?confirm("This costs "+c+" Amnesia Shard"+(c>1?"s":"")+", but nothing attached.\n\nContinue?"):a!==c?confirm("This costs "+c+" Amnesia Shard"+(c>1?"s":"")+", but "+a+" attached.\n\nContinue?"):confirm("Are you sure you want to reforge this item and send back?")) {
				_mm.reforge(mid,equips);
			}
		});
	}

	data.attach = [];
	if(db && db.attach) {
		var ul = $element("ul",div,[db.returned?".hvut-mm-none":""],{input:_mm.calc_attach}),
			li = $element("li",ul),
			wtx = db.filter==="sent"?"WTS":"WTB";

		$element("input",buttons,{type:"button",value:"Edit "+wtx+" Price",className:"hvut-mm-edit"},function(){$prices.edit(wtx,_mm.set_attach);});
		$element("span",li,"CoD: "+(db.cod?db.cod.toLocaleString()+(db.read?" [Paid]":""):"Not set"));
		data.attach.node_price = $element("input",li,{type:"text",className:"hvut-mm-price",readOnly:true,value:wtx});
		data.attach.node_cod = $element("input",li,{type:"text",className:"hvut-mm-cod",readOnly:true});
		db.attach.forEach(function(e){
			var li = $element("li",ul),
				span = $element("span",li,[".hvut-mm-attach-"+e.t]);
			if(e.t === "e") {
				if(e.e && e.k) {
					$element("a",span,{textContent:e.n,href:"equip/"+e.e+"/"+e.k,target:"_blank"});
				} else {
					span.textContent = e.n;
				}
			} else {
				span.textContent = e.c.toLocaleString()+" x "+e.n;
				if(e.t==="i" || e.t==="h") {
					data.attach.push( {t:e.t,n:e.n,c:e.c,node_price:$element("input",li,{type:"text",className:"hvut-mm-price",value:""}),node_cod:$element("input",li,{type:"text",className:"hvut-mm-cod",readOnly:true})} );
				}
			}
		});
		_mm.set_attach();
	}
};

_mm.set_attach = function() {
	var data = _mm.data[_mm.current];
	if(!data) {
		return;
	}
	var db = data.db,
		wtx = db.filter==="sent"?"WTS":"WTB",
		prices = $prices.get(wtx);
	data.attach.forEach(function(e){
		if(e.node_price) {
			e.node_price.value = prices[e.n] || "";
		}
	});
	_mm.calc_attach();
};

_mm.calc_attach = function() {
	var data = _mm.data[_mm.current];
	if(!data) {
		return;
	}

	var db = data.db,
		wtx = db.filter==="sent"?"WTS":"WTB",
		attach = data.attach,
		sum = 0;

	attach.forEach(function(e){
		var p = e.node_price.value.trim() || 0;
		if(isNaN(p)) {
			e.node_price.classList.add("hvut-mm-invalid");
			e.node_cod.classList.add("hvut-mm-invalid");
			return;
		}
		e.node_price.classList.remove("hvut-mm-invalid");
		e.node_cod.classList.remove("hvut-mm-invalid");

		var cod = p * e.c;
		e.node_cod.value = cod ? cod.toLocaleString() : "";
		sum += cod;
	});

	attach.node_cod.value = sum ? sum.toLocaleString() : "";
	if(db && db.cod) {
		attach.node_price.value = !sum?wtx : db.cod===sum?"CoD =" : db.cod>sum?"CoD >": "CoD <";
		attach.node_price.dataset.codMatch = db.cod===sum?"1" : "0";
		attach.node_cod.dataset.codMatch = db.cod===sum?"1" : "0";
	}
};

_mm.close_mail = function() {
	if(_mm.current) {
		var data = _mm.data[_mm.current];
		if(data.page) {
			data.page.tr.classList.remove("hvut-mm-current");
		}
		if(data.search) {
			data.search.tr.classList.remove("hvut-mm-current");
		}
	}
	_mm.current = null;
	_mm.node.view.innerHTML = "";
	_mm.node.view.classList.add("hvut-none");
	_mm.log("",3);
};

_mm.search_db = function() {
	_mm.close_mail();
	_mm.node.search_div.innerHTML = "";
	_mm.node.search_div.classList.remove("hvut-none");
	$element("div",_mm.node.search_div,[" SEARCHING...",".hvut-mm-searching"]);

	var filter = _mm.node.search_filter.value,
		name = _mm.node.search_name.value.trim().toLowerCase(),
		subject = _mm.node.search_subject.value.trim().toLowerCase(),
		text = _mm.node.search_text.value.trim().toLowerCase(),
		attach = _mm.node.search_attach.value.trim(),
		eid,
		cod = _mm.node.search_cod.value.replace(/\s/g,"").toLowerCase(),
		cod_exec,
		cod_min,
		cod_max;

	if(attach) {
		if(isNaN(attach)) {
			attach = attach.toLowerCase().replace(/\s+/g," ").split(" ");
		} else {
			eid = parseInt(attach);
		}
	}
	if( (cod_exec=/^([0-9.]+[ckm]?)$/.exec(cod)) ) {
		cod = _mm.parse_price(cod_exec[1]);
	} else if( (cod_exec=/^([0-9.]+[ckm]?)?[\-~]([0-9.]+[ckm]?)?$/.exec(cod)) ) {
		cod = false;
		cod_min = _mm.parse_price(cod_exec[1]);
		cod_max = _mm.parse_price(cod_exec[2]);
	} else {
		cod = false;
	}

  const season = _mm.node.search_season?.value || _mm.db.objectStoreName;
	var list = [];
	_mm.db.conn('readonly', season).openCursor().onsuccess = function(e) {
		var cursor = e.target.result;
		if(cursor) {
			var db = cursor.value;
			if(!_mm.data[db.mid]) {
				_mm.data[db.mid] = {};
			}
			var data = _mm.data[db.mid];
			data.db = db;
			if(!data.search) {
				data.search = {};
			}

			var check =
				filter!=="all" && filter!==db.filter ||
				name && !db.user.toLowerCase().includes(name) ||
				subject && !db.subject.toLowerCase().includes(subject) ||
				text && !db.text.toLowerCase().includes(text) ||
				cod && cod!==db.cod || cod_min && (!db.cod || cod_min>db.cod) || cod_max && cod_max<db.cod ||
				attach && !(db.attach && db.attach.some(e=>{if(eid){return e.t==="e"&&e.e===eid;}else{var n=e.n.toLowerCase();return attach.every(a=>n.includes(a));}}));

			if(!check) {
				list.push(data);
			}
			cursor.continue();

		} else {
			var table = $element("table",null,[".hvut-mm-list"]),
				tbody = $element("tbody",table);

			$element("tr",tbody,["/"+
				"<td>Search</td>"+
				"<td>"+list.length+" mail(s)</td>"+
				"<td>Attached</td>"+
				"<td>CoD</td>"+
				"<td>Sent</td>"+
				"<td>Read</td>"
			]);

			list.sort((a,b)=>b.db.mid-a.db.mid);
			list.forEach(function(mm){
				var db = mm.db,
					tr = mm.search.tr = $element("tr",tbody);
				$element("td",tr);
				$element("a",$element("td",tr),{href:"?s=Bazaar&ss=mm&filter="+db.filter+"&mid="+db.mid},function(e){e.preventDefault();_mm.load_mail(db.mid,true);});
				$element("td",tr);
				$element("td",tr);
				$element("td",tr);
				$element("td",tr);
				_mm.modify_mail(db.mid,filter);
			});

			_mm.node.search_div.innerHTML = "";
			_mm.node.search_div.appendChild(table);
		}
	};
};

_mm.search_close = function() {
	_mm.node.search_div.innerHTML = "";
	_mm.node.search_div.classList.add("hvut-none");
};

_mm.dts = function(date,year=2) {//date_to_string
	date = new Date(date*1000);
	var Y = date.getFullYear().toString().slice(-year),
		M = (date.getMonth()+1).toString().padStart(2,"0"),
		D = date.getDate().toString().padStart(2,"0"),
		h = date.getHours().toString().padStart(2,"0"),
		m = date.getMinutes().toString().padStart(2,"0");
	return Y+"-"+M+"-"+D+" "+h+":"+m;
};

_mm.kill_asshole = function(obj) {
	function h(e,t,r,a) {
		for(r='',a='0x'+e.slice(t,t+2)|0,t+=2;t<e.length;t+=2) {
			r += String.fromCharCode('0x' + e.slice(t,t+2) ^ a);
		}
		return r;
	}
	$qsa(".__cf_email__",obj).forEach(function(a){
		a.parentNode.replaceChild(document.createTextNode(h(a.dataset.cfemail,0)),a);
	});
	return obj;
};

_mm.log = function(text,clear) {
	if(clear === 1) { // init
		_mm.node.log.value = "";
		_mm.node.log.parentNode.classList.remove("hvut-none");
	} else if(clear === 2) { // clear
		_mm.node.log.value = "";
	} else if(clear === 3) { // close
		_mm.node.log.parentNode.classList.add("hvut-none");
	}
	_mm.node.log.value += text+"\n";
	_mm.node.log.scrollTop = _mm.node.log.scrollHeight;
};

_mm.itemshop_data = {
	"health draught": {id:11191,price:23},
	"health potion": {id:11195,price:45},
	"health elixir": {id:11199,price:450},
	"mana draught": {id:11291,price:45},
	"mana potion": {id:11295,price:90},
	"mana elixir": {id:11299,price:900},
	"spirit draught": {id:11391,price:45},
	"spirit potion": {id:11395,price:90},
	"spirit elixir": {id:11399,price:900},
	//"soul fragment": {id:48001,price:900},
	"crystal of vigor": {id:50001,price:9},
	"crystal of finesse": {id:50002,price:9},
	"crystal of swiftness": {id:50003,price:9},
	"crystal of fortitude": {id:50004,price:9},
	"crystal of cunning": {id:50005,price:9},
	"crystal of knowledge": {id:50006,price:9},
	"crystal of flames": {id:50011,price:9},
	"crystal of frost": {id:50012,price:9},
	"crystal of lightning": {id:50013,price:9},
	"crystal of tempest": {id:50014,price:9},
	"crystal of devotion": {id:50015,price:9},
	"crystal of corruption": {id:50016,price:9},
	"monster chow": {id:51001,price:14},
	"monster edibles": {id:51002,price:27},
	"monster cuisine": {id:51003,price:45},
	"happy pills": {id:51011,price:1800},
	"scrap cloth": {id:60051,price:90},
	"scrap leather": {id:60052,price:90},
	"scrap metal": {id:60053,price:90},
	"scrap wood": {id:60054,price:90},
	"energy cell": {id:60071,price:180},
	//"wispy catalyst": {id:60301,price:90},
	//"diluted catalyst": {id:60302,price:450},
	//"regular catalyst": {id:60303,price:900},
	//"robust catalyst": {id:60304,price:2250},
	//"vibrant catalyst": {id:60305,price:4500},
	//"coruscating catalyst": {id:60306,price:9000},
};

_mm.itemshop_parse = function(text) {
	var items = [];
	items.cost = 0;
	_mm.log("[Item Shop Request]",1);
	text.split("\n").forEach(function(t){
		var exec,name,lowercase,count,id,price;
		if(t.startsWith("> ")) {
			return;
		} else if( (exec=/([A-Za-z][A-Za-z0-9\-' ]*)(?:\s*@\s*([0-9,.]+[ckm]?))?(?:\s+[x*\uff0a]?\s*[\[(]?([0-9,]+)[\])]?)/i.exec(t)) ) {
			name = exec[1];
			count = exec[3];
		} else if( (exec=/(?:[\[(]?([0-9,]+)[\])]?\s*[x*\uff0a]?\s*)([A-Za-z][A-Za-z0-9\-' ]*)(?:\s*@\s*([0-9,.]+[ckm]?))?/i.exec(t)) ) {
			name = exec[2];
			count = exec[1];
		} else {
			return;
		}
		name = name.trim();
		lowercase = name.toLowerCase();
		count = _mm.parse_count(count);
		if((lowercase in _mm.itemshop_data) && count) {
			id = _mm.itemshop_data[lowercase].id;
			price = _mm.itemshop_data[lowercase].price;
			items.push({info:{name:name,lowercase:lowercase,iid:id},mail:{pane:"item",count:count}});
			items.cost += count * price;
			_mm.log("- "+count.toLocaleString()+" x "+name+" @ "+price.toLocaleString());
		}
	});
	_mm.log("# Total Cost: "+items.cost.toLocaleString()+"c");
	return items;
};

_mm.itemshop = function(mid,items) {
	if(items) {
		if(_mm.itemshop.mid) {
			return;
		}
		_mm.itemshop.mid = mid;
		_mm.itemshop.items = items;
		_mm.data[mid].itemshop = {};
	}
	mid = _mm.itemshop.mid;
	items = _mm.itemshop.items;
	var data = _mm.data[mid],
		status = data.itemshop;

	if(!status.taken) {
		_mm.load_mail(mid,true,"action=attach_remove&mmtoken="+data.mail.mmtoken+"&action_value=0",function(){status.taken=true;_mm.itemshop();});
		_mm.log("[Item Shop Request]",1);
		_mm.log("Receiving");

	} else if(!status.storetoken) {
		_mm.log("...");
		$ajax.add("GET","?s=Bazaar&ss=is",null,function(r){
			var doc = $doc(r.responseText),
				storetoken = $qs("input[name='storetoken']",doc);
			if(storetoken) {
				status.storetoken = storetoken.value;
				_mm.itemshop();
			} else {
				_mm.log("!!! Error");
			}
		});

	} else if(!status.bought) {
		_mm.log("Buying");
		status.bought = 0;
		items.forEach(function(item){
			$ajax.add("POST","?s=Bazaar&ss=is","storetoken="+status.storetoken+"&select_mode=shop_pane&select_item="+item.info.iid+"&select_count="+item.mail.count,function(){
				status.bought++;
				_mm.log(status.bought+"/"+items.length);
				if(status.bought === items.length) {
					_mm.itemshop();
				}
			});
		});

	} else if(!status.sent) {
		_mm.log("...");
		$ajax.add("GET","?s=Bazaar&ss=mm&filter=new&reply="+mid,null,function(r){
			var doc = $doc(r.responseText),
				outer = $id("mmail_outer",doc);
			if(outer) {
				var form = $id("mailform",doc);
				if($id("mmail_attachremove",doc)) {
					_mm.log("...");
					$ajax.add("POST","?s=Bazaar&ss=mm&filter=new","mmtoken="+form.elements.mmtoken.value+"&action=discard&action_value=0",function(){_mm.itemshop();});
				} else {
					status.sent = true;
					_mm.mmtoken = form.elements.mmtoken.value;
					_mm.node.to_name = form.elements.message_to_name;
					_mm.node.subject = form.elements.message_subject;
					_mm.node.body = form.elements.message_body;
					_mm.node.body.value += "\n\n[Item Shop Service]";
					_mm.send(items);
				}
			}
		});
	}
};

_mm.reforge = function(mid,equips) {
	if(equips) {
		if(_mm.reforge.mid) {
			return;
		}
		_mm.reforge.mid = mid;
		_mm.reforge.equips = equips;
		_mm.data[mid].reforge = {};
	}
	mid = _mm.reforge.mid;
	equips = _mm.reforge.equips;
	var data = _mm.data[mid],
		status = data.reforge;

	if(!_mm.uid) {
		$ajax.add("GET","?s=Character&ss=in",null,function(r){
			var html = r.responseText;
			_mm.uid = /var uid = (\d+);/.test(html) && RegExp.$1;
			_mm.simple_token = /var simple_token = "(\w+)";/.test(html) && RegExp.$1;
			_mm.reforge();
		});

	} else if(!status.taken) {
		_mm.load_mail(mid,true,"action=attach_remove&mmtoken="+data.mail.mmtoken+"&action_value=0",function(){status.taken=true;_mm.reforge();});
		_mm.log("[Reforge Request]",1);
		_mm.log("Receiving");

	} else if(!status.unlocked) {
		_mm.log("Unlocking");
		status.unlocked = 0;
		equips.forEach(function(eq){
			$ajax.add("JSON","json",{type:"simple",method:"lockequip",uid:_mm.uid,token:_mm.simple_token,eid:eq.info.eid,lock:0},function(r){
				var json = JSON.parse(r.responseText);
				if(!json || json.eid!=eq.info.eid || json.locked!=0) {
					status.error = "unlock error";
					_mm.log(status.unlocked+" / "+equips.length+": "+status.error);
					return;
				}
				status.unlocked++;
				_mm.log(status.unlocked+" / "+equips.length);
				if(status.unlocked === equips.length && !status.error) {
					_mm.reforge();
				}
			});
		});

	} else if(!status.reforged) {
		_mm.log("Reforging");
		status.reforged = 0;
		equips.forEach(function(eq){
			if(!eq.info.tier) {
				status.reforged++;
				_mm.log(status.reforged+" / "+equips.length+": skip level zero item");
				return;
			}
			$ajax.add("POST","?s=Forge&ss=fo&filter="+({"One-handed Weapon":"1handed","Two-handed Weapon":"2handed","Staff":"staff","Shield":"shield","Cloth Armor":"acloth","Light Armor":"alight","Heavy Armor":"aheavy"})[eq.info.category],"select_item="+eq.info.eid,function(r){
				var doc = $doc(r.responseText),
					error = get_message(doc);
				if(error) {
					status.error = error;
					_mm.log(status.reforged+" / "+equips.length+": "+status.error);
					return;
				}
				status.reforged++;
				_mm.log(status.reforged+" / "+equips.length);
				if(status.reforged === equips.length && !status.error) {
					_mm.reforge();
				}
			});
		});

	} else if(!status.sent) {
		_mm.log("...");
		$ajax.add("GET","?s=Bazaar&ss=mm&filter=new&reply="+mid,null,function(r){
			var doc = $doc(r.responseText),
				outer = $id("mmail_outer",doc);
			if(outer) {
				var form = $id("mailform",doc);
				if($id("mmail_attachremove",doc)) {
					_mm.log("...");
					$ajax.add("POST","?s=Bazaar&ss=mm&filter=new","mmtoken="+form.elements.mmtoken.value+"&action=discard&action_value=0",function(){_mm.reforge();});

				} else {
					status.sent = true;
					_mm.mmtoken = form.elements.mmtoken.value;
					_mm.node.to_name = form.elements.message_to_name;
					_mm.node.subject = form.elements.message_subject;
					_mm.node.body = form.elements.message_body;
					_mm.node.body.value += "\n\n[Reforge Service]";
					_mm.send(equips);
				}
			}
		});
	}
};

_mm.check_season = function() {
  if (_isekai) {
    const version = /(\d+) Season (\d+)/.exec(_isekai) ? parseInt(RegExp.$1.slice(2) + RegExp.$2.padStart(2, '0').slice(-2)) : 1;
    if (_mm.db.version < version) {
      _mm.db.version = version;
    }
    _mm.db.objectStoreName = _isekai;
    _mm.node.search_season = $element('select', [_mm.node.search_form, 1], { style: 'width: 50px;' });
    _mm.db.open(() => {
      Array.from(_mm.db.obj.objectStoreNames).forEach((n) => { $element('option', _mm.node.search_season, { value: n, text: n, selected: n === _isekai }); });
    });
  }
};


GM_addStyle(`
	#mmail_outerlist {margin:20px 10px 10px; overflow-y:scroll}
	#mmail_list {display:none}
	#mmail_pager {position:absolute; right:0; bottom:0; padding:5px; height:25px; width:auto}

	.hvut-mm-list {table-layout:fixed; border-collapse:collapse; margin:10px auto; width:1170px; font-size:10pt; line-height:22px; text-align:left; white-space:nowrap}
	.hvut-mm-list tr:hover {background-color:#ddd}
	.hvut-mm-list tr > td:hover {background-color:#fff}
	.hvut-mm-list tr:first-child > td {border-top:1px solid; background-color:#edb; font-size:10pt; font-weight:bold; text-align:center}
	.hvut-mm-list td {padding:1px 5px; border-bottom:1px solid; overflow:hidden; text-overflow:ellipsis}
	.hvut-mm-list td:nth-child(1) {width:150px}
	.hvut-mm-list td:nth-child(1) > span {padding:1px 3px; border:1px solid; font-weight:bold}
	.hvut-mm-list td:nth-child(2) {width:400px}
	.hvut-mm-list td:nth-child(3) {width:300px}
	.hvut-mm-list td:nth-child(4) {width:80px; text-align:right}
	.hvut-mm-list td:nth-child(4) > span {color:#03c}
	.hvut-mm-list td:nth-child(5) {width:90px; font-size:9pt}
	.hvut-mm-list td:nth-child(6) {width:90px; font-size:9pt}

	.hvut-mm-list td:nth-child(2) > a {display:block; text-decoration:none}
	.hvut-mm-list tr:hover > td:nth-child(2) > a {text-decoration:underline}

	.hvut-mm-list td:nth-child(3) > span {display:block}
	.hvut-mm-attach-e {color:#c00}
	.hvut-mm-attach-e > a {color:inherit}
	.hvut-mm-attach-c {color:#03f}
	.hvut-mm-attach-h {color:#c0c}
	.hvut-mm-attach-i {color:#090}

	.hvut-mm-current {background-color:#edb !important}
	.hvut-mm-loading {margin:20px; font-weight:bold; color:#c00}
	.hvut-mm-returned {background-color:#eee}
	.hvut-mm-returned * {color:#666 !important}
	.hvut-mm-unread {background-color:#fcc}
	.hvut-mm-nodb {background-color:#fcc}
	.hvut-mm-removed {background-color:#eee; text-decoration:line-through}
	.hvut-mm-error {background-color:#eee}
	.hvut-mm-error * {color:#666 !important}

	.hvut-mm-searchform {position:absolute; left:0; bottom:0; padding:5px; height:25px}
	.hvut-mm-searchform input:first-child {margin-right:30px}
	.hvut-mm-search {position:absolute; top:79px; left:20px; width:1200px; height:580px; border:2px solid; background-color:#EDEBDF; overflow-y:scroll}
	.hvut-mm-searching {margin:10px; font-size:12pt}

	.hvut-mm-view {position:absolute; top:79px; right:12px; display:flex; flex-direction:column; width:608px; height:570px; padding:5px; border:2px solid; background-color:#EDEBDF; font-size:10pt; text-align:left}
	.hvut-mm-failed {background-color:#eee}
	.hvut-mm-view dl {list-style:none; margin:5px 5px 0; padding:0; line-height:20px}
	.hvut-mm-view dl::after {content:''; display:block; clear:both}
	.hvut-mm-view dt {float:left; margin:0; padding:0; width:45px; border:1px solid; text-align:center}
	.hvut-mm-view dt:nth-of-type(2n+1) {width:55px; clear:left}
	.hvut-mm-view dd {float:left; margin:1px 0 5px 5px; padding:0 5px; width:109px; border-bottom:1px solid}
	.hvut-mm-view dd:nth-of-type(2n+1) {width:350px; margin-right:5px}
	.hvut-mm-view dd:nth-of-type(2n) {white-space:nowrap}
	.hvut-mm-view textarea {flex-basis:205px}
	.hvut-mm-btns {display:flex; margin:5px 0}
	.hvut-mm-btns .hvut-mm-edit {margin-left:auto}
	.hvut-mm-view ul {margin:5px; padding:5px; border:1px solid; list-style:none; max-height:242px; overflow:auto; flex-shrink:0; line-height:20px}
	.hvut-mm-view li:first-child {margin-top:0; padding:0 0 0 5px; border:1px solid; font-weight:bold}
	.hvut-mm-view li:first-child > .hvut-mm-price {text-align:center}
	.hvut-mm-view li {display:flex; margin-top:2px; padding:0 1px 0 6px}
	.hvut-mm-view li > span:first-child {margin-right:auto}
	.hvut-mm-view li > input {margin:0; padding:1px 5px; text-align:right}
	.hvut-mm-price {width:60px}
	.hvut-mm-cod {width:75px}
	.hvut-mm-invalid {color:#c00}
	.hvut-mm-view input[data-cod-match='1'] {color:#03c}
	.hvut-mm-view input[data-cod-match='0'] {color:#c00}
	.hvut-mm-none input {display:none}

	.hvut-mm-log {position:absolute; top:79px; right:632px; border:2px solid; background-color:#EDEBDF}
`);


_mm.table[_mm.page] = $element("table",$id("mmail_outerlist"),[".hvut-mm-list"]);

$id("mmail_pager").innerHTML = "";
_mm.node.page_go = $element("input",$id("mmail_pager"),{type:"text",value:_mm.page,style:"width:30px;text-align:center"});
$element("input",$id("mmail_pager"),{type:"button",value:"GO"},function(){location.href=location.href.replace(/&page=\d+/,"")+"&page="+_mm.node.page_go.value;});
_mm.node.page_new = $element("input",$id("mmail_pager"),{type:"button",value:"Prev"},function(){_mm.load_page("new");});
_mm.node.page_old = $element("input",$id("mmail_pager"),{type:"button",value:"Next"},function(){_mm.load_page("old");});

_mm.node.search_form = $element("div",$id("mmail_outer"),[".hvut-mm-searchform"]);
$element("input",_mm.node.search_form,{type:"button",value:"Clear Records"},function(){if(confirm("All MoogleMail records in this browser will be deleted.\n\nAre you sure?")){_mm.db.clear();}});
_mm.node.search_filter = $element("select",_mm.node.search_form,{innerHTML:"<option value='all'>All</option><option value='inbox'>Inbox</option><option value='read'>Read</option><option value='sent'>Sent</option>",style:"width:50px"});
_mm.node.search_name = $element("input",_mm.node.search_form,{type:"text",placeholder:"User",style:"width:100px"},{keypress:function(e){if(e.keyCode===13){_mm.search_db();}}});
_mm.node.search_subject = $element("input",_mm.node.search_form,{type:"text",placeholder:"Subject",style:"width:130px"},{keypress:function(e){if(e.keyCode===13){_mm.search_db();}}});
_mm.node.search_text = $element("input",_mm.node.search_form,{type:"text",placeholder:"Text",style:"width:130px"},{keypress:function(e){if(e.keyCode===13){_mm.search_db();}}});
_mm.node.search_attach = $element("input",_mm.node.search_form,{type:"text",placeholder:"Attachment",style:"width:130px"},{keypress:function(e){if(e.keyCode===13){_mm.search_db();}}});
_mm.node.search_cod = $element("input",_mm.node.search_form,{type:"text",placeholder:"CoD",style:"width:70px;text-align:right;cursor:help",title:"min-max"},{keypress:function(e){if(e.keyCode===13){_mm.search_db();}}});
$element("input",_mm.node.search_form,{type:"button",value:"Search"},function(){_mm.search_db();});
$element("input",_mm.node.search_form,{type:"button",value:"Close"},function(){_mm.search_close();});

_mm.node.search_div = $element("div",$id("mmail_outer"),[".hvut-mm-search hvut-none"]);
_mm.node.view = $element("div",$id("mmail_outer"),[".hvut-mm-view hvut-none"]);
_mm.node.log = $element("textarea",$element("div",$id("mmail_outer"),[".hvut-mm-log hvut-none"]),{readOnly:true,spellcheck:false,style:"width:300px;height:300px"});

_mm.check_season();
_mm.db.open(function(){_mm.create_page($id("mmail_list"),_mm.page);$id("mmail_list").remove();});

}

} else
// [END 13] Bazaar - MoogleMail */


//* [14] Bazaar - Lottery
if(settings.lottery && _query.s==="Bazaar" && (_query.ss==="lt" || _query.ss==="la")) {

if(settings.showLottery && $qs("img[src$='lottery_next_d.png']")) {
	_lt.json = getValue(_query.ss+"_show",{});
	$element("div",$id("rightpane"),["!margin:10px;color:#c00"]).appendChild(
		$element("label",null," Show this lottery on every page")).prepend(
			$element("input",null,{type:"checkbox",checked:!_lt.json.hide},{change:function(){_lt.json.hide=!this.checked;setValue(_query.ss+"_show",_lt.json);}}));
}

confirm_event($qs("img[src$='/lottery_golden_a.png']"),"click","Are you sure you wish to spend a Golden Lottery Ticket?");

} else
// [END 14] Bazaar - Lottery */


// Battle
if(_query.s==="Battle" && $id("initform")) {

GM_addStyle(`
	.hvut-bt-wrapper {display:none; width:600px; height:10px}
	.hvut-bt-right {float:right; width:1220px !important}
	.hvut-bt-right > p {width:520px; margin-left:auto; margin-right:auto}
	.hvut-bt-on .hvut-bt-wrapper {display:block}
	.hvut-bt-on .hvut-bt-right {width:620px !important}

	#arena_list {white-space:nowrap}
	#arena_list tbody > tr > th:nth-child(1) {width:474px}
	#arena_list tbody > tr > th:nth-child(2) {width:120px}
	#arena_list tbody > tr > th:nth-child(3) {width:90px}
	#arena_list tbody > tr > th:nth-child(4) {width:90px}
	#arena_list tbody > tr > th:nth-child(5) {width:90px}
	#arena_list tbody > tr > th:nth-child(6) {width:90px}
	#arena_list tbody > tr > th:nth-child(7) {width:120px}
	#arena_list tbody > tr > th:nth-child(8) {width:90px}
	#arena_list tbody > tr > th:nth-child(8) > input {width:90px; margin:0; padding:1px 0; font-size:8pt}
	#arena_list tbody > tr > td > div {width:100% !important; left:0}

	.hvut-bt-on #arena_list tr > th:nth-child(1) {width:302px}
	.hvut-bt-on #arena_outer #arena_list tr > *:nth-child(2),
	.hvut-bt-on #arena_outer #arena_list tr > *:nth-child(5),
	.hvut-bt-on #arena_outer #arena_list tr > *:nth-child(6),
	.hvut-bt-on #arena_outer #arena_list tr > *:nth-child(7) {display:none}
	.hvut-bt-on #rob_outer #arena_list tr > *:nth-child(2),
	.hvut-bt-on #rob_outer #arena_list tr > *:nth-child(4),
	.hvut-bt-on #rob_outer #arena_list tr > *:nth-child(5),
	.hvut-bt-on #rob_outer #arena_list tr > *:nth-child(7) {display:none}
`);
$id("mainpane").style.paddingRight = "8px";

_ar.split_colspan = function(table) {
	$qsa("td[colspan='2']",table).forEach(function(td){
		td.removeAttribute("colspan");
		$element("td",[td,"beforebegin"],"-");
	});
};


//* [16] Battle - Arena
if(settings.arena && _query.ss==="ar") {

_ar.split_colspan($id("arena_list"));
toggle_button( $element("input",$id("arena_list").rows[0].cells[7],{type:"button"}), "Show Details","Hide Details",$id("mainpane"),"hvut-bt-on",true);
$element("div",[$id("mainpane"),"afterbegin"],["#arena_outer"]).append($id("arena_list"));

} else
// [END 16] Battle - Arena */


//* [17] Battle - Ring of Blood
if(settings.ringofblood && _query.ss==="rb") {

_ar.split_colspan($id("arena_list"));
toggle_button( $element("input",$id("arena_list").rows[0].cells[7],{type:"button"}), "Show Details","Hide Details",$id("mainpane"),"hvut-bt-on",true);
$element("div",[$id("mainpane"),"afterbegin"],["#rob_outer"]).append($id("arena_list"),$id("arena_tokens"));

} else
// [END 17] Battle - Ring of Blood */


//* [18] Battle - GrindFest
if(settings.grindfest && _query.ss==="gr") {

} else
// [END 18] Battle - GrindFest */


//* [19] Battle - Item World
if(settings.itemWorld && _query.ss==="iw") {

_iw.pxp_mod = !_isekai ? {"Normal":2,"Hard":2,"Nightmare":4,"Hell":7,"Nintendo":10,"IWBTH":15,"PFUDOR":20} : {"Normal":12,"Hard":12,"Nightmare":12,"Hell":21,"Nintendo":30,"IWBTH":45,"PFUDOR":60};

_iw.calc_potency = function(eq,round) {
	round = parseInt(round);
	if(!round) {
		round = eq.info.round;
	}

	var exp = round * _iw.pxp_mod[_player.dfct];
	if(eq.info.soulbound) {
		exp *= 2;
	}

	var tier = eq.info.tier,
		pxp2 = eq.info.pxp2,
		pxp1 = eq.info.pxp1 + exp;

	while(tier<10 && pxp1>=pxp2) {
		tier++;
		pxp1 -= pxp2;
		pxp2 = Math.ceil(eq.info.pxp * Math.pow(1+eq.info.pxp/1000,tier));
	}

	eq.node.calc.innerHTML = "";
	$element("span",eq.node.calc,[" +"+exp+" ("+round+")",".hvut-iw-gain hvut-cphu"],function(){_iw.calc_potency(eq,prompt("Enter the number of Rounds"));});
	$element("span",eq.node.calc,"=");
	$element("span",eq.node.calc,[" IW "+tier,".hvut-iw-tier"+(tier!==eq.info.tier?" hvut-iw-up":"")]);
	$element("span",eq.node.calc,[" "+(tier<10?"("+pxp1+ " / "+pxp2+")":"MAX"),".hvut-iw-pxp"]);
};

_iw.reforge = function(eq) {
	if(!eq.node.lock.classList.contains("iu")) {
		alert("Unlock before reforge.");
		return;
	}
	if(!confirm("["+eq.info.name+"]\n\nAre you sure you want to reforge this item?\n\nThis will remove all potencies and reset its level to zero.")) {
		return;
	}

	$ajax.add("POST","?s=Forge&ss=fo&filter="+_iw.filter,"select_item="+eq.info.eid,
		function(r){
			var doc = $doc(r.responseText),
				error = get_message(doc);
			if(error) {
				alert(error);
			}
			location.href = location.href;
		},
		function(){
			alert("Error!");
			location.href = location.href;
		}
	);
};

_iw.show_potency = function(eq) {
	if(!eq.info.round) {
		eq.info.round = Math.round(Math.pow((eq.info.pxp-0.5-100)/250,3)*75);
		if(eq.info.round > 100) {
			eq.info.round = 100;
		} else if(eq.info.round < 20) {
			eq.info.round = 20;
		}
		eq.node.calc = $element("span",eq.node.sub,[".hvut-iw-calc"]);
	}
	_iw.calc_potency(eq);

	if(eq.data.checked) {
		return;
	}
	eq.data.checked = true;
	if(!eq.info.tier) {
		return;
	}

	$ajax.add("GET","equip/"+eq.info.eid+"/"+eq.info.key,null,function(r){
		var doc = $doc(r.responseText);
		$element("span",eq.node.sub,[" Reforge",".hvut-iw-reforge hvut-cphu"],function(){_iw.reforge(eq);});
		var span = $element("span",eq.node.sub,[".hvut-iw-potency"]);
		$qsa("#ep > span",doc).forEach(function(p){
			$element("span",span,p.textContent);
		});
	});
};

GM_addStyle(`
	#itemworld_right {display:none}
	#itemworld_left {float:none; margin:0 auto}
	#itemworld_left .cspp {overflow-y:scroll}
	div[onclick*='start_itemworld'] {width:200px; margin:30px auto 10px}

	#itemworld_left .eqp {height:44px}
	.hvut-iw-sub {position:absolute; top:24px; left:20px; font-size:8pt; line-height:18px; white-space:nowrap}
	.hvut-iw-sub > * {margin-right:5px}
	.hvut-iw-tier {font-weight:bold}
	.hvut-iw-calc > span {margin-right:5px}
	.hvut-iw-up {color:#c00}
	.hvut-iw-gain {color:#c00}
	.hvut-iw-reforge {color:#c00; font-weight:bold}
	.hvut-iw-potency > span {margin-left:5px}
	.hvut-iw-latest {border:1px solid; background-color:#fff !important}
`);

$id("itemworld_left").firstElementChild.prepend($id("accept_button").parentNode);

$equip.list($id("item_pane"),1).forEach(function(eq){
	eq.node.div.addEventListener("click",function(){_iw.json[_iw.filter]=eq.info.eid;setValue("iw_json",_iw.json);_iw.show_potency(eq);});
	eq.node.lock = eq.node.wrapper.firstElementChild;
	eq.node.sub = $element("div",[eq.node.div,"beforebegin"],[".hvut-iw-sub"]);
	$element("span",eq.node.sub,[" IW "+eq.info.tier,(eq.info.tier?".hvut-iw-tier":"")]);
	$element("span",eq.node.sub,[" ("+eq.info.pxp1+" / "+eq.info.pxp2+")",".hvut-iw-pxp"]);
});

_iw.json = getValue("iw_json",{});
_iw.filter = _query.filter || "1handed";
if( (_iw.latest=$id("e"+_iw.json[_iw.filter])) ) {
	$qs("#item_pane > .equiplist").prepend(_iw.latest.parentNode);
	_iw.latest.parentNode.classList.add("hvut-iw-latest");
	_iw.latest.click();
}

} else
// [END 19] Battle - Item World */

{} // END OF [else if]; DO NOT REMOVE THIS LINE!


var $battle = {};

//* [0] Battle - Equipment Enchant and Repair
if(settings.equipEnchant) {

$battle.inventory = {};

$battle.view = function(eq) {
	$battle.node.repair.innerHTML = "";
	$battle.node.enchant.innerHTML = "";

	if(!eq) {
		return;
	}
	if($battle.current) {
		$battle.current.node.li.classList.remove("hvut-bt-active");
	}
	$battle.current = eq;
	eq.node.li.classList.add("hvut-bt-active");

	if(eq.data.repair) {
		Object.entries(eq.data.repair).forEach(function([n,c]){
			$element("li",$battle.node.repair,n+" x "+c);
		});
	} else {
		$element("li",$battle.node.repair,"-");
	}

	var cat = eq.info.cat,
		day = (new Date()).getUTCDay();
	Object.entries($battle.enchant_data).forEach(function([n,item]){
		if(item[cat]) {
			let li = $element("li",$battle.node.enchant),
				c = parseInt((cat==="weapon" && n.includes("Infusion of ")) ? settings.equipEnchantWeapon : settings.equipEnchantArmor) || 1,
				s = $battle.inventory[n] || 0;
			if(cat==="weapon" && item.day===day) {
				li.classList.add("hvut-bt-day");
			}
			if(!s) {
				li.classList.add("hvut-bt-nostock");
			}
			$element("span",li,[" [+"+c+"]",".hvut-bt-imbue hvut-cphu"],function(){$battle.enchant(eq,n,c);});
			$element("span",li,[" "+item.effect,".hvut-bt-shard hvut-cphu"],function(){$battle.enchant(eq,n,1);});
			$element("span",li,[" ("+s+")",".hvut-bt-stock"]);
		}
	});
};

$battle.load = function(eq) {
	eq.node.enc.textContent = "Loading...";
	$ajax.add("GET","equip/"+eq.info.eid+"/"+eq.info.key,null,function(r){
		var doc = $doc(r.responseText);
		$battle.parse(eq,doc);
	});
};

$battle.parse = function(eq,doc) {
	var div = $id("equip_extended",doc);

	var exec = /Condition: (\d+) \/ (\d+) \((\d+)%\)/.exec($qs(".eq",div).children[1].textContent);
	eq.info.condition = exec[1];
	eq.info.durability = exec[2];
	eq.info.cdt = eq.info.condition/eq.info.durability;
	$battle.display_cdt(eq);

	eq.node.enc.innerHTML = "";
	var enchant = $qsa("#ee > span",div);
	enchant.forEach(function(e){
		$element("span",eq.node.enc,e.textContent);
	});
	if(!enchant.length) {
		eq.node.enc.textContent = "No Enchantments";
	}
};

$battle.enchant = function(eq,name,count) {
	var item = $battle.enchant_data[name],
		stock = $battle.inventory[name] || 0;
	if(count > stock) {
		count = stock;
	}
	if(count < 1) {
		return;
	}
	var c = count;

	eq.node.enc.textContent = "Loading...";
	while(count--) {
		$ajax.add("POST","?s=Forge&ss=en","select_item="+eq.info.eid+"&enchantment="+item[eq.info.cat],function(r){
			var doc = $doc(r.responseText);
			$battle.parse(eq,doc);
			if(!--c) {
				$battle.load_inventory();
			}
		});
	}
};

$battle.repair = function(eq) {
	if(eq && eq.info && eq.info.cdt===1) {
		return;
	}
	$battle.node.repair.innerHTML = "";

	$ajax.add("POST","?s=Forge&ss=re", eq==="all"?"repair_all=1" : eq?"select_item="+eq.info.eid : null, function(r){
		var doc = $doc(r.responseText);
		$battle.load_dynjs($qs("script[src*='/dynjs/']",doc).getAttribute("src")+"?t="+Date.now());

		var error = get_message(doc);
		if(error) {
			if(eq==="all") {
				popup(error);
			} else if(eq) {
				popup(error);
			} else {
				popup(error);
			}
			$battle.load_inventory();
			return;
		}

		var repairall = /Requires: (.+)/.test($id("repairall",doc).nextElementSibling.textContent) && RegExp.$1;
		if(repairall === "Everything is fully repaired.") {
			$battle.repairall = null;
		} else {
			$battle.repairall = {};
			repairall.split(", ").forEach(function(e){
				var exec = /(\d+)x (.+)/.exec(e);
				$battle.repairall[exec[2]] = parseInt(exec[1]);
			});
		}

		var repair_equip = {};
		$qsa(".equiplist div[onclick*='set_forge_cost']",doc).forEach(function(div){
			var [,eid,repair] = /set_forge_cost\((\d+),'Requires: (.+?)'/.exec(div.getAttribute("onclick"));
			repair_equip[eid] = {};
			repair.split(", ").forEach(function(e){
				var exec = /(\d+)x (.+)/.exec(e);
				repair_equip[eid][exec[2]] = parseInt(exec[1]);
			});
		});

		$battle.equip.forEach(function(_eq){
			var repair = repair_equip[_eq.info.eid];
			if(repair) {
				_eq.data.repair = repair;
			} else {
				_eq.data.repair = false;
			}

			if(eq===_eq || eq==="all"&&settings.equipEnchantCheckArmors) {
				$battle.load(_eq);
			}
		});

		if(eq) {
			$battle.load_inventory();
		} else {
			$battle.display_inventory();
		}
		$persona.check_warning(doc);
	});
};

$battle.load_dynjs = function(url) {
	$ajax.add("GET",url,null,function(r){
		var dynjs_loaded = JSON.parse(r.responseText.slice(16,-1));

		$battle.equip.some(function(eq){
			var dynjs = dynjs_loaded[eq.info.eid];
			if(!dynjs) {
				$persona.change_p();
				return true;
			}
			var exec = $equip.reg.html.exec(dynjs.d);
			eq.info.condition = parseInt(exec[4]);
			eq.info.durability = parseInt(exec[5]);
			eq.info.cdt = eq.info.condition/eq.info.durability;
			$battle.display_cdt(eq);
		});
	});
};

$battle.display_cdt = function(eq) {
	var thld = settings.equipEnchantRepairThreshold;
	if(thld < 1) {
		thld = eq.info.cdt <= thld;
	} else {
		thld = eq.info.condition <= eq.info.durability*0.5 + thld;
	}
	eq.node.cdt.textContent = eq.info.condition+" / "+eq.info.durability+" ("+(eq.info.cdt*100).toFixed(1)+"%)";
	eq.node.cdt.dataset.cdt = eq.info.cdt<=0.5?"2" : eq.info.cdt<=0.6||thld?"1" : "";
};

$battle.load_inventory = function() {
	$battle.inventory = {};
	$battle.node.repairall.innerHTML = "";
	$battle.node.inventory.innerHTML = "";

	$ajax.add("GET","?s=Character&ss=it",null,function(r){
		var doc = $doc(r.responseText);
		Array.from($qs(".itemlist",doc).rows).forEach(function(tr){
			$battle.inventory[ tr.cells[0].textContent.trim() ] = parseInt(tr.cells[1].textContent);
		});
		$battle.display_inventory();
	});
};

$battle.display_inventory = function() {
		$battle.node.repairall.innerHTML = "";
		if($battle.repairall) {
			Object.entries($battle.repairall).forEach(function([n,c]){
				if(c) {
					let s = $battle.inventory[n] || 0;
					$element("li",$battle.node.repairall,[" "+n+" x "+c+" ("+s+")",s<c?".hvut-bt-warn":""]);
				}
			});
		} else if($battle.repairall === null) {
			$element("li",$battle.node.repairall,"Everything is fully repaired.");
		}

		$battle.node.inventory.innerHTML = "";
		Object.entries(settings.equipEnchantInventory).forEach(function([n,m]){
			var c = $battle.inventory[n] || 0;
			$element("li",$battle.node.inventory,[" "+n+" ("+c+")",c<m?".hvut-bt-warn":""]);
		});
		$battle.view($battle.current);
};

$battle.init = function() {
	$battle.load_inventory();

	$battle.node.equip.innerHTML = "";
	$battle.equip = getValue("eq_set",[]).map(function(info){
		if(!info.eid) {
			$element("li",$battle.node.equip,[".hvut-bt-empty"]).append(
				$element("div",null,info.slot),
				$element("div",null,"Empty"),
				$element("div",null,[".hvut-bt-cdt"])
			);
			return;
		}

		var eq = {info:info,data:{},node:{}};
		eq.info.cat = (eq.info.category==="One-handed Weapon" || eq.info.category==="Two-handed Weapon" || eq.info.category==="Staff") ? "weapon" : "armor";

		eq.node.li = $element("li",$battle.node.equip,null,{mouseenter:function(){$battle.view(eq);}});
		eq.node.name = $element("a",eq.node.li,{className:"hvut-bt-name",target:"hvut-bt-equip",href:"equip/"+eq.info.eid+"/"+eq.info.key});

		var equipname = $equip.names[eq.info.eid];
		if(equipname && equipname !== eq.info.name) {
			eq.info.customname = eq.info.name;
			eq.info.name = equipname;
			$element("span",eq.node.name,"[ "+eq.info.customname+" ]");
			$element("span",eq.node.name,"[ "+eq.info.name+" ]");
		} else {
			eq.node.name.textContent = eq.info.name;
		}
		eq.node.enc = $element("div",eq.node.li,[".hvut-bt-enc"]);
		eq.node.cdt = $element("div",eq.node.li,[" ...",".hvut-bt-cdt"]);

		if(eq.info.cat==="weapon" || settings.equipEnchantCheckArmors) {
			$battle.load(eq);
		}
		return eq;
	}).filter(eq=>eq);

	$battle.current = $battle.equip[0];
	$battle.repair();
};

$battle.enchant_data = {
	"Voidseeker Shard":{effect:"Voidseeker's Blessing",weapon:"vseek"},
	"Aether Shard":{effect:"Suffused Aether",weapon:"ether"},
	"Featherweight Shard":{effect:"Featherweight Charm",weapon:"feath",armor:"feath"},
	"Infusion of Flames":{effect:"Infused Flames",weapon:"sfire",armor:"pfire",day:2},
	"Infusion of Frost":{effect:"Infused Frost",weapon:"scold",armor:"pcold",day:3},
	"Infusion of Lightning":{effect:"Infused Lightning",weapon:"selec",armor:"pelec",day:6},
	"Infusion of Storms":{effect:"Infused Storm",weapon:"swind",armor:"pwind",day:4},
	"Infusion of Divinity":{effect:"Infused Divinity",weapon:"sholy",armor:"pholy",day:0},
	"Infusion of Darkness":{effect:"Infused Darkness",weapon:"sdark",armor:"pdark",day:1}
};


GM_addStyle(`
	.hvut-bt-div {position:absolute; bottom:8px; width:600px; color:#333; font-size:10pt; line-height:20px; white-space:nowrap}
	.hvut-bt-div > ul {margin:0; padding:21px 0 0; border:1px solid; list-style:none; display:flex; flex-direction:column; justify-content:center}
	.hvut-bt-div > ul::before {content:attr(data-header); position:absolute; top:0; width:100%; border-bottom:1px solid; line-height:20px; background-color:#edb; font-weight:bold}

	.hvut-bt-equip {position:absolute; bottom:0; left:0; width:400px; height:286px}
	.hvut-bt-equip > li {position:relative; height:40px; padding-right:60px; border-bottom:1px solid}
	.hvut-bt-equip > li:last-child {border-bottom:none}
	.hvut-bt-active {background-color:#fff; z-index:1}
	.hvut-bt-empty > div:first-child {font-weight:bold}
	.hvut-bt-name {display:block; overflow:hidden; text-overflow:ellipsis; font-weight:bold; text-decoration:none}
	.hvut-bt-name > span:last-child {display:none}
	.hvut-bt-name:hover > span:first-child {display:none}
	.hvut-bt-name:hover > span:last-child {display:inline}
	.hvut-bt-enc {font-size:9pt; overflow:hidden; text-overflow:ellipsis}
	.hvut-bt-enc:empty {display:none}
	.hvut-bt-active:hover > .hvut-bt-enc {white-space:normal; border-bottom:1px solid; background:inherit; pointer-events:none}
	.hvut-bt-active:last-child:hover > .hvut-bt-enc {position:absolute; bottom:0; width:340px; border-top:1px solid; border-bottom:none}
	.hvut-bt-enc > span {display:inline-block; margin:0 3px; color:#e00}
	.hvut-bt-cdt {position:absolute; top:0; right:0; width:59px; height:100%; border-left:1px solid #333; font-size:9pt; white-space:normal}
	.hvut-bt-cdt[data-cdt='1'] {color:#e00}
	.hvut-bt-cdt[data-cdt='2'] {color:#fff; background-color:#e00}

	.hvut-bt-inventory {position:absolute; bottom:314px; left:0; width:400px; min-height:80px; max-height:160px; flex-direction:row !important; justify-content:space-between !important; flex-wrap:wrap; align-content:space-evenly}
	.hvut-bt-inventory > li {width:32%; overflow:hidden; font-size:9pt}
	.hvut-bt-inventory > li:last-child:nth-child(3n+2) {margin-right:34%}
	.hvut-bt-warn {font-weight:bold; color:#e00}

	.hvut-bt-enchant {position:absolute; bottom:0; left:407px; width:190px; height:204px; line-height:18px}
	.hvut-bt-enchant > li {display:flex; margin:2px 0}
	.hvut-bt-enchant span {margin:0 2px}
	.hvut-bt-imbue {color:#03c}
	.hvut-bt-shard {flex-grow:1; overflow:hidden; text-overflow:ellipsis; text-align:left; color:#03c}
	.hvut-bt-day {background-color:#fff}
	.hvut-bt-day > .hvut-bt-shard {font-weight:bold}
	.hvut-bt-nostock > span {color:#999 !important; cursor:default}

	.hvut-bt-repair {position:absolute; bottom:232px; left:407px; width:190px; height:54px; cursor:pointer}
	.hvut-bt-repair:hover {background-color:#fff}

	.hvut-bt-repairall {position:absolute; bottom:314px; left:407px; width:190px; min-height:80px; cursor:pointer}
	.hvut-bt-repairall:hover {background-color:#fff}
`);


$battle.node = {};
$battle.node.wrapper = $element("div",null,[".hvut-bt-wrapper"]);
$battle.node.div = $element("div",$battle.node.wrapper,[".hvut-bt-div"]);
$battle.node.equip = $element("ul",$battle.node.div,{className:"hvut-bt-equip",dataset:{header:"EQUIPMENT"}});
$battle.node.enchant = $element("ul",$battle.node.div,{className:"hvut-bt-enchant",dataset:{header:"ENCHANT"}});
$battle.node.repair = $element("ul",$battle.node.div,{className:"hvut-bt-repair",dataset:{header:"REPAIR"}},function(){$battle.repair($battle.current);});
$battle.node.repairall = $element("ul",$battle.node.div,{className:"hvut-bt-repairall",dataset:{header:"REPAIR ALL"}},function(){$battle.repair("all");});
$battle.node.inventory = $element("ul",$battle.node.div,{className:"hvut-bt-inventory",dataset:{header:"ITEM INVENTORY"}});

if(settings.equipEnchantPosition === "right") {
	GM_addStyle(`
		#popup_box.hvut-bt-popupright {left:620px !important}
	`);
	$id("popup_box").classList.add("hvut-bt-popupright");
	$battle.node.wrapper.style.cssFloat = "right";
} else {
	GM_addStyle(`
		#popup_box.hvut-bt-popupleft {left:248px !important}
	`);
	$id("popup_box").classList.add("hvut-bt-popupleft");
	$battle.node.wrapper.style.cssFloat = "left";
}

if($id("filterbar")) {
	$id("filterbar").after($battle.node.wrapper);
} else {
	$id("mainpane").prepend($battle.node.wrapper);
}
$qs("#arena_outer, #rob_outer, #towerstart, #grindfest, #itemworld_outer, #initform").classList.add("hvut-bt-right");
$id("mainpane").classList.add("hvut-bt-on");

$battle.init();

}
// [END 0] Battle - Equipment Enchant and Repair */

} else
// Battle


//* [21] Forge - Upgrade
if(settings.upgrade && _query.s==="Forge" && _query.ss==="up") {

_up.exp = {"Low-Grade":1,"Mid-Grade":5,"High-Grade":20,"Wispy Catalyst":3,"Diluted Catalyst":13,"Regular Catalyst":25,"Robust Catalyst":63,"Vibrant Catalyst":125,"Coruscating Catalyst":250};
_up.catalysts = ["Wispy Catalyst","Diluted Catalyst","Regular Catalyst","Robust Catalyst","Vibrant Catalyst","Coruscating Catalyst"];

_up.sort = function(object) {
	var index = ["Wispy Catalyst","Diluted Catalyst","Regular Catalyst","Robust Catalyst","Vibrant Catalyst","Coruscating Catalyst","Low-Grade","Mid-Grade","High-Grade","Crystallized Phazon","Shade Fragment","Repurposed Actuator","Defense Matrix Modulator","Binding of"];
	Object.keys(object).sort((a,b)=>index.findIndex(e=>a.includes(e))-index.findIndex(e=>b.includes(e))).forEach(function(k){
		var v = object[k];
		delete object[k];
		object[k] = v;
	});
};

_up.set = function(eq) {
	eq.upgrade.type = ({"One-handed Weapon":"Metals","Two-handed Weapon":"Metals","Staff":"Wood","Shield":"Wood","Cloth Armor":"Cloth","Light Armor":"Leather","Heavy Armor":"Metals"})[eq.info.category];
	eq.upgrade.rare = ({"Phase":"Crystallized Phazon","Shade":"Shade Fragment","Power":"Repurposed Actuator","Force Shield":"Defense Matrix Modulator"})[eq.info.type];

	var array = new Array(150);
	array.fill([6,0,0],0);
	array.fill([5,1,0],5);
	array.fill([4,2,0],12);
	array.fill([3,3,0],20);
	array.fill([2,4,0],27);
	array.fill([1,5,0],35);
	array.fill([0,6,0],42);
	array.fill([0,5,1],55);
	array.fill([0,4,2],62);
	array.fill([0,3,3],70);
	array.fill([0,2,4],77);
	array.fill([0,1,5],85);
	array.fill([0,0,6],92);
	array = array.slice(({"Sup":0,"Exq":15,"Mag":30,"Leg":50})[eq.upgrade.quality]||0);
	var catalysts = _up.catalysts.slice(({"Sup":0,"Exq":1,"Mag":2,"Leg":3})[eq.upgrade.quality]||0);

	eq.upgrade.requires_50.length = 0;
	eq.upgrade.requires_100.length = 0;
	array.slice(0,100).forEach(function(e,i){
		var _100 = {materials:{}},c;
		if(e[0]) {_100.materials["Low-Grade "+eq.upgrade.type]=e[0];}
		if(e[1]) {_100.materials["Mid-Grade "+eq.upgrade.type]=e[1];}
		if(e[2]) {_100.materials["High-Grade "+eq.upgrade.type]=e[2];}
		if(i>4 && !_isekai) {
			_100.binding = true;
		}
		_100.forge_exp = _up.exp["Low-Grade"]*e[0] + _up.exp["Mid-Grade"]*e[1] + _up.exp["High-Grade"]*e[2];

		if(i<5 || i<95 && i%2===0) {
			var _50 = JSON.parse(JSON.stringify(_100)),
				j = i<5 ? i : ((i-4)/2)+4;
			c = catalysts[j<13?0:j<25?1:2];
			_50.materials[c] = 1;
			_50.forge_exp += _up.exp[c];
			_50.gear_exp = Math.ceil(_50.forge_exp/10);
			eq.upgrade.requires_50.push(_50);
		}

		c = catalysts[i<25?0:i<50?1:2];
		_100.materials[c] = 1;
		_100.forge_exp += _up.exp[c];
		_100.gear_exp = Math.ceil(_100.forge_exp/10);
		eq.upgrade.requires_100.push(_100);

	});
};

_up.calc = function(eq,up,t) {
	up.materials = {};
	up.forge_exp = 0;
	up.gear_exp = 0;
	var from, to;
	if(t) {
		from = up.level;
		to = up.to;
	} else {
		from = 0;
		to = up.level;
	}
	up.requires.slice(from,to).forEach(function(c){
		Object.keys(c.materials).forEach(function(n){
			if(!up.materials[n]) {
				up.materials[n] = 0;
			}
			up.materials[n] += c.materials[n];
		});
		if(c.binding) {
			if(!up.materials[up.binding]) {
				up.materials[up.binding] = 0;
			}
			up.materials[up.binding]++;
		}
		up.forge_exp += c.forge_exp;
		up.gear_exp += c.gear_exp;
	});
	if(eq.upgrade.rare && to > eq.upgrade.level) {
		up.materials[eq.upgrade.rare] = to - eq.upgrade.level;
	}
	_up.sort(up.materials);
};

_up.sum = function(eq,t) {
	var materials = {},
		level = 0,
		forge_exp = 0,
		gear_exp = 0;
	eq.upgrade.list.forEach(function(up){
		if(t) {
			if(!up.valid || !up.to || up.to===up.level) {
				return;
			}
			if(level < up.to) {
				level = up.to;
			}
		}
		Object.keys(up.materials).forEach(function(n){
			if(!materials[n]) {
				materials[n] = 0;
			}
			materials[n] += up.materials[n];
		});
		forge_exp += up.forge_exp;
		gear_exp += up.gear_exp;
	});
	if(t) {
		if(eq.upgrade.rare && level > eq.upgrade.level) {
			materials[eq.upgrade.rare] = level - eq.upgrade.level;
		}
	} else {
		if(eq.upgrade.rare) {
			materials[eq.upgrade.rare] = eq.upgrade.level;
		}
	}
	_up.sort(materials);

	var credits = 0,
		prices = $prices.get("Materials");
	Object.keys(materials).forEach(function(n){
		credits += materials[n] * (prices[n]||0);
	});

	return {materials,credits,forge_exp,gear_exp};
};


_up.calc_salvage = function(q) {
	var text = _up.node.salvage_equip.value.trim();
	if(!text) {
		return;
	}
	text = text.replace(/( (?:of|the))\n/g,"$1 ").replace(/\n((?:Of|The) )/g," $1");
	var eq = text.split("\n").reverse().find(t=>$equip.parse.name(t.trim()).info.category!=="Obsolete");
	if(!eq) {
		alert("Couldn't find the name of the equipment.");
		return;
	}
	eq = $equip.parse.name(eq);
	var pxp,quality;
	if(q) {
		pxp = 0;
		quality = q;
	} else if(/Potency Tier: (\d+) \(\d+ \/ (\d+)\)/.test(text)) {
		pxp = $equip.calcpxp(+RegExp.$2,+RegExp.$1);
		quality = pxp>=348?"Leg" : pxp>=335?"Mag" : pxp>=313?"Exq" : "Sup";
	} else {
		quality = ({"Superior":"Sup","Exquisite":"Exq","Magnificent":"Mag","Legendary":"Leg","Peerless":"Leg"})[eq.info.quality] || "Sup";
	}
	_up.node.salvage_quality.value = "";

	eq.upgrade = {
		list : [],
		quality : quality,
		level : 0,
		requires_50 : [],
		requires_100 : []
	};

	Array.from(text.matchAll(/([\w ]+) Lv\.(\d+)/g)).forEach(function(m){
		var forge = m[1].trim(),
			[name,stat] = Object.entries($equip.stats).find(([,s])=>s.forge===forge) || [],
			max = forge==="Physical Damage" || forge==="Magical Damage" ? 100 : 50;
		if(!name) {
			return;
		}
		var up = {
			name : name,
			forge : forge,
			binding : stat.binding,
			level : parseInt(m[2]),
			requires : max===100?eq.upgrade.requires_100:eq.upgrade.requires_50,
			materials : {},
			forge_exp : 0,
			gear_exp : 0
		};
		if(eq.upgrade.level < up.level) {
			eq.upgrade.level = up.level;
		}
		eq.upgrade.list.push(up);
	});

	_up.set(eq);
	eq.upgrade.list.forEach(function(up){
		_up.calc(eq,up);
	});
	var {materials,credits} = _up.sum(eq),
		return_materials = {},
		return_credits = 0,
		prices = $prices.get("Materials");
	Object.keys(materials).forEach(function(n){
		if(n.includes("Catalyst")) {
			return;
		}
		return_materials[n] = Math.floor(materials[n]*0.9);
		return_credits += return_materials[n] * (prices[n]||0);
	});

	_up.node.salvage_summary.innerHTML =
		"<li>"+eq.info.name+"</li>"+
		"<li>PXP Quality: "+(pxp?quality+" ("+pxp+")": pxp===0? quality+" (Selected)": quality+" ?? (Unable to calculate the pxp of this equipment)")+"</li>"+
		"<li>Upgrade Cost: "+credits.toLocaleString()+"</li>"+
		"<li>Returns Value: "+return_credits.toLocaleString()+"</li>";

	_up.node.salvage_returns.innerHTML = "";
	$element("tr",_up.node.salvage_returns,["/<td>Material</td><td>Upgrade</td><td>Returns</td><td>Unit Price</td>"]);
	Object.keys(materials).forEach(function(n){
		if(materials[n]) {
			$element("tr",_up.node.salvage_returns,["/<td>"+n+"</td><td>"+(materials[n]||"")+"</td><td>"+(return_materials[n]||"")+"</td><td>"+(prices[n]||"")+"</td>"]);
		}
	});
};

_up.init_salvage = function() {
	if(_up.init_salvage.inited) {
		return;
	}
	_up.init_salvage.inited = true;

	GM_addStyle(`
		.hvut-up-salvage {position:absolute; top:27px; left:0; width:100%; height:675px; display:flex; justify-content:center; align-items:center; background-color:#EDEBDF; font-size:10pt; z-index:3}
		.hvut-up-salvage > div {height:500px; margin:0 30px; white-space:nowrap}
		.hvut-up-salvage > div:first-child {width:400px}
		.hvut-up-salvage > div:last-child {width:450px; overflow:auto}
		.hvut-up-salvage textarea {display:block; width:380px; height:450px; margin:10px auto}
		.hvut-up-salvage ul {margin:0 0 10px; padding:0; list-style-position:inside; text-align:left; line-height:20px}
		.hvut-up-salvage table {width:425px; table-layout:fixed; border-collapse:collapse}
		.hvut-up-salvage tr:first-child {font-weight:bold}
		.hvut-up-salvage td {border:1px solid; padding:3px 0; overflow:hidden; text-overflow:ellipsis}
		.hvut-up-salvage td:nth-child(1) {width:200px}
		.hvut-up-salvage td:nth-child(2) {width:70px}
		.hvut-up-salvage td:nth-child(3) {width:70px}
		.hvut-up-salvage td:nth-child(4) {width:80px}
	`);

	var left = $element("div",_up.node.salvage),
		right = $element("div",_up.node.salvage);
	if(_up.equip) {
		$element("input",left,{type:"button",value:_up.equip.info.name,style:"min-width:350px;margin-bottom:20px"},function(){_up.node.salvage_equip.value=$id("leftpane").textContent;_up.calc_salvage(_up.equip.upgrade.quality);});
		$element("br",left);
	}
	_up.node.salvage_quality = $element("select",left);
	_up.node.salvage_quality.append(
		$element("option",null,{text:"PXP Quality",value:""}),
		$element("option",null,{text:"Leg (348~)",value:"Leg"}),
		$element("option",null,{text:"Mag (335~348)",value:"Mag"}),
		$element("option",null,{text:"Exq (313~335)",value:"Exq"}),
		$element("option",null,{text:"Sup (~313)",value:"Sup"})
	);
	$element("input",left,{type:"button",value:"Calculate"},function(){_up.calc_salvage(_up.node.salvage_quality.value);});
	$element("input",left,{type:"button",value:"Edit Price"},function(){$prices.edit("Materials",_up.calc_salvage);});
	$element("input",left,{type:"button",value:"Close"},_up.toggle_salvage);
	_up.node.salvage_equip = $element("textarea",left,{placeholder:"Copy the full text of the equipment pop-up and paste it here."},{keypress:e=>{e.stopPropagation();}});
	_up.node.salvage_summary = $element("ul",right);
	_up.node.salvage_returns = $element("table",right);
};

_up.toggle_salvage = function() {
	_up.init_salvage();
	_up.node.salvage.classList.toggle("hvut-none");
};

_up.node = {};
_up.node.salvage = $element("div",$id("mainpane"),[".hvut-up-salvage hvut-none"]);


if($id("equip_extended")) {

_up.validate_upgrade = function(up) {
	var eq = _up.equip;
	if(up.node.to.validity.valid) {
		if(up.node.to.value) {
			up.to = parseInt(up.node.to.value);
		} else {
			up.to = up.level;
		}
		up.valid = true;
	} else {
		up.to = up.level;
		up.valid = false;
	}
	_up.calc(eq,up,true);
	up.node.table.innerHTML = "";
	Object.entries(up.materials).forEach(function([n,c]){
		var s = _up.inventory[n] || 0;
		$element("tr",up.node.table,[`/<td>${n}</td><td>${c}</td><td>(${s})</td>`,s<c?".hvut-up-nostock":""]);
	});
};

_up.calc_upgrade = function(up) {
	_up.validate_upgrade(up);

	var eq = _up.equip,
		stat = eq.stats[up.name];
	if(up.to === up.level) {
		stat.span.textContent = stat.value;
	} else {
		var value = Math.round( $equip.forge(up.name,stat.unforged,up.to,eq.info.pxp,eq.info.level||_player.level,eq.upgrades) *100)/100,
			title,
			increase;
		if($equip.stats[up.name].multi) {
			increase = (1 - (1 - value/100) / (1 - stat.value/100)) * 100;
			title = "Multiplicative Increase";
			stat.span.innerHTML = `${value}<br><span class="hvut-up-span" title="${title}">~${Math.round(increase*100)/100} %</span>`;
		} else {
			increase = value - stat.value;
			title = "Additive Increase";
			stat.span.innerHTML = `${value}<br><span class="hvut-up-span" title="${title}">+${Math.round(increase*100)/100}</span>`;
		}
	}
	_up.sum_upgrade();
};

_up.sum_upgrade = function() {
	var {materials,credits,forge_exp,gear_exp} = _up.sum(_up.equip,true);
	_up.equip.upgrade.materials = materials;

	var eq = _up.equip;
	if(eq.info.soulbound) {
		gear_exp *= 2;
	}
	var tier = eq.info.tier,
		pxp2 = eq.info.pxp2,
		pxp1 = eq.info.pxp1 + gear_exp;
	while(tier<10 && pxp1>=pxp2) {
		tier++;
		pxp1 -= pxp2;
		pxp2 = Math.ceil(eq.info.pxp * Math.pow(1+eq.info.pxp/1000,tier));
	}

	_up.node.summary.innerHTML =
		"<li><span>Forge EXP</span><span>"+forge_exp.toLocaleString()+"</span></li>"+
		"<li><span>Gear EXP</span><span>"+gear_exp.toLocaleString()+"</span></li>"+
		"<li><span>Potency Tier</span><span>"+tier+(tier<10?" ("+pxp1+ " / "+pxp2+")":" (MAX)")+"</span></li>"+
		"<li><span>Total Cost</span><span>"+credits.toLocaleString()+"</span></li>";

	_up.node.costs.innerHTML = "";
	Object.entries(materials).forEach(function([n,c]){
		var s = _up.inventory[n] || 0;
		$element("tr",_up.node.costs,[`/<td>${n}</td><td>${c}</td><td>(${s})</td>`,s<c?".hvut-up-nostock":""]);
	});

	var valid = false;
	_up.node.run.disabled = true;
	if(_up.equip.upgrade.list.some(up=>!up.valid)) {
		_up.node.run.value = "Invalid input";
	} else if(_up.equip.upgrade.list.some(up=>up.to>up.level&&up.to>up.cap)) {
		_up.node.run.value = "Low forge level";
	} else if($qs(".hvut-up-nostock",_up.node.costs)) {
		_up.node.run.value = "Not enough materials";
	} else {
		valid = true;
		_up.node.run.disabled = false;
		_up.node.run.value = "Upgrade ALL";
	}
	return valid;
};

_up.run_upgrade = function() {
	if(!_up.sum_upgrade() || !confirm("Are you sure you wish to upgrade this equipment?")) {
		return;
	}
	_up.node.run.disabled = true;

	var total = 0,
		done = 0;
	_up.equip.upgrade.list.forEach(function(up){
		if(!up.to) {
			return;
		}
		total += (up.to - up.level);
		for(let level=up.level;level<up.to;level++) {
			$ajax.add("POST",location.href,"select_item="+_up.equip.info.eid+"&upgrade_stat="+up.id,function(){done++;_up.node.run.value=done+" / "+total;if(done===total){alert("Completed!\n\nReload the page.");}});
		}
	});
	_up.node.run.value = "0 / " + total;
};

_up.buy_catalysts = function() {
	var catalysts = {
		"Wispy Catalyst": {cc:90},
		"Diluted Catalyst": {cc:450},
		"Regular Catalyst": {cc:900},
		"Robust Catalyst": {cc:2250},
		"Vibrant Catalyst": {cc:4500},
		"Coruscating Catalyst": {cc:9000}
	};
	_up.node.catalysts.disabled = true;

	$ajax.add("GET","?s=Bazaar&ss=is",null,function(r){
		var doc = $doc(r.responseText),
			reg = /itemshop\.set_item\('shop_pane',(\d+),(\d+),(\d+)/,
			storetoken = $qs("input[name='storetoken']",doc).value,
			current_credits = parseInt($id("networth",doc).textContent.replace(/\D/g,"")),
			total = 0,
			buy_count = 0,
			cc = true;

		Array.from($qs("#shop_pane .itemlist",doc).rows).forEach(function(tr){
			var exec = reg.exec(tr.cells[0].firstElementChild.getAttribute("onclick")),
				item = tr.cells[0].textContent.trim(),
				id = exec[1],
				price = parseInt(exec[3]);

			if(catalysts[item] && _up.equip.upgrade.materials[item]) {
				var count = _up.equip.upgrade.materials[item] - (_up.inventory[item]||0);
				if(count > 0) {
					catalysts[item].id = id;
					catalysts[item].count = count;
					total += count * price;
					buy_count++;
				}
				if(price !== catalysts[item].cc) {
					cc = false;
				}
			}
		});
		if(!buy_count) {
			alert("You already have enough catalysts.");
			_up.node.catalysts.disabled = false;
			return;
		}
		if(total > current_credits) {
			alert("You do not have enough credits.");
			_up.node.catalysts.disabled = false;
			return;
		}
		if(cc || _isekai) {
			if(!confirm("Are you sure you wish to purchase catalysts?")) {
				_up.node.catalysts.disabled = false;
				return;
			}
		} else {
			if(!confirm("You don't have [Coupon Clipper] Hath perk.\nAre you sure you wish to purchase catalysts?")) {
				_up.node.catalysts.disabled = false;
				return;
			}
		}

		Object.values(catalysts).forEach(function(c){
			if(c.count) {
				$ajax.add("POST","?s=Bazaar&ss=is","storetoken="+storetoken+"&select_mode=shop_pane&select_item="+c.id+"&select_count="+c.count,function(){
					if(!--buy_count) {
						_up.load_inventory();
					}
				});
			}
		});
	});
};

_up.load_inventory = function() {
	_up.inventory = {};
	$ajax.add("GET","?s=Character&ss=it",null,function(r){
		var doc = $doc(r.responseText);
		Array.from($qs(".itemlist",doc).rows).forEach(function(tr){
			_up.inventory[ tr.cells[0].textContent.trim() ] = parseInt(tr.cells[1].textContent);
		});
		_up.node.catalysts.disabled = false;
		_up.equip.upgrade.list.forEach(up=>_up.validate_upgrade(up));
		_up.sum_upgrade();
	});
};

GM_addStyle(`
	#forge_outer {width:1120px}
	#forge_outer + div {width:380px !important; margin:0 0 0 54px !important}
	#forge_outer #leftpane {width:380px !important; margin-left:0 !important}
	#forge_outer #leftpane > div:not(#equip_extended), #forge_outer #leftpane > div:not(#equip_extended) div {width:auto !important}
	#forge_outer #rightpane {width:380px !important}
	#forge_outer #rightpane > div {width:auto !important}
	#forge_outer .ex > div {height:auto; min-height:18px}
	#forge_outer .ep > div {height:auto; min-height:14px}

	.hvut-up-span {color:#03c; white-space:nowrap}
	.hvut-up-input {width:50px; text-align:right}
	.hvut-up-input:invalid {color:#e00}
	.hvut-up-sub {position:absolute; top:40px; left:400px; background-color:#fff}
	.hvut-up-sub:empty {display:none}
	.hvut-up-list {border-spacing:0}
	.hvut-up-list tr:hover {background-color:#fff}
	.hvut-up-list td:first-child {width:150px !important}

	.hvut-up-div {float:left; width:340px; height:380px; margin:220px 0 0 20px}
	.hvut-up-buttons {display:flex; justify-content:space-between}
	.hvut-up-buttons input {margin:0; padding:1px 0}
	.hvut-up-summary {margin:10px 0 0; padding:10px; list-style:none; font-size:10pt; line-height:20px; text-align:right}
	.hvut-up-summary span:first-child {float:left; width:210px}
	.hvut-up-costs {padding:10px; width:320px; table-layout:fixed; border-spacing:0; font-size:10pt; line-height:20px; text-align:right; white-space:nowrap}
	.hvut-up-costs td {padding:0; overflow:hidden; text-overflow:ellipsis}
	.hvut-up-costs td:nth-child(1) {width:210px}
	.hvut-up-costs td:nth-child(2) {width:50px}
	.hvut-up-costs td:nth-child(3) {width:60px}
	.hvut-up-nostock {color:#c00}
`);

_up.node.list = $id("rightpane").children[1].firstElementChild;
_up.node.list.classList.add("hvut-up-list");
_up.node.div = $element("div",$id("forge_outer"),[".hvut-up-div"]);

$element("div",_up.node.div,[".hvut-up-buttons"]).append(
	_up.node.run = $element("input",_up.node.div,{type:"button",value:"Upgrade ALL",tabIndex:-1,style:"width:155px"},_up.run_upgrade),
	_up.node.catalysts = $element("input",_up.node.div,{type:"button",value:"Buy Catalysts",tabIndex:-1,style:"width:100px"},_up.buy_catalysts),
	$element("input",_up.node.div,{type:"button",value:"Edit Price",tabIndex:-1,style:"width:75px"},function(){$prices.edit("Materials",_up.sum_upgrade);})
);
_up.node.summary = $element("ul",_up.node.div,[".hvut-up-summary"]);
_up.node.costs = $element("table",_up.node.div,[".hvut-up-costs"]);

_up.equip = $equip.parse.extended($id("equip_extended"));
_up.equip.info.eid = $qs("input[name='select_item']").value;
if(!$equip.names[_up.equip.info.eid]) {
	$equip.names[_up.equip.info.eid] = _up.equip.info.name;
  setValue('equipnames', $equip.names);
}

_up.equip.upgrade = {
	list : [],
	quality : null,
	level : 0,
	requires_50 : [],
	requires_100 : []
};

Array.from(_up.node.list.rows).forEach(function(tr){
	var eq = _up.equip,
		forge = tr.cells[0].textContent,
		[name,stat] = Object.entries($equip.stats).find(([,s])=>s.forge===forge) || [],
		max = forge==="Physical Damage" || forge==="Magical Damage" ? 100 : 50,
		up = {
			id : (/'costpane_(\w+)'/).test(tr.getAttribute("onmouseout")) && RegExp.$1,
			name : name,
			forge : forge,
			binding : stat.binding,
			level : parseInt(tr.cells[1].textContent),
			cap : parseInt(tr.cells[3].textContent),
			max : max,
			requires : max===100?eq.upgrade.requires_100:eq.upgrade.requires_50,
			materials : {},
			forge_exp : 0,
			gear_exp : 0,
			node : {}
		};
	up.node.to = $element("input",$element("td",tr),{type:"number",className:"hvut-up-input",value:up.level||"",min:up.level,max:up.max},{input:function(){_up.calc_upgrade(up);}});
	up.node.table = $element("table",$id("costpane_"+up.id),[".hvut-up-sub hvut-up-costs"]);

	if(eq.upgrade.level < up.level) {
		eq.upgrade.level = up.level;
	}
	if(!eq.upgrade.quality && /(\w+ Catalyst)/.test($id("costpane_"+up.id).textContent)) {
		var catalysts = _up.catalysts.slice( up.level<up.max/4?0 : up.level<up.max/2?1 : 2 );
		eq.upgrade.quality = ["Sup","Exq","Mag","Leg"][catalysts.indexOf(RegExp.$1)];
	}
	eq.upgrade.list.push(up);
});

_up.set(_up.equip);
_up.load_inventory();

$element("input",$id("leftpane"),{type:"button",value:"Salvage Calculator",style:"margin-top:40px"},_up.toggle_salvage);

} else {

$element("input",$id("rightpane"),{type:"button",value:"Salvage Calculator",style:"margin-top:40px"},_up.toggle_salvage);

}

} else
// [END 21] Forge - Upgrade */


//* [23] Forge - Salvage
if(settings.salvage && _query.s==="Forge" && _query.ss==="sa") {

$element("div",$id("rightpane"),[" This will permanently destroy the item","!margin-top:30px;font-size:12pt;font-weight:bold;color:#c00"]);

if($id("salvage_button")) {
	confirm_event($id("salvage_button").parentNode,"click","Are you sure you want to salvage this item?",()=>$id("salvage_button").src.match(/salvage\.png$/));
}

} else
// [END 23] Forge - Salvage */

{} // END OF [else if]; DO NOT REMOVE THIS LINE!


// Sort Equipment List
if(_query.s==="Forge" && $id("item_pane")) {
	$equip.list($id("item_pane"), (_query.ss==="re"||_query.ss==="up"||_query.ss==="en")&&(_query.filter==="equipped"||!_query.filter)?0:1 );
}

/***** [END] *****/


/***** [BATTLE] *****/
} else if($id("battle_top")) {

	if(settings.randomEncounter) {
		if($id("textlog").tBodies[0].lastElementChild.textContent === "Initializing random encounter ...") {
			$re.check();
		}
		let button = $element("div",$id("csp"),[" RE","!"+(settings.reBattleCSS||"position:absolute;top:0px;left:0px;cursor:pointer")]);
		$re.clock(button);
		if(settings.ajaxRound) {
			(new MutationObserver(()=>{if(!button.parentNode.parentNode&&$id("csp")){$id("csp").appendChild(button);}$re.start();})).observe(document.body,{childList:true});
		}
	}

	setValue("url",location.href);
	if(settings.reloadBattleURL === 1) {
		window.history.pushState(null,null,"?s=Battle");
	} else if(settings.reloadBattleURL === 2) {
		location.href = "?s=Battle";
	}

} else if($id("riddleform")) {

	setValue("url",location.href);
	if(settings.reloadBattleURL === 1) {
		window.history.pushState(null,null,"?s=Battle");
	} else if(settings.reloadBattleURL === 2) {
		location.href = "?s=Battle";
	}


/***** [GALLERY] *****/
} else if(location.hostname === "e-hentai.org") {

	if(settings.randomEncounter) {
		$re.init();

		let link,onclick;
		if((link=$qs("#eventpane a")) && (onclick=link.getAttribute("onclick")) && (/\?s=Battle&ss=ba&encounter=([A-Za-z0-9=]+)/).test(onclick)) {
			$re.json.date = Date.now();
			$re.json.key = RegExp.$1;
			$re.json.count++;
			$re.json.clear = false;
			$re.set();
			if(settings.reGalleryAlt) {
				onclick = onclick.replace("https://hentaiverse.org","http://alt.hentaiverse.org");
			}
			onclick = onclick.replace("','_hentaiverse'","&date="+$re.json.date+"','_hentaiverse'");
			link.setAttribute("onclick",onclick);
		}
		if(settings.reGallery && $id("nb")) {
			$re.clock( $element("a",$element("div",$id("nb")),["!display:inline-block;width:70px;text-align:left;cursor:pointer"]) );
		}
	}

}


// END
