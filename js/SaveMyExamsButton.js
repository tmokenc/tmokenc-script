// ==UserScript==
// @name         SaveMyExams download button
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       tmokenc
// @match        https://www.savemyexams.co.uk/*
// @icon         https://www.google.com/s2/favicons?domain=savemyexams.co.uk
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

  const elements = document.querySelectorAll(".tablepress a");

  if (elements.length === 0) {
    return
  }

  for (const element of Array.from(elements)) {
    makeDownloadButton(element)
  }
})();

function makeDownloadButton(element) {
  const btn = document.createElement("button");
  btn.innerText = "⇩";
  btn.onclick = async () => {
    const url = await getFileUrl(element.href);
    const a = document.createElement("a");
    a.setAttribute("href", url);
    a.setAttribute("target", "_blank");
    a.setAttribute("download", true);
    a.click();
  };

  element.parentNode.insertBefore(btn, element);
}

async function getFileUrl(url) {
  if (url.endsWith('.pdf')) {
    return url
  }

  let cache = localStorage.getItem("tmokenc_cache") || "{}";
  cache = JSON.parse(cache);

  if (cache[url]) {
   return cache[url]
  }

  const page = await _fetch(url);
  const reg = /https:\/\/.*\.pdf/i;
  const fileUrl = page.match(reg)[0];

  if (fileUrl) {
    cache[url] = fileUrl;
    localStorage.setItem("tmokenc_cache", JSON.stringify(cache));
    return fileUrl
  }

  throw new Error("Cannot find any")
}

async function _fetch(url) {
  return new Promise((res, rej) => {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status !== 200) {
          rej(this)
        } else {
          res(this.responseText)
        }
      }
    };

    xhttp.open("GET", url, true);
    xhttp.send();
  })
}
