// ==UserScript==
// @name         Radom Kaede
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  add randomness features to the sadpanda
// @author       tmokenc
// @match        https://e-hentai.org/*
// @match        https://exhentai.org/*
// @exclude      https://e-hentai.org/g/*
// @exclude      https://exhentai.org/g/*
// @exclude      https://e-hentai.org/torrents*
// @exclude      https://exhentai.org/torrents*
// @exclude      https://e-hentai.org/popular
// @exclude      https://exhentai.org/popular
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    if (!document.querySelector(".gl1t")) {
        return
    }

    if (getMaxPage() > 1) {
        const page = document.createElement('td')
        page.innerHTML = "<p>Random Page</p>"
        page.addEventListener("click", randomPage)
        document.querySelector(".ptt tr").appendChild(page)
    }

    const gallery = document.createElement('td')
    gallery.innerHTML = "<p>Random Gallery</p>"
    gallery.addEventListener("click", randomGallery)

    document.querySelector(".ptt tr").prepend(gallery)
})();

function randomPage() {
    window.location = getRandomPage()
}

async function randomGallery() {
    let doc = document;

    if (getMaxPage() > 1) {
        const text = await _fetch(getRandomPage())
        const dom = new DOMParser()
        doc = dom.parseFromString(text, 'text/html')
    }

    const urls = Array.from(doc.querySelectorAll(".gl1t")).map(e => e.querySelector("a").href);
    const url = urls[Math.random() * urls.length << 0]

    window.location = url
}

function getRandomPage() {
    const page = Math.floor(Math.random() * getMaxPage())
    const params = window.location.search.replace(/&?page=(\d+)/, '')
    return params
        ? window.location.href.split('?')[0] + params + `&page=${page}`
        : window.location + `?page=${page}`
}

function getMaxPage() {
    return Array.from(document.querySelectorAll(".ptt td"))
        .map(e => Number(e.textContent))
        .filter(e => e)
        .pop()
}

function _fetch(url, {method = "GET", body = "", headers = []} = {}) {
    return new Promise((res, rej) => {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4) {
                if (this.status !== 200) {
                    rej(this)
                } else {
                    res(this.responseText)
                }
            }
        };
        xhttp.open(method, url, true);

        for (const header of headers) {
            xhttp.setRequestHeader(...header)
        }

        xhttp.send(body);
    })
}